<?php 
@session_start();
include("includes/db.php");
if (!isset($_SESSION['seller_user_name'])) {
echo "<script>window.open('login.php','_self');</script>";
}
 ?>
<h1 class="mb-4">Account Settings</h1>
<h5 class="mb-4">Paypal For Withdrawing Revenues</h5>
<form method="post" class="clearfix mb-3">
<div class="form-group row">
<label class="col-md-4">
Enter Paypal	
</label><!--col-md-4 ends-->	
<div class="col-md-8">
<input type="text" name="seller_paypal_email" value="<?php echo($login_seller_paypal_email); ?>" placeholder="Enter Paypal Email Address Where Your Revenues Sent To" class="form-control" required>	
</div>
</div><!--form-group row ends-->
<button type="submit" name="submit_paypal_email" class="btn btn-success float-right"><i class="fa fa-user-md"></i> Change Paypal Email</button>	
</form><!--clearfix mb-3 ends-->
<?php 
if (isset($_POST['submit_paypal_email'])) {
$seller_paypal_email=$_POST['seller_paypal_email'];	
$update_seller="UPDATE sellers set seller_paypal_email='$seller_paypal_email' where seller_id='$login_seller_id'";
$run_seller=mysqli_query($con,$update_seller);
if ($run_seller) {
	echo "<script>alert('Your Payapal Email Address Has hhen Updated');
     window.open('settings.php?account_setting','_self');
	</script>";
}
}
 ?>
<hr>
<h5 class="md-4"> Change Password</h5>
<form class="clearfix mb-3" method="post">
<div class="form-group row">
<label class="col-md-4">
Enter Current Password	
</label><!--col-md-4 ends-->	
<div class="col-md-8">
<input type="text" name="old_pass" class="form-control" required>	
</div>
</div><!--form-group row ends-->
<div class="form-group row">
<label class="col-md-4">
Enter New Password	
</label><!--col-md-4 ends-->	
<div class="col-md-8">
<div class="input-group">
<input type="password" name="new_pass" class="form-control"  id="upass" required>
<span class="input-group-append">
<button type="button" id="toggleBtn" class="input-group-text" onclick="togglePasswors()">
	<i class="fa fa-eye"></i>
</button>
</span>	
</div>
</div>
</div><!--form-group row ends-->
<script>
function togglePasswors(){
	var upass =document.getElementById('upass');
	var toggleBtn= document.getElementById('toggleBtn');
	if (upass.type=="password") {
		upass.type ="text";
		toggleBtn.value="";
	}else{
		upass.type="password";
		toggleBtn.value="";
	}
}	
</script>
<div class="form-group row">
<label class="col-md-4">
Enter New Password Again	
</label><!--col-md-4 ends-->	
<div class="col-md-8">
<input type="password" name="new_pass_again" class="form-control" required>	
</div>
</div><!--form-group row ends-->
<button class="btn btn-success float-right" type="submit" name="change_password"><i class="fa fa-user-md"></i> Change Password</button>	
</form><!--form clearfix mb-3 ends-->
<?php 
if (isset($_POST['change_password'])) {
	$old_pass= $_POST['old_pass'];
	$new_pass= $_POST['new_pass'];
	$new_pass_again= $_POST['new_pass_again'];

$selet_seller="SELECT * from sellers where seller_id='$login_seller_id'";
$run_seller=mysqli_query($con,$selet_seller);

$row_seller=mysqli_fetch_array($run_seller);
$hash_password=$row_seller['seller_pass'];

$decrypt_password=password_verify($old_pass,$hash_password);
if ($decrypt_password ==0) {
echo "<script>alert('Your Current Password is Not VALID!, ReTry!');</script>";
}else{
if($new_pass != $new_pass_again) {
echo "<script>alert('Your New Password Does Not Match!, ReTry!');</script>";	
}else{
$encrypted_password = password_hash($new_pass, PASSWORD_DEFAULT);
$update_pass="UPDATE sellers set seller_pass ='$encrypted_password' where seller_id='$login_seller_id'";

$run_update_pass=mysqli_query($con,$update_pass);
echo "<script>alert('You Password Has been Successfully Change!, Login Again!');
window.open('logout.php','_self');
</script>";
}
}

}
 ?>
<hr>
<h5 class="mb-1">ACCOUNT DEACTIVATION</h5>
<ul class="list-unstyled mb-3 float-right">
<li class="lead">
	<strong>What happens when you deactivate your account?</strong>
</li>
<li>Your profile and product won't be show on Juakali Mall anymore!</li>	
<li>Any Open Order will be cancelled and refunded!</li>
<li>You Won't be able to re-activate your Products!</li>
<li>You Won't be able to reactivate your account</li>
</ul><!--list-unstyle mb-3 float-right ends-->
<div class="clearfix"></div>

<form method="post">
<?php 
if (!$current_balance == 0) { ?>	
<div class="form-group">
<h5>Please WithDraw Your Revenue Before Deactivating!</h5>	
</div><!--form-group ends-->
<button id="custom-disabled" class="btn btn-danger float-right" type="submit" name="deactivate_account" disabled onclick="return confirm('Are you sure you want to leave?')"><i class="fa fa-user-md"></i>Deactivate Account</button>	
<?php } elseif ($current_balance ==0) {?>
<div class="form-group">
<label >Why Are You Leaving?
</label>
<select name="deactivate_reason" class="form-control">
<option class="hidden">Choose A Reason</option>
<option>The quality of services was less than expected</option>
<option>I have no time to use it</option>
<option>I cant find what i was looking for</option>
<option>I had a negative experince with the buyer/seller</option>
<option>I found the site difficult to use</option>
<option>The level of the cusomer services was less than expected</option>
<option>I have another Juakali Mall account</option>
<option>I'M not receiving enough orders</option>
<option>Others</option>
</select>	
</div><!--form-group ends-->
<button class="btn btn-danger float-right" type="submit" name="deactivate_account"><i class="fa fa-user-md"></i>Deactivate Account</button>	
<?php } ?>

</form>
<?php 
if (isset($_POST['deactivate_account'])) {
$update_seller="UPDATE sellers set seller_status='deactivated' where seller_id='$login_seller_id'";
$run_update_seller=mysqli_query($run,$update_seller);

if ($run_update_seller) {
	$sel_orders="SELECT * from orders where seller_id='$login_seller_id' AND order_active='yes'";
$run_orders=mysqli_query($con,$sel_orders);
while ($row_orders=mysqli_fetch_array($run_orders)) {

$order_id=$row_orders['order_id'];		
$seller_id=$row_orders['seller_id'];		
$buyer_id=$row_orders['buyer_id'];		
$order_price=$row_orders['order_price'];		
$notification_date=date("h:i: M d, Y");
$purchase_date=date("F d, Y");

$insert_notification="INSERT INTO notifications (receiver_id, sender_id,order_id,reason,date,status) values ('$buyer_id','$seller_id','$order_id','order_cancelled','$notification_date','unread')";
$run_notification=mysqli_query($con,$insert_notification);

$insert_purchase="INSERT into purchases (seller_id,order_id,amount,date,method) values('$buyer_id','$order_id','$order_price','$purchase_date','order_cancelled')"; 
$run_purchase=mysqli_query($con,$insert_purchase);

$update_balance="UPDATE seller_accounts set used_purchases=used_purchases-$order_price, current_balance= current_balance + $order_price where seller_id='$buyer_id'";
$run_update_balance=mysqli_fetch_array($con,$update_balance);

$update_order_status="UPDATE orders set order_status='cancelled', order_active='no' where order_id='$order_id'";

$run_update_order_status=mysqli_query($con,$update_order_status);


}

$pause_proposals="UPDATE proposals set proposal_status='paused' where proposal_seller_id='$login_seller_id'";
$run_pause_proposals=mysqli_query($con,$pause_proposals);
unset($_SESSION['seller_user_name']);
echo "<script>alert('Your Account Has Been Deactivate!, Goodby!');
window.open('index.php','_self');
</script>";
}

}
 ?>







