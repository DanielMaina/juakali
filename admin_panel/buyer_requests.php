<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{
 ?>
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard / View Pending Buyer Requests	
</li>	
</ol>	
</div>	
</div><!--first row ends-->

 <div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-money-bill-alt"></i> View Pending Buyer Request.	
</h4>	
</div><!--card-header ends-->	
<div class="card-body">
<div class="table-responsive">
<table class="table table-bordered table-hover table-striped">
<thead>
<tr>
<th>Buyer </th>
<th>Title </th>
<th>Description </th>
<th>File</th>
<th>Duration </th>
<th>Budget </th>
<th>Action </th>
</tr>	
</thead>
<tbody>
<?php 
$per_page=5;
$get_requests="SELECT * from buyer_requests where request_status='pending' order by 1 DESC LIMIT 0,$per_page";
$run_requests=mysqli_query($con,$get_requests);
while($row_requests=mysqli_fetch_array($run_requests)) {
$request_id=$row_requests['request_id'];	
$request_title=substr($row_requests['request_title'],0, 20)."...";	
$request_description=substr($row_requests['request_description'],0,50)."...";	
$seller_id=$row_requests['seller_id'];	
$request_budget=$row_requests['request_budget'];	
$delivery_time=$row_requests['delivery_time'];	
$request_file=$row_requests['request_file'];	
$request_status=$row_requests['request_status'];	

$get_sellers="SELECT * from sellers where seller_id='$seller_id'";
$run_sellers=mysqli_query($con,$get_sellers);
$row_sellers=mysqli_fetch_array($run_sellers);
$seller_user_name=$row_sellers['seller_user_name'];
?>
<tr>
<td><?php echo $seller_user_name; ?></td>	
<td width="15"><?php echo $request_title; ?></td>	
<td width="150"><?php echo $request_description; ?></td>	
<td>
<?php
if (!empty($request_file)) { ?>
<a href="../requests/request_files/<?php echo($request_file); ?>" download><i class="fa fa-download"></i><?php echo($request_file); ?></a>	
 <?php }else{ ?>
No File Attched
 <?php } ?>	
 </td>	
<td><?php echo $delivery_time; ?></td>	
<td>
<?php if (!empty($request_budget )) { ?>
Ksh <?php echo($request_budget); ?>
<?php }else{ ?>	
No Budget Set!
<?php } ?>	
 	
 </td>
 <td>
 <a class="btn btn-success" href="index.php?approve_request=<?php echo($request_id); ?>" onclick="return confirm('Do You Realy want to Approve this request?')"><i class="fa fa-thumbs-up"></i> Apv</a>
 <a class="btn btn-info" href="index.php?unapprove_request=<?php echo($request_id); ?>" onclick="return confirm('Do You Realy want to Un-Approve this request?')"><i class="fa fa-thumbs-down"></i> Un-Apv</a>
 </td>	
</tr>

<?php } ?>	
</tbody>	
</table>	
</div><!--table-responsive ends-->
<div class="d-flex justify-content-center">
<ul class="pagination">
<?php 
#/Get All data from the table 
$query="SELECT * from buyer_requests where request_status='pending' order by 1 DESC";
$run_query=mysqli_query($con,$query);
#count total
$total_records=mysqli_num_rows($run_query);
#use the ceil get whole numbers]
$total_pages=ceil($total_records / $per_page);
echo "
<li class='page-item'>
<a href='index.php?buyer_requests_pagination=1' class='page-link'> First Page</a>
</li>
";
for ($i=1; $i <=$total_pages; $i++) { 
echo "
<li class='page-item'>
<a href='index.php?buyer_requests_pagination=".$i."' class='page-link'>".$i."</a>
</li>
";		
	}

echo "
<li class='page-item'>
<a href='index.php?buyer_requests_pagination=$total_pages' class='page-link'> Last Page</a>
</li>
";		
 ?>
</ul>	
</div><!--d-flex ends-->	
</div><!--card-body-->
</div><!--card-ends-->	
</div>	
</div><!--2 row ends-->
<?php } ?>
