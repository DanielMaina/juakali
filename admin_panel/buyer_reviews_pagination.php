<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{
$get_buyer_reviews="SELECT * from buyer_reviews";
$run_buyer_reviews=mysqli_query($con,$get_buyer_reviews);
$count_buyer_reviews=mysqli_num_rows($run_buyer_reviews);


$get_seller_reviews="SELECT * from seller_reviews";
$run_seller_reviews=mysqli_query($con,$get_seller_reviews);
$count_seller_reviews=mysqli_num_rows($run_seller_reviews);

 ?>
 <div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard /View reviews	
</li>	
</ol>	
</div>	
</div><!--first row ends-->

<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4"><i class="fa fa-money-bill-alt"></i></h4>View Buyer Reviews	
</div><!--card-header ends-->
<div class="card-body">
<a href="index.php?view_buyer_reviews" class="text-muted mr-2">
Buyer Reviews (<?php echo($count_buyer_reviews); ?>)	
</a> 
<span class="mr-2">|| </span>
<a href="index.php?view_seller_reviews" class="mr-2">
Seller Reviews (<?php echo($count_seller_reviews); ?>)	
</a>	
<div class="table-responsive mt-4">
<table class="table table-bordered table-hover table-striped">
<thead>
<tr>
<th>Order No</th>	
<th>Buyer</th>	
<th>Product</th>	
<th>Rating</th>	
<th>Date</th>	
<th>Delete</th>	
</tr>	
</thead>
<tbody>
<?php 
$per_page=5;
if (isset($_GET['buyer_reviews_pagination'])) {
$page=$_GET['buyer_reviews_pagination'];
}else{
$page=1;
}
//page srt from zero and multply per page
$strat_from=($page-1)*$per_page;
$get_buyer_reviews="SELECT * from buyer_reviews order by 1 DESC LIMIT $strat_from, $per_page";
$run_buyer_reviews=mysqli_query($con,$get_buyer_reviews);
while ($row_buyer_reviews=mysqli_fetch_array($run_buyer_reviews)) {
$review_id=$row_buyer_reviews['review_id'];
$order_id=$row_buyer_reviews['order_id'];
$proposal_id=$row_buyer_reviews['proposal_id'];
$review_buyer_id=$row_buyer_reviews['review_buyer_id'];
$buyer_rating=$row_buyer_reviews['buyer_rating'];
$review_date=$row_buyer_reviews['review_date'];

$sel_orders="SELECT * from  orders where order_id='$order_id'";
$run_orders=mysqli_query($con,$sel_orders);
$row_orders=mysqli_fetch_array($run_orders);
$order_number=$row_orders['order_number'];

$get_proposals="SELECT * from proposals where proposal_id='$proposal_id'";
$run_proposals=mysqli_query($con,$get_proposals);
$row_proposals =mysqli_fetch_array($run_proposals);
$proposal_title=$row_proposals['proposal_title'];

$select_buyer="SELECT * from sellers where seller_id='$review_buyer_id'";
$run_buyers=mysqli_query($con,$select_buyer);
$row_buyer=mysqli_fetch_array($run_buyers);
$buyer_use_name=$row_buyer['seller_user_name'];
?>
<tr>
<td>
<?php 
if ($order_id=="0") {
echo "
Review Inserted By Admin <small class='text-primary'>No Order Number</small>
";	
}else{ ?>
<a href="index.php?single_order=<?php echo($order_id); ?>">#<?php echo($order_number); ?></a>
<?php } ?>	

</td>
<td>
<a href="index.php?single_seller=<?php echo($review_buyer_id); ?>"><?php echo($buyer_use_name); ?></a>	
</td>
<td><?php echo($proposal_title); ?></td>	
<td><?php echo($buyer_rating); ?></td>	
<td><?php echo($review_date); ?></td>	
<td>
<a class="btn btn-danger" href="index.php?delete_buyer_review=<?php echo($review_id);?>" onclick="return confirm('Do you really want to delete This Review? It will remove seller rating and product rating will chnage')">
	<i class="fa fa-trash-alt"></i> Delete Reviews
</a>	
</td>	
</tr>
<?php } ?>	
</tbody>	
</table><!--table table-bordered table-hover table-striped ends-->
</div><!--table responsive ends-->
<div class="d-flex justify-content-center">
<ul class="pagination">
<?php 
//Data FROM TABLE buyer_reviews
$query="SELECT * from buyer_reviews order by 1 DESC";
$run_query=mysqli_query($con,$query);
 //count total records
$total_requireds=mysqli_num_rows($run_query);
//use the ceil function to divide page
$total_page=ceil($total_requireds / $per_page);
echo "

<li class='page-item'>
<a class='page-link' href='index.php?buyer_reviews_pagination=1'>First Page</a>
</li>
";
for ($i=1; $i <=$total_page ; $i++) { 
echo "

<li class='page-item ";
if ($i==$page) {
	echo " active ";
}
echo "'>
<a class='page-link' href='index.php?buyer_reviews_pagination=".$i."'>".$i."</a>
</li>
";	
}
echo "

<li class='page-item'>
<a class='page-link' href='index.php?buyer_reviews_pagination=$total_page'>Last Page</a>
</li>
";
 ?>	
</ul><!--pagination ends-->	
</div><!--d-flex justify-content-center ends-->
</div><!--card-body ends-->	
</div><!--card ends-->	
</div><!--col-lg-12--->	
</div><!--2 row ends-->
 <?php } ?>