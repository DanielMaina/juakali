<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{
 ?>

<?php 
if (isset($_GET['cancel_order'])) {
$order_id=$_GET['cancel_order'];

$update_order="UPDATE orders set order_active='no',order_status='cancelled' where order_id='$order_id'";
$run_order=mysqli_query($con,$update_order);
if ($run_order) {

$sel_orders="SELECT * from orders where order_id='$order_id'";
  $run_orders=mysqli_query($con,$sel_orders);
  $row_order=mysqli_fetch_array($run_orders);
  $seller_id=$row_order['seller_id'];
  $buyer_id=$row_order['buyer_id'];
  $order_price=$row_order['order_price'];
  $order_number=$row_order['order_number'];

$message="Order Cancellation By Customer Support";

date_default_timezone_set("africa/nairobi");
$date=date("h:i: M d, Y");
$purchase_date=date("F d, Y");

$insert_purchase="INSERT INTO purchases (seller_id,order_id,amount,date,method) values('$buyer_id','$order_id','$order_price','$purchase_date','order_cancellation')";
$run_purchases=mysqli_query($con,$insert_purchase);

$insert_order_conversation="INSERT INTO order_conversations(order_id,sender_id,message,file,date,status) values('$order_id','$buyer_id','$message','','$date','cancelled_by_customer_support')";
$run_order_conversation=mysqli_query($con,$insert_order_conversation);

$insert_notification = "INSERT INTO notifications (receiver_id,sender_id,order_id,reason,date,status) values ('$seller_id','$buyer_id','$order_id','cancelled_by_customer_support','$date','unread')";
$run_notification=mysqli_query($con,$insert_notification);

$update_balance="UPDATE seller_accounts set used_purchases=used_purchases-$order_price,current_balance=current_balance+$order_price where seller_id='$buyer_id'";
$run_balance=mysqli_query($con,$update_balance);

if ($run_balance) {
echo "<script>
alert('Order: $order_number Has Been Cancelled, Order Amount Has been Returned To Buyer Shopping Balance, Successfully!');
window.open('index.php?view_orders','_self');
</script>";
}
 }	

}
 ?>

 <?php } ?>