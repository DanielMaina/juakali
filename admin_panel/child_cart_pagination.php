<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{
 ?>
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard / View Sub Categories
</li>	
</ol>	
</div>	
</div><!--first row ends-->
<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-money-bill-alt"></i> View Sub Category	
</h4>	
</div><!-- card-header ends-->	
<div class="card-body">
<div class="table-responsive">
<table class="table table-hover table-bordered">
<thead>
<tr>
<th>Sub Category ID</th>	
<th>Sub Category Title</th>	
<th>Sub Category Description</th>	
<th>Parent Category Title</th>	
<th>Delete</th>	
<th>Edit</th>		
</tr>	
</thead>
<tbody>
<?php 
$i=0;
$per_page=10;
if (isset($_GET['child_cart_pagination'])) {
$page=$_GET['child_cart_pagination'];
}else{
$page=1;
}
//page srt from zero and multply per page
$strat_from=($page-1)*$per_page;
$get_child_cats="SELECT * from categories_childs order by 1 DESC LIMIT $strat_from,$per_page";
$run_child_cats=mysqli_query($con,$get_child_cats);
while ($row_child_cats=mysqli_fetch_array($run_child_cats)) {
$child_id=$row_child_cats['child_id'];
$child_parent_id=$row_child_cats['child_parent_id'];
$child_title=$row_child_cats['child_title'];
$child_desc=$row_child_cats['child_desc'];

$get_cats="SELECT * FROM categories where cat_id='$child_parent_id'";
$run_cats=mysqli_query($con,$get_cats);
$row_cats=mysqli_fetch_array($run_cats);
$cat_title=$row_cats['cat_title'];
$i++;
 ?>
<tr>
<td><?php echo $i; ?></td>	
<td><?php echo $child_title; ?></td>	
<td width="500"><?php echo $child_desc; ?></td>	
<td><?php echo $cat_title; ?></td>	
<td>
<a class="btn btn-danger" href="index.php?delete_child_cat=<?php echo($child_id);?>" onclick="return confrim('Do you want to DELETE Sub Category?');">
	<i class="fa fa-trash-alt"></i> Delete
</a>	
</td>	
<td>
<a class="btn btn-info" href="index.php?edit_child_cat=<?php echo($child_id);?>">
	<i class="fa fa-pencil-alt"></i> Edit
</a>	
</td>	
</tr>

<?php } ?> 	
</tbody>	
</table>	
</div><!--table-responsive ends-->	
<div class="d-flex justify-content-center">
<ul class="pagination">
<?php 
#/Get All data from the table 
$query="SELECT * from categories_childs order by 1 DESC";
$run_query=mysqli_query($con,$query);
#count total
$total_records=mysqli_num_rows($run_query);
#use the ceil get whole numbers]
$total_pages=ceil($total_records / $per_page);
echo "
<li class='page-item'>
<a href='index.php?child_cart_pagination=1' class='page-link'> First Page</a>
</li>
";
for ($i=1; $i <=$total_pages; $i++) { 
echo "
<li class='page-item";
if ($i==$page) {
	echo " active ";
}
echo "'>
<a href='index.php?child_cart_pagination=".$i."' class='page-link'>".$i."</a>
</li>
";		
	}

echo "
<li class='page-item'>
<a href='index.php?child_cart_pagination=$total_pages' class='page-link'> Last Page</a>
</li>
";		
 ?>
</ul>	
</div><!--d-flex ends-->
</div><!--card-body ends-->
</div><!--card ends-->	
</div>	
</div><!--2 row ends-->
 <?php } ?>