<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{
$get_contact_us="SELECT * from contact_support ";
$run_contact_support=mysqli_query($con,$get_contact_us);
$row_contact_support=mysqli_fetch_array($run_contact_support);
$contact_email=$row_contact_support['contact_email'];
$contact_heading=$row_contact_support['contact_heading'];
$contact_desc=$row_contact_support['contact_desc'];

 ?>
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard /Edit Customer Settings	
</li>	
</ol>	
</div>	
</div><!--first row ends-->  

<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-money-bill-alt"></i> Edit Customer Support Settins	
</h4>	
</div><!--card-header ends-->
<div class="card-body" style="background:#cccfcf;">
<form action="" method="POST">
<div class="form-group row">
<label class="col-md-3 control-label">From /Reply Email:</label>
<div class="col-md-6">
<input type="text" name="contact_email" class="form-control" value="<?php echo($contact_email); ?>" required>
</div>	
</div>	
<div class="form-group row">
<label class="col-md-3 control-label">Page Heading:</label>
<div class="col-md-6">
<input type="text" name="contact_heading" class="form-control" value="<?php echo($contact_heading); ?>" required>
</div>	
</div>		
<div class="form-group row">
<label class="col-md-3 control-label">Page Short Description:</label>
<div class="col-md-6">
<textarea name="contact_desc" class="form-control" rows="6"><?php echo($contact_desc); ?></textarea>
</div>	
</div>	
<div class="form-group row">
<label class="col-md-3 control-label"></label>
<div class="col-md-6">
<input type="submit" name="submit" class="btn btn-primary form-control" value="Update Customer Settings">
</div>	
</div>		
</form>	
</div><!--card-body ends-->	
</div><!--card ends-->	
</div><!---col-lg-12 ends-->	
</div><!--2 row ends-->
<?php 
if (isset($_POST['submit'])) {
$contact_email=mysqli_real_escape_string($con,$_POST['contact_email']);	
$contact_heading=mysqli_real_escape_string($con,$_POST['contact_heading']);	
$contact_desc=mysqli_real_escape_string($con,$_POST['contact_desc']);	
$update_contact_support="UPDATE  contact_support set contact_email='$contact_email',contact_heading='$contact_heading',contact_desc='$contact_desc'";
$run_contact_support=mysqli_query($con,$update_contact_support);
if ($run_contact_support) {
echo "<script>
alert('Customer Support Setting Has Been Updated');
window.open('index.php?customer_support_settings','_self');
</script>";
}
}

 ?>
 <?php } ?>