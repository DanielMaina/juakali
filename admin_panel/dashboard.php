<div class="row">
<div class="col-lg-12">		
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>&nbsp;Dashboard	
</li>	
</ol>
</div>
</div><!--ends row-->
<div class="row">
<div class="col-lg-3 col-md-3 col-sm-6">
<div class="card text-white border-primary mb-xl-0 mb-sm-3 mb-3">
<div class="card-header bg-primary">
<div class="row">
<div class="col-3">
<i class="fa fa-table fa-5x"></i>	
</div>
<div class="col-9 text-right">
<div class="huge"><?php echo($count_proposals); ?></div>	
<div class="badge badge-danger">Product</div><br>
Pending Approval	
</div>	
</div><!--ends of inner row-->
</div>	<!--end of card-header-->
<a href="index.php?view_proposals">
<div class="card-footer">
<span class="float-left">View Details</span>
<span class="float-right"><i class="fa fa-arrow-circle-right"></i></span>	
<div class="clearfix"></div>
</div>	
</a>
</div><!--card ends-->

</div><!--col-lg-3 col-md-3 ends proposal views-->
<div class="col-lg-3 col-md-3 col-sm-6">
<div class="card text-white border-success mb-xl-0 mb-sm-3 mb-3">
<div class="card-header bg-success">
<div class="row">
<div class="col-3">
<i class="fa fa-list fa-5x"></i>	
</div>
<div class="col-9 text-right">
<div class="huge"><?php echo($count_sellers); ?></div>	
<div class="text-caption">Sellers</div>
</div>	
</div><!--ends of inner row-->
</div>	<!--end of card-header-->
<a href="index.php?view_sellers">
<div class="card-footer">
<span class="float-left">View Details</span>
<span class="float-right"><i class="fa fa-arrow-circle-right"></i></span>	
<div class="clearfix"></div>
</div>	
</a>
</div><!--card ends-->	
</div><!--col-lg-3 col-md-3 ends -->
<div class="col-lg-3 col-md-3 col-sm-6">
<div class="card text-white border-warning mb-xl-0 mb-sm-3 mb-3">
<div class="card-header bg-warning">
<div class="row">
<div class="col-3">
<i class="fa fa-plane fa-5x"></i>	
</div>
<div class="col-9 text-right">
<div class="huge"><?php echo($count_orders); ?></div>	
<div class="text-caption">Active Order</div>	
</div>	
</div><!--ends of inner row-->
</div>	<!--end of card-header-->
<a href="index.php?view_orders">
<div class="card-footer">
<span class="float-left">View Details</span>
<span class="float-right"><i class="fa fa-arrow-circle-right"></i></span>	
<div class="clearfix"></div>
</div>	
</a>
</div><!--card ends-->	
</div><!--col-lg-3 col-md-3 ends-->	
<div class="col-lg-3 col-md-3 col-sm-6">
<div class="card text-white border-danger mb-xl-0 mb-sm-3 mb-3">
<div class="card-header bg-danger">
<div class="row">
<div class="col-3">
<i class="fa fa-phone-square fa-5x"></i>	
</div>
<div class="col-9 text-right">
<div class="huge"><?php echo($count_support_ticket); ?></div>	
<div class="text-caption">Open Support Request</div>	
</div>	
</div><!--ends of inner row-->
</div>	<!--end of card-header-->
<a href="index.php?view_support_requests">
<div class="card-footer">
<span class="float-left">View Details</span>
<span class="float-right"><i class="fa fa-arrow-circle-right"></i></span>	
<div class="clearfix"></div>
</div>	
</a>
</div><!--card ends-->	
</div><!--col-lg-3 col-md-3 ends-->			
</div><!--second row ends-->

<div class="row mt-5">
<div class="col-md-8">
<div class="card mb-3">
<div class="card-header">
<h5 class="h5"><i class="fa fa-money-bill-alt fa-fw"></i>&nbsp;Event Statistics!</h5>	
</div><!--card-header ends-->
<div class="card-body">
<div class="table-responsive">
<table class="table table-bordered table-hover links-table">
<thead>
<tr>
<th>Summary:</th>	
<th>Results:</th>	
</tr>	
</thead>
<tbody>
<tr class="hover-blue" onclick="location.href='index.php?view_support_requests'">
<td>Open Support Request</td>		
<td><?php echo($count_proposals); ?></td>		
</tr>
<tr class="hover-blue" onclick="location.href='index.php?view_proposals'">
<td>Product Awaiting Approval</td>		
<td><?php echo($count_request); ?></td>		
</tr>
<tr class="hover-blue" onclick="location.href='index.php?buyer_requests'">
<td>Buyer Requests Awaiting Approval</td>		
<td><?php echo($count_referrals); ?></td>		
</tr>
<tr class="hover-blue" onclick="location.href='index.php?view_referrals'">
<td>Referrals Awainting Approval</td>		
<td>5</td>		
</tr>	
</tbody>
</table>	
</div><!--ends of table[-responsive-->	
</div><!--card-boat ends-->
</div>	
</div><!--col-md-8 ends-->	
<div class="col-md-4">
<div class="card">
<div class="card-body">
<div class="admin-info mb-3">
<img src="admin_images/<?php echo($admin_image); ?>" class="rounded img-fluid">
<div class="admin-info-title">
<div class="admin-info-inner"><?php echo($admin_name); ?></div>	
<div class="admin-info-type"><?php echo($admin_job); ?></div>	
</div><!--admin-info-title ends-->	
</div><!--admin-info mb-3 ends-->	
<div class="mb-3">
<div class="widget-content-expanded">
<i class="fa fa-user"></i><span>Email:</span><?php echo($admin_email); ?><br>	
<i class="fa fa-user"></i><span>Country:</span><?php echo($admin_country); ?><br>	
<i class="fa fa-user"></i><span>Email:</span><?php echo($admin_contact); ?><br>	
<hr class="dotted">
<h5 class="text-muted">About</h5>
<p><?php echo($admin_about); ?></p>
</div><!--widget-content-expande ends-->	
</div><!--mb-3 ends-->
</div>	<!--ends of card-body-->
</div><!--card ends-->	
</div><!--col-md-4 ends-->
</div><!--Thrid row ends-->


