<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";

}else{

if (isset($_GET['edit_box'])) {
$edit_id=$_GET['edit_box'];
$get_boxes="SELECT * from section_boxes where box_id='$edit_id'";
$run_boxes=mysqli_query($con,$get_boxes);
$row_boxes=mysqli_fetch_array($run_boxes);
$box_id=$row_boxes['box_id'];
$box_title=$row_boxes['box_title'];
$box_desc=$row_boxes['box_desc'];
}

 ?>

<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard /Edit Box
</li>	
</ol>	
</div>	
</div><!--first row ends-->
<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="card-title">
<i class="fa fa-money-bill-alt fa-fw"></i>Edit Box	
</h4>	
</div><!--card-header ends-->	
<div class="card-body" style="background: #cccfcf;">
<form action="" method="POST" enctype="multipart/form-data">
<div class="form-group row">
<label class="col-md-3 control-label">Box Title:</label>
<div class="col-md-6">
<input type="text" name="box_title" class="form-control" value="<?php echo($box_title); ?>">
</div>	
</div>	
<div class="form-group row">
<label class="col-md-3 control-label">Box Description:</label>
<div class="col-md-6">
<textarea name="box_desc" class="form-control" cols="6"><?php echo($box_desc); ?></textarea>
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label"></label>
<div class="col-md-6">
<input type="submit" name="update_box" class="form-control btn btn-success" value="Update Box">
</div>	
</div>
</form>	
</div>
</div><!--card ends-->	
</div><!--col-lg-12 ends-->	
</div><!--2 row ends-->
<?php 
if (isset($_POST['update_box'])) {
$box_title=mysqli_real_escape_string($con,$_POST['box_title']);	
$box_desc=mysqli_real_escape_string($con,$_POST['box_desc']);
$update_box="UPDATE section_boxes set box_title='$box_title',box_desc='$box_desc' where box_id='$box_id'";
$run_update_box=mysqli_query($con,$update_box);	
if ($run_update_box) {
echo "<script>
alert('One Box Has Been Updated');
window.open('index.php?general_settings','_self');
</script>";	
}

}
 ?>


 <?php } ?>