<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{
if(isset($_GET['edit_cat'])) {
$edit_id=$_GET['edit_cat'];
$edit_cat="SELECT * from categories where cat_id='$edit_id'";
$run_edit=mysqli_query($con,$edit_cat);
$row_edit=mysqli_fetch_array($run_edit);
$c_title=$row_edit['cat_title'];
$c_desc=$row_edit['cat_desc'];
$c_image=$row_edit['cat_image'];
$c_featured=$row_edit['cat_featured'];
}
 ?>
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard / Edit Ctategory	
</li>	
</ol>	
</div>	
</div><!--first row ends-->
<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-money-bill-alt"></i> Edit Category	
</h4>	
</div><!--card-header ends-->
<div class="card-body" style="background: #cccfcf;">
<form action="" method="POST" enctype="multipart/form-data">
<div class="form-group row">
<label class="col-md-3 control-label">Cartegory Title</label>
<div class="col-md-6">
<input type="text" name="cat_title" class="form-control" value="<?php echo($c_title); ?>" required>
</div>	
</div>	
<div class="form-group row">
<label class="col-md-3 control-label">Cartegory Description</label>
<div class="col-md-6">
<textarea class="form-control" name="cat_desc" required><?php echo($c_desc); ?></textarea>
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label">Show as Featured Category</label>
<div class="col-md-6">
<input type="radio" name="cat_featured"  value="yes" required
<?php 
if ($c_featured=="yes") {
echo " checked ";	
}

 ?>
>
<label >Yes</label>
<input type="radio" name="cat_featured"  value="no" required
<?php 
if ($c_featured=="no") {
echo " checked ";	
}

 ?>
>
<label >No</label>
</div>	
</div>	
<div class="form-group row">
<label class="col-md-3 control-label">Cartegory Image</label>
<div class="col-md-6">
<input type="file" name="cat_image" class="form-control">
<br>
<?php if (!empty($c_image)) { ?>
<img src="../cat_images/<?php echo($c_image);?>" width="60" height="60">
<?php }else{ ?>
<img src="../cat_images/empty-image.jpg" width="60" height="60">
<?php } ?>
</div>	
</div>	
<div class="form-group row">
<label class="col-md-3 control-label"></label>
<div class="col-md-6">
<input type="submit" name="update_cat" value="Update Category" class=" btn btn-primary form-control">
</div>	
</div>			
</form>	
</div><!--card-body ends--->	
</div><!--card- ends-->	
</div><!--col-lg-12 ends-->	
</div><!--second row ends-->
<?php 
#insert category to database
if (isset($_POST['update_cat'])) {
$cat_title=mysqli_real_escape_string($con,$_POST['cat_title']);
$cat_desc=mysqli_real_escape_string($con,$_POST['cat_desc']);
$cat_featured=mysqli_real_escape_string($con,$_POST['cat_featured']);


$cat_image =$_FILES['cat_image']['name'];
$tmp_cat_image =$_FILES['cat_image']['tmp_name'];
move_uploaded_file($tmp_cat_image, "../cat_images/$cat_image");

if (empty($cat_image)) {
$cat_image=$c_image;	
}
$update_cat="UPDATE categories set cat_title='$cat_title', cat_desc='$cat_desc',cat_image='$cat_image',cat_featured='$cat_featured' where cat_id='$edit_id' ";
$run_cat=mysqli_query($con,$update_cat);
if ($run_cat) {
echo "<script>
alert('A category Has Been Update!');
window.open('index.php?view_cats','_self');
</script>";	
}
}
 ?>

 <?php } ?>


