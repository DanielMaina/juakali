<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{

 ?>
 <?php 
if (isset($_GET['edit_coupon'])) {
$edit_id=$_GET['edit_coupon'];
$edit_coupon="SELECT * from coupons where coupon_id='$edit_id'";
$run_edit=mysqli_query($con,$edit_coupon);
$row_edit=mysqli_fetch_array($run_edit);
$coupon_title=$row_edit['coupon_title'];
$coupon_price=$row_edit['coupon_price'];
$coupon_code=$row_edit['coupon_code'];
$coupon_limit=$row_edit['coupon_limit'];
$proposal_id=$row_edit['proposal_id'];

$get_proposals="SELECT * from proposals where proposal_id='$proposal_id'";
$run_proposals=mysqli_query($con,$get_proposals);
$row_proposals =mysqli_fetch_array($run_proposals);
$proposal_title=$row_proposals['proposal_title'];

}
  ?>
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard / Edit Coupon
</li>	
</ol>	
</div>	
</div><!--first row ends-->
<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-money-bill-alt"></i> Edit Coupon	
</h4>	
</div><!--card-header ends--->
<div class="card-body" style="background: #ECEFF1">
<form action="" method="POST">
<div class="form-group row">
<label class="col-md-3 control-label">Coupon Title</label>
<div class="col-md-6">
<input type="text" name="coupon_title" class="form-control" value="<?php echo($coupon_title); ?>" required>
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label">Coupon price</label>
<div class="col-md-6">
<input type="number" name="coupon_price" class="form-control" value="<?php echo($coupon_price); ?>" min="100" required>
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label">Coupon Code </label>
<div class="col-md-6">
<input type="text" name="coupon_code" class="form-control" value="<?php echo($coupon_code); ?>" required>
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label">Coupon Limit </label>
<div class="col-md-6">
<input type="number" name="coupon_limit" class="form-control" value="<?php echo($coupon_limit); ?>" min="1">
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label">Select Propisal</label>
<div class="col-md-6">
<select name="proposal_id" class="form-control" required>
<option value="<?php echo($proposal_id); ?>"><?php echo($proposal_title); ?></option>
<?php 
$get_proposals="SELECT * from proposals where proposal_status='active'";
$run_proposals=mysqli_query($con,$get_proposals);
while ($row_proposals =mysqli_fetch_array($run_proposals)) {
$proposal_id=$row_proposals['proposal_id'];
$proposal_title=$row_proposals['proposal_title'];
echo "<option value='$proposal_id'>$proposal_title</option>";
}

 ?>
</select>	
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label"></label>
<div class="col-md-6">
<input type="submit" name="update" class="btn btn-success form-control" value="Update Coupon">
</div>	
</div>	
</form>	
</div><!--card-body ends-->	
</div><!--card ends-->			
</div><!--col-lg-12 ends-->	
</div><!--second row ends-->
<?php 
if (isset($_POST['update'])) {
$coupon_title=mysqli_real_escape_string($con,$_POST['coupon_title']);	
$coupon_price=mysqli_real_escape_string($con,$_POST['coupon_price']);	
$coupon_code=mysqli_real_escape_string($con,$_POST['coupon_code']);	
$coupon_limit=mysqli_real_escape_string($con,$_POST['coupon_limit']);	
$proposal_id=mysqli_real_escape_string($con,$_POST['proposal_id']);	
$get_coupons="SELECT * from coupons where coupon_code='$coupon_code'";
$run_coupons=mysqli_query($con,$get_coupons);
$check_coupons=mysqli_num_rows($run_coupons);
if ($check_coupons == 1) {
echo "<script>
alert('Coupon Code Already Used!...');
</script>";	
}else{
$update_coupon="UPDATE coupons set proposal_id='$proposal_id',coupon_title='$coupon_title',coupon_price='$coupon_price',coupon_code='$coupon_code',coupon_limit='$coupon_limit' where coupon_id='$edit_id'";
$run_coupons=mysqli_query($con,$update_coupon);
if ($run_coupons) {
echo "<script>
alert('One Coupon Code Has Been Updated!');
window.open('index.php?view_coupons','_self');
</script>";
}
}
}

 ?>
 <?php } ?> 