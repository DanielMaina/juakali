<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{
if (isset($_GET['edit_delivery_time'])) {
 $edit_id=$_GET['edit_delivery_time'];
 $edit_delivery_time="SELECT * from delivery_times where delivery_id='$edit_id'";
$run_edit=mysqli_query($con,$edit_delivery_time);	
$row_edit=mysqli_fetch_array($run_edit);
$delivery_id=$row_edit['delivery_id'];
$delivery_title=$row_edit['delivery_title'];
$delivery_proposal_title=$row_edit['delivery_proposal_title'];
}


 ?>
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard / Edit Delivery Time	
</li>	
</ol>	
</div>	
</div><!--first row ends-->
<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-money-bill-alt"></i> Edit Delivery Time	
</h4>	
</div><!--card-header-->
<div class="card-body">
<form action="" method="POST">
<div class="form-group row">
<label class="col-md-3 control-label">Delivery Time Title</label>
<div class="col-md-6">
<input type="text" name="delivery_title" class="form-control" value="<?php echo($delivery_title); ?>" required>
<small class="form-text text-muted">This Delivery Title Will Show On Categories, Sub-Categories And Search Page</small>
</div>	
</div><!--form-group row ends-->

<div class="form-group row">
<label class="col-md-3 control-label">Product Delivery Time Title</label>
<div class="col-md-6">
<input type="text" name="delivery_proposal_title" class="form-control"  required value="<?php echo($delivery_proposal_title); ?>">  
<small class="form-text text-muted">This Delivery Title Will Show On Categorieson Product related pages</small>
</div>	
</div><!--form-group row ends-->

<div class="form-group row">
<label class="col-md-3 control-label"></label>
<div class="col-md-6">
<input type="submit" name="update_delivery_time" class="btn btn-primary form-control" value="Edit Delivery Time">
</div>	
</div><!--form-group row ends-->

</form>	
</div><!--card-body-->	
</div>	<!--card end-->
</div><!--col-lg-12 ends-->	
</div><!--second row ends-->
<?php 
if (isset($_POST['update_delivery_time'])) {
$delivery_title=mysqli_real_escape_string($con,$_POST['delivery_title']);
$delivery_proposal_title=mysqli_real_escape_string($con,$_POST['delivery_proposal_title']);
$update_delivery_time="UPDATE delivery_times set delivery_title='$delivery_title',delivery_proposal_title='$delivery_proposal_title' where delivery_id='$delivery_id'";
$run_delivery_time=mysqli_query($con,$update_delivery_time);
if ($run_delivery_time) {
	echo "<script>
alert('One Delivery Time Has Been Updated');
window.open('index.php?view_delivery_times','_self')
	</script>";
	}	
}
 ?>
<?php } ?>