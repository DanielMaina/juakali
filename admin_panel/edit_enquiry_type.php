<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{

if (isset($_GET['edit_enquiry_type'])) {
$edit_id=$_GET['edit_enquiry_type'];
$edit_enquiry_type="SELECT * from enquiry_types where enquiry_id='$edit_id'";
$run_edit=mysqli_query($con,$edit_enquiry_type);
$row_edit=mysqli_fetch_array($run_edit);
$enquiry_id=$row_edit['enquiry_id'];	
$enquiry_title=$row_edit['enquiry_title'];	
}
 ?>

 <div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard / Edit Enquiry Type
</li>	
</ol>	
</div>	
</div><!--first row ends-->
<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-money-bill-alt"></i> Edit Enquiry Type
</h4>	
</div><!--card-header ends-->	
<div class="card-body">
<form method="POST" action="">

<div class="form-group row">
<label class="col-md-3 control-label">Equiry Type Title</label>
<div class="col-md-6">
<input type="text" name="enquiry_title" class="form-control" value="<?php echo($enquiry_title); ?>">
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label"></label>
<div class="col-md-6">
<input type="submit" name="update" class="btn btn-primary form-control" value="Update Enquiry Type">
</div>	
</div>	
</form>	
</div><!--card-body-->
</div><!--card ends-->	
</div><!--col-lg-12 ends-->	
</div><!--second row ends-->
<?php 
if (isset($_POST['update'])) {
$enquiry_title=mysqli_real_escape_string($con,$_POST['enquiry_title']);
$update_enquiry_type="UPDATE enquiry_types set enquiry_title='$enquiry_title' where enquiry_id='$enquiry_id'";
$run_enquiry_type=mysqli_query($con,$update_enquiry_type);
if ($run_enquiry_type) {
echo "<script>
alert('One Query Has Been Update!');
window.open('index.php?view_enquiry_types','_self');
</script>";	
}
}
 ?>
 <?php } ?>