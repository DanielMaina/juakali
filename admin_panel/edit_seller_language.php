<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{
if (isset($_GET['edit_seller_language'])) {
$edit_id=$_GET['edit_seller_language'];
$edit_language="SELECT * from seller_languages where language_id='$edit_id'";
$run_edit=mysqli_query($con,$edit_language);
$row_edit=mysqli_fetch_array($run_edit);
$language_id=$row_edit['language_id'];
$language_title=$row_edit['language_title'];	
	
}
 ?>
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard / Edit Seller Task Execution/Performance		
</li>	
</ol>	
</div>	
</div><!--first row ends-->
<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-money-bill-alt"></i> Edit Seller Task Execution/Performance		
</h4>	
</div><!--card-header ends-->
<div class="card-body">
<form action="" method="POST">
<div class="form-group row">
<label class="col-md-3 control-label">Seller Performance Title</label>
<div class="col-md-6">
<input type="text" name="language_title" class="form-control" value="<?php echo($language_title); ?>" required>
</div>	
</div>	
<div class="form-group row">
<label class="col-md-3 control-label"></label>
<div class="col-md-6">
<input type="submit" name="update_seller_language" class=" btn btn-primary form-control" value="Update Seller Performance">
</div>	
</div>	
</form>	
</div><!--card-body ends-->	
</div><!--card ends-->	
</div><!--col-lg-12 ends-->	
</div><!--first row ends-->
<?php 
if (isset($_POST['update_seller_language'])) {
$language_title=mysqli_real_escape_string($con,$_POST['language_title']);
 $update_seller_language="UPDATE seller_languages set language_title='$language_title' where language_id='$language_id'";	
$run_seller_language=mysqli_query($con,$update_seller_language);
if ($run_seller_language) {
echo "<script>
alert('One Seller Performance level Has Been Updated');
window.open('index.php?view_seller_languages','_self');
</script>";
}
}
?>
 
<?php } ?>