<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{

if (isset($_GET['edit_seller_skill'])) {
$edit_id=$_GET['edit_seller_skill'];

$edit_skill="SELECT * from seller_skills where skill_id='$edit_id'";
$run_edit=mysqli_query($con,$edit_skill);

$row_edit=mysqli_fetch_array($run_edit);
$skill_id=$row_edit['skill_id'];		
$skill_title=$row_edit['skill_title'];		
	}
 ?>

<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard / Edit Seller Skill	
</li>	
</ol>	
</div>	
</div><!--first row ends-->
<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4"></h4> <i class ="fa fa-money-bill-alt"></i> Edit Seller Skills		
</div><!---card-header srt and end-->
<div class="card-body">
<form action="" method="POST">

<div class="form-group row">
<label class="col-md-3 control-label">Seller Skill Title</label>
<div class="col-md-6">
<input type="text" name="skill_title" class="form-control" value="<?php echo($skill_title); ?>" required>
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label"></label>
<div class="col-md-6">
<input type="submit" name="update_seller_skill" class=" btn btn-primary form-control" value="Update Seller Skill">
</div>	
</div>
</form>	
</div><!--card-body-->
</div><!--card ends -->	
</div>	
</div><!--2nd row ends-->
<?php 
if (isset($_POST['update_seller_skill'])) {
	$skill_title=$_POST['skill_title'];

$update_seller_skill="UPDATE seller_skills set skill_title='$skill_title' where skill_id='$skill_id'";
$run_seller_skill=mysqli_query($con,$update_seller_skill);

if ($run_seller_skill) {
echo "<script>
alert('One Seller Skill Has Been Updated');
window.open('index.php?view_seller_skills','_self');
</script>";
}
}

 ?>
<?php } ?>