<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{
	
 ?>
 <?php 
if (isset($_GET['edit_slide'])) {
$edit_id=$_GET['edit_slide'];	
$edit_slide="SELECT * from slider where slide_id='$edit_id'";
$run_edit=mysqli_query($con,$edit_slide);
$row_edit=mysqli_fetch_array($run_edit);
$s_name=$row_edit['slide_name'];
$s_desc=$row_edit['slide_desc'];
$s_image=$row_edit['slide_image'];
$s_url=$row_edit['slide_url'];
}
  ?>
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard / Edit Slide	
</li>	
</ol>	
</div>	
</div><!--first row ends-->
<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-money-bill-alt"></i> Edit Slide
</h4>	
</div><!--card-header ends-->
<div class="card-body" style="background: #ECEFF1">
<form action="" method="POST" enctype="multipart/form-data">

<div class="form-group row">
<label class="col-md-3 control-label">Slide Name</label>
<div class="col-md-6">
<input type="text" name="slide_name" class="form-control" value="<?php echo($s_name); ?>" required>
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label">Slide Description</label>
<div class="col-md-6">
<textarea name="slide_desc" class="form-control" required><?php echo($s_desc); ?></textarea>
</div>	
</div>	
<div class="form-group row">
<label class="col-md-3 control-label">Slide Image</label>
<div class="col-md-6">
<input type="file" name="slide_image" class="form-control"><br>
<img src="../slides_images/<?php echo($s_image); ?>" width="150" height="60">
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label">Slide URL</label>
<div class="col-md-6">
<input type="text" name="slide_url" class="form-control" value="<?php echo($s_url); ?>" required>
</div>	
</div>	
<div class="form-group row">
<label class="col-md-3 control-label"></label>
<div class="col-md-6">
<input type="submit" name="update" class="btn btn-primary form-control" value="Insert Slide">
</div>	
</div>				
</form>	
</div><!--card body ends-->
</div><!--card ends-->	
</div><!--col-lg-12 ends-->	
</div><!--2 row ends-->

<?php 
if (isset($_POST['update'])) {
$slide_name=mysqli_real_escape_string($con,$_POST['slide_name']);	
$slide_desc=mysqli_real_escape_string($con,$_POST['slide_desc']);	
$slide_url=mysqli_real_escape_string($con,$_POST['slide_url']);

$slide_image=$_FILES['slide_image']['name'];	
$tmp_slide_image=$_FILES['slide_image']['tmp_name'];	
move_uploaded_file($tmp_slide_image, "../slides_images/$slide_image");
if (empty($slide_image)) {
$slide_image=$s_image;	
}
$update_slide="UPDATE slider set slide_name='$slide_name', slide_desc='$slide_desc',slide_image='$slide_image',slide_url='$slide_url' where slide_id='$edit_id'";
$run_slide=mysqli_query($con,$update_slide);
if ($run_slide) {
echo "<script>
alert('One slide has been Updated!');
window.open('index.php?view_slides','_self');
</script>";
}
}

 ?>

<?php } ?> 