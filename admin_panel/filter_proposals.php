<?php
session_start();
include("includes/db.php");
if (!isset($_SESSION['admin_email'])) {
echo "<script>window.open('login.php','_self');</script>";	
}
if ((time() -$_SESSION['loggedin_time']) > 3600) {
echo "<script>window.open('logout.php?session_expired','_self');</script>";	
}

$admin_email=$_SESSION['admin_email'];
$get_admin="SELECT * from admins where admin_email='$admin_email'";
$run_admin=mysqli_query($con,$get_admin);
$row_admin=mysqli_fetch_array($run_admin);
 $admin_id=$row_admin['admin_id'];
 $admin_name=$row_admin['admin_name'];
 $admin_image=$row_admin['admin_image'];
 $admin_country=$row_admin['admin_country'];
 $admin_job=$row_admin['admin_job'];
 $admin_contact=$row_admin['admin_contact'];
 $admin_about=$row_admin['admin_about'];

$get_poposals="SELECT * from proposals where proposal_status='pending'";
$run_proposals=mysqli_query($con,$get_poposals);
$count_proposals=mysqli_num_rows($run_proposals);


$get_orders="SELECT * from orders where order_active='yes'";
$run_orders=mysqli_query($con,$get_orders);
$count_orders=mysqli_num_rows($run_orders);


$get_sellers="SELECT * from sellers";
$run_sellers=mysqli_query($con,$get_sellers);
$count_sellers=mysqli_num_rows($run_sellers);

$get_support_tickets="SELECT * from support_tickets where status='open'";
$run_support_ticket=mysqli_query($con,$get_support_tickets);
$count_support_ticket=mysqli_num_rows($run_support_ticket);

$get_requests="SELECT * from buyer_requests where request_status='pending'";
$run_request=mysqli_query($con,$get_requests);
$count_request=mysqli_num_rows($run_request);


$get_referrals="SELECT * from referrals where status='pending'";
$run_referrals=mysqli_query($con,$get_referrals);
$count_referrals=mysqli_num_rows($run_referrals);


?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Adm||board</title>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100" rel="stylesheet" >
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="font_awesome/css/fontawesome-all.css">
<script src="js/jquery.slim.min.js"></script>
</head>
<body>
<div id="wrapper">
<?php include("includes/sidebar.php"); ?>	
<div id="page-wrapper">
<div class="container-fruid">
<div class="row">
<div class="col-lg-12">
<div class="p-3 mb-3 filter-form">
<h2>Filter Product</h2>
<form method="GET" action="filter_proposals.php" class="form-inline">
<div class="form-group">
<label>Delivery Time:</label>	
<select name="delivery_id" class="form-control mb-2 mr-sm-2 mb-sm-0">
<?php 
$get_delivery_id=$_GET['delivery_id'];

$get_delivery_times="SELECT * from delivery_times where delivery_id='$get_delivery_id'";
$run_delivery_times=mysqli_query($con,$get_delivery_times);
$row_delivery_times=mysqli_fetch_array($run_delivery_times);
$delivery_id=$row_delivery_times['delivery_id'];		
$delivery_title=$row_delivery_times['delivery_title'];
echo "<option value='$delivery_id'>$delivery_title</option>";		

$get_delivery_times="SELECT * from delivery_times where NOT delivery_id='$get_delivery_id'";
$run_delivery_times=mysqli_query($con,$get_delivery_times);
while ($row_delivery_times=mysqli_fetch_array($run_delivery_times)) {
$delivery_id=$row_delivery_times['delivery_id'];		
$delivery_title=$row_delivery_times['delivery_title'];
echo "<option value='$delivery_id'>$delivery_title</option>";		
}
 ?>
</select>
</div>

<div class="form-group">
<label>Seller Level:</label>	
<select name="level_id" class="form-control mb-2 mr-sm-2 mb-sm-0">
<?php
$get_level_id=$_GET['level_id'];

$get_seller_levels="SELECT * from seller_levels where level_id='$get_level_id'";
$run_seller_levels=mysqli_query($con,$get_seller_levels);
$row_seller_levels=mysqli_fetch_array($run_seller_levels);
$level_id=$row_seller_levels['level_id'];	
$level_title=$row_seller_levels['level_title'];
echo "<option value='$level_id'>$level_title</option>";	


$get_seller_levels="SELECT * from seller_levels where NOT level_id='$get_level_id' ";
$run_seller_levels=mysqli_query($con,$get_seller_levels);
while ($row_seller_levels=mysqli_fetch_array($run_seller_levels)) {
$level_id=$row_seller_levels['level_id'];	
$level_title=$row_seller_levels['level_title'];
echo "<option value='$level_id'>$level_title</option>";	
}
 ?>
</select>
</div>	
<div class="form-group">
<label>Ctaegory:</label>	
<select name="cat_id" class="form-control mb-2 mr-sm-2 mb-sm-0">

<?php 
$get_cat_id=$_GET['cat_id'];

$get_categories="SELECT * from categories where cat_id='$get_cat_id'";
$run_categories=mysqli_query($con,$get_categories);
$row_categories=mysqli_fetch_array($run_categories);
$cat_id=$row_categories['cat_id'];
$cat_title=$row_categories['cat_title'];
echo "<option value='$cat_id'>$cat_title</option>";


$get_categories="SELECT * from categories where NOT cat_id='$get_cat_id'";
$run_categories=mysqli_query($con,$get_categories);
while ($row_categories=mysqli_fetch_array($run_categories)) {
$cat_id=$row_categories['cat_id'];
$cat_title=$row_categories['cat_title'];
echo "<option value='$cat_id'>$cat_title</option>";
}
 ?>
</select>
</div>
<button type="submit" class="btn btn-default">Filter Product</button>
</form>
</div>		
</div>	
</div><!--first row ends-->

<div class="row mt-3">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4"><i class="fa fa-money-bill-alt"></i>View Products</h4>	
</div><!--card-header ends-->	
<div class="card-body">
<div class="table-respnsive">
<table class="table able-hover table border">
<thead>
<tr>
<th>Product Title</th>	
<th>Product Image</th>	
<th>Product Price</th>	
<th>Product Category</th>	
<th>Product Order Queue</th>	
<th>Product Options</th>	
</tr>	
</thead>
<tbody>
<?php 
$get_proposals="SELECT * from proposals where delivery_id='$get_delivery_id' or level_id='$get_level_id' or proposal_cat_id='$get_cat_id' order by 1 DESC";
$run_proposals=mysqli_query($con,$get_proposals);
while ($row_proposals =mysqli_fetch_array($run_proposals)) {
$proposal_id=$row_proposals['proposal_id'];
$proposal_title=$row_proposals['proposal_title'];
$proposal_img1=$row_proposals['proposal_img1'];
$proposal_url=$row_proposals['proposal_url'];
$proposal_price=$row_proposals['proposal_price'];
$proposal_cat_id=$row_proposals['proposal_cat_id'];
$proposal_status=$row_proposals['proposal_status'];

$select_orders="SELECT * from orders where  proposal_id='$proposal_id' AND NOT order_status='complete' AND proposal_id='$proposal_id' AND NOT order_status='cancelled'";
$run_orders=mysqli_query($con,$select_orders);
$count_orders=mysqli_num_rows($run_orders);
$proposal_order_queue=$count_orders;
$get_categories="SELECT * from categories where cat_id='$proposal_cat_id'";
$run_categories=mysqli_query($con,$get_categories);
$row_categories=mysqli_fetch_array($run_categories);
$cat_title=$row_categories['cat_title'];
 ?>
<tr>
<td>
<a href="index.php?single_proposal=<?php echo($proposal_id); ?>">
<?php echo($proposal_title); ?>
</a>
</td>	
<td>
<img src="../proposals/proposal_files/<?php echo($proposal_img1); ?>" width="50" height="50">
</td>	
<td><?php echo($proposal_price); ?></td>	
<td><?php echo($cat_title); ?></td>	
<td><?php echo($proposal_order_queue); ?></td>	
<?php 
if ($proposal_status=="active") { ?>	
 <td><a href="../proposals/<?php echo($proposal_url); ?>" target="_blank"><i class="fa fa-eye"></i>View</a> |
<a href="index.php?pause_propoal=<?php echo($proposal_id); ?>" ><i class="fa fa-pause-circle"></i>Pause</a> |
<a class="text-danger" href="index.php?move_to_trash=<?php echo($proposal_id); ?>" ><i class="fa fa-trash-alt "></i>Trash</a></td>

<?php }elseif ($proposal_status=="pause") { ?>
<td><a href="../proposals/<?php echo($proposal_url); ?>" target="_blank"><i class="fa fa-eye"></i>View</a> |
<a href="#" >Paused</a> |
<a class="text-danger" href="index.php?move_to_trash=<?php echo($proposal_id); ?>" ><i class="fa fa-trash-alt"></i>Trash</a></td>
 <?php }elseif ($proposal_status=="pending") { ?>
<td>
<a href="index.php?approve_proposal=<?php echo($proposal_id); ?>" ><i class="fa fa-eye"></i>Approval</a> |
<a href="index.php?submit_modification=<?php echo($proposal_id); ?>" ><i class="fa fa-eye"></i>Modify</a>	
</td>
 <?php }elseif ($proposal_status=="trash") { ?>
 <td>
<a href="index.php?restore_proposal=<?php echo($proposal_id); ?>" ><i class="fa fa-eye"></i>Restore</a> |
<a class="text-danger" href="index.php?delete_proposal=<?php echo($proposal_id); ?>" ><i class="fa fa-trash-alt"></i>Delete </a>	
</td>	
 <?php } ?>	
</tr>
<?php } ?>		
</tbody>	
</table>	
</div><!--table-respnsive mt-4 ends-->
</div><!--card-body ends-->
</div><!--card-ends-->	
</div><!--col-lg-12 ends-->	
</div><!--2 row ends-->
</div>	<!--end container-fluid-->
</div><!--page-wrapper ends-->	
</div><!--wrapper ends-->	

<script src="js/proper.js"></script>
<script src="js/bootstrap.min.js"></script> 	
</body>
</html>