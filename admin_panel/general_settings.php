<?php
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{
$get_general_setting="SELECT * from general_settings";
$run_general_setting=mysqli_query($con,$get_general_setting);
$row_general_setting=mysqli_fetch_array($run_general_setting);
$site_title=$row_general_setting['site_title'];
$site_desc=$row_general_setting['site_desc'];
$site_keywords=$row_general_setting['site_keywords'];
$site_url=$row_general_setting['site_url'];
$site_author=$row_general_setting['site_author'];
$site_email_address=$row_general_setting['site_email_address'];
$level_one_rating=$row_general_setting['level_one_rating'];
$level_one_orders=$row_general_setting['level_one_orders'];
$level_two_rating=$row_general_setting['level_two_rating'];
$level_two_orders=$row_general_setting['level_two_orders'];
$level_top_rating=$row_general_setting['level_top_rating'];
$level_top_orders=$row_general_setting['level_top_orders'];
$enable_referrals=$row_general_setting['enable_referrals'];
$referral_money=$row_general_setting['referral_money'];

//Update the Section area... get existing data
$get_section="SELECT * from home_section";
$run_setion=mysqli_query($con,$get_section);
$row_section=mysqli_fetch_array($run_setion);
$section_id=$row_section['section_id'];
$section_title=$row_section['section_title'];
$section_short_desc=$row_section['section_short_desc'];
$section_desc=$row_section['section_desc'];
$section_button=$row_section['section_button'];
$section_button_url=$row_section['section_button_url'];
$db_section_image=$row_section['section_image'];


?>
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard / General Settings	
</li>	
</ol>	
</div>	
</div><!--first row ends-->

<div class="row">
<div class="col-lg-12">
<div class="card mb-5">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-money-bill-alt fa-fw"></i>General Setting	
</h4>	
</div><!--ends card-header-->
<div class="card-body" style="background: #cccfcf;">
<form method="POST" enctype="multipart/form-data">
<div class="form-group row">
<label class="col-md-3 control-label">Site Title</label>
<div class="col-md-6">
<input type="text" name="site_title" class="form-control" value="<?php echo($site_title); ?>">
</div>	
</div>	

<div class="form-group row">
<label class="col-md-3 control-label">Site Descriptions</label>
<div class="col-md-6">
<textarea class="form-control" name="site_desc" id="" cols="19" rows="6"><?php echo($site_desc); ?></textarea>
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label">Site Keyword</label>
<div class="col-md-6">
<input type="text" name="site_keywords" class="form-control" value="<?php echo($site_keywords); ?>">
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label">Site Author</label>
<div class="col-md-6">
<input type="text" name="site_author" class="form-control" value="<?php echo($site_author); ?>">
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label">Site URL</label>
<div class="col-md-6">
<input type="text" name="site_url" class="form-control" value="<?php echo($site_url); ?>">
<small class="form-text text-muted"><span class="text-danger">IMPORTANT!</span> Ensure you put Full Address of Installation Directory</small>
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label">Site Email Address</label>
<div class="col-md-6">
<input type="text" name="site_email_address" class="form-control" value="<?php echo($site_email_address); ?>">
</div>	
</div>

<div class="form-group row">
<label class="col-md-3 control-label">Level One Seller rating</label>
<div class="col-md-6">
<input type="text" name="level_one_rating" class="form-control" value="<?php echo($level_one_rating); ?>">
<small class="form-text text-muted">
The Overall Posistive Rating Percentage A seller Need To Become Level One	
</small>
</div>	
</div>

<div class="form-group row">
<label class="col-md-3 control-label">Level One Seller Completed Order</label>
<div class="col-md-6">
<input type="text" name="level_one_orders" class="form-control" value="<?php echo($level_one_orders); ?>">
<small class="form-text text-muted">
How May Completed Orders A Seller Needs To Have In Order to Be Level One </small>
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label">Level Two Seller Ratings</label>
<div class="col-md-6">
<input type="text" name="level_two_rating" class="form-control" value="<?php echo($level_two_rating); ?>">
<small class="form-text text-muted">
The Overall Posistive Rating Percentage A seller Need To Become Level Two!
 </small>
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label">Level Two Seller Completed Orders</label>
<div class="col-md-6">
<input type="text" name="level_two_orders" class="form-control" value="<?php echo($level_two_orders); ?>">
<small class="form-text text-muted">
How May Completed Orders A Seller Needs To Have In Order to Be Level Two
</small>
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label">Top Rated Seller Ratings</label>
<div class="col-md-6">
<input type="text" name="level_top_rating" class="form-control" value="<?php echo($level_top_rating); ?>">
<small class="form-text text-muted">
The Overall Rating Percentage A Seller Need To Have In Order To Become Top Rated	
</small>
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label">Top Rated seller Completed Order</label>
<div class="col-md-6">
<input type="text" name="level_top_orders" class="form-control" value="<?php echo($level_top_orders); ?>">
<small class="form-text text-muted">
How Many Completed Orders A Seller Need To Have In Order To Become Top Rated	
</small>
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label">Enable Referrals</label>
<div class="col-md-6">
<select name="enable_referrals" class="form-control">
<?php if ($enable_referrals== "yes") { ?>	
<option value="yes">Yes</option>	
<option value="no">No</option>	
<?php }elseif ($enable_referrals =="no") { ?>
<option value="no">No</option>
<option value="yes">Yes</option>			
<?php } ?>
</select>
<small class="form-text text-muted">
ENABLE OR DISABLE THE REFRRALS SYSTEM!	
</small>
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label">Money To Pay For Each Referral</label>
<div class="col-md-6">
<div class="input-group">
<span class="input-group-prepend"><span class="input-group-text">Kshs</span></span>
<input type="number" name="referral_money" class="form-control" min="1" value="<?php echo($referral_money); ?>">	
</div><!--input-group ends-->

<small class="form-text text-muted">
Enter Amount Of Money To Pay the Person for Each Referral  Signs Up	
</small>
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label"></label>
<div class="col-md-6">
<input type="submit" name="general_settings_update" class="form-control btn btn-success" value="Update General Settings">
</div>	
</div>
</form>
</div>
</div><!--card ends-->	
</div><!--col-lg-12-->	
</div><!--end of the second row-->
<div class="row">
<div class="col-lg-12">
<div class="card mb-5">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-cubes fa-fw"></i>View Boxes
<small class="text-muted">
This Boxes Will show if any Visitor Open Application without being Logged in	
</small>	
</h4>	
</div><!--card-header ends-->	
<div class="card-body">
<a class="btn btn-success btn-lg float-right" href="index.php?insert_box">Add New Box</a>
<div class="clearfix mb-4"></div>	
<div class="row">
<?php 
$get_boxes="SELECT * from section_boxes";
$run_boxes=mysqli_query($con,$get_boxes);
while ($row_boxes=mysqli_fetch_array($run_boxes)) {
$box_id=$row_boxes['box_id'];
$box_title=$row_boxes['box_title'];
$box_desc=$row_boxes['box_desc'];
 ?>
<div class="col-lg-4 col-md-6 mb-lg-0 mb-3">
<div class="card mb-3">
<div class="card-header text-center">
<h4 class="h4">
<?php echo($box_title);?>	
</h4>	
</div><!--ends card-header-->
<div class="card-body">
<p><?php echo($box_desc); ?></p>	
</div>	<!--card-body ends-->
<div class="card-footer">
<a class="float-left text-danger" href="index.php?delete_box=<?php echo($box_id); ?>"> <i class="fa fa-trash-alt"></i>Delete</a>	
<a class="float-right text-success" href="index.php?edit_box=<?php echo($box_id); ?>"> <i class="fa fa-pencil-alt"></i>Edit</a>
<div class="clearfix"></div>	
</div><!--card-footer ends-->
</div><!--card ends-->	
</div><!--col-lg-4 col-md-6 mb-lg-0 mb-3 ends-->
<?php } ?> 
</div><!--Inner row ends-->
</div><!--ends card-body-->
</div><!--card ends-->
</div><!--col-lg-12 ends-->	
</div><!--Third row-->

<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">	
<h4 class="h4">
<i class="fa fa-home fa-fw"></i>
Home Page Call To Action Section
<small class="text-muted">
The Section Only Show if The Visitor Has NOT Logged To System	
</small>	
</h4>
</div><!--end card-header-->
<div class="card-body" style="background: #cccfcf;">
<form action="" method="POST" enctype="multipart/form-data">
<div class="form-group row">
<label class="col-md-3 control-label">Section Title</label>
<div class="col-md-6">
<input type="text" name="section_title" class="form-control" value="<?php echo($section_title); ?>">
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label">Section Short Description</label>
<div class="col-md-6">
<input type="text" name="section_short_desc" class="form-control" value="<?php echo($section_short_desc); ?>">
</div>	
</div>	
<div class="form-group row">
<label class="col-md-3 control-label">Section Description</label>
<div class="col-md-6">
<textarea class="form-control" name="section_desc"  cols="19" ><?php echo($section_desc); ?></textarea>
</div>	
</div>	
<div class="form-group row">
<label class="col-md-3 control-label">Section Button</label>
<div class="col-md-6">
<input type="text" name="section_button" class="form-control" value="<?php echo($section_button); ?>">
</div>	
</div>	
<div class="form-group row">
<label class="col-md-3 control-label">Section Button URL</label>
<div class="col-md-6">
<input type="text" name="section_button_url" class="form-control" value="<?php echo($section_button_url); ?>">
</div>	
</div>	
<div class="form-group row">
<label class="col-md-3 control-label">Section Image</label>
<div class="col-md-6">
<input type="file" name="section_image" class="form-control" value=""><br>
<img src="../images/<?php echo($db_section_image); ?>" width="60" height="60">
</div>	
</div>	
<div class="form-group row">
<label class="col-md-3 control-label"></label>
<div class="col-md-6">
<input type="submit" name="update_section" class="form-control btn btn-success " value="Update Section">
</div>	
</div>			
</form>	
</div><!--card-body ends-->
</div><!--card ends-->	
</div><!--col-lg-12 ends-->	
</div><!--fourth row srt-->


<?php 
if (isset($_POST['general_settings_update'])) {
$site_title=mysqli_real_escape_string($con,$_POST['site_title']);	
$site_desc=mysqli_real_escape_string($con,$_POST['site_title']);	
$site_keywords=mysqli_real_escape_string($con,$_POST['site_keywords']);	
$site_author=mysqli_real_escape_string($con,$_POST['site_author']);	
$site_url=mysqli_real_escape_string($con,$_POST['site_url']);	
$site_email_address=mysqli_real_escape_string($con,$_POST['site_email_address']);	
$level_one_rating=mysqli_real_escape_string($con,$_POST['level_one_rating']);
$level_one_orders=mysqli_real_escape_string($con,$_POST['level_one_orders']);	
$level_two_rating=mysqli_real_escape_string($con,$_POST['level_two_rating']);		
$level_two_orders=mysqli_real_escape_string($con,$_POST['level_two_orders']);	
$level_top_rating=mysqli_real_escape_string($con,$_POST['level_top_rating']);	
$level_top_orders=mysqli_real_escape_string($con,$_POST['level_top_orders']);	
$enable_referrals=mysqli_real_escape_string($con,$_POST['enable_referrals']);	
$referral_money=mysqli_real_escape_string($con,$_POST['referral_money']);

$update_general_settings="UPDATE general_settings set site_title='$site_title',site_desc='$site_desc',site_keywords='$site_keywords',site_author='$site_author',site_url='$site_url',site_email_address='$site_email_address',level_one_rating='$level_one_rating',level_one_orders='$level_one_orders',level_two_rating='$level_two_rating',level_two_orders='$level_two_orders',level_top_rating='$level_top_rating',level_top_orders='$level_top_orders', enable_referrals='$enable_referrals',referral_money='$referral_money' ";	
$run_update_general_settings=mysqli_query($con,$update_general_settings);
if ($run_update_general_settings) {
	echo "<script>
   alert('General Settings Has Been Updated Successfully');
   window.open('index.php?general_settings','_self');
	</script>";
}

}

//UPADATE THE SECTION DATA HEAR...
 if (isset($_POST['update_section'])) {
 $section_title=mysqli_real_escape_string($con,$_POST['section_title']);	
 $section_short_desc=mysqli_real_escape_string($con,$_POST['section_short_desc']);	
 $section_desc=mysqli_real_escape_string($con,$_POST['section_desc']);	
 $section_button=mysqli_real_escape_string($con,$_POST['section_button']);	
 $section_button_url=mysqli_real_escape_string($con,$_POST['section_button_url']);	
$section_image=$_FILES['section_image']['name'];
$section_image_tmp=$_FILES['section_image']['tmp_name'];

if (empty($section_image)) {
$section_image=$db_section_image;		
}

move_uploaded_file($section_image_tmp, "../images/$section_image");
$update_section="UPDATE home_section set section_title='$section_title',section_short_desc='$section_short_desc',section_desc='$section_desc',section_button='$section_button',section_button_url='$section_button_url',section_image='$section_image' where section_id='$section_id'";
$run_update_section=mysqli_query($con,$update_section);
if ($run_update_section) {
echo "<script>
alert('Home Page Calls To Action Has Been Updated Successfully');
window.open('index.php?general_settings','_self');
</script>";	
}
 }
 ?>


<?php } ?>