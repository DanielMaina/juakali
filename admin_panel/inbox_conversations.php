<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{
 ?>
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard /View Inbox Conversations	
</li>	
</ol>	
</div>	
</div><!--first row ends-->
<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-money-bill-alt fa-fw"></i>View Ibox Conversations	
</h4>	
</div><!--card-header ends-->
<div class="card-body">
<div class="table-responsive">
<table class="table table-hover table-bordered table-hover links-table">
<thead>
<tr>
<th>Sender : </th>	
<th>Receiver : </th>	
<th>Message : </th>	
<th>Attachment: </th>	
<th>Updated : </th>	
</tr>	
</thead>
<tbody>
<?php 
$per_page=5;
$get_inbox_sellers="SELECT * from inbox_sellers where NOT message_status='empty' order by 1 DESC LIMIT 0, $per_page";
$run_inbox_sellers=mysqli_query($con,$get_inbox_sellers);
while ($row_inbox_sellers=mysqli_fetch_array($run_inbox_sellers)){
$sender_id=$row_inbox_sellers['sender_id'];	
$receiver_id=$row_inbox_sellers['receiver_id'];	
$message_id=$row_inbox_sellers['message_id'];	
$message_group_id=$row_inbox_sellers['message_group_id'];	

$get_inbox_messages="SELECT * from inbox_messages where message_id='$message_id'";
$run_inbox_messages=mysqli_query($con,$get_inbox_messages);
$row_inbox_messages=mysqli_fetch_array($run_inbox_messages);
$message_file=$row_inbox_messages['message_file'];	
$message_desc=substr($row_inbox_messages['message_desc'], 0, 50)." ...";	
$message_date=$row_inbox_messages['message_date'];	

$select_sender="SELECT * from sellers where seller_id='$sender_id'";
$run_sender=mysqli_query($con,$select_sender);
$row_sender=mysqli_fetch_array($run_sender);
$sender_user_name=$row_sender['seller_user_name'];

$select_receiver="SELECT * from sellers where seller_id='$receiver_id'";
$run_receiver=mysqli_query($con,$select_receiver);
$row_receiver=mysqli_fetch_array($run_receiver);
$receiver_user_name=$row_receiver['seller_user_name'];

 ?>	
 <tr class="hover-blue" onclick="location.href='index.php?single_inbox_message=<?php echo($message_group_id); ?>'">
 <td><?php echo($sender_user_name); ?></td>
 <td><?php echo($receiver_user_name); ?></td>	
 <td width="300"><?php echo($message_desc); ?></td>	
 <td>
<?php 
if(empty($message_file)) {
echo "No File Attached";	
}else{
?>
<a href="#"><i class="fa fa-download"></i></a><?php echo($message_file); ?>	
<?php } ?>
</td>	
<td><?php echo($message_date); ?></td>	 
 	
 </tr>
<?php } ?>
</tbody>	
</table>	
</div><!--table-responsive-->
<div class="d-flex justify-content-center">
<ul class="pagination">
<?php 
//select all data from table
$query="SELECT * from inbox_messages where NOT message_status='empty' order by 1 DESC";
$run_query=mysqli_query($con,$query);
$total_records=mysqli_num_rows($run_query);

//use of the ceil php function to divide total number
$total_pages=ceil($total_records / $per_page);
echo "
<li class='page-item'>
<a href='index.php?inbox_conversations_pagination=1' class='page-link'>First Page</a>
</li>
";
for ($i=1; $i <=$total_pages ; $i++) { 
echo "
<li class='page-item'>
<a href='index.php?inbox_conversations_pagination=".$i."' class='page-link'>".$i."</a>
</li>
";
}
echo "
<li class='page-item'>
<a href='index.php?inbox_conversations_pagination=$total_pages' class='page-link'>Last Page</a>
</li>
";
 ?>	
</ul>	
</div><!--d-flex justify-content-center ends--> 	
</div><!--card-body ends-->	
</div><!--card ends-->
</div><!--col-lg-12 ends-->	
</div><!--2 row ends-->
<?php } ?>

