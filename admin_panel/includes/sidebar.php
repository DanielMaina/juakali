 <?php
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{

?>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top mb-5" id="mainNav">
<a href="index.php?dashboard" class="navbar-brand">ReachComputer Control Center</a>	
<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSidebar">
<span class="navbar-toggler-icon"></span>	
</button>
<div class="collapse navbar-collapse" id="navbarSidebar">
<ul class="navbar-nav side-nav nav">
<li class="nav-item">
<a href="index.php?dashboard" class="nav-link"><i class="fa fa-tachometer-alt fa-1x"></i>Dashboard</a>	
</li>
<li class="nav-item">
<a href="#" data-toggle="collapse" data-target="#setting" class="nav-link"><i class="fa fa-fw fa-cog"></i>Setting
<i class="fa fa-fw fa-caret-down"></i>
</a>
<ul id="setting" class="collapse">
<li>
<a href="index.php?general_settings">General Settings</a>	
</li>
<li>
<a href="index.php?layout_settings">Layout Settings</a>	
</li>
<li>
<a href="index.php?payment_settings">Payment Settings</a>	
</li>	
</ul>	
</li>
<li class="nav-item">
<a href="index.php?view_proposals" class="nav-link"><i class="fa fa-fw fa-table"></i>Products
<?php 
if (!$count_proposals == 0) {

 ?>
<span class="badge badge-secondary"><?php echo($count_proposals); ?></span>
<?php } ?>
</a>	
</li>
<li class="nav-item">
<a href="index.php?inbox_conversations" class="nav-link"><i class="fa fa-fw fa-comment"></i>Inbox Conversations</a>	
</li>
<li class="nav-item">
<a href="#" data-toggle="collapse" data-target="#reviews" class="nav-link"><i class="fa fa-fw fa-comments"></i>Review
<i class="fa fa-fw fa-caret-down"></i>
</a>
<ul id="reviews" class="collapse">
<li>
<a href="index.php?insert_review">Insert Review</a>	
</li>
<li>
<a href="index.php?view_buyer_reviews">View Reviews</a>	
</li>	
</ul>	
</li>

<li class="nav-item">
<a href="index.php?buyer_requests" class="nav-link"><i class="fa fa-fw fa-bars"></i>Buyer Requests
<?php 
if (!$count_request == 0) {

 ?>	
<span class="badge badge-secondary"><?php echo($count_request); ?></span>
<?php } ?>
</a>	
</li>

<li class="nav-item">
<a href="#" data-toggle="collapse" data-target="#cats" class="nav-link"><i class="fa fa-fw fa-pencil-alt"></i>Categories
<i class="fa fa-fw fa-caret-down"></i>
</a>
<ul id="cats" class="collapse">
<li>
<a href="index.php?insert_cat">Insert Categoreis</a>	
</li>
<li>
<a href="index.php?view_cats">View Categories</a>	
</li>	
</ul>	
</li>

<li class="nav-item">
<a href="#" data-toggle="collapse" data-target="#child-cats" class="nav-link"><i class="fa fa-fw fa-camera"></i> Sub Categories
<i class="fa fa-fw fa-caret-down"></i>
</a>
<ul id="child-cats" class="collapse">
<li>
<a href="index.php?insert_child_cats">Insert Sub Categoreis</a>	
</li>
<li>
<a href="index.php?view_child_cats">View  Sub Categories</a>
</li>	
</ul>	
</li>
<li class="nav-item">
<a href="#" data-toggle="collapse" data-target="#delivery_time" class="nav-link"><i class="fa fa-fw fa-pencil-alt"></i>Delivery Time
<i class="fa fa-fw fa-caret-down"></i>
</a>
<ul id="delivery_time" class="collapse">
<li>
<a href="index.php?insert_delivery_time">Insert Delivery Time</a>	
</li>
<li>
<a href="index.php?view_delivery_times">View  Delivery Time</a>
</li>
</ul>
</li>
<li class="nav-item">
<a href="#" data-toggle="collapse" data-target="#seller_language" class="nav-link"><i class="fa fa-fw fa-language"></i> Seller Task Performance	
<i class="fa fa-fw fa-caret-down"></i>
</a>
<ul id="seller_language" class="collapse">
<li>
<a href="index.php?insert_seller_language">Insert Seller Task Performance	</a>	
</li>
<li>
<a href="index.php?view_seller_languages">View  Seller Performance	</a>
</li>
</ul>
</li>

<li class="nav-item">
<a href="#" data-toggle="collapse" data-target="#seller_skill" class="nav-link"><i class="fa fa-fw fa-globe"></i> Seller Skills
<i class="fa fa-fw fa-caret-down"></i>
</a>
<ul id="seller_skill" class="collapse">
<li>
<a href="index.php?insert_seller_skill">Insert Seller Skill</a>	
</li>
<li>
<a href="index.php?view_seller_skills">View  Seller Skills</a>
</li>
</ul>
</li>

<li class="nav-item">
<a href="#" data-toggle="collapse" data-target="#support_center" class="nav-link"><i class="fa fa-fw fa-ticket-alt"></i> Customer Support
<i class="fa fa-fw fa-caret-down"></i>
</a>
<ul id="support_center" class="collapse">
<li>
<a href="index.php?customer_support_settings">Customer Support Setting</a>	
</li>
<li>
<a href="index.php?view_support_requests">
View Support Requests 
<?php 
if (!$count_support_ticket == 0) {
	
 ?>
<span class="badge badge-secondary"><?php echo($count_support_ticket); ?></span>
<?php } ?>
</a>
</li>
<li>
<a href="index.php?insert_enquiry_type">Insert Enquiry Type</a>	
</li>
<li>
<a href="index.php?view_enquiry_types">View Enquery Types</a>	
</li>
</ul>
</li>

<li class="nav-item">
<a href="#" data-toggle="collapse" data-target="#coupones" class="nav-link"><i class="fa fa-fw fa-gift"></i> Coupons
<i class="fa fa-fw fa-caret-down"></i>
</a>
<ul id="coupones" class="collapse">
<li>
<a href="index.php?insert_coupon">Insert Coupon</a>	
</li>
<li>
<a href="index.php?view_coupons">View Coupons</a>
</li>
</ul>
</li>

<li class="nav-item">
<a href="#" data-toggle="collapse" data-target="#slides" class="nav-link"><i class="fas fa-recycle"></i> Slides
<i class="fa fa-fw fa-caret-down"></i>
</a>
<ul id="slides" class="collapse">
<li>
<a href="index.php?insert_slide">Insert Slide</a>	
</li>
<li>
<a href="index.php?view_slides">View Slides</a>
</li>
</ul>
</li>

<li class="nav-item">
<a href="#" data-toggle="collapse" data-target="#terms" class="nav-link"><i class="fa fa-fw fa-table"></i> Terms
<i class="fa fa-fw fa-caret-down"></i>
</a>
<ul id="terms" class="collapse">
<li>
<a href="index.php?insert_term">Insert Term</a>	
</li>
<li>
<a href="index.php?view_terms">View Terms</a>
</li>
</ul>
</li>
<li class="nav-item">
<a href="index.php?view_sellers" class="nav-link"><i class="fa fa-fw fa-list"></i>View Sellers</a>	
</li>
<li class="nav-item">
<a href="index.php?view_orders" class="nav-link"><i class="fa fa-fw fa-pencil-alt"></i>View Orders</a>	
</li>

<li class="nav-item">
<a href="index.php?view_referrals" class="nav-link"><i class="fa fa-fw fa-universal-access"></i>View Referrals
<?php 
if (!$count_referrals == 0) {
	
 ?>
<span class="badge badge-secondary"><?php echo($count_referrals); ?></span>
<?php } ?>
</a>	
</li>

<li class="nav-item">
<a href="#" data-toggle="collapse" data-target="#files" class="nav-link"><i class="fa fa-fw fa-file"></i> Files
<i class="fa fa-fw fa-caret-down"></i>
</a>
<ul id="files" class="collapse">
<li>
<a href="index.php?view_proposals_files">Products File</a>	
</li>
<li>
<a href="index.php?view_inbox_files">Inbox Conversations File</a>
</li>
<li>
<a href="index.php?view_order_files">Order Conversations File</a>
</li>
</ul>
</li>

<li class="nav-item">
<a href="#" data-toggle="collapse" data-target="#users" class="nav-link"><i class="fa fa-fw fa-users"></i> Users
<i class="fa fa-fw fa-caret-down"></i>
</a>
<ul id="users" class="collapse">
<li>
<a href="index.php?insert_user">Add User</a>	
</li>
<li>
<a href="index.php?view_users">View Users</a>
</li>
<li>
<a href="index.php?user_profile=<?php echo($login_admin_id); ?>">Edit Profile</a>
</li>
</ul>
</li>

<li class="nav-item">
<a href="logout.php" class="nav-link"><i class="fa fa-fw fa-sign-out-alt"></i>LogOut</a>	
</li>

</ul><!--navbar-nav side-nav nav ends-->
<ul class="navbar-nav ml-auto mr-5">

<li class="nav-item mr-3">
<div class="dropdown">
<button class="btn btn-outline-secondary btn-sm dropdown-toggle text-white" id="deopdownMenuButton" data-toggle="dropdown">
<img src="admin_images/<?php echo($admin_image); ?>" width="30" height="30" class="rounded-circle">&nbsp;<?php echo($admin_name); ?>	
</button>
<div class="dropdown-menu user-menu">
<a href="index.php?user_profile=<?php echo($admin_id); ?>" class="dropdown-item"><i class="fa fa-fw fa-users"></i>Your Profile</a>
<a href="index.php?view_proposals" class="dropdown-item"><i class="fa fa-fw fa-table"></i>
<span class="text-danger">Pending</span>
<span class="badge badge-secondary"><?php echo($count_proposals); ?></span></a>
<a href="index.php?view_sellers" class="dropdown-item"><i class="fa fa-fw fa-list"></i>Sellers
<span class="badge badge-secondary"><?php echo($count_sellers); ?></span>
</a>
<a href="index.php?view_orders" class="dropdown-item"><i class="fa fa-fw fa-plane"></i>Active Order
<span class="badge badge-secondary"><?php echo($count_orders); ?></span>
</a>
<a href="index.php?view_support_requests" class="dropdown-item"><i class="fa fa-fw fa-phone-square"></i>
<span class="text-danger">Open</span><br>
Support Request<span class="badge badge-secondary"><?php echo($count_support_ticket); ?></span></a>
<div class="dropdown-divider"></div>
<a href="logout.php" class="dropdown-item"><i class="fa fa-fw fa-sign-out-alt"></i>Logout</a>	
</div><!--dropdown-menu user-menu ends-->	
</div>
</li>	
</ul><!--navbar-nav ml-auto ends-->	
</div><!--ends of navbarSidebar-->
</nav><!--end navbar main-->

<?php } ?>