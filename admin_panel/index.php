<?php
session_start();
include("includes/db.php");
if (!isset($_SESSION['admin_email'])) {
echo "<script>window.open('login.php','_self');</script>";	
}
if ((time() - $_SESSION['loggedin_time']) > 3600) {
echo "<script>window.open('logout.php?session_expired','_self');</script>";	
}

$admin_email=$_SESSION['admin_email'];
$get_admin="SELECT * from admins where admin_email='$admin_email'";
$run_admin=mysqli_query($con,$get_admin);
$row_admin=mysqli_fetch_array($run_admin);
 $admin_id=$row_admin['admin_id'];
 $login_admin_id=$row_admin['admin_id'];
 $admin_name=$row_admin['admin_name'];
 $admin_image=$row_admin['admin_image'];
 $admin_country=$row_admin['admin_country'];
 $admin_job=$row_admin['admin_job'];
 $admin_contact=$row_admin['admin_contact'];
 $admin_about=$row_admin['admin_about'];

$get_poposals="SELECT * from proposals where proposal_status='pending'";
$run_proposals=mysqli_query($con,$get_poposals);
$count_proposals=mysqli_num_rows($run_proposals);


$get_orders="SELECT * from orders where order_active='yes'";
$run_orders=mysqli_query($con,$get_orders);
$count_orders=mysqli_num_rows($run_orders);


$get_sellers="SELECT * from sellers";
$run_sellers=mysqli_query($con,$get_sellers);
$count_sellers=mysqli_num_rows($run_sellers);

$get_support_tickets="SELECT * from support_tickets where status='open'";
$run_support_ticket=mysqli_query($con,$get_support_tickets);
$count_support_ticket=mysqli_num_rows($run_support_ticket);

$get_requests="SELECT * from buyer_requests where request_status='pending'";
$run_request=mysqli_query($con,$get_requests);
$count_request=mysqli_num_rows($run_request);


$get_referrals="SELECT * from referrals where status='pending'";
$run_referrals=mysqli_query($con,$get_referrals);
$count_referrals=mysqli_num_rows($run_referrals);


?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Adm||board</title>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100" rel="stylesheet" >
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="font_awesome/css/fontawesome-all.css">
<script src="js/jquery.slim.min.js"></script>
</head>
<body>
<div id="wrapper">
<?php include("includes/sidebar.php"); ?>	
<div id="page-wrapper">
<div class="container-fruid">

<?php if (isset($_GET['dashboard'])) {
	include("dashboard.php");
} 

 if (isset($_GET['general_settings'])) {
	include("general_settings.php");
} 
 if (isset($_GET['insert_box'])) {
	include("insert_box.php");
} 
 if (isset($_GET['delete_box'])) {

	include("delete_box.php");
} 	

 if (isset($_GET['edit_box'])) {
	
	include("edit_box.php");
} 
 if (isset($_GET['layout_settings'])) {
	
	include("layout_settings.php");
} 
 if (isset($_GET['delete_link'])) {
	include("delete_link.php");
} 
 if (isset($_GET['payment_settings'])) {
	include("payment_settings.php");
} 
 if (isset($_GET['view_proposals'])) {
	include("view_proposals.php");
} 
 if (isset($_GET['proposals_pagination'])) {
	include("proposals_pagination.php");
} 
 if (isset($_GET['view_proposals_active'])) {
	include("view_proposals_active.php");
} 

if (isset($_GET['view_proposals_pending'])) {
	include("view_proposals_pending.php");
} 

if (isset($_GET['view_proposals_paused'])) {
	include("view_proposals_paused.php");
}
if (isset($_GET['view_proposals_trash'])) {
	include("view_proposals_trash.php");
} 
if (isset($_GET['pause_proposal'])) {
	include("pause_proposal.php");
}
if (isset($_GET['move_to_trash'])) {
	include("move_to_trash.php");
} 
if (isset($_GET['approve_proposal'])) {
	include("approve_proposal.php");
}
if (isset($_GET['submit_modification'])) {
	include("submit_modification.php");
} 
if (isset($_GET['submit_modification'])) {
	include("submit_modification.php");
} 
if (isset($_GET['restore_proposal'])) {
	include("restore_proposal.php");
} 
if (isset($_GET['delete_proposal'])) {
	include("delete_proposal.php");
}
if (isset($_GET['inbox_conversations'])) {
	include("inbox_conversations.php");
} 
if (isset($_GET['inbox_conversations_pagination'])) {
	include("inbox_conversations_pagination.php");
} 
if (isset($_GET['single_inbox_message'])) {
	include("single_inbox_message.php");
}
if (isset($_GET['insert_review'])) {
	include("insert_review.php");
}
if (isset($_GET['view_buyer_reviews'])) {
	include("view_buyer_reviews.php");
}
if (isset($_GET['buyer_reviews_pagination'])) {
	include("buyer_reviews_pagination.php");
}
if (isset($_GET['delete_buyer_review'])) {
	include("delete_buyer_review.php");
}
if (isset($_GET['view_seller_reviews'])) {
	include("view_seller_reviews.php");
}
if (isset($_GET['seller_reviews_pagination'])) {
	include("seller_reviews_pagination.php");
}
if (isset($_GET['delete_seller_review'])) {
	include("delete_seller_review.php");
}
if (isset($_GET['buyer_requests'])) {
	include("buyer_requests.php");
}
if (isset($_GET['buyer_requests_pagination'])) {
	include("buyer_requests_pagination.php");
}
if (isset($_GET['approve_request'])) {
	include("approve_request.php");
}
if (isset($_GET['unapprove_request'])) {
	include("unapprove_request.php");
}
if (isset($_GET['insert_cat'])) {
 include("insert_cat.php");
}
if (isset($_GET['view_cats'])) {
 include("view_cats.php");
}
if (isset($_GET['delete_cat'])) {
 include("delete_cat.php");
}
if (isset($_GET['edit_cat'])) {
 include("edit_cat.php");
}

if (isset($_GET['insert_child_cats'])) {
 include("insert_child_cats.php");
}

if (isset($_GET['view_child_cats'])) {
 include("view_child_cats.php");
}
if (isset($_GET['child_cart_pagination'])) {
 include("child_cart_pagination.php");
}
if (isset($_GET['delete_child_cat'])) {
 include("delete_child_cat.php");
}
if (isset($_GET['edit_child_cat'])) {
 include("edit_child_cat.php");
}
if (isset($_GET['insert_delivery_time'])) {
 include("insert_delivery_time.php");
}
if(isset($_GET['view_delivery_times'])) {
 include("view_delivery_times.php");
}
if(isset($_GET['delete_delivery_time'])) {
 include("delete_delivery_time.php");
}
if(isset($_GET['edit_delivery_time'])) {
 include("edit_delivery_time.php");
}
if(isset($_GET['insert_seller_language'])) {
 include("insert_seller_language.php");
}

if(isset($_GET['view_seller_languages'])) {
 include("view_seller_languages.php");
}

if(isset($_GET['delete_seller_language'])) {
 include("delete_seller_language.php");
}
if(isset($_GET['edit_seller_language'])) {
 include("edit_seller_language.php");
}

if(isset($_GET['insert_seller_skill'])) {
 include("insert_seller_skill.php");
}
if(isset($_GET['view_seller_skills'])) {
 include("view_seller_skills.php");
}
if(isset($_GET['delete_seller_skill'])) {
 include("delete_seller_skill.php");
}
if(isset($_GET['edit_seller_skill'])) {
 include("edit_seller_skill.php");
}
if(isset($_GET['customer_support_settings'])) {
 include("customer_support_settings.php");
}

if(isset($_GET['view_support_requests'])) {
 include("view_support_requests.php");
}
if(isset($_GET['support_requests_pagination'])) {
 include("support_requests_pagination.php");
}
if(isset($_GET['single_request'])) {
 include("single_request.php");
}
if(isset($_GET['insert_enquiry_type'])) {
 include("insert_enquiry_type.php");
}
if(isset($_GET['view_enquiry_types'])) {
 include("view_enquiry_types.php");
}
if(isset($_GET['delete_enquiry_type'])) {
 include("delete_enquiry_type.php");
}
if(isset($_GET['edit_enquiry_type'])) {
 include("edit_enquiry_type.php");
}
if(isset($_GET['insert_coupon'])) {
 include("insert_coupon.php");
}

if(isset($_GET['view_coupons'])) {
 include("view_coupons.php");
}
if(isset($_GET['delete_coupon'])) {
 include("delete_coupon.php");
}
if(isset($_GET['edit_coupon'])) {
 include("edit_coupon.php");
}

if(isset($_GET['insert_slide'])) {
 include("insert_slide.php");
}
if(isset($_GET['view_slides'])) {
 include("view_slides.php");
}
if(isset($_GET['delete_slide'])) {
 include("delete_slide.php");
}
if(isset($_GET['edit_slide'])) {
 include("edit_slide.php");
}
if(isset($_GET['insert_term'])) {
 include("insert_term.php");
}
if(isset($_GET['view_terms'])) {
 include("view_terms.php");
}
if(isset($_GET['delete_term'])) {
 include("delete_term.php");
}
if(isset($_GET['edit_term'])) {
 include("edit_term.php");
}
if(isset($_GET['view_sellers'])) {
 include("view_sellers.php");
}
if (isset($_GET['sellers_pagination'])) {
	include("sellers_pagination.php");
}
if (isset($_GET['single_seller'])) {
	include("single_seller.php");
}
if (isset($_GET['seller_login'])) {
	include("seller_login.php");
}
if (isset($_GET['unblock_seller'])) {
	include("unblock_seller.php");
}
if (isset($_GET['ban_seller'])) {
	include("ban_seller.php");
}
if (isset($_GET['view_orders'])) {
	include("view_orders.php");
}
if (isset($_GET['orders_pagination'])) {
	include("orders_pagination.php");
}
if (isset($_GET['filter_orders'])) {
	include("filter_orders.php");
}
if (isset($_GET['single_order'])) {
	include("single_order.php");
}
if (isset($_GET['cancel_order'])) {
	include("cancel_order.php");
}
if (isset($_GET['view_referrals'])) {
	include("view_referrals.php");
}
if (isset($_GET['approval_referral'])) {
	include("approval_referral.php");
} 
 #TRACK 29.10.2019

if (isset($_GET['decline_referral'])) {
	include("decline_referral.php");
}
if (isset($_GET['view_proposals_files'])) {
	include("view_proposals_files.php");
}
if (isset($_GET['proposals_file_pagination'])) {
	include("proposals_file_pagination.php");
}
if (isset($_GET['delete_proposal_file'])) {
	include("delete_proposal_file.php");
}
if (isset($_GET['view_inbox_files'])) {
	include("view_inbox_files.php");
}

if (isset($_GET['inbox_file_pagination'])) {
	include("inbox_file_pagination.php");
}
if (isset($_GET['delete_inbox_file'])) {
	include("delete_inbox_file.php");
}
if (isset($_GET['view_order_files'])) {
	include("view_order_files.php");
}
if (isset($_GET['order_file_pagination'])) {
	include("order_file_pagination.php");
}
if (isset($_GET['delete_order_file'])) {
	include("delete_order_file.php");
}
if (isset($_GET['insert_user'])) {
	include("insert_user.php");
}
if (isset($_GET['view_users'])) {
	include("view_users.php");
}
if (isset($_GET['delete_user'])) {
	include("delete_user.php");
}
if (isset($_GET['user_profile'])) {
	include("user_profile.php");
}
if (isset($_GET['single_proposal'])) {
	include("single_proposal.php");
}
?>
</div>	<!--end container-fluid-->
</div><!--page-wrapper ends-->	
</div><!--wrapper ends-->	

<script src="js/proper.js"></script>
<script src="js/bootstrap.min.js"></script> 	
</body>
</html>