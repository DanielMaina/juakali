<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{
 ?>

<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard /Insert Box
</li>	
</ol>	
</div>	
</div><!--first row ends-->
<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="card-title">
<i class="fa fa-money-bill-alt fa-fw"></i>Insert Box	
</h4>	
</div><!--card-header ends-->	
<div class="card-body" style="background: #cccfcf;">
<form action="" method="POST" enctype="multipart/form-data">
<div class="form-group row">
<label class="col-md-3 control-label">Box Title:</label>
<div class="col-md-6">
<input type="text" name="box_title" class="form-control">
</div>	
</div>	
<div class="form-group row">
<label class="col-md-3 control-label">Box Description:</label>
<div class="col-md-6">
<textarea name="box_desc" class="form-control" cols="6"></textarea>
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label"></label>
<div class="col-md-6">
<input type="submit" name="submit" class="form-control btn btn-success" value="Insert Box">
</div>	
</div>
</form>	
</div>
</div><!--card ends-->	
</div><!--col-lg-12 ends-->	
</div><!--2 row ends-->
<?php 
if (isset($_POST['submit'])) {
$box_title=mysqli_real_escape_string($con,$_POST['box_title']);	
$box_desc=mysqli_real_escape_string($con,$_POST['box_desc']);

$Insert_box="INSERT INTO section_boxes(box_title,box_desc)values('$box_title','$box_desc')";
$run_insert_box=mysqli_query($con,$Insert_box);	
if ($run_insert_box) {
echo "<script>
alert('One Box Has Been Added');
window.open('index.php?general_settings','_self');
</script>";	
}

}
 ?>


 <?php } ?>