<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{
 ?>

<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard / Insert Sub Category	
</li>	
</ol>	
</div>	
</div><!--first row ends-->

<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-money-bill-alt"></i>Insert Sub Category	
</h4>	
</div><!--card-header-->
<div class="card-body" style="background: #cccfcf;">
<form action="" method="POST">

<div class="form-group row">
<label class="col-md-3 control-label">Sub Category Title:</label>
<div class="col-md-6">
<input type="text" name="child_title" class="form-control" value="" required>
</div>	
</div>	

<div class="form-group row">
<label class="col-md-3 control-label">Sub Category Description:</label>
<div class="col-md-6">
<textarea name="child_desc" class="form-control"></textarea>
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label">Select Parent Title:</label>
<div class="col-md-6">
<select name="parent_cat" class="form-control">
<option value="">Select A Parent</option>	
<?php 
$get_cats="SELECT * FROM categories";
$run_cats=mysqli_query($con,$get_cats);
while ($row_cats=mysqli_fetch_array($run_cats)) {
$cat_id=$row_cats['cat_id'];
$cat_title=$row_cats['cat_title'];
echo "<option value='$cat_id'>$cat_title</option>";
}
 ?>
</select>
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label"></label>
<div class="col-md-6">
<input type="submit" name="submit" value="Insert Sub Category" class="btn btn-success form-control">
</div>	
</div>

</form>	
</div><!--card-body ends-->	
</div><!--card ends-->	
</div><!--col-lg-12-->	
</div><!--2 row ends-->
<?php 
if (isset($_POST['submit'])) {
$child_title=mysqli_real_escape_string($con,$_POST['child_title']);	
$child_desc=mysqli_real_escape_string($con,$_POST['child_desc']);	
$parent_cat=mysqli_real_escape_string($con,$_POST['parent_cat']);	

$insert_child_cat="INSERT INTO categories_childs (child_parent_id,child_title,child_desc) values('$parent_cat','$child_title','$child_desc')";
$run_child_cat=mysqli_query($con,$insert_child_cat);
if ($run_child_cat) {
echo "<script>
alert('Ones Sub-Category Has Been Added');
window.open('index.php?view_child_cats','_self');
</script>";	
}
}
 ?>
 <?php } ?>