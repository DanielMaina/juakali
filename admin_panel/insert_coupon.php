<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{
 ?>
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard / Insert Coupon
</li>	
</ol>	
</div>	
</div><!--first row ends-->
<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-money-bill-alt"></i> Insert Coupon	
</h4>	
</div><!--card-header ends--->
<div class="card-body" style="background: #ECEFF1">
<form action="" method="POST">
<div class="form-group row">
<label class="col-md-3 control-label">Coupon Title</label>
<div class="col-md-6">
<input type="text" name="coupon_title" class="form-control" value="" required>
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label">Coupon price</label>
<div class="col-md-6">
<input type="number" name="coupon_price" class="form-control" value="100" min="100" required>
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label">Coupon Code </label>
<div class="col-md-6">
<input type="text" name="coupon_code" class="form-control" value="" required>
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label">Coupon Limit </label>
<div class="col-md-6">
<input type="number" name="coupon_limit" class="form-control" value="1" min="1">
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label">Select Products</label>
<div class="col-md-6">
<select name="proposal_id" class="form-control" required>
<option value="">-------------------Products-----------------------</option>
<?php 
$get_proposals="SELECT * from proposals where proposal_status='active'";
$run_proposals=mysqli_query($con,$get_proposals);
while ($row_proposals =mysqli_fetch_array($run_proposals)) {
$proposal_id=$row_proposals['proposal_id'];
$proposal_title=$row_proposals['proposal_title'];
echo "<option value='$proposal_id'>$proposal_title</option>";
}	
 ?>
</select>	
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label"></label>
<div class="col-md-6">
<input type="submit" name="submit" class="btn btn-success form-control" value="Insert Coupon">
</div>	
</div>	
</form>	
</div><!--card-body ends-->	
</div><!--card ends-->			
</div><!--col-lg-12 ends-->	
</div><!--second row ends-->
<?php 
if (isset($_POST['submit'])) {
$coupon_title=mysqli_real_escape_string($con,$_POST['coupon_title']);	
$coupon_price=mysqli_real_escape_string($con,$_POST['coupon_price']);	
$coupon_code=mysqli_real_escape_string($con,$_POST['coupon_code']);	
$coupon_limit=mysqli_real_escape_string($con,$_POST['coupon_limit']);	
$proposal_id=mysqli_real_escape_string($con,$_POST['proposal_id']);	
$get_coupons="SELECT * from coupon where coupon_code='$coupon_code'";
$run_coupons=mysqli_query($con,$get_coupons);
$check_coupons=mysqli_num_rows($con,$run_coupons);
if ($check_coupons == 1) {
echo "<script>
alert('Coupon Code Already Used!...');
</script>";	
}else{
$insert_coupon="INSERT INTO coupons (proposal_id,coupon_title,coupon_price,coupon_code,coupon_limit) values('$proposal_id','$coupon_title','$coupon_price','$coupon_code','$coupon_limit')";
$run_coupons=mysqli_query($con,$insert_coupon);
if ($run_coupons) {
echo "<script>
alert('One Coupon Code Has Been Added!');
window.open('index.php?view_coupons','_self');
</script>";
}
}
}

 ?>
 <?php } ?> 