<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{
 ?>
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard / Insert Delivery Time	
</li>	
</ol>	
</div>	
</div><!--first row ends-->
<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-money-bill-alt"></i> Insert Delivery Time	
</h4>	
</div><!--card-header-->
<div class="card-body">
<form action="" method="POST">
<div class="form-group row">
<label class="col-md-3 control-label">Delivery Time Title</label>
<div class="col-md-6">
<input type="text" name="delivery_title" class="form-control" required>
<small class="form-text text-muted">This Delivery Title Will Show On Categories, Sub-Categories And Search Page</small>
</div>	
</div><!--form-group row ends-->

<div class="form-group row">
<label class="col-md-3 control-label">Product Delivery Time Title</label>
<div class="col-md-6">
<input type="text" name="delivery_proposal_title" class="form-control" required>
<small class="form-text text-muted">This Delivery Title Will Show On Categorieson products related pages</small>
</div>	
</div><!--form-group row ends-->

<div class="form-group row">
<label class="col-md-3 control-label"></label>
<div class="col-md-6">
<input type="submit" name="submit" class="btn btn-primary form-control" value="Insert Delivery Time">
</div>	
</div><!--form-group row ends-->

</form>	
</div><!--card-body-->	
</div>	<!--card end-->
</div><!--col-lg-12 ends-->	
</div><!--second row ends-->
<?php 
if (isset($_POST['submit'])) {
$delivery_title=mysqli_real_escape_string($con,$_POST['delivery_title']);
$delivery_proposal_title=mysqli_real_escape_string($con,$_POST['delivery_proposal_title']);
$insert_query="INSERT INTO delivery_times(delivery_title,delivery_proposal_title) values('$delivery_title','$delivery_proposal_title')";
$run_delivery_time=mysqli_query($con,$insert_query);
if ($run_delivery_time) {
	echo "<script>
alert('One Delivery Time Has Been Added');
window.open('index.php?view_delivery_times','_self')
	</script>";
	}	
}
 ?>
<?php } ?>