<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{
 ?>
 <script src="js/jquery.barrating.min.js"></script>
 <link rel="stylesheet" href="css/css-stars.css">
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard / Insert reviews	
</li>	
</ol>	
</div>	
</div><!--first row ends-->
<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-money-bill-alt"></i>Insert Reviews!	
</h4>	
</div><!--card-header ends-->	
<div class="card-body">
<form action="" method="POST">
<div class="form-group row">
<label class="col-md-3 control-label">Select Review Product</label>	
<div class="col-md-6">
<select name="proposal_id" class="form-control" required>
<option value="">-------------------Product----------</option>
<?php 
$get_proposals="SELECT * from proposals where proposal_status='active'";
$run_proposals=mysqli_query($con,$get_proposals);
while ($row_proposals =mysqli_fetch_array($run_proposals)) {
$proposal_id=$row_proposals['proposal_id'];
$proposal_title=$row_proposals['proposal_title'];
echo "<option value='$proposal_id'>$proposal_title</option>";
}	
 ?>
</select>	
</div>
</div><!--form-group row-->	

<div class="form-group row">
<label class="col-md-3 control-label">Select Review Seller</label>	
<div class="col-md-6">
<select name="seller_id" class="form-control" required>
<option value="">---------------------Seller-----------</option>
<?php 
$get_sellers="SELECT * from sellers where NOT seller_status='deactivated' AND NOT seller_status='block-ban'";
$run_sellers=mysqli_query($con,$get_sellers);
while ($row_sellers=mysqli_fetch_array($run_sellers)) {
$seller_id=$row_sellers['seller_id'];	
$seller_user_name=$row_sellers['seller_user_name'];
echo "<option value='$seller_id'>$seller_user_name</option>";	
}
 ?>
</select>	
</div>
</div><!--form-group row-->	
<div class="form-group row">
<label class="col-md-3 control-label">Select Review Rating </label>	
<div class="col-md-6">
<select name="review_rating" id="rating">
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option>
</select>	
<script>
$("#rating").barrating({
	theme: 'css-stars'
});	
</script>
</div>
</div><!--form-group row-->	
<div class="form-group row">
<label class="col-md-3 control-label">Review Comment</label>	
<div class="col-md-6">
<textarea class="form-control" name="review_comment" placeholder="Enter Your Review Comments" rows="6"></textarea>
</div>
</div><!--form-group row-->	
<div class="form-group row">
<label class="col-md-3 control-label"></label>	
<div class="col-md-6">
<input type="submit" name="insert_review" value="Insert Rview" class="btn btn-success form-control">
</div>
</div><!--form-group row-->	
</form>	
</div>
</div><!--card ends-->	
</div><!--col-lg-12 ends-->	
</div><!--ends of 2 row-->
<?php 
if (isset($_POST['insert_review'])) {
	$proposal_id=mysqli_real_escape_string($con,$_POST['proposal_id']);
	$review_seller_id=mysqli_real_escape_string($con,$_POST['seller_id']);
	$review_rating=mysqli_real_escape_string($con,$_POST['review_rating']);
	$review_comment=mysqli_real_escape_string($con,$_POST['review_comment']);
	$date=date("M d Y");

$get_proposals="SELECT * from proposals where proposal_id='$proposal_id'";
$run_proposals=mysqli_query($con,$get_proposals);
$row_proposals=mysqli_fetch_array($run_proposals);
$proposal_seller_id=$row_proposals['proposal_seller_id'];

 $get_sellers="SELECT * from sellers where seller_id=$proposal_seller_id";
$run_sellers=mysqli_query($con,$get_sellers);
$row_sellers=mysqli_fetch_array($run_sellers); 
$proposal_seller_rating=$row_sellers['seller_rating'];

$insert_review="INSERT INTO buyer_reviews(proposal_id,order_id,review_buyer_id,buyer_rating,buyer_review,review_seller_id,review_date) values('$proposal_id','','$review_seller_id','$review_rating','$review_comment','$proposal_seller_id','$date')";
$run_review=mysqli_query($con,$insert_review);

$ratings=array();
$sel_proposal_reviews="SELECT * from buyer_reviews where proposal_id='$proposal_id'";
$run_poposals_reviews=mysqli_query($con,$sel_proposal_reviews);
while($row_proposals_reviews=mysqli_fetch_array($run_poposals_reviews)) {
$proposal_buyer_rating=$row_proposals_reviews['buyer_rating'];
array_push($ratings,$proposal_buyer_rating);
}
array_push($ratings,$review_rating);	
$total=array_sum($ratings);
$avg=$total / count($ratings);

$update_proposal_rating =substr($avg,0,1);

if ($review_rating=="5") {
if ($proposal_seller_rating=="100") {
}else{
$update_seller_rating="UPDATE sellers set seller_rating= seller_rating + 7 where seller_id='$proposal_seller_id'";
$run_update_seller_rating=mysqli_query($con,$update_seller_rating);
}

}elseif ($review_rating =="4") {

if ($proposal_seller_rating=="100") {
}else{
$update_seller_rating="UPDATE sellers set seller_rating= seller_rating + 2 where seller_id='$proposal_seller_id'";
$run_update_seller_rating=mysqli_query($con,$update_seller_rating);
}	

}elseif ($review_rating=="3") {
$update_seller_rating="UPDATE sellers set seller_rating= seller_rating -3 where seller_id='$proposal_seller_id'";
$run_update_seller_rating=mysqli_query($con,$update_seller_rating);

}elseif ($review_rating=="2") {
$update_seller_rating="UPDATE sellers set seller_rating= seller_rating -5 where seller_id='$proposal_seller_id'";
$run_update_seller_rating=mysqli_query($con,$update_seller_rating);
}elseif ($review_rating== "1"){

$update_seller_rating="UPDATE sellers set seller_rating= seller_rating - 7 where seller_id='$proposal_seller_id'";
$run_update_seller_rating=mysqli_query($con,$update_seller_rating);
	
}

$update_proposal="UPDATE proposals set proposal_rating='$update_proposal_rating' where proposal_id='$proposal_id'";
$run_update_proposal=mysqli_query($con,$update_proposal);
if ($run_update_proposal) {
	echo "<script>
alert('Your Review Has Been Added');
window.open('index.php?view_buyer_reviews','_self');
	</script>";
}


}


 ?>

<?php } ?> 