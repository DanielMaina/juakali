<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{

 ?>
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard / Insert Seller Task Execution/Performance		
</li>	
</ol>	
</div>	
</div><!--first row ends-->
<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-money-bill-alt"></i> Insert Seller Task Execution/Performance	
</h4>	
</div><!--card-header ends-->
<div class="card-body">
<form action="" method="POST">
<div class="form-group row">
<label class="col-md-3 control-label">Seller Performance Title</label>
<div class="col-md-6">
<input type="text" name="language_title" class="form-control" value="" required>
</div>	
</div>	
<div class="form-group row">
<label class="col-md-3 control-label"></label>
<div class="col-md-6">
<input type="submit" name="submit" class=" btn btn-primary form-control" value="Insert Seller Performance">
</div>	
</div>	
</form>	
</div><!--card-body ends-->	
</div><!--card ends-->	
</div><!--col-lg-12 ends-->	
</div><!--first row ends-->
<?php 
if (isset($_POST['submit'])) {
$language_title=mysqli_real_escape_string($con,$_POST['language_title']);
$insert_sellers_laguages ="INSERT INTO seller_languages(language_title) values('$language_title')";	
$run_seller_language=mysqli_query($con,$insert_sellers_laguages);
if ($run_seller_language) {
echo "<script>
alert('One Seller Performance Level Has Been Added');
window.open('index.php?view_seller_languages','_self');
</script>";
}
}
?>
 
<?php } ?>