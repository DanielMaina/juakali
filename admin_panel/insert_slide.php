<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{
 ?>
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard / Insert Slide	
</li>	
</ol>	
</div>	
</div><!--first row ends-->
<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-money-bill-alt"></i> Insert Slide
</h4>	
</div><!--card-header ends-->
<div class="card-body" style="background: #ECEFF1">
<form action="" method="POST" enctype="multipart/form-data">

<div class="form-group row">
<label class="col-md-3 control-label">Slide Name</label>
<div class="col-md-6">
<input type="text" name="slide_name" class="form-control" value="" required>
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label">Slide Description</label>
<div class="col-md-6">
<textarea name="slide_desc" class="form-control" required></textarea>
</div>	
</div>	
<div class="form-group row">
<label class="col-md-3 control-label">Slide Image</label>
<div class="col-md-6">
<input type="file" name="slide_image" class="form-control"  required>
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label">Slide URL</label>
<div class="col-md-6">
<input type="text" name="slide_url" class="form-control" required>
</div>	
</div>	
<div class="form-group row">
<label class="col-md-3 control-label"></label>
<div class="col-md-6">
<input type="submit" name="submit" class="btn btn-primary form-control" value="Insert Slide">
</div>	
</div>				
</form>	
</div><!--card body ends-->
</div><!--card ends-->	
</div><!--col-lg-12 ends-->	
</div><!--2 row ends-->

<?php 
if (isset($_POST['submit'])) {
$slide_name=mysqli_real_escape_string($con,$_POST['slide_name']);	
$slide_desc=mysqli_real_escape_string($con,$_POST['slide_desc']);	
$slide_url=mysqli_real_escape_string($con,$_POST['slide_url']);

$slide_image=$_FILES['slide_image']['name'];	
$tmp_slide_image=$_FILES['slide_image']['tmp_name'];	
move_uploaded_file($tmp_slide_image, "../slides_images/$slide_image");
$insert_slide="INSERT INTO slider(slide_name,slide_desc,slide_image,slide_url)values('$slide_name','$slide_desc','$slide_image','$slide_url')";
$run_slide=mysqli_query($con,$insert_slide);
if ($run_slide) {
echo "<script>
alert('One slide has been Added');
window.open('index.php?view_slides','_self');
</script>";
}
}

 ?>

<?php } ?> 