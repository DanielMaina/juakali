<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{

 ?>
 <!-- for the textarea customized UI tinymce -->
	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
  <script>tinymce.init({ selector:'textarea'});</script>
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard / Insert Terms	
</li>	
</ol>	
</div>	
</div><!--first row ends-->

<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-money-bill-alt"></i> Insert Terms	
</h4>	
</div><!--card-header ends-->
<div class="card-body" style="background: #ECEFF1;">
<form action="" method="POST">
<div class="form-group row">
<label class="col-md-3 control-label">Term Title</label>
<div class="col-md-6">
<input type="text" name="term_title" class="form-control" value="" required>
</div>	
</div>	
<div class="form-group row">
<label class="col-md-3 control-label">Term Description:</label>
<div class="col-md-6">
<textarea class="form-control" name="term_desc" rows="7"></textarea>
</div>	
</div>	
<div class="form-group row">
<label class="col-md-3 control-label">Term Link </label>
<div class="col-md-6">
<input type="text" name="term_link" class="form-control" value="" required>
</div>	
</div>	
<div class="form-group row">
<label class="col-md-3 control-label"></label>
<div class="col-md-6">
<input type="submit" name="submit" class="btn btn-primary form-control" value="Insert Term">
</div>	
</div>		 
</form>	
</div><!--card-body ends-->	
</div><!--card end -->	
</div><!--col-lg-12 ends--->	
</div><!--2 row ends-->
<?php 
if (isset($_POST['submit'])) {
$term_title=mysqli_real_escape_string($con,$_POST['term_title']);
$term_desc=mysqli_real_escape_string($con,$_POST['term_desc']);
$term_link=mysqli_real_escape_string($con,$_POST['term_link']);
$insert_term="INSERT INTO terms(term_title, term_link,term_description) values('$term_title','$term_link','$term_desc')";
$run_term=mysqli_query($con,$insert_term);
if ($run_term) {
echo "<script>
alert('One Term Has Been Insert!');
window.open('index.php?view_terms','_self');
</script>";
}
}
 ?>
 <?php } ?>