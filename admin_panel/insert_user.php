<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{
 ?>

<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard / Add User	
</li>	
</ol>	
</div>	
</div><!--first row ends-->
<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-money-bill-alt"></i> Add User	
</h4>	
</div><!--card-heder ends-->
<div class="card-body" style="background: #ECEFF1">
<form action="" method="POST" enctype="multipart/form-data">
<div class="form-group row">
<label class="col-md-3 control-label">User Name</label>
<div class="col-md-6">
<input type="text" name="admin_name" class="form-control" value="" required>
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label">User Email</label>
<div class="col-md-6">
<input type="email" name="admin_email" class="form-control" value="" required>
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label">User Country</label>
<div class="col-md-6">
<input type="text" name="admin_country" class="form-control" required value="">
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label">User Job</label>
<div class="col-md-6">
<input type="text" name="admin_job" class="form-control" value="" required>
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label">User Contact</label>
<div class="col-md-6">
<input type="text" name="admin_contact" class="form-control" value="" required>
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label">User About</label>
<div class="col-md-6">
<textarea name="admin_about" class="form-control" rows="3"></textarea>
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label">User Image</label>
<div class="col-md-6">
<input type="file" name="admin_image" class="form-control" value="" required>
</div>	
</div>
<hr class="card-hr">
<h4 class="h4 mb-3">Account Password</h4>
<div class="form-group row">
<label class="col-md-3 control-label">User Password</label>
<div class="col-md-6 password-strength-checker">
<div class="input-group">
<span class="input-group-prepend">
<span class="input-group-text"><i class="fa fa-check tick1 text-success"></i><i class="fa fa-times cross1 text-danger"></i></span>	
</span>
<input type="password" name="admin_pass" id="password" class="form-control" required>
<span class="input-group-append"><div id="meter_wrapper" class="input-group-text"><span id="pass_type"></span><div id="meter"></div></div></span>	
</div><!--input-group ends-->
</div>	
</div>
<div class="form-group row">
<label class="col-md-3 control-label">Confirm User Password</label>
<div class="col-md-6">
<div class="input-group">
<span class="input-group-prepend">
<span class="input-group-text"><i class="fa fa-check tick2 text-success"></i><i class="fa fa-times cross2 text-danger"></i></span>	
</span>
<input type="password" name="confirm_admin_pass" id="confirm_password" class="form-control" required>	
</div><!--input-group ends-->
</div>	
</div>
<hr class="card-hr">
<div class="form-group row">
<label class="col-md-3 control-label"></label>
<div class="col-md-6">
<input type="submit" name="submit" class=" btn btn-primary form-control" value="Add New User!">
</div>	
</div>	
</form>
</div><!--card-body end-->	
</div><!---card ends-->	
</div><!--col-lg-12 ends -->	
</div><!--2 row ends-->
<?php 

if (isset($_POST['submit'])) {
$admin_name=mysqli_real_escape_string($con,$_POST['admin_name']);
$admin_email=mysqli_real_escape_string($con,$_POST['admin_email']);
$admin_country=mysqli_real_escape_string($con,$_POST['admin_country']);
$admin_job=mysqli_real_escape_string($con,$_POST['admin_job']);
$admin_about=mysqli_real_escape_string($con,$_POST['admin_about']);
$admin_pass=mysqli_real_escape_string($con,$_POST['admin_pass']);
$confirm_admin_pass=mysqli_real_escape_string($con,$_POST['confirm_admin_pass']);
$admin_name=mysqli_real_escape_string($con,$_POST['admin_name']);

$admin_image=$_FILES['admin_image']['name'];
$tmp_admin_image=$_FILES['admin_image']['tmp_name'];

$get_email="SELECT * from admins where admin_email='$admin_email'";
$run_email=mysqli_query($con,$get_email);
$count_email=mysqli_num_rows($run_email);
if ($count_email > 0) {
echo "<script>alert('This Email Alreay Exits');</script>";	
}else{
if ($admin_pass !==$confirm_admin_pass) {	
echo "<script>alert('Your Password Does NOT! match Re-Try');</script>";	
}else{
$enctype_password=password_hash($admin_pass, PASSWORD_DEFAULT);
move_uploaded_file($tmp_admin_image, "admin_images/$admin_image");
$insert_admin="INSERT INTO admins(admin_name,admin_email,admin_pass,admin_image,admin_contact,admin_country,admin_job,admin_about) values('$admin_name','$admin_email','$enctype_password','$admin_image','$admin_contact','$admin_country','$admin_job','$admin_about')";
$run_admin=mysqli_query($con,$insert_admin);
if ($run_admin) {
echo "<script>
alert('One Admin Has Been Added Successfully');
window.open('index.php?view_users','_self');
</script>";	
}
}
}

}

 ?>

<script>

$(document).ready(function(){
$("#password").keyup(function(){
check_pass();
});
});

function check_pass() {
var val = document.getElementById("password").value;
var meter = document.getElementById("meter");
var no=0;
if(val!=""){
// If the password length is less than or equal to 10
if(val.length<=10)no=1;
// If the password length is greater than 10 and contain any lowercase alphabet or any number or any special character
if(val.length>10 && (val.match(/[a-z]/) || val.match(/\d+/) || val.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)))no=2;
// If the password length is greater than 10 and contain alphabet,number,special character respectively
if(val.length>10 && ((val.match(/[a-z]/) && val.match(/\d+/)) || (val.match(/\d+/) && val.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)) || (val.match(/[a-z]/) && val.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/))))no=3;
// If the password length is greater than 10 and must contain alphabets,numbers and special characters
if(val.length>10 && val.match(/[a-z]/) && val.match(/\d+/) && val.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/))no=4;
if(no==1){
$("#meter").animate({width:'50px'},300);
meter.style.backgroundColor="red";
document.getElementById("pass_type").innerHTML="Very Weak";
}
if(no==2){
$("#meter").animate({width:'100px'},300);
meter.style.backgroundColor="#F5BCA9";
document.getElementById("pass_type").innerHTML="Weak";
}
if(no==3){
$("#meter").animate({width:'150px'},300);
meter.style.backgroundColor="#FF8000";
document.getElementById("pass_type").innerHTML="Good";
}
if(no==4){
$("#meter").animate({width:'200px'},300);
meter.style.backgroundColor="#00FF40";
document.getElementById("pass_type").innerHTML="Strong";
}
}
else{
meter.style.backgroundColor="";
document.getElementById("pass_type").innerHTML="";
}
}

</script>


<script>
$(document).ready(function(){
$('.tick1').hide();
$('.cross1').hide();
$('.tick2').hide();
$('.cross2').hide();
$('#confirm_password').focusout(function(){
var password = $('#password').val();
var confirmPassword = $('#confirm_password').val();
if(password == confirmPassword){
$('.tick1').show();
$('.cross1').hide();
$('.tick2').show();
$('.cross2').hide();
}
else{
$('.tick1').hide();
$('.cross1').show();
$('.tick2').hide();
$('.cross2').show();
}
});
});
</script

 <?php } ?>