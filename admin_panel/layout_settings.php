<?php
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{

?>
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard /Layout Settings	
</li>	
</ol>	
</div>	
</div><!--first row ends-->

<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">Footer Layout Settings</h4>	
</div><!--card-header ends-->
<div class="card-body row">
<div class="col-md-3 border-right">
<ul class="nav nav-pills flex-column">
<li class="nav-item">
<a href="#categories" class="nav-link active" data-toggle="pill">Categories Column</a>	
</li>
<li class="nav-item">
<a href="#about" class="nav-link" data-toggle="pill">About Column</a>	
</li>
<li class="nav-item">
<a href="#support" class="nav-link " data-toggle="pill">Support Column</a>	
</li>
<li class="nav-item">
<a href="#follow" class="nav-link " data-toggle="pill">Follow Us Column</a>	
</li>	
</ul>	
</div><!--col-md-3 border-right ends-->	
<div class="col-md-9">
<div class="tab-content">
<div id="categories" class="tab-pane fade show active">
<?php 
$get_footer_links="SELECT * from footer_links where link_section='categories'";
$run_footer_links=mysqli_query($con,$get_footer_links);
while ($row_footer_links=mysqli_fetch_array($run_footer_links)) {
$link_id=$row_footer_links['link_id'];	
$link_title=$row_footer_links['link_title'];	
$link_section=$row_footer_links['link_section'];	

?>	
<div class="mb-2">
<?php echo $link_title; ?>--<span><?php echo ucwords($link_section); ?></span>
<a href="index.php?delete_link=<?php echo $link_id; ?>" onclick="return confirm('Do you Realy want To Delete This Category permanently?')"><i class="fa fa-trash-alt text-danger"></i></a>	
</div>
<?php } ?>
<div class="bg-light p-3">
<h4>Add New Link</h4>
<form class="form-inline" method="POST" >
<input class="form-control mb-3 mr-sm-2 mb-sm-0" type="text" name="link_title" placeholder="Link Title">
<input class="form-control mb-3 mr-sm-2 mb-sm-0" type="text" name="link_url" placeholder="Link Url">
<button type="submit" class="btn btn-success form-control" name="add_link_category">Add new Link</button>	
</form>	
</div><!--bg-light p-3 ends-->
<?php 
if (isset($_POST['add_link_category'])) {
$link_title=mysqli_real_escape_string($con,$_POST['link_title']);	
$link_url=mysqli_real_escape_string($con,$_POST['link_url']);
$insert_link="INSERT INTO footer_links(link_title,link_url,link_section) values('$link_title','$link_url','categories')";
$run_link=mysqli_query($con,$insert_link);
if ($run_link) {
echo "<script>window.open('index.php?layout_settings','_self');</script>";	
}

}
 ?>
</div><!--tab-pane fade show active ends-->	

<div id="about" class="tab-pane fade in">
<?php 
$get_footer_links="SELECT * from footer_links where link_section='about'";
$run_footer_links=mysqli_query($con,$get_footer_links);
while ($row_footer_links=mysqli_fetch_array($run_footer_links)) {
$link_id=$row_footer_links['link_id'];	
$link_title=$row_footer_links['link_title'];	
$link_section=$row_footer_links['link_section'];	

?>	
<div class="mb-2">
<?php echo $link_title; ?>--<span><?php echo ucwords($link_section); ?></span>
<a href="index.php?delete_link=<?php echo $link_id; ?>" onclick="return confirm('Do you Realy want To Delete This About permanently?')"><i class="fa fa-trash-alt text-danger"></i></a>	
</div>
<?php } ?>
<div class="bg-light p-3">
<h4>Add New Link</h4>
<form class="form-inline" method="POST" >
<input class="form-control mb-3 mr-sm-2 mb-sm-0" type="text" name="link_title" placeholder="Link Title">
<input class="form-control mb-3 mr-sm-2 mb-sm-0" type="text" name="link_url" placeholder="Link Url">
<button type="submit" class="btn btn-success form-control" name="add_link_about">Add new Link</button>	
</form>	
</div><!--bg-light p-3 ends-->
<?php 
if (isset($_POST['add_link_about'])) {
$link_title=mysqli_real_escape_string($con,$_POST['link_title']);	
$link_url=mysqli_real_escape_string($con,$_POST['link_url']);
$insert_link="INSERT INTO footer_links(link_title,link_url,link_section) values('$link_title','$link_url','about')";
$run_link=mysqli_query($con,$insert_link);
if ($run_link) {
echo "<script>window.open('index.php?layout_settings','_self');</script>";	
}

}
 ?>
</div><!--tab-pane fade in ends-->

<div id="support" class="tab-pane fade in">
<?php 
$get_footer_links="SELECT * from footer_links where link_section='support'";
$run_footer_links=mysqli_query($con,$get_footer_links);
while ($row_footer_links=mysqli_fetch_array($run_footer_links)) {
$link_id=$row_footer_links['link_id'];	
$link_title=$row_footer_links['link_title'];	
$link_section=$row_footer_links['link_section'];	

?>	
<div class="mb-2">
<?php echo $link_title; ?>--<span><?php echo ucwords($link_section); ?></span>
<a href="index.php?delete_link=<?php echo $link_id; ?>" onclick="return confirm('Do you Realy want To Delete This Support permanently?')"><i class="fa fa-trash-alt text-danger"></i></a>	
</div>
<?php } ?>
<div class="bg-light p-3">
<h4>Add New Link</h4>
<form class="form-inline" method="POST" >
<input class="form-control mb-3 mr-sm-2 mb-sm-0" type="text" name="link_title" placeholder="Link Title">
<input class="form-control mb-3 mr-sm-2 mb-sm-0" type="text" name="link_url" placeholder="Link Url">
<button type="submit" class="btn btn-success form-control" name="add_link_support">Add new Link</button>	
</form>	
</div><!--bg-light p-3 ends-->
<?php 
if (isset($_POST['add_link_support'])) {
$link_title=mysqli_real_escape_string($con,$_POST['link_title']);	
$link_url=mysqli_real_escape_string($con,$_POST['link_url']);
$insert_link="INSERT INTO footer_links(link_title,link_url,link_section) values('$link_title','$link_url','support')";
$run_link=mysqli_query($con,$insert_link);
if ($run_link) {
echo "<script>window.open('index.php?layout_settings','_self');</script>";	
}

}
 ?>
</div><!--tab-pane fade in ends-->

<div id="follow" class="tab-pane fade in">
<?php 
$get_footer_links="SELECT * from footer_links where link_section='follow'";
$run_footer_links=mysqli_query($con,$get_footer_links);
while ($row_footer_links=mysqli_fetch_array($run_footer_links)) {
$link_id=$row_footer_links['link_id'];	
$link_title=$row_footer_links['link_title'];	
$link_section=$row_footer_links['link_section'];	

?>	
<div class="mb-2">
<?php echo $link_title; ?>--<span><?php echo ucwords($link_section); ?></span>
<a href="index.php?delete_link=<?php echo $link_id; ?>" onclick="return confirm('Do you Realy want To Delete This Follow permanently?')"><i class="fa fa-trash-alt text-danger"></i></a>	
</div>
<?php } ?>
<div class="bg-light p-3">
<h4>Add New Link</h4>
<form class="form-inline" method="POST" >
<input class="form-control mb-3 mr-sm-2 mb-sm-0" type="text" name="link_title" placeholder="Link Title">
<input class="form-control mb-3 mr-sm-2 mb-sm-0" type="text" name="link_url" placeholder="Link Url">
<button type="submit" class="btn btn-success form-control" name="add_link_follow">Add new Link</button>	
</form>	
</div><!--bg-light p-3 ends-->
<?php 
if (isset($_POST['add_link_follow'])) {
$link_title=mysqli_real_escape_string($con,$_POST['link_title']);	
$link_url=mysqli_real_escape_string($con,$_POST['link_url']);
$insert_link="INSERT INTO footer_links(link_title,link_url,link_section) values('$link_title','$link_url','follow')";
$run_link=mysqli_query($con,$insert_link);
if ($run_link) {
echo "<script>window.open('index.php?layout_settings','_self');</script>";	
}

}
 ?>
</div><!--tab-pane fade in ends-->


</div>	
</div><!--col-md-9 ends-->
</div><!--card-body row ends-->	
</div><!--card-ends-->	
</div><!--col-lg-12 ends-->	
</div><!--row 2 ends-->

<?php 
$css_file="../styles/custom.css";
if (file_exists($css_file)) {
$css_file_data=file_get_contents($css_file);

}
 ?>
<div class="row mt-4">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">Custom Css</h4>	
</div><!--card-header ends-->
<div class="card-body">
<p class="lead">Enter Your Custome Modification Css Style</p>
<form action="" method="POST">
<div class="form-group row">
<div class="col-md-12">
<textarea name="custome_css" class="form-control" rows="20"><?php echo($css_file_data); ?></textarea>	
</div><!--ends of col-md-12-->	
</div><!--form-group row ends-->
<div class="form-group row">
<label class="col-md-3"></label>
<div class="col-md-9">
<input type="submit" name="save_changes" value="Save Changes" class="btn btn-success float-right">	
</div>	
</div><!--form-group row ends-->	
</form>	
</div><!--card-body ends-->	
</div><!--card ends-->	
</div>	
</div><!--ends of row 3-->
<?php 
if (isset($_POST['save_changes'])) {
$newData=$_POST['custome_css'];
$handle=fopen($css_file, "w");
fwrite($handle, $newData);
fclose($handle);
echo "<script>
alert('Custom Css Has Been Updated Successfully');
window.open('index.php?layout_settings','_self');
</script>";
}
 ?>
<?php }?>
