<?php 
session_start();
include("includes/db.php");
if (isset($_SESSION['admin_email'])) {
echo "<script>window.open('index.php?dashboard','_self');</script>";	
}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Adm||LogIn</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100" rel="stylesheet" >
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/login.css">
<link rel="stylesheet" href="font_awesome/css/fontawesome-all.css">
<script src="js/jquery.slim.min.js"></script>
<script src="js/proper.js"></script>
<script src="js/bootstrap.min.js"></script>
</head>
<body style="background-color:#000;">
<div class="container-fluid body-login">		
<div class="container-login">
<?php if (isset($_GET['session_expired'])) { ?>
<div class="alert alert-danger alert-dismissible fade show" style="margin-top: -100px;">
<button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>	
<strong>You Session Expired,</strong> please Login Again!
</div><!--alert alert-danger alert-dismissible fade show ends-->
<?php } ?>		
<div class="form">
<h2>Login To Jukali Mall Admin</h2>
<form class="form-login" method="post">
<div class="inputBox">
<input type="text" name="admin_email" placeholder="Email Address">
</div>
<div class="inputBox">
<input type="password" name="admin_pass" placeholder="Password">
</div>
<div class="inputBox">
<input type="submit" name="admin_login" value="LogIn">
</div>
</form>
</div>
</div>
</div><!--end of container-->	
</body>
</html>
<?php 
if (isset($_POST['admin_login'])) {
$admin_email=mysqli_real_escape_string($con,$_POST['admin_email']);	
$admin_pass=mysqli_real_escape_string($con,$_POST['admin_pass']);	
$select_admins="SELECT * from admins where admin_email='$admin_email'";
$run_admins=mysqli_query($con,$select_admins);
$row_admins=mysqli_fetch_array($run_admins);
$hash_password=$row_admins['admin_pass'];
$decrypt_password=password_verify($admin_pass,$hash_password);
if ($decrypt_password == 0) {
echo "<script>alert('Email Or Password is Wrong, Please Re-Try!');</script>";	
}else{

$get_admin="SELECT * from admins where admin_email='$admin_email' AND admin_pass='$hash_password'";
$run_admin=mysqli_query($con,$get_admin);
if ($run_admin) {
$_SESSION['admin_email']=$admin_email;
$_SESSION['loggedin_time']=time();
echo "<script>alert('You Are LoggedIn To Admin Panel');</script>";
echo "<script>window.open('index.php?dashboard','_self');</script>";
}

}
}
 ?>