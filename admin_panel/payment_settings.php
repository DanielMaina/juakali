<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{
$get_payment_settings="SELECT * from payment_settings";
$run_payment_settings=mysqli_query($con,$get_payment_settings);
$row_payment_settings=mysqli_fetch_array($run_payment_settings);
//for the general payment settings
$comission_percentage=$row_payment_settings['commission_percentage'];
$days_before_withdraw=$row_payment_settings['days_before_withdraw'];
$withdrawal_limit=$row_payment_settings['withdrawal_limit'];
$featured_fee=$row_payment_settings['featured_fee'];
$featured_duration=$row_payment_settings['featured_duration'];
$processing_fee=$row_payment_settings['processing_fee'];
//for paypal setting code
$enable_paypal=$row_payment_settings['enable_paypal'];
$paypal_email=$row_payment_settings['paypal_email'];
$paypal_currency_code=$row_payment_settings['paypal_currency_code'];
$paypal_api_username=$row_payment_settings['paypal_api_username'];
$paypal_api_password=$row_payment_settings['paypal_api_password'];
$paypal_api_signature=$row_payment_settings['paypal_api_signature'];
$paypal_app_id=$row_payment_settings['paypal_app_id'];
$paypal_sandbox=$row_payment_settings['paypal_sandbox'];
//stripe setting codes
$enable_stripe=$row_payment_settings['enable_stripe'];
$stripe_secret_key=$row_payment_settings['stripe_secret_key'];
$stripe_publishable_key=$row_payment_settings['stripe_publishable_key'];
$stripe_currency_code=$row_payment_settings['stripe_currency_code'];

 ?>

<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard /Payment Settings	
</li>	
</ol>	
</div>	
</div><!--first row ends-->
<div class="row">
<div class="col-lg-12">
<div class="card mb-5">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-money-bill-alt fa-fw"></i>Update General Payment Settings	
</h4>	
</div><!--card-header ends-->
<div class="card-body bg-light">
<form method="POST">
<div class="form-group row">
<label class="col-md-3 control-label">Order Commission Percentage:</label>
<div class="col-md-6">
<div class="input-group">	
<input type="number" name="comission_percentage" class="form-control" min="10" placeholder="10 Minimum" value="<?php echo($comission_percentage);?>">
<span class="input-group-append">
	<span class="input-group-text">%</span>
</span>
</div><!--input-group ends-->
<small class="form-text text-muted">
PERCENTANGE COMMISSION TO BE TAKEN FROM EACH ORDER PURCHASED!	
</small>
</div><!--col-md-6 ends-->	
</div><!--form-group row ends-->
<div class="form-group row">
<label class="col-md-3 control-label">Days Before Available For Withdrawal:</label>
<div class="col-md-6">
<div class="input-group">	
<input type="number" name="days_before_withdraw" class="form-control" min="1" placeholder="1 Minimum" value="<?php echo($days_before_withdraw);?>">
<span class="input-group-append">
	<span class="input-group-text">Days</span>
</span>
</div><!--input-group ends-->
<small class="form-text text-muted">
Number of Days Before Revenue Is Available For Withdrawal	
</small>
</div><!--col-md-6 ends-->	
</div><!--form-group row ends-->
<div class="form-group row">
<label class="col-md-3 control-label">Minimum Withdrawal Limit:</label>
<div class="col-md-6">
<div class="input-group">
<span class="input-group-append">
	<span class="input-group-text">Kshs</span>
</span>	
<input type="number" name="withdrawal_limit" class="form-control" min="100" placeholder="100 Minimum" value="<?php echo($withdrawal_limit);?>">
</div><!--input-group ends-->
<small class="form-text text-muted">
Minimum Available Balance Seller Should have to reqest for withdrawal	
</small>
</div><!--col-md-6 ends-->	
</div><!--form-group row ends-->
<div class="form-group row">
<label class="col-md-3 control-label">Featured Product Listing Fee:</label>
<div class="col-md-6">
<div class="input-group">
<span class="input-group-append">
	<span class="input-group-text">Ksh</span>
</span>	
<input type="number" name="featured_fee" class="form-control" min="10" placeholder="10 Minimum" value="<?php echo($featured_fee);?>">
</div><!--input-group ends-->
<small class="form-text text-muted">
Amount To Be Charged For The Featured Product	
</small>
</div><!--col-md-6 ends-->	
</div><!--form-group row ends-->
<div class="form-group row">
<label class="col-md-3 control-label">Featured Product Listing Duration:</label>
<div class="col-md-6">
<div class="input-group">	
<input type="number" name="featured_duration" class="form-control" min="1" placeholder="1 Minimum" value="<?php echo($featured_duration);?>">
<span class="input-group-append">
	<span class="input-group-text">Days</span>
</span>
</div><!--input-group ends-->
<small class="form-text text-muted">
Number Of Days Each Product is Featured 	
</small>
</div><!--col-md-6 ends-->	
</div><!--form-group row ends-->
<div class="form-group row">
<label class="col-md-3 control-label">Processing Fee:</label>
<div class="col-md-6">
<div class="input-group">
<span class="input-group-append">
	<span class="input-group-text">Ksh</span>
</span>	
<input type="number" name="processing_fee" class="form-control" min="100" placeholder="100 Minimum" value="<?php echo($processing_fee);?>">
</div><!--input-group ends-->
<small class="form-text text-muted">
The Proccesing Fee Changed For Each Purchase	
</small>
</div><!--col-md-6 ends-->	
</div><!--form-group row ends-->
<div class="form-group row">
<label class="col-md-3 control-label"></label>
<div class="col-md-6">
<input type="submit" name="update_general_payment_settings" value="Update General Payment" class="form-control btn btn-success">
</div>
</div><!--form-group row ends-->		
</form>	
</div><!--card-body ends-->	
</div><!--card mb-5-->	
</div><!--col-lg-12	-->
</div><!--second row ends-->
<div class="row">
<div class="col-lg-12">
<div class="card mb-5">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-money-bill-alt"></i> Update Paypal Settings	
</h4>	
</div><!--card-header ends-->
<div class="card-body">
<form method="POST">

<div class="form-group row">
<label class="col-md-3 control-label">Enable Paypal:</label>
<div class="col-md-6">
<select name="enable_paypal" class="form-control">
<?php if ($enable_paypal =="yes") { ?>	
<option value="yes">Yes</option>	
<option value="no">No</option>
<?php }elseif ($enable_paypal="no") { ?>
<option value="no">No</option>	
<option value="yes">Yes</option>
<?php } ?>	
</select>
<small class="form-text tex-tmuted">Allow Buyers To Pay Using Paypal</small>
</div><!--col-md-6 ends-->	
</div><!--form-group row ends-->
<div class="form-group row">
<label class="col-md-3 control-label">Paypal Email:</label>
<div class="col-md-6">
<input type="text" class="form-control" name="paypal_email" value="<?php echo($paypal_email); ?>">
<small class="form-text tex-tmuted">Ensure you Enter the paypal business Email to receive payment And for payment to seller where necessary</small>
</div><!--col-md-6 ends-->	
</div><!--form-group row ends-->
<div class="form-group row">
<label class="col-md-3 control-label">Paypal Currency Code:</label>
<div class="col-md-6">
<input type="text" class="form-control" name="paypal_currency_code" value="<?php echo($paypal_currency_code); ?>">
<small class="form-text tex-tmuted">Currency Uesd For Paypal <a href="https://developer.paypal.com/docs/api/reference/currency-codes/" target="_blank"> Click Hear To Get All PayPal currency Code</a></small>
</div><!--col-md-6 ends-->	
</div><!--form-group row ends-->
<div class="form-group row">
<label class="col-md-3 control-label">Paypal Api Username:</label>
<div class="col-md-6">
<input type="text" class="form-control" name="paypal_api_username" value="<?php echo($paypal_api_username); ?>">
</div><!--col-md-6 ends-->	
</div><!--form-group row ends-->
<div class="form-group row">
<label class="col-md-3 control-label">Paypal Api Password:</label>
<div class="col-md-6">
<input type="text" class="form-control" name="paypal_api_password" value="<?php echo($paypal_api_password); ?>">
</div><!--col-md-6 ends-->	
</div><!--form-group row ends-->
<div class="form-group row">
<label class="col-md-3 control-label">Paypal Api Signature:</label>
<div class="col-md-6">
<input type="text" class="form-control" name="paypal_api_signature" value="<?php echo($paypal_api_signature); ?>">
</div><!--col-md-6 ends-->	
</div><!--form-group row ends-->
<div class="form-group row">
<label class="col-md-3 control-label">Paypal App Id:</label>
<div class="col-md-6">
<input type="text" class="form-control" name="paypal_app_id" value="<?php echo($paypal_app_id); ?>">
</div><!--col-md-6 ends-->	
</div><!--form-group row ends-->
<div class="form-group row">
<label class="col-md-3 control-label">Paypal Sandbox:</label>
<div class="col-md-6">
<div class="form-check form-check-inline">	
<input type="radio" class="form-check form-check-input" name="paypal_sandbox" value="on" required <?php if ($paypal_sandbox =="on") {echo "checked";}else{ } ?>><label class="form-check-label">On</label></div>
<div class="form-check form-check-inline">	
<input type="radio" class="form-check form-check-input"  name="paypal_sandbox" value="off" required <?php if ($paypal_sandbox =="off") {echo "checked";}else{ } ?>><label class="form-check-label">Off</label>
</div>
</div><!--col-md-6 ends-->	
</div><!--form-group row ends-->
<div class="form-group row">
<label class="col-md-3 control-label"></label>
<div class="col-md-6">
<input type="submit" name="update_paypal_settings" class="btn btn-success form-control" value="Update Paypal Setting">	
</div><!--col-md-6 ends-->	
</div><!--form-group row ends-->
</form>	
</div><!--card-body ends-->	
</div><!--card mb-5 ends-->	
</div><!--col-lg-12 ends-->	
</div><!--ends of the thrid row-->
<div class="row">
<div class="col-lg-12">
<div class="card mb-5">
<div class="card-header">
<h4 class="h4"><i class="fa fa-money-bill-alt"></i>Update Stripe Settings</h4>	
</div><!--card-header ends-->
<div class="card-body">
<form method="POST" >

<div class="form-group row">
<label class="col-md-3 control-label">Enable Stripe:</label>
<div class="col-md-6">
<select name="enable_stripe" class="form-control">
<?php if ($enable_stripe=="yes") { ?>	
<option value="yes">Yes</option>	
<option value="no">No</option>
<?php }elseif ($enable_stripe=="no") { ?>
	<option value="no">No</option>
<option value="yes">Yes</option>	
<?php } ?>	
</select>
<small class="form-text tex-tmuted">Allow Buyers To Pay Using Stripe</small>
</div><!--col-md-6 ends-->	
</div><!--form-group row ends-->
<div class="form-group row">
<label class="col-md-3 control-label">Stripe Secret Key:</label>
<div class="col-md-6">
<input type="text" name="stripe_secret_key" class="form-control" value="<?php echo($stripe_secret_key); ?>">
<small class="form-text tex-tmuted">Your Stripe Secret Key Assigned</small>
</div><!--col-md-6 ends-->	
</div><!--form-group row ends-->

<div class="form-group row">
<label class="col-md-3 control-label">Stripe Publisable Key:</label>
<div class="col-md-6">
<input type="text" name="stripe_publishable_key" class="form-control" value="<?php echo($stripe_publishable_key);?>">
<small class="form-text tex-tmuted">Your Stripe Publishable Key Assigned</small>
</div><!--col-md-6 ends-->	
</div><!--form-group row ends-->
<div class="form-group row">
<label class="col-md-3 control-label">Stripe Currency Code:</label>
<div class="col-md-6">
<input type="text" name="stripe_currency_code" class="form-control" value="<?php echo($stripe_currency_code); ?>">
<small class="form-text tex-tmuted">Currency Used For Payment <a href="https://stripe.com/docs/currencies" target="_blank">Click Hear To Get all</a></small>
</div><!--col-md-6 ends-->	
</div><!--form-group row ends-->
<div class="form-group row">
<label class="col-md-3 control-label"></label>
<div class="col-md-6">
<input type="submit" name="update_stripe_setting" class=" btn btn-success form-control" value="Update Stripe Settings">
</div><!--col-md-6 ends-->	
</div><!--form-group row ends-->
</form>
</div><!--card-body ends-->	
</div><!--card mb-5 ends-->	
</div><!--col-lg-12 ends-->	
</div><!--end of the fourth row-->


<?php 
if (isset($_POST['update_general_payment_settings'])) {
$comission_percentage=mysqli_real_escape_string($con,$_POST['comission_percentage']);
$days_before_withdraw=mysqli_real_escape_string($con,$_POST['days_before_withdraw']);
$withdrawal_limit=mysqli_real_escape_string($con,$_POST['withdrawal_limit']);
$featured_fee=mysqli_real_escape_string($con,$_POST['featured_fee']);
$featured_duration=mysqli_real_escape_string($con,$_POST['featured_duration']);
$processing_fee=mysqli_real_escape_string($con,$_POST['processing_fee']);

$update_general_payment_settings="UPDATE  payment_settings set featured_fee='$featured_fee',commission_percentage='$comission_percentage',days_before_withdraw='$days_before_withdraw',withdrawal_limit='$withdrawal_limit', processing_fee='$processing_fee'";
$run_general_payment_settings=mysqli_query($con,$update_general_payment_settings);
if ($run_general_payment_settings) {
echo "<script>
alert('General Settings Has Been Updated Successfully');
window.open('index.php?payment_settings','_self');
</script>";	
}
}


//code to update the paypal Settings
if (isset($_POST['update_paypal_settings'])) {
$enable_paypal=mysqli_real_escape_string($con,$_POST['enable_paypal']);	
$paypal_email=mysqli_real_escape_string($con,$_POST['paypal_email']);	
$paypal_currency_code=mysqli_real_escape_string($con,$_POST['paypal_currency_code']);	
$paypal_api_username=mysqli_real_escape_string($con,$_POST['paypal_api_username']);	
$paypal_api_password=mysqli_real_escape_string($con,$_POST['paypal_api_password']);	
$paypal_api_signature=mysqli_real_escape_string($con,$_POST['paypal_api_signature']);	
$paypal_app_id=mysqli_real_escape_string($con,$_POST['paypal_app_id']);	
$paypal_sandbox=mysqli_real_escape_string($con,$_POST['paypal_sandbox']);	
$update_paypal_settings="UPDATE payment_settings set enable_paypal='$enable_paypal',paypal_email='$paypal_email',paypal_currency_code='$paypal_currency_code',paypal_api_username='$paypal_api_username',paypal_api_password='$paypal_api_password',paypal_api_signature='$paypal_api_signature',paypal_app_id='$paypal_app_id',paypal_sandbox='$paypal_sandbox'";
$run_payapal_setting=mysqli_query($con,$update_paypal_settings);
if ($run_payapal_setting) {
echo "<script>
alert('PayPal Settings Has Been Updated Successfully');
window.open('index.php?payment_settings','_self');
</script>";		
}
}

//UPDATE stripe to the database

if (isset($_POST['update_stripe_setting'])) {
$enable_stripe=mysqli_real_escape_string($con,$_POST['enable_stripe']);
$stripe_secret_key=mysqli_real_escape_string($con,$_POST['stripe_secret_key']);
$stripe_publishable_key=mysqli_real_escape_string($con,$_POST['stripe_publishable_key']);
$stripe_currency_code=mysqli_real_escape_string($con,$_POST['stripe_currency_code']);

$update_stripe_settings="UPDATE payment_settings set enable_stripe='$enable_stripe',stripe_secret_key='$stripe_secret_key',stripe_publishable_key='$stripe_publishable_key',stripe_currency_code='$stripe_currency_code'";
$run_stripe_settings=mysqli_query($con,$update_stripe_settings);
if ($run_stripe_settings) {
echo "<script>
alert('Stripe Settings Has Been Updated Successfully');
window.open('index.php?payment_settings','_self');
</script>";		
}
}
 ?>
 <?php } ?>