<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{

$get_buyer_reviews="SELECT * from buyer_reviews";
$run_buyer_reviews=mysqli_query($con,$get_buyer_reviews);
$count_buyer_reviews=mysqli_num_rows($run_buyer_reviews);


$get_seller_reviews="SELECT * from seller_reviews";
$run_seller_reviews=mysqli_query($con,$get_seller_reviews);
$count_seller_reviews=mysqli_num_rows($run_seller_reviews);
 ?>
 <div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard /View seller Reviews	
</li>	
</ol>	
</div>	
</div><!--first row ends-->

 <div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-money-bill-alt"></i>View Seller Reviews	
</h4>	
</div><!--card-header ends-->
<div class="card-body">
<a href="index.php?view_buyer_reviews" class=" mr-2">
Buyer Reviews (<?php echo($count_buyer_reviews); ?>)	
</a> 
<span class="mr-2">|| </span>
<a href="index.php?view_seller_reviews" class="text-muted mr-2">
Seller Reviews (<?php echo($count_seller_reviews); ?>)	
</a>	
<div class="table-responsive">
<table class="table table-hover table-bordered table-striped">
<thead>
<tr>
<th>Order</th>	
<th>Seller</th>	
<th>Product</th>	
<th>Rating</th>	
<th>Deleted</th>	
</tr>	
</thead>
<tbody>
<?php 
$per_page=4;

if (isset($_GET['seller_reviews_pagination'])) {
$page = $_GET['seller_reviews_pagination'];

}else{
$page=1;
}
#page will strat  from zero and multily by per_page
$start_from=($page-1)*$per_page;

$get_seller_reviews="SELECT * from seller_reviews order by 1 DESC LIMIT $start_from, $per_page";
$run_seller_reviews=mysqli_query($con,$get_seller_reviews);
while ($row_seller_reviews=mysqli_fetch_array($run_seller_reviews)) {
 $review_id=$row_seller_reviews['review_id'];	
 $order_id=$row_seller_reviews['order_id'];	
 $review_seller_id=$row_seller_reviews['review_seller_id'];	
 $seller_rating=$row_seller_reviews['seller_rating'];	

$sel_orders="SELECT * from  orders where order_id='$order_id'";
$run_orders=mysqli_query($con,$sel_orders);
$row_orders=mysqli_fetch_array($run_orders);
$order_number=$row_orders['order_number'];
$proposal_id=$row_orders['proposal_id'];

$get_proposals="SELECT * from proposals where proposal_id='$proposal_id'";
$run_proposals=mysqli_query($con,$get_proposals);
$row_proposals =mysqli_fetch_array($run_proposals);
$proposal_title=$row_proposals['proposal_title'];

$get_sellers="SELECT * from sellers where seller_id='$review_seller_id'";
$run_sellers=mysqli_query($con,$get_sellers);
$row_sellers=mysqli_fetch_array($run_sellers);
$seller_user_name=$row_sellers['seller_user_name'];

 ?>	
<tr>
<td>
<a href="index.php?single_order=<?php echo($order_id); ?>">
#<?php echo($order_number); ?>	
</a>	
</td>

<td>
<a href="index.php?single_seller=<?php echo($review_seller_id); ?>">
<?php echo($seller_user_name); ?>	
</a>	
</td>
<td><?php echo($proposal_title);?></td>	
<td><?php echo($seller_rating);?></td>	
<td>
<a href="index.php?delete_seller_review=<?php echo($review_id);?>" class="btn btn-danger" onclick="return confirm('Do you Want To DELETE This Review Permanently')">
<i class="fa fa-trash-alt"></i>&nbsp;Delete Review	
</a>	
</td>	
</tr>
<?php } ?> 
</tbody>	
</table>	
</div><!--table-responsive ends-->
<div class="d-flex justify-content-center">
<ul class="pagination">
<?php 
#/Get All data from the table 
$query="SELECT * from seller_reviews order by 1 DESC";
$run_query=mysqli_query($con,$query);
#count total
$total_records=mysqli_num_rows($run_query);
#use the ceil get whole numbers]
$total_pages=ceil($total_records / $per_page);
echo "
<li class='page-item'>
<a href='index.php?seller_reviews_pagination=1' class='page-link'> First Page</a>
</li>
";
for ($i=1; $i <=$total_pages; $i++) { 
echo "
<li class='page-item";
if ($i==$page) {
echo " active ";	
}
echo "'>
<a href='index.php?seller_reviews_pagination=".$i."' class='page-link'>".$i."</a>
</li>
";		
	}

echo "
<li class='page-item'>
<a href='index.php?seller_reviews_pagination=$total_pages' class='page-link'> Last Page</a>
</li>
";		
 ?>	
</ul>	
</div><!--d-flex ends-->
</div><!--card-body ends-->	
</div><!--card-ends-->	
</div><!--col-lg-12 ends-->	
</div><!--second row ends-->
<?php } ?>

