<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{
 ?>

<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard / View Single Conversations
</li>	
</ol>	
</div>	
</div><!--first row ends-->
<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-money-bill-alt"></i> View Single Convesations
</h4>	
</div><!--card-header-->	
<div class="card-body">
<?php

$single_messages_id=$_GET['single_inbox_message'];
$get_inbox_messages="SELECT * from inbox_messages where message_group_id='$single_messages_id'";
$run_inbox_messages=mysqli_query($con,$get_inbox_messages);
while($row_inbox_message=mysqli_fetch_array($run_inbox_messages)){
$message_id=$row_inbox_message['message_id'];	
$message_sender=$row_inbox_message['message_sender'];	
$message_desc=$row_inbox_message['message_desc'];	
$message_date=$row_inbox_message['message_date'];	
$message_group_id=$row_inbox_message['message_group_id'];	
$message_file=$row_inbox_message['message_file'];	
$message_offer_id=$row_inbox_message['message_offer_id'];

if (!$message_offer_id==0) {
$seller_offer="SELECT * from messages_offers where offer_id='$message_offer_id'";
$run_offer=mysqli_query($con,$seller_offer);
$row_offer=mysqli_fetch_array($run_offer);
$sender_id=$row_offer['sender_id'];	
$proposal_id=$row_offer['proposal_id'];	
$description=$row_offer['description'];	
$delivery_time=$row_offer['delivery_time'];	
$amount=$row_offer['amount'];	
$order_id=$row_offer['order_id'];	
$status=$row_offer['status'];

$get_proposals="SELECT * from proposals where proposal_id='$proposal_id' ";
$run_proposals=mysqli_query($con,$get_proposals);
$row_proposals =mysqli_fetch_array($run_proposals);
$proposal_title=$row_proposals['proposal_title'];
$proposal_img1=$row_proposals['proposal_img1'];


}
$select_sender="SELECT * from sellers where seller_id='$message_sender'";
$run_sender=mysqli_query($con,$select_sender);
$row_sender=mysqli_fetch_array($run_sender);
$sender_user_name=$row_sender['seller_user_name'];
$sender_image=$row_sender['seller_image'];

?>	
<div class="message-div">
<?php 
if (!empty($sender_image)) { ?>	
<img class="message-image" src="../user_images/<?php echo($sender_image); ?>">
<?php }else{ ?> 
<img src="../user_images/empty-image.png"  class="message-image">
<?php } ?>	
<h5><?php echo($sender_user_name); ?></h5>
<p class="message-desc">
<?php echo($message_desc); ?>

<?php if (!empty($message_file)) { ?>
<a class="d-block mt-2 ml-1" href="../conversations/conversations_files/<?php echo($message_file); ?>"  download>
<i class="fa fa-download"></i><?php echo($message_file); ?>	
</a>
<?php }else{ ?>

<?php } ?>	
</p>
<?php 
if (!$message_offer_id==0) {
	
 ?>
<div class="message-offer">
<div class="row">
<div class="col-lg-2 col-md-3">
<img class="img-fluid" src="../proposals/proposal_files/<?php echo($proposal_img1); ?>">	
</div><!--col-lg-2 ends-->	
<div class="col-lg1-10 col-md-9">
<h5 class="mt-md-0 mt-2">
<?php echo($proposal_title); ?>	
<span class="price float-right d-sm-block d-block">Kshs <?php echo($amount); ?></span>
</h5>
<p><?php echo($description); ?></p>	
<p class="d-block d-sm-none"><b>Price / Amount : </b><?php echo($amount); ?></p>
<p><b>Delivery Time: </b><?php echo($delivery_time); ?></p>
<?php if ($status=="active") { ?>
<button class="btn-success rounded-0 mt-2 float-right">
	Offer Has Not  been Accepted
</button>	
<?php }elseif ($status=='accepted') { ?>
<p class="float-right">
<a href="../order_details.php?order_id=<?php echo($order_id); ?>" class="btn" target="_blank">View Order</a>
<button class="btn btn-success rounded-0" disabled>Offer Accepted</button>	
</p>
<?php } ?>	
</div><!--col-lg1-10 col-md-9 ends-->
</div><!--row ends-->	
</div><!--message_offer ends-->
<?php } ?>
</div><!--message_div ends-->
<?php } ?>
</div><!--card-body-->
</div><!--ends card-->
</div><!--ends col-lg-12-->	
</div><!--ends row 2-->
 <?php } ?>