<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{
 ?>
<?php 
if (isset($_GET['single_order'])) {
$order_id=	$_GET['single_order'];
$get_order="SELECT * from orders where order_id='$order_id'";
$run_order=mysqli_query($con,$get_order);
$row_orders=mysqli_fetch_array($run_order);
$order_number=$row_orders['order_number'];
$proposal_id=$row_orders['proposal_id'];
$seller_id=$row_orders['seller_id'];
$buyer_id=$row_orders['buyer_id'];
$order_price=$row_orders['order_price'];
$order_qty=$row_orders['order_qty'];
$order_date=$row_orders['order_date'];
$order_duration=$row_orders['order_duration'];
$order_fee=$row_orders['order_fee'];
$order_description=$row_orders['order_description'];
$total=$order_price+$order_fee;

//select order proposal details
$get_proposals="SELECT * from proposals where proposal_id='$proposal_id'"; 
$run_proposals=mysqli_query($con,$get_proposals);
$row_proposals=mysqli_fetch_array($run_proposals);
$proposal_title=$row_proposals['proposal_title'];
$proposal_img1=$row_proposals['proposal_img1'];
$proposal_url=$row_proposals['proposal_url'];


//Get Seller Details
$get_seller="SELECT * from sellers where seller_id='$seller_id'";
$run_seller=mysqli_query($con,$get_seller);
$row_seller=mysqli_fetch_array($run_seller);
$seller_user_name=$row_seller['seller_user_name'];

#Select order buyer Details
$select_buyer="SELECT * from sellers where seller_id='$buyer_id'";
$run_buyer=mysqli_query($con,$select_buyer);
$row_buyer=mysqli_fetch_array($run_buyer);
$buyer_user_name=$row_buyer['seller_user_name'];
}
 ?>

<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active font-weight-bold">
<i class="fa fa-money-bill-alt"></i> You Are Viewing Full Details Of This Order
<small># <?php echo $order_number; ?></small>	
</li>	
</ol>	
</div>	
</div><!--first row ends-->
<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-info-circle text-primary"></i> Order Info.	
</h4>	
</div><!--card-header ends-->
<div class="card-body row">
<div class="col-md-2">
<img src="../proposals/proposal_files/<?php echo($proposal_img1);?>" class="img-fluid">	
</div><!--col-md-2 ends-->
<div class="col-md-10">
<h1 class="float-right"><?php echo $order_price; ?>.00</h1>
<h4><?php echo $proposal_title; ?>
<small class="h6">
<a href="../proposals/<?php echo($proposal_url)?>" target="_blank">View Product</a>
</small>
</h4>
<p class="text-muted">Seller :
<a href="index.php?single_seller=<?php echo($seller_id); ?>" target="_blank"><?php echo $seller_user_name; ?></a>
&nbsp;|&nbsp;
<p class="text-muted">Buyer :
<a href="index.php?single_seller=<?php echo($buyer_id); ?>" target="_blank"><?php echo $buyer_user_name; ?></a>
&nbsp;|&nbsp;

Order : # <?php echo $order_number; ?>
&nbsp;|&nbsp;

Date : # <?php echo $order_date; ?>
</p>	
</div><!--col-md-10 ends-->	
<div class="col-md-12">
<table class="order-table table mt-3">
<thead>
<tr>
<th>Item</th>	
<th>Quantity</th>	
<th>Duration</th>	
<th>Amount</th>	
</tr>	
</thead>
<tbody>
<tr>
<td width="600"><?php echo($proposal_title); ?></td>	
<td><?php echo($order_qty); ?></td>	
<td><?php echo($order_duration); ?></td>	
<td>Ksh <?php echo($order_price); ?>.00</td>	
</tr>
<?php if(!empty($order_fee)){ ?>
<tr>
<td width="600">Processing Fee</td>
<td></td>
<td></td>
<td>Ksh <?php echo($order_fee); ?>.00</td>
</tr>
<?php } ?>
<tr>
<td colspan="4">
<span class="float-right mr-5"><b>Total : </b> Ksh <?php echo($total); ?>.00</span>	
</td>	
</tr>	
</tbody>	
</table>
<?php 
if (!empty($order_description)) { ?>
<table class="order-table table">
<thead>
<tr>
<td>Description</td>	
</tr>
</thead>
<tbody>
<tr>
<td><?php echo($order_description); ?></td>	
</tr>	
</tbody>	
</table><!--order-table table ends -->	
<?php } ?>	
</div><!--col-md-12 ens--->
</div>	<!--card-body row end-->
</div><!--card ends-->	
</div>	
</div><!--2 row ends-->

<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-money-bill-alt"></i> View Order Conversation Beteen Seller And Buyer	
</h4>	
</div><!--card-header-ends-->
<div class="card-body">
<?php 
$get_order_conversations="SELECT * from order_conversations where order_id='$order_id'";
$run_order_conversations=mysqli_query($con,$get_order_conversations);
$count_order_conversations=mysqli_num_rows($run_order_conversations);

if ($count_order_conversations==0) {
echo"
<h3 class='text-center'>This Order Has No Coversation!</h3>
";
}
while ($row_conversations=mysqli_fetch_array($run_order_conversations)) {
$sender_id=$row_conversations['sender_id'];	
$message=$row_conversations['message'];	
$status=$row_conversations['status'];	
$file=$row_conversations['file'];	
$date=$row_conversations['date'];	

$select_sender="SELECT * from sellers where seller_id='$sender_id'";
$run_sender=mysqli_query($con,$select_sender);
$row_sender=mysqli_fetch_array($run_sender);
$sender_image=$row_sender['seller_image'];
$sender_user_name=$row_sender['seller_user_name'];
if($seller_id==$sender_id) {
$receiver_name = "Buyer";	
}else{
$receiver_name = "Seller";	
}
?>	
<?php if ($status=='message') { ?>
<div class="message-div">
<?php if (!empty($sender_image)) { ?>
<img src="../user_images/<?php echo($sender_image); ?>" class="message-image">
<?php }else{ ?>
<img src="../user_images/empty-image.php" class="message-image">
<?php } ?>	
<h5><?php echo($sender_user_name); ?></h5>
<p class="message-desc">
<?php echo($message); ?>
<?php if (!empty($file)) { ?>
<a href="../order_files/<?php echo($file); ?>" class="d-block mt-2 ml-1"><i class="fa fa-download"></i><?php echo($file); ?></a>
<?php }else{ ?>	

<?php } ?>	
</p>
<p class="float-right text-muted"><?php echo($date); ?></p>
</div><!--message-div ends-->
<?php }elseif($status=="delivered"){ ?>
<h2 class="mt-3 mb-4" align="center">Order Delivered</h2>

<div class="message-div">
<?php if (!empty($sender_image)) { ?>
<img src="../user_images/<?php echo($sender_image); ?>" class="message-image">
<?php }else{ ?>
<img src="../user_images/empty-image.php" class="message-image">
<?php } ?>	
<h5><?php echo($sender_user_name); ?></h5>
<p class="message-desc">
<?php echo($message); ?>
<?php if (!empty($file)) { ?>
<a href="../order_files/<?php echo($file); ?>" class="d-block mt-2ml-1"><i class="fa fa-download"></i><?php echo($file); ?></a>
<?php }else{ ?>	

<?php } ?>	
</p>
<p class="float-right text-muted"><?php echo($date); ?></p>
</div><!--message-div ends-->

<?php }elseif($status=="revision"){ ?>	

<h2 class="mt-3 mb-4" align="center">Revision Request By <?php echo($sender_user_name); ?> </h2>

<div class="message-div">
<?php if (!empty($sender_image)) { ?>
<img src="../user_images/<?php echo($sender_image); ?>" class="message-image">
<?php }else{ ?>
<img src="../user_images/empty-image.php" class="message-image">
<?php } ?>	
<h5><?php echo($sender_user_name); ?></h5>
<p class="message-desc">
<?php echo($message); ?>
<?php if (!empty($file)) { ?>
<a href="../order_files/<?php echo($file); ?>" class="d-block mt-2ml-1"><i class="fa fa-download"></i><?php echo($file); ?></a>
<?php }else{ ?>	

<?php } ?>	
</p>
<p class="float-right text-muted"><?php echo($date); ?></p>
</div><!--message-div ends-->
<?php }elseif($status=="cancellation_request"){ ?>	
<h2 class="mt-3 mb-4" align="center">Order Cancellation Request By <?php echo($sender_user_name); ?> </h2>

<div class="message-div">
<?php if (!empty($sender_image)) { ?>
<img src="../user_images/<?php echo($sender_image); ?>" class="message-image">
<?php }else{ ?>
<img src="../user_images/empty-image.php" class="message-image">
<?php } ?>	
<h5><?php echo($sender_user_name); ?></h5>
<p class="message-desc">
<?php echo($message); ?>
<?php if (!empty($file)) { ?>
<a href="../order_files/<?php echo($file); ?>" class="d-block mt-2ml-1"><i class="fa fa-download"></i><?php echo($file); ?></a>
<?php }else{ ?>	

<?php } ?>	
</p>
<p class="float-right text-muted"><?php echo($date); ?></p>
</div><!--message-div ends-->
<div class="order-status-message">
<i class="fa fa-times fa-3x text-danger"></i>
<h5 class="text-danger">
<?php if ($sender_id==$buyer_id) { ?>
The Cancellation Request By Buyer, Seller has not accepted it yet!
<?php }elseif($sender_id==$seller_id){ ?>
The Cancellation Request By Seller, Buyer has not accepted it yet!
<?php } ?>	
</h5>	
</div><!--order-status-message ends-->
<?php }elseif($status=="decline_cancellation_request") { ?>

<h2 class="mt-3 mb-4" align="center">Order Cancellation Request By <?php echo($sender_user_name); ?> </h2>

<div class="message-div">
<?php if (!empty($sender_image)) { ?>
<img src="../user_images/<?php echo($sender_image); ?>" class="message-image">
<?php }else{ ?>
<img src="../user_images/empty-image.php" class="message-image">
<?php } ?>	
<h5><?php echo($sender_user_name); ?></h5>
<p class="message-desc">
<?php echo($message); ?>
<?php if (!empty($file)) { ?>
<a href="../order_files/<?php echo($file); ?>" class="d-block mt-2ml-1"><i class="fa fa-download"></i><?php echo($file); ?></a>
<?php }else{ ?>	

<?php } ?>	
</p>
<p class="float-right text-muted"><?php echo($date); ?></p>
</div><!--message-div ends-->
<div class="order-status-message">
<i class="fa fa-times fa-3x text-danger"></i>
<h5 class="text-danger">
Cancellation Request Declined By <?php echo($receiver_name); ?>	
</h5>	
</div><!--order-status-message ends-->

<?php }elseif($status=="accept_cancellation_request"){ ?>


<h2 class="mt-3 mb-4" align="center">Order Cancellation Request By <?php echo($sender_user_name); ?> </h2>

<div class="message-div">
<?php if (!empty($sender_image)) { ?>
<img src="../user_images/<?php echo($sender_image); ?>" class="message-image">
<?php }else{ ?>
<img src="../user_images/empty-image.php" class="message-image">
<?php } ?>	
<h5><?php echo($sender_user_name); ?></h5>
<p class="message-desc">
<?php echo($message); ?>
<?php if (!empty($file)) { ?>
<a href="../order_files/<?php echo($file); ?>" class="d-block mt-2ml-1"><i class="fa fa-download"></i><?php echo($file); ?></a>
<?php }else{ ?>	

<?php } ?>	
</p>
<p class="float-right text-muted"><?php echo($date); ?></p>
</div><!--message-div ends-->
<div class="order-status-message">
<i class="fa fa-times fa-3x text-danger"></i>
<h5 class="text-danger">
Order Cancelled By Mutual Agreement!
</h5>
<p>Order Was Cancelled By Mutual Agreement Between Seller And Buyer</p>	
</div><!--order-status-message ends-->
<?php }elseif($status=="cancelled_by_customer_support"){ ?>	
<div class="order-status-message">
<i class="fa fa-times fa-3x text-danger"></i>
<h5 class="text-danger">
Order Cancelled By Customer Support!
</h5>
<p>Payment for this order was refunded to buyer ReachComputer Balance</p>	
</div><!--order-status-message ends-->
<?php } ?>

<?php } ?>
</div><!--card-body-->	
</div><!--card ends-->	
</div><!--col-lg-12 ends-->	
</div><!--3 row ends-->
 <?php } ?>