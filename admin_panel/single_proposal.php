<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{
?>
<?php 
if (isset($_GET['single_proposal'])){
 	$proposal_id= $_GET['single_proposal'];

 $get_proposals="SELECT * from proposals where proposal_id='$proposal_id'";
$run_proposals=mysqli_query($con,$get_proposals);
$row_proposals =mysqli_fetch_array($run_proposals);
$proposal_title=$row_proposals['proposal_title'];
$proposal_url=$row_proposals['proposal_url'];		
$proposal_price=$row_proposals['proposal_price'];	
$proposal_img1=$row_proposals['proposal_img1'];	
$delivery_id=$row_proposals['delivery_id'];	
$proposal_cat_id=$row_proposals['proposal_cat_id'];	
$proposal_child_id=$row_proposals['proposal_child_id'];	
$proposal_seller_id=$row_proposals['proposal_seller_id'];	

$get_delivery_times="SELECT * from delivery_times where delivery_id='$delivery_id'";
$run_delivery_times=mysqli_query($con,$get_delivery_times);
$row_delivery_times=mysqli_fetch_array($run_delivery_times);
$delivery_proposal_title=$row_delivery_times['delivery_proposal_title'];

$get_sellers="SELECT * from sellers where seller_id='$proposal_seller_id'";
$run_sellers=mysqli_query($con,$get_sellers);
$row_sellers=mysqli_fetch_array($run_sellers);
$seller_user_name=$row_sellers['seller_user_name'];

$get_cats="SELECT * FROM categories where  cat_id='$proposal_cat_id'";
$run_cats=mysqli_query($con,$get_cats);
$row_cats=mysqli_fetch_array($run_cats);
$cat_title=$row_cats['cat_title'];

$get_child_cats="SELECT * from categories_childs where child_id='$proposal_child_id'";
$run_child_cats=mysqli_query($con,$get_child_cats);
$row_child_cats=mysqli_fetch_array($run_child_cats);
$child_title=$row_child_cats['child_title'];

$select_orders="SELECT * from orders where  proposal_id='$proposal_id' AND NOT order_status='complete' AND proposal_id='$proposal_id' AND NOT order_status='cancelled'";
$run_orders=mysqli_query($con,$select_orders);
$count_orders=mysqli_num_rows($run_orders);
$proposal_order_queue=$count_orders;
 } 
 ?>

<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard / View Product
</li>	
</ol>	
</div>	
</div><!--first row ends-->

<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-info-circle text-primary"></i> Product Info!	
</h4>	
</div><!--card header ends-->
<div class="card-body row">
<div class="col-md-4">
<img class="rounded img-fluid" src="../proposals/proposal_files/<?php echo($proposal_img1); ?>" >
<div class="mt-4 mb-3">
<div class="widget-content-expanded">
<p class="lead">
<i class="fa fa-user"></i><span>Title: </span> <?php echo($proposal_title); ?>
</p>
<p class="lead">
<i class="fa fa-user"></i><span>Seller : </span> 
<a href="index.php?single_seller=<?php echo($proposal_seller_id); ?>">
<?php echo($seller_user_name); ?></a>
</p>
<p class="lead">
<i class="fa fa-user"></i><span>Price: </span> <?php echo($proposal_price); ?>
</p>
<p class="lead">
<i class="fa fa-user"></i><span>Category: </span> <?php echo($cat_title); ?>
</p>
<p class="lead">
<i class="fa fa-user"></i><span>Sub Category: </span> <?php echo($child_title); ?>
</p>
<p class="lead">
<i class="fa fa-user"></i><span>Delivery Time: </span> <?php echo($delivery_proposal_title); ?>
</p>	
<p class="lead">
<i class="fa fa-user"></i><span>Order Queue: </span> <?php echo($proposal_order_queue); ?>
</p>
</div><!---widget-content-expanded ends-->
<hr>
<a class="btn btn-primary btn-block text-white" target="_blank" href="../proposals/<?php echo($proposal_url); ?>">View This Product</a>
</div><!--mt-4 mb-3 ends-->	
</div><!--col-md-4 ends-->	
<div class="col-md-8">
<h2>Product Orders</h2>	
<div class="row box">
<div class="text-center border-box col-md-3">
<p>Cancelled Orders</p>
<?php 
$sel_orders="SELECT * from orders where proposal_id='$proposal_id' AND order_status='cancelled'";
$run_orders=mysqli_query($con,$sel_orders);
$count_orders=mysqli_num_rows($run_orders);
 ?>	 
 <h2><?php echo $count_orders; ?></h2>
</div><!--text-center border-box col-md-3 ends-->

<div class="text-center border-box col-md-3">
<p>Delivered Orders</p>
<?php 
$sel_orders="SELECT * from orders where proposal_id='$proposal_id' AND order_status='delivered'";
$run_orders=mysqli_query($con,$sel_orders);
$count_orders=mysqli_num_rows($run_orders);
 ?>	 
 <h2><?php echo $count_orders; ?></h2>
</div><!--text-center border-box col-md-3 ends-->

<div class="text-center border-box col-md-3">
<p>Completed Orders</p>
<?php 
$sel_orders="SELECT * from orders where proposal_id='$proposal_id' AND order_status='completed'";
$run_orders=mysqli_query($con,$sel_orders);
$count_orders=mysqli_num_rows($run_orders);
 ?>	 
 <h2><?php echo $count_orders; ?></h2>
</div><!--text-center border-box col-md-3 ends-->
<div class="text-center border-box col-md-3">
<p>Active Orders</p>
<?php 
$sel_orders="SELECT * from orders where proposal_id='$proposal_id' AND order_active='yes'";
$run_orders=mysqli_query($con,$sel_orders);
$count_orders=mysqli_num_rows($run_orders);
 ?>	 
 <h2><?php echo $count_orders; ?></h2>
</div><!--text-center border-box col-md-3 ends-->

</div><!--row box ends-->

<h2>Other Products by 
<a href="index.php?single_seller=<?php echo $proposal_seller_id; ?>">
<?php echo($seller_user_name); ?>	
</a>
 </h2>
<div class="table-responsive mt-4">
<table class="table table-bordered table-hover">
<thead>
<tr>
<th>Product Title</th>	
<th>Product Image</th>	
<th>Product Pice</th>	
<th>Product Status</th>	
</tr>	
</thead>
<tbody>
<?php 

$get_proposals="SELECT * from proposals where proposal_seller_id='$proposal_seller_id' AND NOT proposal_id='$proposal_id'"; 
$run_proposals=mysqli_query($con,$get_proposals);
$count_proposals=mysqli_num_rows($run_proposals);

if ($count_proposals== 0) {
echo "
<tr>
<td colspan='4'>
<h3 class='text-center'> This Seller Has No Proposlas</h3>
</td>
</tr>
";
}
while ($row_proposals=mysqli_fetch_array($run_proposals)) {
$proposal_title=$row_proposals['proposal_title'];	
$proposal_img1=$row_proposals['proposal_img1'];	
$proposal_price=$row_proposals['proposal_price'];	
$proposal_status=$row_proposals['proposal_status'];	

?>
<tr>
<td><?php echo($proposal_title); ?></td>	
<td>
<img src="../proposals/proposal_files/<?php echo($proposal_img1); ?>" width="60" height="60">
</td>	
<td><?php echo($proposal_price); ?></td>	
<td><?php echo($proposal_status); ?></td>	
</tr>
<?php } ?>	
</tbody>	
</table>	
</div><!--table-responsive ends-->

</div><!--col-md-8 ends-->
</div><!--card-body ends-->	
</div><!--card ends-->	
</div>	
</div><!--second row ends-->

<?php } ?>