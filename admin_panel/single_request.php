<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{
$single_request=$_GET['single_request'];

$get_support_tickets = "SELECT * from support_tickets where ticket_id='$single_request'";
$run_support_tickets = mysqli_query($con,$get_support_tickets);
$row_support_tickets = mysqli_fetch_array($run_support_tickets);  
$ticket_id = $row_support_tickets['ticket_id'];
$sender_id = $row_support_tickets['sender_id'];
$subject = $row_support_tickets['subject'];
$status = $row_support_tickets['status'];
$enquiry_id = $row_support_tickets['enquiry_id'];
$message= $row_support_tickets['message'];
$attachment = $row_support_tickets['attachment'];
$order_number = $row_support_tickets['order_number'];
$order_rule = $row_support_tickets['order_rule'];
$date = $row_support_tickets['date'];

$select_sender = "SELECT * from sellers where seller_id='$sender_id'";
$run_sender = mysqli_query($con,$select_sender);
$row_sender = mysqli_fetch_array($run_sender);
$sender_user_name = $row_sender['seller_user_name'];
$sender_email = $row_sender['seller_email'];

$get_enquiry_types = "SELECT * from enquiry_types where enquiry_id='$enquiry_id'";
$run_enquiry_types = mysqli_query($con,$get_enquiry_types);
$row_enquiry_types = mysqli_fetch_array($run_enquiry_types);
$enquiry_title = $row_enquiry_types['enquiry_title'];

 ?>

<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard / View Single Request	
</li>	
</ol>	
</div>	
</div><!--first row ends-->
<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-money-bill-alt"></i> View Single Request	
</h4>	
</div><!--card-header ends-->	
<div class="card-body">
<p>
<b>Enquiry Type:</b> <?php echo($enquiry_title); ?>	
</p>
<p>
<b>Sender Uname:</b> <?php echo($sender_user_name); ?>	
</p>
<p>
<b>sender Email Address:</b> <?php echo($sender_email); ?>	
</p>
<p>
<b>Subject:</b> <?php echo($subject); ?>	
</p>
<p>
<b>Message:</b> <?php echo($message); ?>	
</p>
<?php 
if (!empty($order_rule)) {
 ?>
<p>
<b>Enquiry Type:</b> <?php echo($order_rule); ?>	
</p>
<?php } ?>

<?php
if (!empty($order_number)) {
 ?>
<p>
<b>Order Number:</b> <?php echo($order_number); ?>	
</p>
<?php } ?>

<?php
if (!empty($attachment)) {
 ?>
<p>
<b>Attachment:</b> <a href="../ticket_files/<?php echo($attachment); ?>" download><?php echo($attachment); ?></a>	
</p>
<?php } ?>
<p>
<b>Request Created Date:</b> <?php echo($date); ?>	
</p>
<p>	
<b>Enquiry Type:</b><?php echo ucwords($status); ?>&nbsp; &nbsp;
</p>
<p>
<b>Enquiry Type:</b> <a href="#" class="btn btn-primary" data-toggle="collapse" data-target="#status">Change status</a>	
</p>
<form id="status" class="collapse from-inline" method="POST">
<p class="text-muted mb-3">
<span class="text-danger">Important :</span>
If you Solve Customer Requested Problem Them Change Request Status From open to closed. <b>Please write either closed or open<u> ONLY</u></b> 	
</p>
<div class="form-group">
<label class="mb-2 mr-sm-2 mb-sm-0">Change Status</label>
<input type="text" name="status" class="form-control mb-2 mr-sm-2 mb-sm-0" value="<?php echo($status); ?>">	

<input type="submit" value="Change Status" name="update_status" class="form-control brn btn-success">
</div><!---form-group ends-->
<?php 
if (isset($_POST['update_status'])) {
$status=$_POST['status'];
$update_support_ticket="UPDATE support_tickets set status='$status' where ticket_id='$single_request'";
$run_update_support_ticket=mysqli_query($con,$update_support_ticket);
if ($run_update_support_ticket) {
echo "<script>
alert('One support Request Has been Changed!');
window.open('index.php?view_support_requests','_self');
</script>";	
}
}
 ?>
</form>
	
</div><!--card-body ends-->
</div><!--card ends-->	
</div><!--col-lg-12 ends-->	
</div><!--second row ends-->
<?php } ?> 