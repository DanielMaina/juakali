<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{
 ?>
 <?php 
if (isset($_GET['single_seller'])) {
$seller_id=$_GET['single_seller'];	
$get_seller="SELECT * from sellers where seller_id='$seller_id'";
$run_seller=mysqli_query($con,$get_seller);
$row_seller=mysqli_fetch_array($run_seller);
$seller_name=$row_seller['seller_name'];
$seller_user_name=$row_seller['seller_user_name'];
$seller_level=$row_seller['seller_level'];
$seller_email=$row_seller['seller_email'];
$seller_image=$row_seller['seller_image'];
$seller_about=$row_seller['seller_about'];
$seller_verification=$row_seller['seller_verification'];
$seller_headline=$row_seller['seller_headline'];
$seller_country=$row_seller['seller_country'];
$seller_ip=$row_seller['seller_ip'];
$seller_register_date=$row_seller['seller_register_date'];
$seller_language=$row_seller['seller_language'];

$select_seller_accounts="SELECT * from seller_accounts where seller_id='$seller_id'";
$run_seller_accounts=mysqli_query($con,$select_seller_accounts);
$row_seller_accounts=mysqli_fetch_array($run_seller_accounts);
$withdrawn=$row_seller_accounts['withdrawn'];
$used_purchases=$row_seller_accounts['used_purchases'];
$pending_cleanrance=$row_seller_accounts['pending_cleanrance'];
$current_balance=$row_seller_accounts['current_balance'];

$get_seller_languages="SELECT * from seller_languages where language_id='$seller_language'";
$run_seller_languages=mysqli_query($con,$get_seller_languages);
$row_seller_languages=mysqli_fetch_array($run_seller_languages);
$language_title=$row_seller_languages['language_title'];

$get_seller_levels="SELECT * from seller_levels where level_id='$seller_level'";
$run_seller_levels=mysqli_query($con,$get_seller_levels);
$row_seller_levels=mysqli_fetch_array($run_seller_levels);
$level_title=$row_seller_levels['level_title'];
}
  ?>
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard / Sngler Seller Detail	
</li>	
</ol>	
</div>	
</div><!--first row ends-->
<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-info-circle text-primary"></i> View Seller Details
</h4>	
</div><!--card-header ends-->
<div class="card-body row">
<div class="col-md-4">
<div class="seller-info mb-3">
<?php 
if (!empty($seller_image)) { ?>
<img class="rounded img-fluid" src="..//user_images/<?php echo($seller_image); ?>">
<?php }else{ ?>
<img class="rounded img-fluid" src="..//user_images/empty-image.png">
<?php } ?>	

<div class="seller-info-title">
<span class="seller-info-inner"><?php echo $seller_user_name;?></span>
<span class="seller-info-type"><?php echo $seller_country;?>
</span>	
</div><!--seller-info-title ends-->
</div><!--seller-info mb-3 ends-->
<div class="mb-3">
<div class="widget-content-expanded">
<p class="lead">
<i class="fa fa-user"></i> <span>Full Name :</span> <?php echo($seller_name); ?>	
</p>
<p class="lead">
<i class="fa fa-user"></i> <span>UserName :</span> <?php echo($seller_user_name); ?>	
</p>
<p class="lead">
<i class="fa fa-user"></i> <span>Email :</span> <?php echo($seller_email); ?>	
</p>
<p class="lead">
<i class="fa fa-user"></i> <span>Levele :</span> <?php echo($level_title); ?>	
</p>
<p class="lead">
<i class="fa fa-user"></i> <span>Task Performance: </span><?php echo($language_title); ?>
</p>
<p class="lead">
<i class="fa fa-user"></i> <span>Email Verification:</span> 
<?php 
if ($seller_verification=="ok" ) {
echo ucfirst($seller_verification);	
}else{
echo "NO";	
}
 ?>
	
</p>	
<p class="lead">
<i class="fa fa-user"></i> <span>IP :</span> <?php echo($seller_ip); ?>
</p>
<p class="lead">
<i class="fa fa-user"></i> <span>Location :</span> <?php echo($seller_country); ?>	
</p>
<p class="lead">
<i class="fa fa-user"></i> <span>Register Date : </span><?php echo($seller_register_date); ?>
</p>
</div><!--widget-content-expanded ends-->
<hr class="dotted short">
<h5 class="text-muted"> Headline </h5>
<p>
<?php echo($seller_headline); ?>	
</p>
</div>	<!--mb-3 ends-->
<div class="mb-3">
<hr class="botted">
<h5 class="text-muted">About</h5>
<p><?php echo($seller_about); ?></p>	
</div><!--mb-3 ends--->
</div><!--col-md-4 ends-->
<div class="col-md-8">
<h2>Seller Orders</h2>
<div class="row box">
<div class="text-center border-box col-md-3">
<p>Cancelled Orders</p>
<?php 
$sel_orders="SELECT * from orders where seller_id='$seller_id' AND order_status='cancelled'";
$run_orders=mysqli_query($con,$sel_orders);
$count_orders=mysqli_num_rows($run_orders);
 ?>	 
 <h2><?php echo $count_orders; ?></h2>
</div><!--text-center border-box col-md-3 ends-->

<div class="text-center border-box col-md-3">
<p>Delivered Orders</p>
<?php 
$sel_orders="SELECT * from orders where seller_id='$seller_id' AND order_status='delivered'";
$run_orders=mysqli_query($con,$sel_orders);
$count_orders=mysqli_num_rows($run_orders);
 ?>	 
 <h2><?php echo $count_orders; ?></h2>
</div><!--text-center border-box col-md-3 ends-->

<div class="text-center border-box col-md-3">
<p>Completed Orders</p>
<?php 
$sel_orders="SELECT * from orders where seller_id='$seller_id' AND order_status='completed'";
$run_orders=mysqli_query($con,$sel_orders);
$count_orders=mysqli_num_rows($run_orders);
 ?>	 
 <h2><?php echo $count_orders; ?></h2>
</div><!--text-center border-box col-md-3 ends-->
<div class="text-center border-box col-md-3">
<p>Active Orders</p>
<?php 
$sel_orders="SELECT * from orders where seller_id='$seller_id' AND order_active='yes'";
$run_orders=mysqli_query($con,$sel_orders);
$count_orders=mysqli_num_rows($run_orders);
 ?>	 
 <h2><?php echo $count_orders; ?></h2>
</div><!--text-center border-box col-md-3 ends-->

</div><!--row box ends-->	
<h2>Seller Earnings</h2>
<div class="row box">
<div class="text-center border-box col-md-3">
<p>Withdrawals</p>
<h2><?php echo($withdrawn); ?></h2> 
</div><!--text-center border-box col-md-3 ends-->	

<div class="text-center border-box col-md-3">
<p>Used for Order</p>
<h2><?php echo($used_purchases); ?></h2> 
</div><!--text-center border-box col-md-3 ends-->

<div class="text-center border-box col-md-3">
<p>Pending Clearance</p>
<h2><?php echo($pending_cleanrance); ?></h2> 
</div><!--text-center border-box col-md-3 ends-->

<div class="text-center border-box col-md-3">
<p>Available Income</p>
<h2><?php echo($current_balance); ?></h2> 
</div><!--text-center border-box col-md-3 ends-->
</div><!--row box ends-->
<h2>Seller Product</h2>
<div class="table-responsive mt-4">
<table class="table table-bordered table-hover">
<thead>
<tr>
<th>Product Title</th>	
<th>Product Image</th>	
<th>Product Pice</th>	
<th>Product Status</th>	
</tr>	
</thead>
<tbody>
<?php 

$get_proposals="SELECT * from proposals where proposal_seller_id='$seller_id'"; 
$run_proposals=mysqli_query($con,$get_proposals);
$count_proposals=mysqli_num_rows($run_proposals);

if ($count_proposals== 0) {
echo "
<tr>
<td colspan='4'>
<h3 class='text-center'> This Seller Has No Product</h3>
</td>
</tr>
";
}
while ($row_proposals=mysqli_fetch_array($run_proposals)) {
$proposal_title=$row_proposals['proposal_title'];	
$proposal_img1=$row_proposals['proposal_img1'];	
$proposal_price=$row_proposals['proposal_price'];	
$proposal_status=$row_proposals['proposal_status'];	

?>
<tr>
<td><?php echo($proposal_title); ?></td>	
<td>
<img src="../proposals/proposal_files/<?php echo($proposal_img1); ?>" width="60" height="60">
</td>	
<td><?php echo($proposal_price); ?></td>	
<td><?php echo($proposal_status); ?></td>	
</tr>
<?php } ?>	
</tbody>	
</table>	
</div><!--table-responsive ends-->
</div><!--col-md-8 ends--->
</div><!--card-body row ends-->
</div><!--card- ends-->	
</div><!--col-md-12 ends-->	
</div><!--2 row ends-->
<?php } ?>