<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{

 ?>
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard / View Support Request	
</li>	
</ol>	
</div>	
</div><!--first row ends-->
<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-money-bill-alt"></i> View Suport Request	
</h4>	
</div><!--card-header ends-->
<div class="card-body"><!--- card-body Starts --->
<span class="text-muted mr-2">
<?php 
$get_support_tickets = "SELECT * from support_tickets where status='open'";
$run_support_tickets = mysqli_query($con,$get_support_tickets);
$count_support_tickets = mysqli_num_rows($run_support_tickets);
?>
All (<?php echo $count_support_tickets; ?>)
</span>
<?php

$get_support_tickets = "SELECT * from support_tickets where status='open'";

$run_support_tickets = mysqli_query($con,$get_support_tickets);

while($row_support_tickets = mysqli_fetch_array($run_support_tickets)){

$enquiry_id = $row_support_tickets['enquiry_id'];


$get_enquiry_types = "SELECT * from enquiry_types where enquiry_id='$enquiry_id'";
$run_enquiry_types = mysqli_query($con,$get_enquiry_types);
$row_enquiry_types = mysqli_fetch_array($run_enquiry_types);
$enquiry_title = $row_enquiry_types['enquiry_title'];

$get_enquiry_support_tickets = "SELECT * from support_tickets where status='open' and enquiry_id='$enquiry_id'";

$run_enquiry_support_tickets = mysqli_query($con,$get_enquiry_support_tickets);

$count_enquiry_support_tickets = mysqli_num_rows($run_enquiry_support_tickets);

?>

<span class="mr-2">|</span>
<span class="mr-2">
<?php echo $enquiry_title; ?> (<?php echo $count_enquiry_support_tickets; ?>)
</span>
<?php } ?>
<div class="table-responsive mt-4"><!--- table-responsive mt-4 Starts --->
<table class="table table-bordered table-hover links-table"><!--- table table-bordered table-hover links-table Starts ---> 
<thead><!--- thead Starts --->
<tr>
<th>Enquiry Type:</th>
<th>Sender Username:</th>
<th>Sender Email:</th>
<th>Subject:</th>
<th>Status:</th>
</tr>
</thead><!--- thead Ends --->
<tbody><!--- tbody Starts --->
<?php

$per_page = 4;

if (isset($_GET['support_requests_pagination'])) {
$page = $_GET['support_requests_pagination'];

}else{
$page=1;
}
#page will strat  from zero and multily by per_page
$start_from=($page-1)*$per_page;
$get_support_tickets = "SELECT * from support_tickets where status='open' order by 1 DESC LIMIT $start_from,$per_page";
$run_support_tickets = mysqli_query($con,$get_support_tickets);
while($row_support_tickets = mysqli_fetch_array($run_support_tickets)){
$ticket_id = $row_support_tickets['ticket_id'];
$sender_id = $row_support_tickets['sender_id'];
$subject = $row_support_tickets['subject'];
$status = $row_support_tickets['status'];
$enquiry_id = $row_support_tickets['enquiry_id'];


$select_sender = "SELECT * from sellers where seller_id='$sender_id'";
$run_sender = mysqli_query($con,$select_sender);
$row_sender = mysqli_fetch_array($run_sender);
$sender_user_name = $row_sender['seller_user_name'];
$sender_email = $row_sender['seller_email'];

$get_enquiry_types = "SELECT * from enquiry_types where enquiry_id='$enquiry_id'";
$run_enquiry_types = mysqli_query($con,$get_enquiry_types);
$row_enquiry_types = mysqli_fetch_array($run_enquiry_types);
$enquiry_title = $row_enquiry_types['enquiry_title'];

?>

<tr onclick="location.href='index.php?single_request=<?php echo $ticket_id; ?>'">
<td><?php echo $enquiry_title; ?></td>
<td><?php echo $sender_user_name; ?></td>
<td><?php echo $sender_email; ?></td>
<td><?php echo $subject; ?></td>
<td>
<button class="btn btn-success"><?php echo ucwords($status); ?></button>
</td>
</tr>
<?php } ?>
</tbody><!--- tbody Ends --->
</table><!--- table table-bordered table-hover links-table Ends ---> 
</div><!--- table-responsive mt-4 Ends --->
<div class="d-flex justify-content-center"><!--- d-flex justify-content-center Starts --->
<ul class="pagination"><!--- pagination Starts --->
<?php 
/// Now Select All From Data From Table
$query = "SELECT * from support_tickets where status='open' order by 1 DESC";
$run_query = mysqli_query($con,$query);
/// Count The Total Records 
$total_records = mysqli_num_rows($run_query);
/// Using ceil function to divide the total records on per page
$total_pages = ceil($total_records / $per_page);
echo "
<li class='page-item'>
<a href='index.php?support_requests_pagination=1' class='page-link'>
First Page
</a>
</li>
";

for($i=1; $i<=$total_pages; $i++){
	
echo "

<li class='page-item";
if ($i==$page) {
echo " active ";	
}
echo "'>

<a href='index.php?support_requests_pagination=".$i."' class='page-link'>

".$i."

</a>

</li>

";
	
}

echo "

<li class='page-item'>

<a href='index.php?support_requests_pagination=$total_pages' class='page-link'>

Last Page

</a>

</li>

";


?>

</ul><!--- pagination Ends --->

</div><!--- d-flex justify-content-center Ends --->
</div><!--- card-body Ends --->
</div><!--card-ends-->	
</div><!--col-12-lg ends-->	
</div><!--second row ends-->
 <?php } ?>