<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{
 ?>
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard / View Category	
</li>	
</ol>	
</div>	
</div><!--first row ends-->

<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4"><i class="fa fa-money-bill-alt"></i>View Categories</h4>	
</div><!--card-herader ends-->
<div class="card-body">
 <table class="table table-bordered table-hover">
 <thead>
<tr>
<th>Category ID</th>	
<th>Category Title</th>	
<th>Category Description</th>	
<th>Category Featured</th>	
<th>Delete Category</th>	
<th>Edit Category</th>		
</tr> 	
 </thead>
 <tbody>
 <?php 
$i=0;
$get_cats="SELECT * FROM categories";
$run_cats=mysqli_query($con,$get_cats);
while ($row_cats=mysqli_fetch_array($run_cats)) {
$cat_id=$row_cats['cat_id'];
$cat_title=$row_cats['cat_title'];
$cat_desc=$row_cats['cat_desc'];
$cat_featured=$row_cats['cat_featured'];
$i++;	 
  ?>
<tr>
<td><?php echo $i; ?></td>
<td><?php echo $cat_title; ?></td>
<td width="700"><?php echo $cat_desc; ?></td>
<td><?php echo $cat_featured; ?></td>
<td>
<a class="btn btn-danger" href="index.php?delete_cat=<?php echo($cat_id); ?>" onclick="return confirm('Do you want to DELETE this Category Together with its Sub-Categories?')">
<i class="fa fa-trash-alt"></i> DELETE	
</a>	
</td>
<td>
<a class="btn btn-info" href="index.php?edit_cat=<?php echo($cat_id); ?>">
<i class="fa fa-pencil-alt"></i> EDIT	
</a>	
</td> 

</tr>
 <?php } ?> 	

 </tbody>	
 </table>	
</div><!--card-hadders-->	
</div><!--card ends-->	
</div><!--col-lg-12-->	
</div><!--second row ends-->
 <?php } ?>