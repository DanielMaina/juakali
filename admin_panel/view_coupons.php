<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{
 ?>

<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard / View Coupons
</li>	
</ol>	
</div>	
</div><!--first row ends-->
<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-money-bill-alt fa-fw"></i> View Coupons	
</h4>	
</div><!--card-header ends-->
<div class="card-body">
<div class="table-responsive">
<table class="table table-hover table-bordered">
<thead>
<tr>
<th>Coupon ID</th>	
<th>Coupon Title</th>	
<th>Coupon Product</th>	
<th>Coupon Code</th>	
<th>Coupon Price</th>	
<th>Coupon Limit</th>	
<th>Coupon Used</th>	
<th>Delete Coupon</th>	
<th>Edit Coupon</th>	
</tr>	
</thead>
<tbody>
<?php 
$i=0;
$get_coupons="SELECT * from coupons order by 1 DESC";
$run_coupons=mysqli_query($con,$get_coupons);
while ($row_coupons=mysqli_fetch_array($run_coupons)) {
$coupon_id=$row_coupons['coupon_id'];	
$coupon_title=$row_coupons['coupon_title'];	
$coupon_price=$row_coupons['coupon_price'];	
$coupon_code=$row_coupons['coupon_code'];	
$coupon_limit=$row_coupons['coupon_limit'];	
$coupon_used=$row_coupons['coupon_used'];		
$proposal_id=$row_coupons['proposal_id'];		

$get_proposals="SELECT * from proposals where proposal_id='$proposal_id'";
$run_proposals=mysqli_query($con,$get_proposals);
$row_proposals =mysqli_fetch_array($run_proposals);
$proposal_title=$row_proposals['proposal_title'];
$i++;		
 ?>
<tr>
<td><?php echo($i); ?></td>	
<td><?php echo($coupon_title); ?></td>	
<td><?php echo($proposal_title); ?></td>	
<td><?php echo($coupon_code); ?></td>	
<td><?php echo($coupon_price); ?></td>	
<td><?php echo($coupon_limit); ?></td>	
<td><?php echo($coupon_used); ?></td>	
<td>
<a href="index.php?delete_coupon=<?php echo($coupon_id);?>" onclick="return confirm('Do You Want To Delete This Coupon')">
<i class="fa fa-trash-alt"></i>	Delete
</a>	
</td>	
<td>
<a href="index.php?edit_coupon=<?php echo($coupon_id);?>">
<i class="fa fa-pencil-alt"></i>	Edit
</a>
</td>	
</tr>
 <?php } ?>	
</tbody>	
</table>	
</div><!--table-responsive ends-->	
</div><!--card-body ends-->	
</div><!--card ends-->	
</div>	
</div><!--2 row ends-->

 <?php } ?>