<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{
 ?>

<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard / View Delivery Time
</li>	
</ol>	
</div>	
</div><!--first row ends-->

<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-money-bill-alt"></i> View Delivery Timews
</h4>	
</div><!--card-header ends-->
<div class="card-body">
<div class="table-responsive">
<table class="table table-bordered table-hover">
<thead>
<tr>
<th>Delivery Time ID</th>	
<th>Delivery Time Title</th>	
<th>Product Delivery Time Title</th>	
<th>Delete Delivery Time ID</th>	
<th>Edit Delivery Time ID</th>	
</tr>	
</thead>
<tbody>
<?php 
$i=0;
$get_delivery_times="SELECT * from delivery_times order by 1 DESC";
$run_delivery_times=mysqli_query($con,$get_delivery_times);
while ($row_delivery_times=mysqli_fetch_array($run_delivery_times)) {
$delivery_id=$row_delivery_times['delivery_id'];	
$delivery_title=$row_delivery_times['delivery_title'];	
$delivery_proposal_title=$row_delivery_times['delivery_proposal_title'];	
$delivery_id=$row_delivery_times['delivery_id'];	

$i++;
 ?>
<tr>
<td><?php echo($i); ?></td>	
<td><?php echo($delivery_title); ?></td>	
<td><?php echo($delivery_proposal_title); ?></td>	
<td>
<a class="text-danger" href="index.php?delete_delivery_time=<?php echo($delivery_id); ?>" onclick="return confirm('Do You want to Delete this Delivery Time?')">
<i class="fa fa-trash-alt"></i> Delete	
</a>	
</td>
<td>
<a class="text-info" href="index.php?edit_delivery_time=<?php echo($delivery_id); ?>">
<i class="fa fa-pencil-alt"></i> Edit	
</a>	
</td>	
</tr>	 	
 <?php } ?>	
</tbody>	
</table>	
</div><!--table-responsive-->	 
</div><!--card-body ends-->
</div><!--card ends-->
</div><!--col-lg-12 ends-->	
</div><!--second row ends-->
 <?php } ?>