<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{
 ?>
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard / View Equiry Types
</li>	
</ol>	
</div>	
</div><!--first row ends-->
<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-money-bil-alt"></i>	View Enquiry Types
</h4>	
</div><!--card-header ends-->
<div class="card-body">
<div class="table-responsive">
<table class="table table-bordered table-hover">	
<thead>
<tr>
<th>Enquiry Type ID:</th>	
<th>Enquiry Type Title:</th>	
<th>Delete Enquiry Type:</th>	
<th>Edit Enquery Type ID:</th>	
</tr>	
</thead>
<tbody>
<?php 
$i=0;
$get_enquiry_types="SELECT * from enquiry_types";
$run_enquiry_types=mysqli_query($con,$get_enquiry_types);
while($row_enquiry_types=mysqli_fetch_array($run_enquiry_types)){
$enquiry_id=$row_enquiry_types['enquiry_id'];	
$enquiry_title=$row_enquiry_types['enquiry_title'];
$i++;	
 ?>
<tr>
<td><?php echo($i);?></td>	
<td><?php echo($enquiry_title);?></td>	
<td>
<a class="text-danger" href="index.php?delete_enquiry_type=<?php echo($enquiry_id); ?>" onclick="return confirm('Do You Want To delete This Enquiry?')">
<i class="fa fa-trash-alt"></i> Delete	
</a>	
</td>	
<td>
<a class="text-warning" href="index.php?edit_enquiry_type=<?php echo($enquiry_id); ?>">
<i class="fa fa-pencil-alt"></i> Edit	
</a>	
</td>	
	
</tr>
 <?php } ?>	
</tbody>	
</table>
</div><!--table-responsive-->

</div><!--card-body ends-->	
</div><!--card ends-->	
</div>	
</div><!--2 row ends-->
 <?php } ?>