<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{

 ?>

<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard / View Orders	
</li>	
</ol>	
</div>	
</div><!--first row ends-->

<div class="row mb-4">
<div class="col-lg-12">
<div class="card">
<div class="card-body">
<h4 class="mb-4">
Order Summary
<?php 
$get_orders="SELECT * from orders";
$run_orders=mysqli_query($con,$get_orders);
$count_orders=mysqli_num_rows($run_orders);
 ?>
<small>All Orders (<?php echo $count_orders; ?>)</small>
</h4>
<div class="row">
<div class="text-center border-box col-md-3">
<p>Active Orders</p>
<?php 
$get_orders="SELECT * from orders where order_active='yes'";
$run_orders=mysqli_query($con,$get_orders);
$count_orders=mysqli_num_rows($run_orders);
 ?>	
 <h2><?php echo $count_orders; ?></h2>
</div><!--text-center border-box col-md-3 ends-->
<div class="text-center border-box col-md-3">
<p>Completed Orders</p>
<?php 
$get_orders="SELECT * from orders where order_status='completed'";
$run_orders=mysqli_query($con,$get_orders);
$count_orders=mysqli_num_rows($run_orders);
 ?>	
 <h2><?php echo $count_orders; ?></h2>
</div><!--text-center border-box col-md-3 ends-->
<div class="text-center border-box col-md-3">
<p>Deliveres Orders</p>
<?php 
$get_orders="SELECT * from orders where order_status='delivered'";
$run_orders=mysqli_query($con,$get_orders);
$count_orders=mysqli_num_rows($run_orders);
 ?>	
 <h2><?php echo $count_orders; ?></h2>
</div><!--text-center border-box col-md-3 ends-->
<div class="text-center border-box col-md-3">
<p>Cancelled Orders</p>
<?php 
$get_orders="SELECT * from orders where order_status='cancelled'";
$run_orders=mysqli_query($con,$get_orders);
$count_orders=mysqli_num_rows($run_orders);
 ?>	
 <h2><?php echo $count_orders; ?></h2>
</div><!--text-center border-box col-md-3 ends-->	
</div>	<!--ends of row -->
</div><!--card-body ends-->	
</div><!--card ends-->	
</div><!--col-lg-12 ends-->	
</div><!--2 row ends-->

<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-money-bill-alt"></i> View Orders
</h4>	
</div><!--card-header ends-->	
<div class="card-body">
<h3 class="text-center mb-3">Filter Orders</h3>	
<form action="index.php?filter_orders" class="form-inline d-flex mb-4 justify-content-center" method="POST">
<div class="form-group">
<label class="mb-2 mr-sm-2 mb-sm-0">Enter Order Number</label>
<div class="input-group">
<span class="input-group-prepend">
	<b class="input-group-text">#</b>
</span>	
<input type="number" name="order_number" class="form-control mb-2 mr-sm-2 mb-sm-0" placeholder="45678665" required>
</div><!--input-group-->	
</div><!---form-group ends-->
<div class="form-group">
<input type="submit" name="submit" class=" btn btn-success form-control" value="Filter Orders">	
</div><!--form-group ends-->	
</form>
<div class="table-responsive">
<table class="table table-bordered table-hover">
<thead>
<tr>
<td>Order Number</td>	
<td>Order Total</td>	
<td>Order QTY</td>	
<td>Order Date</td>	
<td>Order status</td>	
<td>Order Action</td>	
</tr>	
</thead>
<tbody>
<?php 
  $per_page=7;
  $sel_orders="SELECT * from orders order by 1 DESC LIMIT 0,$per_page";
  $run_orders=mysqli_query($con,$sel_orders);
  while ($row_orders=mysqli_fetch_array($run_orders)) {
$order_id=$row_orders['order_id'];
$order_price=$row_orders['order_price'];
$order_number=$row_orders['order_number'];
$order_qty=$row_orders['order_qty'];
$order_date=$row_orders['order_date'];
$order_status=$row_orders['order_status'];

 ?> 
<tr>
<td># <?php echo($order_number); ?></td>	
<td>Kshs <?php echo($order_price); ?></td>	
<td> <?php echo($order_qty); ?></td>	
<td> <?php echo($order_date); ?></td>	
<td> <?php echo ucfirst($order_status); ?></td>	
<td>
<div class="dropdown">
<button class="btn btn-success dropdown-toggle" type="button" data-toggle="dropdown">Actions</button>
<div class="dropdown-menu">
<a href="index.php?single_order=<?php echo($order_id); ?>" class="dropdown-item"><i class="fa fa-info-circle"></i> View Order Full Details</a>
<?php if($order_status=="cancelled" or $order_status =="completed"){ ?>
<?php }else{ ?>
<a href="index.php?cancel_order=<?php echo($order_id); ?>" class="dropdown-item" onclick="return confirm('Do You want to cancell this order? Amount Will Be Returned To Buyer')"><i class="fa fa-ban"></i>Cancel Order</a>
 <?php } ?>


</div><!--dropdown-menu ends-->
</div><!--dropdown ends-->	

</td>		
</tr>
 <?php } ?> 	
</tbody>	
</table>	
</div><!--table-responsive ends-->
<div class="d-flex justify-content-center">
<ul class="pagination">
<?php 
#/Get All data from the table 
$query="SELECT * from orders order by 1 DESC";
$run_query=mysqli_query($con,$query);
#count total
$total_records=mysqli_num_rows($run_query);
#use the ceil get whole numbers]
$total_pages=ceil($total_records / $per_page);
echo "
<li class='page-item'>
<a href='index.php?orders_pagination=1' class='page-link'> First Page</a>
</li>
";
for ($i=1; $i <=$total_pages; $i++) { 
echo "
<li class='page-item";
if ($i==$page) {
echo " active ";	
}
echo "'>
<a href='index.php?orders_pagination=".$i."' class='page-link'>".$i."</a>
</li>
";		
	}

echo "
<li class='page-item'>
<a href='index.php?orders_pagination=$total_pages' class='page-link'> Last Page</a>
</li>
";		
 ?>	
</ul>	
</div><!--d-flex ends-->
</div><!--card-body ends-->
</div><!--card ends-->	
</div><!--col-lg-1 ends--->	
</div><!--3 row ends-->
 <?php } ?>
