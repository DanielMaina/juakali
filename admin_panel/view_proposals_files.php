<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{
 ?>
<?php 

$directory="../proposals/proposal_files/";
$handle=opendir($directory);
$restricted=array(".","..","Thumbs.db");
while ($file=readdir($handle)) {
if (!in_array($file, $restricted)) {
$files_array[]=$file;	
}

}
$count=0;
$limit=12;
$page=1;
$start =($page-1)*$limit;
 ?>
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard / View Products Files	
</li>	
</ol>	
</div>	
</div><!--first row ends-->
<div class="row">
<?php 
for ($i=$start; $i < count($files_array); $i++) {
$count++; 
 ?>	
<div class="col-lg-3 col-sm-4 col-sm-6  mb-3">
 <div class="card">
 <div class="card-header">
 <?php echo($files_array[$i]); ?>	
 </div><!---card-header ends-->	
 <div class="card-body text-center">
 <?php 
if (exif_imagetype('../proposals/proposal_files/' .$files_array[$i])== IMAGETYPE_JPEG OR exif_imagetype('../proposals/proposal_files/' .$files_array[$i])== IMAGETYPE_PNG OR exif_imagetype('../proposals/proposal_files/' .$files_array[$i])== IMAGETYPE_GIF ) { ?>
<img src="../proposals/proposal_files/<?php echo($files_array[$i]); ?>" class="img-fluid" style="height: 70px;">	
<?php }else{ ?>
<i class="fa fa-file-code fa-5x"></i>
  <?php } ?>	
 </div><!--ends card-body text-center-->
 <div class="card-footer">
 <a class="text-primary float-left" href="../proposals/proposal_files/<?php echo($files_array[$i]); ?>"><i class="fa fa-download"></i>Download</a>	
 <a class="text-danger float-right" href="index.php?delete_proposal_file=<?php echo($files_array[$i]); ?>" onclick="return confirm('Are You Sure To Delete This file?')"><i class="fa fa-trash-alt"></i> Delete</a>	
 </div><!--card-footer -->
 </div><!--card ends-->	
</div><!--end of col-lg-3-->
<?php 
if ($limit==$count) {
	break;
}
 ?>
<?php } ?>
</div><!--second row ends-->

<div class="row">
<div class="col-lg-12">
<div class="pagination d-flex justify-content-center">
<?php 
#use the ceil function
$total_pages=ceil(count($files_array)/$limit);

echo "<li class='page-item'>
<a href='index.php?proposals_file_pagination=1' class='page-link'>First Page</a>
</li>";
for ($i=1; $i<=$total_pages; $i++) { 
echo "<li class='page-item'>
<a href='index.php?proposals_file_pagination=".$i."' class='page-link'>".$i."</a>
</li>";	
}
echo "<li class='page-item'>
<a href='index.php?proposals_file_pagination=$total_pages' class='page-link'>Last Page</a>
</li>";

 ?>	
</div><!-- ends pagination  d-flex justify-content-center--->	
</div><!--col-lg-12 ends-->	
</div><!--3 row ends-->
 <?php } ?>