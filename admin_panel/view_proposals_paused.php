<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{

$get_all_proposals="SELECT * from proposals";
$run_all_proposals=mysqli_query($con,$get_all_proposals);
$count_all_proposals=mysqli_num_rows($run_all_proposals);

$get_active_proposals="SELECT * from proposals where proposal_status='active'";
$run_active_proposals=mysqli_query($con,$get_active_proposals);
$count_active_proposals=mysqli_num_rows($run_active_proposals);

$get_pending_proposals="SELECT * from proposals where proposal_status='pending'";
$run_pending_proposals=mysqli_query($con,$get_pending_proposals);
$count_pending_proposals=mysqli_num_rows($run_pending_proposals);

$get_pause_proposals="SELECT * from proposals where proposal_status='pause'";
$run_pause_proposals=mysqli_query($con,$get_pause_proposals);
$count_pause_proposals=mysqli_num_rows($run_pause_proposals);

$get_trash_proposals="SELECT * from proposals where proposal_status='trash'";
$run_trash_proposals=mysqli_query($con,$get_trash_proposals);
$count_trash_proposals=mysqli_num_rows($run_trash_proposals);

 ?>
<div class="row">
<div class="col-lg-12">
<div class="p-3 mb-3 filter-form">
<h2>Filter Product</h2>
<form method="GET" action="filter_proposals.php" class="form-inline">
<div class="form-group">
<label>Delivery Time:</label>	
<select name="delivery_id" class="form-control mb-2 mr-sm-2 mb-sm-0">
<option value="">----------------</option>
<?php 
$get_delivery_times="SELECT * from delivery_times";
$run_delivery_times=mysqli_query($con,$get_delivery_times);
while ($row_delivery_times=mysqli_fetch_array($run_delivery_times)) {
$delivery_id=$row_delivery_times['delivery_id'];		
$delivery_title=$row_delivery_times['delivery_title'];
echo "<option value='$delivery_id'>$delivery_title</option>";		
}
 ?>
</select>
</div>

<div class="form-group">
<label>Seller Level:</label>	
<select name="level_id" class="form-control mb-2 mr-sm-2 mb-sm-0">
<option value="">------------</option>
<?php 
$get_seller_levels="SELECT * from seller_levels";
$run_seller_levels=mysqli_query($con,$get_seller_levels);
while ($row_seller_levels=mysqli_fetch_array($run_seller_levels)) {
$level_id=$row_seller_levels['level_id'];	
$level_title=$row_seller_levels['level_title'];
echo "<option value='$level_id'>$level_title</option>";	
}
 ?>
</select>
</div>	
<div class="form-group">
<label>Ctaegory:</label>	
<select name="cat_id" class="form-control mb-2 mr-sm-2 mb-sm-0">
<option value="">--------------------</option>
<?php 
$get_categories="SELECT * from categories";
$run_categories=mysqli_query($con,$get_categories);
while ($row_categories=mysqli_fetch_array($run_categories)) {
$cat_id=$row_categories['cat_id'];
$cat_title=$row_categories['cat_title'];
echo "<option value='$cat_id'>$cat_title</option>";
}
 ?>
</select>
</div>
<button type="submit" class="btn btn-default">Filter Product</button>
</form>
</div>		
</div>	
</div><!--first row ends-->

<div class="row mt-3">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4"><i class="fa fa-money-bill-alt"></i>View Product</h4>	
</div><!--card-header ends-->	
<div class="card-body">
<a href="index.php?view_proposals" class=" mr-2">
	All(<?php echo($count_all_proposals); ?>)
</a>	
<span class="mr-2">|</span>
<a href="index.php?view_proposals_active" class=" mr-2">
	Active(<?php echo($count_active_proposals); ?>)
</a>	
<span class="mr-2">|</span>
<a href="index.php?view_proposals_pending" class=" mr-2">
	Pending(<?php echo($count_pending_proposals); ?>)
</a>	
<span class="mr-2">|</span>
<a href="index.php?view_proposals_paused" class="text-muted mr-2">
	Paused(<?php echo($count_pause_proposals); ?>)
</a>	
<span class="mr-2">|</span>
<a href="index.php?view_proposals_trash" class=" mr-2">
	Trash(<?php echo($count_trash_proposals); ?>)
</a>	

<div class="table-respnsive mt-4">
<table class="table able-hover table border">
<thead>
<tr>
<th>Product Title</th>	
<th>Product Image</th>	
<th>Product Price</th>	
<th>Product Category</th>	
<th>Product Order Queue</th>	
<th>Product Options</th>	
</tr>	
</thead>
<tbody>
<?php 

$get_proposals="SELECT * from proposals where proposal_status='pause' order by 1 DESC";
$run_proposals=mysqli_query($con,$get_proposals);
while ($row_proposals =mysqli_fetch_array($run_proposals)) {
$proposal_id=$row_proposals['proposal_id'];
$proposal_title=$row_proposals['proposal_title'];
$proposal_img1=$row_proposals['proposal_img1'];
$proposal_url=$row_proposals['proposal_url'];
$proposal_price=$row_proposals['proposal_price'];
$proposal_cat_id=$row_proposals['proposal_cat_id'];
$proposal_status=$row_proposals['proposal_status'];

$select_orders="SELECT * from orders where  proposal_id='$proposal_id' AND NOT order_status='complete' AND proposal_id='$proposal_id' AND NOT order_status='cancelled'";
$run_orders=mysqli_query($con,$select_orders);
$count_orders=mysqli_num_rows($run_orders);
$proposal_order_queue=$count_orders;
$get_categories="SELECT * from categories where cat_id='$proposal_cat_id'";
$run_categories=mysqli_query($con,$get_categories);
$row_categories=mysqli_fetch_array($run_categories);
$cat_title=$row_categories['cat_title'];
 ?>
<tr>
<td>	
<a href="index.php?single_proposal=<?php echo($proposal_id); ?>">
<?php echo($proposal_title); ?>
</a>
</td>
<td>
<img src="../proposals/proposal_files/<?php echo($proposal_img1); ?>" width="50" height="50">
</td>	
<td><?php echo($proposal_price); ?></td>	
<td><?php echo($cat_title); ?></td>	
<td><?php echo($proposal_order_queue); ?></td>	
	
 <td>
 <a href="../proposals/<?php echo($proposal_url); ?>" target="_blank"><i class="fa fa-eye"></i>View</a> |
<a href="index.php?restore_proposal=<?php echo($proposal_id); ?>" ><i class="fa fa-eye"></i>Restore</a>|
<a class="text-danger" href="index.php?move_to_trash=<?php echo($proposal_id); ?>" ><i class="fa fa-trash-alt"></i>Trash</a>	
</td>
<?php } ?>		
</tbody>	
</table>	
</div><!--table-respnsive mt-4 ends-->
</div><!--card-body ends-->
</div><!--card-ends-->	
</div><!--col-lg-12 ends-->	
</div><!--2 row ends-->


 <?php } ?>