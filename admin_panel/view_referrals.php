<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{
 ?>
 <div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard / View Referrals	
</li>	
</ol>	
</div>	
</div><!--first row ends-->
<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-money-bill-alt"></i> View Referrals	
</h4>	
</div><!--card-header ends-->
<div class="card-body">
<div class="table-responsive">
<table class="table table-bordered table-hover">
<thead>
<tr>
<th>No:</th>	
<th>UserName:</th>	
<th>Referred:</th>	
<th>Money / Commision:</th>	
<th>Ip Address:</th>	
<th>Sign Up Date:</th>	
<th>States:</th>	
<th>Actions:</th>	
</tr>	
</thead>
<tbody>
<?php 
$i=0;
$get_referrals="SELECT * from referrals where status='pending'";
$run_referrals=mysqli_query($con,$get_referrals);

while ($row_referrals=mysqli_fetch_array($run_referrals)) {
	
$refarral_id=$row_referrals['refarral_id'];
$seller_id=$row_referrals['seller_id'];
$referred_id=$row_referrals['referred_id'];
$comission=$row_referrals['comission'];
$date=$row_referrals['date'];
$ip=$row_referrals['ip'];
$status=$row_referrals['status'];

//Get Seller Details
$get_seller="SELECT * from sellers where seller_id='$seller_id'";
$run_seller=mysqli_query($con,$get_seller);
$row_seller=mysqli_fetch_array($run_seller);
$seller_user_name=$row_seller['seller_user_name'];

$sel_referred="SELECT * from sellers where seller_id='$referred_id'";
$run_referred=mysqli_query($con,$sel_referred);
$row_referred=mysqli_fetch_array($run_referred);
$referred_user_name=$row_referred['seller_user_name'];
$i++;
?>	
<tr>
<td><?php echo($i);?></td>	
<td><?php echo($seller_user_name);?></td>	
<td><?php echo($referred_user_name);?></td>	
<td>Kshs <?php echo($comission);?></td>	
<td><?php echo($ip);?></td>	
<td><?php echo($date);?></td>	
<td><?php echo($status);?></td>	
<td>
<div class="dropdown">
<button class="btn btn-success dropdown-toggle" data-toggle="dropdown">Actions</button>
<div class="dropdown-menu">
<a class="dropdown-item text-warning" href="index.php?approval_referral=<?php echo($refarral_id); ?>" onclick="return confirm('Do Your Want To Approve This Referral And add Kshs <?php echo $comission; ?>.00 to <?php echo($seller_user_name); ?> Balance');"><i class="fa fa-thumbs-up"></i> Approve</a>

<a class="dropdown-item text-danger" href="index.php?decline_referral=<?php echo($refarral_id); ?>" onclick="return confirm('Do Your Want To Decline This Referral And deny Kshs <?php echo $comission; ?>.00 to <?php echo($seller_user_name); ?> Balance');"><i class="fa fa-ban"></i> Decline</a>	
</div><!--btn btn-success dropdown-toggle-->	
</div><!--dropdown-->	
</td>	
</tr>

<?php } ?>
</tbody>	
</table>	
</div><!--table-responsive-->	
</div><!--card-body ends-->	
</div><!--card ends-->	
</div><!--col-lg-12  ends-->	
</div><!--2 row ends-->
 <?php } ?>