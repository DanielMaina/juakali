<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{
 ?>
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard / View Seller Task Execution/Performance	
</li>	
</ol>	
</div>	
</div><!--first row ends-->

<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-headder">
<h4 class="h4">
<i class="fa fa-money-bill-alt"></i> View seller Performance
</h4>	
</div><!--card-header ends-->
<div class="card-body">
<div class="table-responsive">
<table class="table table-hovered table-hover">
<thead>
<tr>
<th>No.</th>	
<th>Seller Performance Title</th>	
<th> Delete </th>	
<th> Edit </th>	
</tr>
</thead>
<tbody>
<?php 
$i=0;
$get_seller_languages="SELECT * from seller_languages order by 1 DESC";
$run_seller_languages=mysqli_query($con,$get_seller_languages);
while ($row_seller_languages=mysqli_fetch_array($run_seller_languages)) {
$language_id=$row_seller_languages['language_id'];	
$language_title=$row_seller_languages['language_title'];	
$i++;
 ?>	
<tr>
<td><?php echo($i); ?></td>	
<td><?php echo($language_title); ?></td>	
<td>
<a href="index.php?delete_seller_language=<?php echo($language_id)?>" onclick=" return confirm('Do you want to delete seller Performance?');">
	<i class="fa fa-trash-alt"></i> Delete
</a>	
</td>
<td>
<a href="index.php?edit_seller_language=<?php echo($language_id)?>">
	<i class="fa fa-trash-alt"></i> Edit
</a>
</td>	
</tr>
<?php } ?>
</tbody>		
</table>
</div><!---table-responsive ends-->	
</div><!--card-body -->	
</div><!--card ends-->
</div><!--col-lg-12 emds-->	
</div><!--2 row ends-->
<?php } ?>