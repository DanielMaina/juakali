<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{

 ?>
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard / view Seller Skill	
</li>	
</ol>	
</div>	
</div><!--first row ends-->

<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-money-bill-alt"></i> Views Sellers Skill	
</h4>	
</div><!--card-header ends-->	
<div class="card-body">
<div class="table-responsive">
<table class="table table-boreded table-hover">
<thead>
<tr>
<th>Skill Skill ID</th>	
<th>Skill Skill Title</th>	
<th>Delete Skill Skill </th>	
<th>Edit Skill Skill </th>	

</tr>	
</thead>
<tbody>
<tr>	
<?php 
$i=0;
$get_seller_skils="SELECT  * from seller_skills order by 1 DESC";
$run_seller_skills=mysqli_query($con,$get_seller_skils);
while ($row_seller_skill= mysqli_fetch_array($run_seller_skills)) {
$skill_id=$row_seller_skill['skill_id'];	
$skill_title=$row_seller_skill['skill_title'];	
$i++;	
?>
<td><?php echo($i); ?></td>
<td><?php echo($skill_title); ?></td>
<td>
<a href="index.php?delete_seller_skill=<?php echo($skill_id); ?>" class="text-danger" onclick="return confirm('Areyou Sure you Want to delete?')">
	<i class="fa fa-trash-alt"></i> Delete
</a>	
</td>
<td>
<a href="index.php?edit_seller_skill=<?php echo($skill_id); ?>" class="text-info">
	<i class="fa fa-pencil-alt"></i> Edite
</a>	
</td>

</tr>
<?php } ?>
</tbody>	
</table>	
</div><!--table-responsive ends-->	
</div><!---card--->
</div><!---card end-->	
</div>	
</div><!--2 row ends-->
<?php } ?>