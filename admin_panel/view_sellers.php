<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{
 ?>

<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard / View Sellers	
</li>	
</ol>	
</div>	
</div><!--first row ends-->
<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-body">
<?php 
$get_sellers="SELECT * from sellers";
$run_sellers=mysqli_query($con,$get_sellers);
$count_seller=mysqli_num_rows($run_sellers);
 ?>
 <h4 class="mb-4">
 Seller Summary
<small>All Sellers (<?php echo($count_seller); ?>)</small>
 </h4>
 <div class="row">
 <div class="text-center border-box col-md-3">
 <p>Top Rated Sellers</p>
<?php 
$get_sellers="SELECT * from sellers where seller_level=4";
$run_sellers=mysqli_query($con,$get_sellers);
$count_seller=mysqli_num_rows($run_sellers);
 ?>
 <h2><?php echo($count_seller) ?></h2>
 </div><!--text-center border-box col-mb-3 ends-->
 <div class="text-center border-box col-md-3">
 <p>Level Two Sellers</p>
<?php 
$get_sellers="SELECT * from sellers where seller_level=3";
$run_sellers=mysqli_query($con,$get_sellers);
$count_seller=mysqli_num_rows($run_sellers);
 ?>
 <h2><?php echo($count_seller) ?></h2>
 </div><!--text-center border-box col-mb-3 ends-->	
 <div class="text-center border-box col-md-3">
 <p>Levele One Sellers</p>
<?php 
$get_sellers="SELECT * from sellers where seller_level=2";
$run_sellers=mysqli_query($con,$get_sellers);
$count_seller=mysqli_num_rows($run_sellers);
 ?>
 <h2><?php echo($count_seller) ?></h2>
 </div><!--text-center border-box col-mb-3 ends-->	
 <div class="text-center col-md-3">
 <p>New Sellers</p>
<?php 
$get_sellers="SELECT * from sellers where seller_level=1";
$run_sellers=mysqli_query($con,$get_sellers);
$count_seller=mysqli_num_rows($run_sellers);
 ?>
 <h2><?php echo($count_seller) ?></h2>
 </div><!--text-center col-mb-3 ends-->		
 </div><!--row  ends-->
</div><!--card-body-->	
</div><!--card ends-->	
</div><!--col-lg-12 ends-->	
</div><!--2 row ends-->

<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-money-bill-alt"></i> View Sellers		
</h4>
</div><!--card-header ends-->
<div class="card-body">
<div class="table-responsive">
<table class="table table-bordered table-hover">
<thead>
<tr>
<th>Seller No.</th>	
<th>Seller UserName</th>	
<th>Seller Email</th>	
<th>Seller Level</th>	
<th>Seller Register Date</th>	
<th>Actions</th>	
</tr>	
</thead>
<tbody>
<?php 
$i=0;
$per_page=7;
$get_sellers="SELECT * from sellers order by 1 DESC LIMIT 0,$per_page";
$run_sellers=mysqli_query($con,$get_sellers);
while ($row_sellers=mysqli_fetch_array($run_sellers)) {
$seller_id=$row_sellers['seller_id'];
$seller_user_name=$row_sellers['seller_user_name'];
$seller_email=$row_sellers['seller_email'];
$seller_level=$row_sellers['seller_level'];
$seller_register_date=$row_sellers['seller_register_date'];
$seller_status=$row_sellers['seller_status'];

$get_seller_levels="SELECT * from seller_levels where level_id='$seller_level'";
$run_seller_levels=mysqli_query($con,$get_seller_levels);
$row_seller_levels=mysqli_fetch_array($run_seller_levels);
$level_title=$row_seller_levels['level_title'];
$i++;
 ?>
<tr>
<td><?php echo($i); ?></td>	
<td><?php echo($seller_user_name); ?></td>	
<td><?php echo($seller_email); ?></td>	
<td><?php echo($seller_level); ?></td>	
<td><?php echo($seller_register_date); ?></td>	
<td>
<div class="dropdown">
	<button class="btn btn-success" type="button" data-toggle="dropdown">Actions</button>
<div class="dropdown-menu">
<a href="index.php?single_seller=<?php echo($seller_id); ?>" class="dropdown-item"><i class="fa fa-info-circle"></i> View Seller Full Details </a>
<a href="index.php?seller_login=<?php echo($seller_user_name); ?>" class="dropdown-item"><i class="fa fa-sign-in-alt"></i> Login As <?php echo($seller_user_name); ?> </a>
<?php if ($seller_status=="block-ban") { ?>
<a href="index.php?unblock_seller=<?php echo($seller_id); ?>" class="dropdown-item"><i class="fa fa-unlock"></i> Unblock </a>
<?php }else{ ?>
<a href="index.php?ban_seller=<?php echo($seller_id); ?>" class="dropdown-item"><i class="fa fa-ban"></i> Block </a>
<?php } ?>	
</div>	
</div><!--dropdown ends-->	

</td>
</tr>
 <?php } ?>	
</tbody>	
</table>	
</div><!--table-responsive-->
<div class="d-flex justify-content-center">
<ul class="pagination">
<?php 
//select all from proposal table
$query ="SELECT * from sellers  order by 1 DESC";
$result=mysqli_query($con,$query);
//CountTotolRecord
$total_records=mysqli_num_rows($result);
//use cell function to divide as per-Page
$total_pages=ceil($total_records / $per_page);
echo "<li class='page-item'>
<a href='index.php?sellers_pagination=1' class='page-link'>First Page</a>
</li>";
for ($i=1; $i<=$total_pages; $i++) { 
echo "<li class='page-item'>
<a href='index.php?sellers_pagination=".$i."' class='page-link'>".$i."</a>
</li>";	
}
echo "<li class='page-item'>
<a href='index.php?sellers_pagination=$total_pages' class='page-link'>Last Page</a>
</li>";
 ?>	
</ul><!--pagination-->	
</div><!--d-flex justify-content-center ends-->	
</div><!--card-body ends-->	
</div><!--card ends-->	
</div><!--col-lg-12 ends-->	
</div><!--3 row ends-->
 <?php } ?>
