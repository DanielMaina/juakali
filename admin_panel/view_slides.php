<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{

 ?>

<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard / View Slides	
</li>	
</ol>	
</div>	
</div><!--first row ends-->
<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">	
<h4 class="h4">
<i class="fa fa-money-bill-alt"></i> View slide	
</h4>
</div><!--card -header ends-->
<div class="card-body">
<div class="row">
<?php 
$get_slides="SELECT * from slider";
$run_slides=mysqli_query($con,$get_slides);
while ($row_slides=mysqli_fetch_array($run_slides)) {
$slide_id=$row_slides['slide_id'];	
$slide_name=$row_slides['slide_name'];	
$slide_image=$row_slides['slide_image'];

 ?>
 <div class="col-lg-3 col-md-6 mb-3 mb-3">
 <div class="card">
 <div class="card-header">
 <h5 class="text-center"><?php echo $slide_name; ?></h5>	
 </div>	<!--inner card-header ends-->
<div class="card-body">
<img class="img-fluid" src="../slides_images/<?php echo($slide_image); ?>" >
</div><!--card-body ends-->
<div class="card-footer">
<a class="float-left text-danger" href="index.php?delete_slide=<?php echo($slide_id); ?>" onclick="return confirm('Do You Want To Delete This Slider?')" ><i class="fa fa-trash-alt"></i> Delete</a>	
<a class="float-right text-warning" href="index.php?edit_slide=<?php echo($slide_id); ?>"><i class="fa fa-pencil-alt"></i> Edit</a>
<div class="clearfix"></div>	
</div><!--card-footer ends-->
 </div><!--inner card ends-->
 </div><!--col-lg-3 col-md-6 mb-lg-0mb-3 ends-->

<?php } ?> 
</div><!--inner row ends-->	
</div><!--card-body ends-->
</div><!--card ends-->	
</div>	<!--col-lg-12 ends-->
</div><!--2 row ends-->
 <?php } ?>