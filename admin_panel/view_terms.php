<?php 
@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{
 ?>
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard / View Terms	
</li>	
</ol>	
</div>	
</div><!--first row ends-->
<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-money-bill-alt"></i> View Terms	
</h4>	
</div><!--card-header ends--->
<div class="card-body">
<div class="row">
<?php 
$get_terms="SELECT * from terms ";
$run_terms=mysqli_query($con,$get_terms);
while ($row_terms=mysqli_fetch_array($run_terms)) {
$term_id=$row_terms['term_id'];
$term_title=$row_terms['term_title'];
$term_description=$row_terms['term_description'];
 ?>	
<div class="col-lg-4 col-md-6 mb-3">
<div class="card">
<div class="card-header">
<h4 class="h4 text-center">
<?php echo $term_title; ?>	
</h4>	
</div><!--card-header ends-->
<div class="card-body">
<p><?php echo($term_description); ?></p>
</div><!--card-body ends-->	
<div class="card-footer">
<a href="index.php?delete_term=<?php echo($term_id); ?>" class="float-left text-danger" onclick="return confirm('Confirm Delete Terms?')"><i class="fa fa-trash-alt"></i> Delete</a>
<a href="index.php?edit_term=<?php echo($term_id); ?>" class="float-right text-warning"><i class="fa fa-pencil-alt"></i> Edit</a>	
<div class="clearfix"></div>
</div>
</div><!--card-ends-->	
</div><!--col-lg-4 col-md-6 mb-3 ends-->
<?php } ?>
</div><!--row ends-->
</div><!--card-pbody ends--->	
</div><!--card ends-->	
</div><!--col-lg-12-->	
</div><!--2 row ends-->

<?php } ?>
