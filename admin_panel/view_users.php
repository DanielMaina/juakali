<?php 

@session_start();
 if (!isset($_SESSION['admin_email'])) { 
echo "<script>window.open('login.php','_self');</script>";
}else{
 ?>
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li class="active">
<i class="fa fa-home"></i>Dashboard / View Users	
</li>	
</ol>	
</div>	
</div><!--first row ends-->
<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h4 class="h4">
<i class="fa fa-money-bill-alt"></i> View Users	
</h4>	
</div><!--card-header ends-->
<div class="card-body">
<div class="table-responsive">
<table class="table table-bordered table-hover">
<thead>
<tr>
<th>UserName</th>	
<th>UserEmail</th>	
<th>UserImage</th>	
<th>UserCountry</th>	
<th>UserJob</th>	
<th>DeleteUser</th>	
</tr>	
</thead>
<tbody>
<?php 
$get_admins="SELECT * from admins";
$run_admins=mysqli_query($con,$get_admins);
while ($row_admins=mysqli_fetch_array($run_admins)) {
$admin_id=$row_admins['admin_id'];	
$admin_name=$row_admins['admin_name'];	
$admin_email=$row_admins['admin_email'];	
$admin_image=$row_admins['admin_image'];	
$admin_country=$row_admins['admin_country'];	
$admin_job=$row_admins['admin_job'];	
 ?>	
<tr>
<td><?php echo($admin_name) ?></td>	
<td><?php echo($admin_email) ?></td>	
<td><img src="admin_images/<?php echo($admin_image) ?>" width="60" height="50"/></td>	
<td><?php echo($admin_country) ?></td>	
<td><?php echo($admin_job) ?></td>	
<td>
<?php if ($login_admin_id==$admin_id) { ?>
ConnotDelete_Self!
<?php }else { ?>
<a class="text-danger" href="index.php?delete_user=<?php echo($admin_id); ?>"><i class="fa fa-trash-alt"></i> Delete</a>
<?php } ?>	
</td>
</tr>
<?php } ?>

</tbody>	
</table><!--ends of table table-bordered table-hover-->

</div><!--table-responsive ends-->
</div><!--card-body ends-->	
</div><!--card-ends-->	
</div>	
</div><!--2 row ends-->


 <?php } ?>