-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 15, 2019 at 11:34 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `juakali`
--

-- --------------------------------------------------------

--
-- Table structure for table `adds`
--

CREATE TABLE `adds` (
  `add_id` int(10) NOT NULL,
  `add_site` varchar(255) NOT NULL,
  `add_code` text NOT NULL,
  `add_place` varchar(255) NOT NULL,
  `enable_add` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `admin_id` int(11) NOT NULL,
  `admin_name` text NOT NULL,
  `admin_email` text NOT NULL,
  `admin_pass` text NOT NULL,
  `admin_image` text NOT NULL,
  `admin_contact` text NOT NULL,
  `admin_country` text NOT NULL,
  `admin_job` text NOT NULL,
  `admin_about` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`admin_id`, `admin_name`, `admin_email`, `admin_pass`, `admin_image`, `admin_contact`, `admin_country`, `admin_job`, `admin_about`) VALUES
(1, 'Daniel Maina', 'simpledaniel.1818@gmail.com', '$2y$10$FhFpJPXFeyG0NtT1RPgcHejPuEBOlwKJNE3sZPhFaFR7q3Mo4htl2', 'admin.png', '0728889191', 'Kenya', 'HotAdmin', 'I am a technology enthusiast who has a keen interest in programming I am pursuing Software Developmenr Online I like to unwind by watching movies and English sitcomsI have a keen interest in music'),
(3, 'Joseph', 'joseph@joseph.joseph', '$2y$10$EseVKdd0UqpUWmDwn/SZ7.5wZagLtdsT04AxtNiptXJoPneDuUNzC', '1573737019145-120016693.jpg', '0728889191', 'Kenya', 'Fullstack', 'Ertyhcddghhv');

-- --------------------------------------------------------

--
-- Table structure for table `buyer_requests`
--

CREATE TABLE `buyer_requests` (
  `request_id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `child_id` int(11) NOT NULL,
  `request_title` text NOT NULL,
  `request_description` text NOT NULL,
  `request_file` text NOT NULL,
  `delivery_time` text NOT NULL,
  `request_budget` text NOT NULL,
  `request_date` text NOT NULL,
  `request_status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `buyer_requests`
--

INSERT INTO `buyer_requests` (`request_id`, `seller_id`, `cat_id`, `child_id`, `request_title`, `request_description`, `request_file`, `delivery_time`, `request_budget`, `request_date`, `request_status`) VALUES
(1, 4, 4, 43, 'Optimized youtube seo video', 'i want a well optimize youtube video for my channel', 'download.jpg', '1 Day', '1500', '9/24/2019', 'unapproved'),
(2, 4, 6, 11, '  php 7 and laravel', ' Design web Apps using php 7 and laravel', 'download.jpg', '5 Days', '3000', '9/23/2019', 'active'),
(3, 4, 2, 20, 'core laravel and yii', 'I want you Do web development in laravel core and yii', 'download.jpg', '3 Days', '3500', '9/22/2019', 'pending'),
(4, 4, 1, 1, 'Logo design', 'I Will Design A logo and Brand for you', 'download.jpg', '1 Day', '500', '9/22/2019', 'active'),
(5, 4, 4, 43, 'Web based application using laravel', 'If you have great hands on skill please join my team and develop.I want i great laravel developer.', 'web-development-2.png', '2 Days', '1020', 'October 11, 2019', 'active'),
(6, 4, 6, 56, 'i want i great laravel developer', 'Hi please help my group do this project. i want i great laravel developer', 'wordpress-3.jpg', '3 Days', '750', 'October 11, 2019', 'active'),
(7, 5, 6, 56, 'Help me do a php 7 object oriented project', 'Hi!, i want someone who can Help me do a php 7 object oriented project ', 'fix-php-errors-1.png', '3 Days', '1500', 'October 11, 2019', 'active'),
(8, 3, 6, 56, 'i want good react application', 'Application should meet all the expected standard', 'andorid-2.jpg', '5 Days', '2000', 'October 18, 2019', 'active'),
(9, 1, 2, 20, 'i want good custom video', 'The video should meet all the expected standard', 'web-development-2.png', '5 Days', '1000', 'October 23, 2019', 'unapproved'),
(10, 4, 4, 43, 'i want an optimized video youtube content', 'The video should meet all the expected standard', 'web-development-2.png', '5 Days', '1000', 'October 23, 2019', 'active'),
(11, 3, 6, 63, 'i want a designer men trouser', 'Should be blue, 27 waist and 33 long', 'translate-3.jpg', '5 Days', '500', 'December 13, 2019', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `buyer_reviews`
--

CREATE TABLE `buyer_reviews` (
  `review_id` int(11) NOT NULL,
  `proposal_id` int(11) NOT NULL,
  `order_id` int(10) NOT NULL,
  `review_buyer_id` int(10) NOT NULL,
  `buyer_rating` int(10) NOT NULL,
  `buyer_review` text NOT NULL,
  `review_seller_id` int(10) NOT NULL,
  `review_date` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `buyer_reviews`
--

INSERT INTO `buyer_reviews` (`review_id`, `proposal_id`, `order_id`, `review_buyer_id`, `buyer_rating`, `buyer_review`, `review_seller_id`, `review_date`) VALUES
(2, 2, 1, 3, 5, 'Great work', 2, 'September 2, 2019 '),
(3, 3, 9, 2, 5, 'Outstanding Experience', 1, 'September 15, 2019'),
(6, 7, 8, 6, 5, 'great work...Expert!  ', 4, 'Oct 05 2019'),
(11, 1, 0, 1, 5, 'splendid', 1, 'Oct 23 2019'),
(12, 8, 25, 7, 4, 'This guys is a great expert', 4, 'Oct 30 2019'),
(13, 6, 26, 1, 3, 'Improve on development pattern', 4, 'Oct 30 2019'),
(14, 1, 27, 4, 4, 'True expert,,, sawa', 1, 'Dec 06 2019'),
(15, 6, 28, 11, 4, 'The work is now Ok! ', 4, 'Dec 06 2019'),
(16, 8, 33, 11, 4, 'Took some time but the best thank you', 4, 'Dec 13 2019');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `cart_id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `proposal_id` int(11) NOT NULL,
  `proposal_price` int(11) NOT NULL,
  `proposal_qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`cart_id`, `seller_id`, `proposal_id`, `proposal_price`, `proposal_qty`) VALUES
(2, 4, 3, 400, 1),
(6, 7, 5, 1000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `cat_id` int(11) NOT NULL,
  `cat_title` text NOT NULL,
  `cat_desc` text NOT NULL,
  `cat_image` text NOT NULL,
  `cat_featured` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`cat_id`, `cat_title`, `cat_desc`, `cat_image`, `cat_featured`) VALUES
(1, 'Agriculture', 'jUA KALI MALL! is application focused to help the the jua kali industry developed product for agriculture use reach to the market with ease and speed and despite of the location  of the customer. the jua kali product are of quality in that they usually go through quality check and verification before opened for sale ', 'graphic-design.png', 'yes'),
(2, 'Kitchen Appliances', 'jUA KALI MALL! is application focused to help the the jua kali industry developed product for kItchen use reach to the market with ease and speed and despite of the location  of the customer. the jua kali product are of quality in that they usually go through quality check and verification before opened for sale ', 'digital marketing.jpg', 'yes'),
(3, 'Building & Construction', 'jUA KALI MALL! is application focused to help the the jua kali industry developed product for Construction use reach to the market with ease and speed and despite of the location  of the customer. the jua kali product are of quality in that they usually go through quality check and verification before opened for sale ', 'writting.jpg', 'yes'),
(4, 'Furniture and House Design', 'jUA KALI MALL! is application focused to help the the jua kali industry developed product for Furniture use reach to the market with ease and speed and despite of the location  of the customer. the jua kali product are of quality in that they usually go through quality check and verification before opened for sale ', 'video-animation.jpg', 'yes'),
(5, 'Designer Shoe', 'jUA KALI MALL! is application focused to help the the jua kali industry developed product for shoes use reach to the market with ease and speed and despite of the location  of the customer. the jua kali product are of quality in that they usually go through quality check and verification before opened for sale ', 'music-audio.jpg', 'yes'),
(6, 'Well-Fit Cloth', 'jUA KALI MALL! is application focused to help the the jua kali industry developed product for cloth use reach to the market with ease and speed and despite of the location  of the customer. the jua kali product are of quality in that they usually go through quality check and verification before opened for sale ', 'program-tech.jpg', 'yes'),
(7, 'Welding & Metalic', 'jUA KALI MALL! is application focused to help the the jua kali industry developed product for welding use reach to the market with ease and speed and despite of the location  of the customer. the jua kali product are of quality in that they usually go through quality check and verification before opened for sale ', 'business.jpg', 'yes'),
(8, 'Domestic product', 'jUA KALI MALL! is application focused to help the the jua kali industry developed product for Domestic use reach to the market with ease and speed and despite of the location  of the customer. the jua kali product are of quality in that they usually go through quality check and verification before opened for sale ', 'fun-life-style.jpg', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `categories_childs`
--

CREATE TABLE `categories_childs` (
  `child_id` int(10) NOT NULL,
  `child_parent_id` int(10) NOT NULL,
  `child_title` text NOT NULL,
  `child_desc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories_childs`
--

INSERT INTO `categories_childs` (`child_id`, `child_parent_id`, `child_title`, `child_desc`) VALUES
(1, 1, 'wheelbaarrow', 'Jakali designed tool that  upholds ergonomics considerations,\r\n which means that they are being designed to induce less stress\r\n on the human body when used. The most efficient tools keep the\r\n body in a neutral position to help reduce the stress on joints and muscles'),
(2, 1, 'farm hoe', 'Jakali designed tool that  upholds ergonomics considerations,\r\n which means that they are being designed to induce less stress\r\n on the human body when used. The most efficient tools keep the\r\n body in a neutral position to help reduce the stress on joints and muscles'),
(3, 1, 'panga', 'Jakali designed tool that  upholds ergonomics considerations,\r\n which means that they are being designed to induce less stress\r\n on the human body when used. The most efficient tools keep the\r\n body in a neutral position to help reduce the stress on joints and muscles'),
(4, 1, 'watering can', 'Jakali designed tool that  upholds ergonomics considerations,\r\n which means that they are being designed to induce less stress\r\n on the human body when used. The most efficient tools keep the\r\n body in a neutral position to help reduce the stress on joints and muscles'),
(5, 1, 'farm fork', 'Jakali designed tool that  upholds ergonomics considerations,\r\n which means that they are being designed to induce less stress\r\n on the human body when used. The most efficient tools keep the\r\n body in a neutral position to help reduce the stress on joints and muscles'),
(6, 1, 'farm rake', 'Jakali designed tool that  upholds ergonomics considerations,\r\n which means that they are being designed to induce less stress\r\n on the human body when used. The most efficient tools keep the\r\n body in a neutral position to help reduce the stress on joints and muscles'),
(7, 1, 'farm spade', 'Jakali designed tool that  upholds ergonomics considerations,\r\n which means that they are being designed to induce less stress\r\n on the human body when used. The most efficient tools keep the\r\n body in a neutral position to help reduce the stress on joints and muscles'),
(8, 1, 'farm sickle', 'Jakali designed tool that  upholds ergonomics considerations,\r\n which means that they are being designed to induce less stress\r\n on the human body when used. The most efficient tools keep the\r\n body in a neutral position to help reduce the stress on joints and muscles'),
(9, 1, 'Farm Mattock', 'Jakali designed tool that  upholds ergonomics considerations,\r\n which means that they are being designed to induce less stress\r\n on the human body when used. The most efficient tools keep the\r\n body in a neutral position to help reduce the stress on joints and muscles'),
(10, 2, 'Cooking Pot & Sufuria', 'Jua kali kitchen utensils are design with ease of use,\r\n equipment is used for, baking, mixing, the, they are long-lasting being developed from native\r\nmetallic materials and traditional hardwood. they are cheap and faster delivered to the  customer'),
(11, 6, 'sleeve hoods', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.'),
(12, 1, 'Sprinkler', 'Jakali designed tool that  upholds ergonomics considerations,\r\n which means that they are being designed to induce less stress\r\n on the human body when used. The most efficient tools keep the\r\n body in a neutral position to help reduce the stress on joints and muscles'),
(13, 1, 'farm Axe', 'Jakali designed tool that  upholds ergonomics considerations,\r\n which means that they are being designed to induce less stress\r\n on the human body when used. The most efficient tools keep the\r\n body in a neutral position to help reduce the stress on joints and muscles'),
(14, 1, 'farm sholver', 'Jakali designed tool that  upholds ergonomics considerations,\r\n which means that they are being designed to induce less stress\r\n on the human body when used. The most efficient tools keep the\r\n body in a neutral position to help reduce the stress on joints and muscles'),
(15, 1, 'farm trowel', 'Jakali designed tool that  upholds ergonomics considerations,\r\n which means that they are being designed to induce less stress\r\n on the human body when used. The most efficient tools keep the\r\n body in a neutral position to help reduce the stress on joints and muscles'),
(16, 1, 'Other', 'Jakali designed tool that  upholds ergonomics considerations,\r\n which means that they are being designed to induce less stress\r\n on the human body when used. The most efficient tools keep the\r\n body in a neutral position to help reduce the stress on joints and muscles'),
(17, 2, 'PLate and Bowl', 'Jua kali kitchen utensils are design with ease of use,\r\n equipment is used for, baking, mixing, the, they are long-lasting being developed from native\r\nmetallic materials and traditional hardwood. they are cheap and faster delivered to the  customer'),
(18, 2, 'Flaying Pans', 'Jua kali kitchen utensils are design with ease of use,\r\n equipment is used for, baking, mixing, the, they are long-lasting being developed from native\r\nmetallic materials and traditional hardwood. they are cheap and faster delivered to the  customer'),
(19, 2, 'Spatula (mwiko)', 'Jua kali kitchen utensils are design with ease of use,\r\n equipment is used for, baking, mixing, the, they are long-lasting being developed from native\r\nmetallic materials and traditional hardwood. they are cheap and faster delivered to the  customer'),
(20, 2, 'Steel Kettle', 'Jua kali kitchen utensils are design with ease of use,\r\n equipment is used for, baking, mixing, the, they are long-lasting being developed from native\r\nmetallic materials and traditional hardwood. they are cheap and faster delivered to the  customer'),
(21, 2, 'Serving basin and Spoon', 'Jua kali kitchen utensils are design with ease of use,\r\n equipment is used for, baking, mixing, the, they are long-lasting being developed from native\r\nmetallic materials and traditional hardwood. they are cheap and faster delivered to the  customer'),
(22, 2, 'Skimmer Spoon', 'Jua kali kitchen utensils are design with ease of use,\r\n equipment is used for, baking, mixing, the, they are long-lasting being developed from native\r\nmetallic materials and traditional hardwood. they are cheap and faster delivered to the  customer'),
(23, 2, 'Kitchen Knife', 'Jua kali kitchen utensils are design with ease of use,\r\n equipment is used for, baking, mixing, the, they are long-lasting being developed from native\r\nmetallic materials and traditional hardwood. they are cheap and faster delivered to the  customer'),
(24, 2, 'Cutting Boards', 'Jua kali kitchen utensils are design with ease of use,\r\n equipment is used for, baking, mixing, the, they are long-lasting being developed from native\r\nmetallic materials and traditional hardwood. they are cheap and faster delivered to the  customer'),
(25, 2, 'Kitchen Tongs', 'Jua kali kitchen utensils are design with ease of use,\r\n equipment is used for, baking, mixing, the, they are long-lasting being developed from native\r\nmetallic materials and traditional hardwood. they are cheap and faster delivered to the  customer'),
(26, 2, 'Steel Sink', 'Jua kali kitchen utensils are design with ease of use,\r\n equipment is used for, baking, mixing, the, they are long-lasting being developed from native\r\nmetallic materials and traditional hardwood. they are cheap and faster delivered to the  customer'),
(27, 2, 'Rolling pin and board', 'Jua kali kitchen utensils are design with ease of use,\r\n equipment is used for, baking, mixing, the, they are long-lasting being developed from native\r\nmetallic materials and traditional hardwood. they are cheap and faster delivered to the  customer'),
(28, 2, 'Custom Made', 'Jua kali kitchen utensils are design with ease of use,\r\n equipment is used for, baking, mixing, the, they are long-lasting being developed from native\r\nmetallic materials and traditional hardwood. they are cheap and faster delivered to the  customer'),
(29, 2, 'Others', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.Jua kali kitchen utensils are design with ease of use,\r\n equipment is used for, baking, mixing, the, they are long-lasting being developed from native\r\nmetallic materials and traditional hardwood. they are cheap and faster delivered to the  customer'),
(30, 3, 'Chisel', 'This are material with great aspect of design for use. the are cheap.\r\n the designer focus on how to develop a component that can be used for multiple functionality.\r\nthis helps solve the issues of having too may artifact which at time are dormant '),
(31, 3, 'Wheelbarrow', 'This are material with great aspect of design for use. the are cheap.\r\n the designer focus on how to develop a component that can be used for multiple functionality.\r\nthis helps solve the issues of having too may artifact which at time are dormant '),
(32, 3, 'Hand Spade', 'This are material with great aspect of design for use. the are cheap.\r\n the designer focus on how to develop a component that can be used for multiple functionality.\r\nthis helps solve the issues of having too may artifact which at time are dormant '),
(33, 3, 'Scraper for Masonry', 'This are material with great aspect of design for use. the are cheap.\r\n the designer focus on how to develop a component that can be used for multiple functionality.\r\nthis helps solve the issues of having too may artifact which at time are dormant '),
(34, 3, 'Custom Bricks', 'This are material with great aspect of design for use. the are cheap.\r\n the designer focus on how to develop a component that can be used for multiple functionality.\r\nthis helps solve the issues of having too may artifact which at time are dormant '),
(35, 3, 'Trowel ', 'This are material with great aspect of design for use. the are cheap.\r\n the designer focus on how to develop a component that can be used for multiple functionality.\r\nthis helps solve the issues of having too may artifact which at time are dormant '),
(36, 3, 'Hummer for masonry', 'This are material with great aspect of design for use. the are cheap.\r\n the designer focus on how to develop a component that can be used for multiple functionality.\r\nthis helps solve the issues of having too may artifact which at time are dormant '),
(37, 3, 'Plumb Bob', 'This are material with great aspect of design for use. the are cheap.\r\n the designer focus on how to develop a component that can be used for multiple functionality.\r\nthis helps solve the issues of having too may artifact which at time are dormant '),
(38, 3, 'shovel for Construction', 'This are material with great aspect of design for use. the are cheap.\r\n the designer focus on how to develop a component that can be used for multiple functionality.\r\nthis helps solve the issues of having too may artifact which at time are dormant '),
(39, 3, 'Digging Bar', 'This are material with great aspect of design for use. the are cheap.\r\n the designer focus on how to develop a component that can be used for multiple functionality.\r\nthis helps solve the issues of having too may artifact which at time are dormant '),
(40, 3, 'Other', 'This are material with great aspect of design for use. the are cheap.\r\n the designer focus on how to develop a component that can be used for multiple functionality.\r\nthis helps solve the issues of having too may artifact which at time are dormant '),
(41, 4, ' Designer Dresses', 'Furniture design such as tables, chairs such as full set and well customized for user need, beds, desks, \r\nand other houuse and office design are widely built and focus on user spesification. \r\nThese objects are usually kept in a house or other building\r\n to make it suitable or comfortable for living or working'),
(42, 4, 'Cupboards', 'Furniture design such as tables, chairs such as full set and well customized for user need, beds, desks, \r\nand other houuse and office design are widely built and focus on user spesification. \r\nThese objects are usually kept in a house or other building\r\n to make it suitable or comfortable for living or working'),
(43, 4, 'Bend ', 'Furniture design such as tables, chairs such as full set and well customized for user need, beds, desks, \r\nand other houuse and office design are widely built and focus on user spesification. \r\nThese objects are usually kept in a house or other building\r\n to make it suitable or comfortable for living or working'),
(44, 4, 'Designer Desk', 'Furniture design such as tables, chairs such as full set and well customized for user need, beds, desks, \r\nand other houuse and office design are widely built and focus on user spesification. \r\nThese objects are usually kept in a house or other building\r\n to make it suitable or comfortable for living or working'),
(45, 4, 'Customized chair', 'Furniture design such as tables, chairs such as full set and well customized for user need, beds, desks, \r\nand other houuse and office design are widely built and focus on user spesification. \r\nThese objects are usually kept in a house or other building\r\n to make it suitable or comfortable for living or working'),
(46, 4, 'Designer Tables', 'Furniture design such as tables, chairs such as full set and well customized for user need, beds, desks, \r\nand other houuse and office design are widely built and focus on user spesification. \r\nThese objects are usually kept in a house or other building\r\n to make it suitable or comfortable for living or working'),
(48, 4, 'Other', 'Furniture design such as tables, chairs such as full set and well customized for user need, beds, desks, \r\nand other houuse and office design are widely built and focus on user spesification. \r\nThese objects are usually kept in a house or other building\r\n to make it suitable or comfortable for living or working'),
(49, 5, 'Official Black', 'Walk and run while keeping your feet supported \r\nA cushioned insole is the standout feature of this shoe.\r\n Best Sandal for Men and women:  Comfortable  at all use. \r\nThe design is meant to take any pressure off of your foot.\r\nBest and  Professionals design'),
(50, 5, 'Ladies Heel', 'Walk and run while keeping your feet supported \r\nA cushioned insole is the standout feature of this shoe.\r\n Best Sandal for Men and women:  Comfortable  at all use. \r\nThe design is meant to take any pressure off of your foot.\r\nBest and  Professionals design'),
(51, 5, 'Sport Shoe', 'Walk and run while keeping your feet supported \r\nA cushioned insole is the standout feature of this shoe.\r\n Best Sandal for Men and women:  Comfortable  at all use. \r\nThe design is meant to take any pressure off of your foot.\r\nBest and  Professionals design'),
(52, 5, 'Designer Boots', 'Walk and run while keeping your feet supported \r\nA cushioned insole is the standout feature of this shoe.\r\n Best Sandal for Men and women:  Comfortable  at all use. \r\nThe design is meant to take any pressure off of your foot.\r\nBest and  Professionals design'),
(53, 5, 'Men variety', 'Walk and run while keeping your feet supported \r\nA cushioned insole is the standout feature of this shoe.\r\n Best Sandal for Men and women:  Comfortable  at all use. \r\nThe design is meant to take any pressure off of your foot.\r\nBest and  Professionals design'),
(54, 5, 'Women variety', 'Walk and run while keeping your feet supported \r\nA cushioned insole is the standout feature of this shoe.\r\n Best Sandal for Men and women:  Comfortable  at all use. \r\nThe design is meant to take any pressure off of your foot.\r\nBest and  Professionals design'),
(55, 5, 'Back To School Variety', 'Walk and run while keeping your feet supported \r\nA cushioned insole is the standout feature of this shoe.\r\n Best Sandal for Men and women:  Comfortable  at all use. \r\nThe design is meant to take any pressure off of your foot.\r\nBest and  Professionals design'),
(56, 6, 'Jackets variety', 'Finding the right balance between comfort and style in the variety of clothe.\r\nat cheap price and of high quality customized.\r\n place state what you would like to sused and then we will do it for you \r\nwe provide what you are looking for, but not forcing you take what we have'),
(58, 6, 'Designer Skirt ', 'Finding the right balance between comfort and style in the variety of clothe.\r\nat cheap price and of high quality customized.\r\n place state what you would like to sused and then we will do it for you \r\nwe provide what you are looking for, but not forcing you take what we have'),
(60, 6, 'Trousers', 'Finding the right balance between comfort and style in the variety of clothe.\r\nat cheap price and of high quality customized.\r\n place state what you would like to sused and then we will do it for you \r\nwe provide what you are looking for, but not forcing you take what we have'),
(61, 6, 'Designer Suits', 'Finding the right balance between comfort and style in the variety of clothe.\r\nat cheap price and of high quality customized.\r\n place state what you would like to sused and then we will do it for you \r\nwe provide what you are looking for, but not forcing you take what we have'),
(62, 6, 'Dress variety', 'Finding the right balance between comfort and style in the variety of clothe.\r\nat cheap price and of high quality customized.\r\n place state what you would like to sused and then we will do it for you \r\nwe provide what you are looking for, but not forcing you take what we have'),
(63, 6, 'Men Cloth', 'Finding the right balance between comfort and style in the variety of clothe.\r\nat cheap price and of high quality customized.\r\n place state what you would like to sused and then we will do it for you \r\nwe provide what you are looking for, but not forcing you take what we have'),
(64, 6, 'Women Cloth', 'Finding the right balance between comfort and style in the variety of clothe.\r\nat cheap price and of high quality customized.\r\n place state what you would like to sused and then we will do it for you \r\nwe provide what you are looking for, but not forcing you take what we have'),
(65, 6, 'Designer sleeves', 'Finding the right balance between comfort and style in the variety of clothe.\r\nat cheap price and of high quality customized.\r\n place state what you would like to sused and then we will do it for you \r\nwe provide what you are looking for, but not forcing you take what we have'),
(66, 6, 'School Uniform', 'Finding the right balance between comfort and style in the variety of clothe.\r\nat cheap price and of high quality customized.\r\n place state what you would like to sused and then we will do it for you \r\nwe provide what you are looking for, but not forcing you take what we have'),
(67, 6, 'Sport Fits', 'Finding the right balance between comfort and style in the variety of clothe.\r\nat cheap price and of high quality customized.\r\n place state what you would like to sused and then we will do it for you \r\nwe provide what you are looking for, but not forcing you take what we have'),
(68, 6, 'Shirt and T-shirt', 'Finding the right balance between comfort and style in the variety of clothe.\r\nat cheap price and of high quality customized.\r\n place state what you would like to sused and then we will do it for you \r\nwe provide what you are looking for, but not forcing you take what we have'),
(69, 6, 'Other', 'Finding the right balance between comfort and style in the variety of clothe.\r\nat cheap price and of high quality customized.\r\n place state what you would like to sused and then we will do it for you \r\nwe provide what you are looking for, but not forcing you take what we have'),
(70, 7, 'Others', 'This are material product from the industry. They are customized to for our customer and make sure that our customer are satisfied in that their need are met '),
(71, 7, 'Others', 'This are material product from the industry. They are customized to for our customer and make sure that our customer are satisfied in that their need are met '),
(72, 7, 'Others', 'This are material product from the industry. They are customized to for our customer and make sure that our customer are satisfied in that their need are met '),
(73, 7, 'Others', 'This are material product from the industry. They are customized to for our customer and make sure that our customer are satisfied in that their need are met '),
(74, 7, 'Metalic Door and Window ', 'This are material product from the industry. They are customized to for our customer and make sure that our customer are satisfied in that their need are met '),
(75, 7, 'Round Barbecue Grill ', 'This are material product from the industry. They are customized to for our customer and make sure that our customer are satisfied in that their need are met '),
(76, 7, 'Metaric Boxes', 'This are material product from the industry. They are customized to for our customer and make sure that our customer are satisfied in that their need are met '),
(77, 7, 'Metallic Trolley ', 'This are material product from the industry. They are customized to for our customer and make sure that our customer are satisfied in that their need are met '),
(78, 7, 'Gas Burner Grill', 'This are material product from the industry. They are customized to for our customer and make sure that our customer are satisfied in that their need are met '),
(79, 7, 'Home Gates', 'This are material product from the industry. They are customized to for our customer and make sure that our customer are satisfied in that their need are met '),
(80, 7, 'Other', 'This are material product from the industry. They are customized to for our customer and make sure that our customer are satisfied in that their need are met '),
(81, 8, 'Storage Tank', 'This are material product from the industry. They are customized to for our customer and make sure that our customer are satisfied in that their need are met '),
(82, 8, 'Milking Can', 'This are material product from the industry. They are customized to for our customer and make sure that our customer are satisfied in that their need are met '),
(83, 8, 'Chicken feeder and pot', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.'),
(84, 8, 'Metallic jiko', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.'),
(85, 8, 'Metallic Stand', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.'),
(86, 8, 'Home Cabinet', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.'),
(87, 8, 'Other', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.'),
(88, 8, 'Other', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.'),
(89, 8, 'Other', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.'),
(91, 8, 'Other', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.'),
(92, 8, 'Other', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.'),
(93, 8, 'Other', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.');

-- --------------------------------------------------------

--
-- Table structure for table `contact_support`
--

CREATE TABLE `contact_support` (
  `contact_id` int(11) NOT NULL,
  `contact_email` text NOT NULL,
  `contact_heading` text NOT NULL,
  `contact_desc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_support`
--

INSERT INTO `contact_support` (`contact_id`, `contact_email`, `contact_heading`, `contact_desc`) VALUES
(1, 'simpledaniel.1818@gmail.com', 'Submit A Query Request', 'If you have any question, please feel free to contact us, our customer service center is working for you 24/7');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `coupon_id` int(11) NOT NULL,
  `proposal_id` int(11) NOT NULL,
  `coupon_title` text NOT NULL,
  `coupon_price` int(11) NOT NULL,
  `coupon_code` text NOT NULL,
  `coupon_limit` int(11) NOT NULL,
  `coupon_used` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`coupon_id`, `proposal_id`, `coupon_title`, `coupon_price`, `coupon_code`, `coupon_limit`, `coupon_used`) VALUES
(1, 5, 'Back To School', 700, '12346', 10, 6),
(2, 3, 'Agriculture', 400, 'agri1', 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `delivery_times`
--

CREATE TABLE `delivery_times` (
  `delivery_id` int(11) NOT NULL,
  `delivery_title` text NOT NULL,
  `delivery_proposal_title` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `delivery_times`
--

INSERT INTO `delivery_times` (`delivery_id`, `delivery_title`, `delivery_proposal_title`) VALUES
(1, 'Up to 24 hours', '1 Day'),
(2, 'Up to 3 days', '3 Days'),
(3, 'Up to 5 days', '5 Days'),
(4, 'Up to 7 days', '7 Days'),
(5, 'Up To 10 Days', '10 Days');

-- --------------------------------------------------------

--
-- Table structure for table `enquiry_types`
--

CREATE TABLE `enquiry_types` (
  `enquiry_id` int(11) NOT NULL,
  `enquiry_title` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `enquiry_types`
--

INSERT INTO `enquiry_types` (`enquiry_id`, `enquiry_title`) VALUES
(1, 'Order Support'),
(2, 'Review Removal'),
(3, 'Account Support'),
(4, 'Report A Bug!'),
(5, 'Feature Support');

-- --------------------------------------------------------

--
-- Table structure for table `favorites`
--

CREATE TABLE `favorites` (
  `favorite_id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `proposal_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `featured_proposals`
--

CREATE TABLE `featured_proposals` (
  `featured_id` int(11) NOT NULL,
  `proposal_id` int(11) NOT NULL,
  `end_date` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `footer_links`
--

CREATE TABLE `footer_links` (
  `link_id` int(11) NOT NULL,
  `link_title` text NOT NULL,
  `link_url` text NOT NULL,
  `link_section` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `footer_links`
--

INSERT INTO `footer_links` (`link_id`, `link_title`, `link_url`, `link_section`) VALUES
(1, 'Agriculture', 'http://127.0.0.1:8090/juakali/category.php?cat_id=1', 'categories'),
(2, 'Kitchen appliances ', 'http://127.0.0.1:8090/juakali/category.php?cat_id=2', 'categories'),
(3, 'Building and Construction', 'http://127.0.0.1:8090/juakali/category.php?cat_id=3', 'categories'),
(4, 'Furniture and house Appliances', 'http://127.0.0.1:8090/juakali/category.php?cat_id=4', 'categories'),
(5, 'Designer Shoes', 'http://127.0.0.1:8090/jukali/category.php?cat_id=5', 'categories'),
(6, 'Fitting Clothe ', 'http://127.0.0.1:8090/juakali/category.php?cat_id=6', 'categories'),
(7, 'Welding and metal dealer', 'http://127.0.0.1:8090/juakali/category.php?cat_id=7', 'categories'),
(8, 'Domestic appliances', 'http://127.0.0.1:8090/juakali/category.php?cat_id=8', 'categories'),
(9, 'Term and Conditions', 'http://127.0.0.1:8090/juakali/terms.php', 'about'),
(10, 'Customer Support', 'http://127.0.0.1:8090/juakali/contact.php', 'support'),
(11, '<i class=\"fab fa-google-plus-g\"></i>  Google+', '#', 'follow'),
(12, '<i class=\"fab fa-twitter\"> </i> Twitter', '#', 'follow'),
(13, '<i class=\"fab fa-facebook\"></i> Facebook', '#', 'follow'),
(14, ' <i class=\"fab fa-linkedin\"></i> Linkedin', '#', 'follow'),
(15, '<i class=\"fab fa-pinterest\"></i>Pinterest', '#', 'follow');

-- --------------------------------------------------------

--
-- Table structure for table `general_settings`
--

CREATE TABLE `general_settings` (
  `id` int(11) NOT NULL,
  `site_title` text NOT NULL,
  `site_desc` text NOT NULL,
  `site_keywords` text NOT NULL,
  `site_author` text NOT NULL,
  `site_url` text NOT NULL,
  `site_email_address` text NOT NULL,
  `level_one_rating` int(11) NOT NULL,
  `level_one_orders` int(11) NOT NULL,
  `level_two_rating` int(11) NOT NULL,
  `level_two_orders` int(11) NOT NULL,
  `level_top_rating` int(11) NOT NULL,
  `level_top_orders` int(11) NOT NULL,
  `enable_referrals` text NOT NULL,
  `referral_money` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `general_settings`
--

INSERT INTO `general_settings` (`id`, `site_title`, `site_desc`, `site_keywords`, `site_author`, `site_url`, `site_email_address`, `level_one_rating`, `level_one_orders`, `level_two_rating`, `level_two_orders`, `level_top_rating`, `level_top_orders`, `enable_referrals`, `referral_money`) VALUES
(1, 'Jua Kali mall Project', 'Jua Kali Mall Project', 'jua kali product, building material, agriculture, kitchen, shoe, clothe', 'Jua kali product developers', 'http://127.0.0.1:8090/juakali', 'simpledaniel.1818@gmail.com', 90, 10, 93, 30, 95, 60, 'yes', 100);

-- --------------------------------------------------------

--
-- Table structure for table `hide_seller_messages`
--

CREATE TABLE `hide_seller_messages` (
  `id` int(11) NOT NULL,
  `hider_id` int(11) NOT NULL,
  `hide_seller_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hide_seller_messages`
--

INSERT INTO `hide_seller_messages` (`id`, `hider_id`, `hide_seller_id`) VALUES
(1, 4, 8),
(2, 4, 3),
(3, 4, 0),
(4, 4, 12);

-- --------------------------------------------------------

--
-- Table structure for table `home_section`
--

CREATE TABLE `home_section` (
  `section_id` int(11) NOT NULL,
  `section_title` text NOT NULL,
  `section_short_desc` text NOT NULL,
  `section_desc` text NOT NULL,
  `section_button` text NOT NULL,
  `section_button_url` text NOT NULL,
  `section_image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_section`
--

INSERT INTO `home_section` (`section_id`, `section_title`, `section_short_desc`, `section_desc`, `section_button`, `section_button_url`, `section_image`) VALUES
(1, 'Searching for a designer product and you will find them here. welcome   ', 'Call for action and our 24-hour team will respond quickly', 'We have a creative programme which provide quality services to buyers at affordable rates and buyers will also have the option to cancel the orders if they don\'t like the work and request for a more user customized products ', 'Contact Us', 'contact.php', 'platform-image.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `inbox_messages`
--

CREATE TABLE `inbox_messages` (
  `message_id` int(11) NOT NULL,
  `message_group_id` int(11) NOT NULL,
  `message_sender` int(11) NOT NULL,
  `message_receiver` int(11) NOT NULL,
  `message_offer_id` int(11) NOT NULL,
  `message_desc` text NOT NULL,
  `message_file` text NOT NULL,
  `message_date` text NOT NULL,
  `message_status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inbox_messages`
--

INSERT INTO `inbox_messages` (`message_id`, `message_group_id`, `message_sender`, `message_receiver`, `message_offer_id`, `message_desc`, `message_file`, `message_date`, `message_status`) VALUES
(2, 54321, 2, 4, 0, 'I need to combine 13 PDF document of about 10 pages each into 1 PDF , can you do it?', '', '11:40, September 22,2019', 'read'),
(3, 67874321, 3, 4, 0, 'Hi, need of something well customized web based application', '', '11:40, October 22,2019', 'read'),
(5, 67874321, 4, 3, 0, 'no coma', 'livechat-1.png', '01:51: October 13, 2019', 'read'),
(6, 67874321, 4, 3, 0, 'oky', '', '09:59: October 13, 2019', 'read'),
(7, 67874321, 4, 3, 1, 'i am sending an offer', '', '12:50: October 14, 2019', 'read'),
(8, 678743212, 4, 3, 0, 'I need my work to be done please', '', '12:50: October 22, 2019', 'read'),
(9, 12345, 8, 4, 0, 'I will send the offer to be done please', '', '17:50: October 22, 2019', 'read'),
(10, 397382108, 4, 12, 0, 'which languages do you speak', '', '11:49: December 06, 2019', 'read'),
(11, 1525482458, 12, 4, 0, 'hello sir i saw your work can you do such for me!', '', '01:11: December 06, 2019', 'read'),
(12, 1525482458, 12, 4, 0, 'can you reply men!', '', '01:39: December 06, 2019', 'read'),
(13, 1525482458, 12, 4, 0, 'still waiting!', '', '01:51: December 06, 2019', 'read'),
(14, 1525482458, 4, 12, 0, 'ok', '', '02:03: December 06, 2019', 'read');

-- --------------------------------------------------------

--
-- Table structure for table `inbox_sellers`
--

CREATE TABLE `inbox_sellers` (
  `inbox_seller_id` int(11) NOT NULL,
  `message_group_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `offer_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `message_status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inbox_sellers`
--

INSERT INTO `inbox_sellers` (`inbox_seller_id`, `message_group_id`, `message_id`, `offer_id`, `sender_id`, `receiver_id`, `message_status`) VALUES
(1, 12345, 9, 0, 1, 4, 'read'),
(2, 54321, 2, 0, 4, 3, 'read'),
(5, 67874321, 7, 2, 4, 3, 'read'),
(6, 678743212, 8, 3, 4, 3, 'read'),
(7, 397382108, 10, 0, 4, 12, 'read'),
(8, 1525482458, 14, 0, 4, 12, 'read'),
(9, 1104167405, 0, 0, 0, 3, 'empty'),
(10, 691143155, 0, 0, 11, 4, 'empty');

-- --------------------------------------------------------

--
-- Table structure for table `language_relation`
--

CREATE TABLE `language_relation` (
  `relation_id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `language_level` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `language_relation`
--

INSERT INTO `language_relation` (`relation_id`, `seller_id`, `language_id`, `language_level`) VALUES
(1, 4, 1, 'Basic'),
(2, 3, 2, 'Conversational'),
(3, 3, 3, 'Fluent'),
(4, 4, 4, 'Native or Bilingual'),
(7, 4, 2, 'conversational'),
(8, 12, 1, 'conversational');

-- --------------------------------------------------------

--
-- Table structure for table `messages_offers`
--

CREATE TABLE `messages_offers` (
  `offer_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `proposal_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `delivery_time` text NOT NULL,
  `amount` text NOT NULL,
  `status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages_offers`
--

INSERT INTO `messages_offers` (`offer_id`, `sender_id`, `proposal_id`, `order_id`, `description`, `delivery_time`, `amount`, `status`) VALUES
(1, 4, 9, 0, 'do as you said\r\n', '1 Day', '700', 'accepted');

-- --------------------------------------------------------

--
-- Table structure for table `my_buyers`
--

CREATE TABLE `my_buyers` (
  `id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `buyer_id` int(11) NOT NULL,
  `completed_orders` int(11) NOT NULL,
  `amount_spent` int(11) NOT NULL,
  `last_order_date` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `my_buyers`
--

INSERT INTO `my_buyers` (`id`, `seller_id`, `buyer_id`, `completed_orders`, `amount_spent`, `last_order_date`) VALUES
(1, 3, 4, 0, 0, 'September 28, 2019'),
(2, 3, 4, 0, 0, 'September 28, 2019'),
(3, 4, 3, 1, 1000, 'September 28, 2019'),
(4, 4, 5, 1, 1500, 'September 28, 2019'),
(5, 4, 1, 1, 701, 'September 29, 2019'),
(6, 4, 3, 1, 501, 'October 09, 2019'),
(7, 2, 4, 1, 550, 'October 10, 2019'),
(8, 4, 4, 1, 700, 'October 17, 2019'),
(9, 4, 4, 1, 700, 'October 17, 2019'),
(10, 4, 3, 1, 1500, 'October 18, 2019'),
(11, 4, 7, 1, 500, 'October 30, 2019'),
(12, 4, 1, 1, 2000, 'October 30, 2019'),
(13, 1, 4, 1, 1000, 'December 05, 2019'),
(14, 4, 11, 1, 2000, 'December 06, 2019'),
(15, 3, 4, 1, 1500, 'December 06, 2019'),
(16, 1, 4, 1, 1000, 'December 10, 2019'),
(17, 2, 3, 1, 550, 'December 10, 2019'),
(18, 3, 11, 1, 501, 'December 13, 2019'),
(19, 4, 11, 1, 1000, 'December 14, 2019'),
(20, 4, 11, 1, 1000, 'December 15, 2019');

-- --------------------------------------------------------

--
-- Table structure for table `my_sellers`
--

CREATE TABLE `my_sellers` (
  `id` int(11) NOT NULL,
  `buyer_id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `completed_orders` int(11) NOT NULL,
  `amount_spent` int(11) NOT NULL,
  `last_order_date` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `my_sellers`
--

INSERT INTO `my_sellers` (`id`, `buyer_id`, `seller_id`, `completed_orders`, `amount_spent`, `last_order_date`) VALUES
(1, 4, 3, 0, 0, 'September 28, 2019'),
(2, 2, 4, 1, 1500, 'September 28, 2019'),
(3, 4, 1, 1, 1000, 'September 28, 2019'),
(4, 4, 3, 0, 0, 'September 28, 2019'),
(5, 5, 4, 1, 701, 'September 29, 2019'),
(6, 3, 4, 1, 501, 'October 09, 2019'),
(7, 4, 2, 1, 550, 'October 10, 2019'),
(8, 4, 4, 1, 700, 'October 17, 2019'),
(9, 4, 4, 1, 700, 'October 17, 2019'),
(10, 3, 4, 1, 1500, 'October 18, 2019'),
(11, 7, 4, 1, 500, 'October 30, 2019'),
(12, 1, 4, 1, 2000, 'October 30, 2019'),
(13, 4, 1, 1, 1000, 'December 05, 2019'),
(14, 11, 4, 1, 2000, 'December 06, 2019'),
(15, 4, 3, 1, 1500, 'December 06, 2019'),
(16, 4, 1, 1, 1000, 'December 10, 2019'),
(17, 3, 2, 1, 550, 'December 10, 2019'),
(18, 11, 3, 1, 501, 'December 13, 2019'),
(19, 11, 4, 1, 1000, 'December 14, 2019'),
(20, 11, 4, 1, 1000, 'December 15, 2019');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `notification_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `reason` text NOT NULL,
  `date` text NOT NULL,
  `status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`notification_id`, `receiver_id`, `sender_id`, `order_id`, `reason`, `date`, `status`) VALUES
(5, 3, 4, 14, 'order', 'September 28, 2019', 'unread'),
(6, 3, 4, 15, 'order', 'September 28, 2019', 'unread'),
(7, 1, 4, 16, 'order', 'September 28, 2019', 'unread'),
(8, 3, 4, 17, 'order', 'September 28, 2019', 'unread'),
(9, 6, 4, 18, 'order', 'September 29, 2019', 'unread'),
(10, 3, 4, 17, 'order_message', '04:43: Oct 03, 2019', 'unread'),
(12, 3, 4, 17, 'order_message', '12:11: Oct 04, 2019', 'unread'),
(13, 4, 3, 17, 'order_message', '12:15: Oct 04, 2019', 'unread'),
(14, 3, 4, 17, 'order_message', '12:16: Oct 04, 2019', 'unread'),
(15, 4, 3, 17, 'order_message', '12:23: Oct 04, 2019', 'unread'),
(16, 4, 3, 17, 'order_delivered', '01:10 Oct 04 2019', 'unread'),
(17, 3, 4, 17, 'order_revision', '09:31: Oct 04, 2019', 'unread'),
(18, 4, 3, 17, 'cancellation request', '12:08: Oct 05, 2019', 'unread'),
(19, 3, 4, 17, 'decline_cancellation_request', '01:37: Oct 05, 2019', 'unread'),
(20, 3, 4, 17, 'cancellation request', '07:28: Oct 05, 2019', 'unread'),
(21, 4, 3, 17, 'accept_cancellation_request', '07:29: Oct 05, 2019', 'unread'),
(22, 4, 8, 7, 'order_message', '08:15: Oct 05, 2019', 'read'),
(23, 4, 8, 7, 'cancellation request', '08:35: Oct 05, 2019', 'read'),
(24, 8, 4, 7, 'decline_cancellation_request', '08:36: Oct 05, 2019', 'unread'),
(25, 4, 6, 8, 'order_message', '10:22: Oct 05, 2019', 'unread'),
(26, 6, 4, 8, 'order_delivered', '11:10 Oct 05 2019', 'unread'),
(27, 4, 6, 8, 'order_completed', '11:23: Oct 05, 2019', 'unread'),
(28, 4, 6, 8, 'buyer_order_review', '04:10: Oct 05 2019', 'unread'),
(29, 4, 3, 19, 'order', 'October 09, 2019', 'unread'),
(30, 4, 3, 19, 'order_message', '12:08: Oct 10, 2019', 'read'),
(31, 3, 4, 19, 'order_message', '12:10: Oct 10, 2019', 'unread'),
(32, 4, 3, 19, 'order_message', '12:11: Oct 10, 2019', 'read'),
(33, 3, 4, 19, 'order_delivered', '12:10 Oct 10 2019', 'unread'),
(34, 4, 3, 19, 'order_completed', '12:19: Oct 10, 2019', 'read'),
(35, 2, 4, 20, 'order', 'October 10, 2019', 'unread'),
(36, 2, 4, 20, 'order_message', '01:36: Oct 11, 2019', 'unread'),
(37, 4, 4, 22, 'order', 'October 17, 2019', 'read'),
(38, 4, 3, 24, 'order', 'October 18, 2019', 'read'),
(39, 4, 3, 24, 'order_message', '11:25: Oct 18, 2019', 'read'),
(44, 2, 4, 20, 'cancelled_by_customer_support', '11:46: Oct 27, 2019', 'unread'),
(45, 4, 7, 25, 'order', 'October 30, 2019', 'read'),
(46, 4, 7, 25, 'order_message', '05:47: Oct 30, 2019', 'read'),
(47, 7, 4, 25, 'order_message', '05:47: Oct 30, 2019', 'unread'),
(48, 7, 4, 25, 'order_delivered', '06:10 Oct 30 2019', 'unread'),
(49, 4, 7, 25, 'order_completed', '06:19: Oct 30, 2019', 'read'),
(50, 4, 7, 25, 'buyer_order_review', '06:10: Oct 30 2019', 'read'),
(51, 4, 1, 26, 'order', 'October 30, 2019', 'read'),
(52, 4, 1, 26, 'order_message', '06:42: Oct 30, 2019', 'read'),
(53, 1, 4, 26, 'order_delivered', '06:10 Oct 30 2019', 'unread'),
(54, 4, 1, 26, 'order_completed', '07:43: Oct 30, 2019', 'read'),
(55, 4, 1, 26, 'buyer_order_review', '07:10: Oct 30 2019', 'read'),
(56, 1, 4, 27, 'order', 'December 05, 2019', 'read'),
(57, 4, 1, 27, 'order_message', '12:16: Dec 06, 2019', 'read'),
(58, 1, 4, 27, 'order_message', '12:17: Dec 06, 2019', 'unread'),
(59, 4, 1, 27, 'order_delivered', '12:12 Dec 06 2019', 'read'),
(60, 1, 4, 27, 'order_completed', '12:20: Dec 06, 2019', 'unread'),
(61, 1, 4, 27, 'buyer_order_review', '12:12: Dec 06 2019', 'unread'),
(62, 4, 11, 28, 'order', 'December 06, 2019', 'read'),
(63, 4, 11, 28, 'order_message', '10:05: Dec 06, 2019', 'read'),
(64, 11, 4, 28, 'order_message', '10:12: Dec 06, 2019', 'read'),
(65, 11, 4, 28, 'order_delivered', '10:12 Dec 06 2019', 'read'),
(66, 4, 11, 28, 'order_completed', '10:22: Dec 06, 2019', 'read'),
(67, 4, 11, 28, 'buyer_order_review', '10:12: Dec 06 2019', 'read'),
(68, 3, 4, 29, 'order', 'December 06, 2019', 'unread'),
(69, 3, 4, 29, 'order_message', '11:56: Dec 06, 2019', 'unread'),
(70, 4, 3, 24, 'cancelled_by_customer_support', '03:15: Dec 06, 2019', 'read'),
(71, 1, 4, 30, 'order', 'December 10, 2019', 'unread'),
(72, 2, 3, 32, 'order', 'December 10, 2019', 'unread'),
(73, 3, 11, 34, 'order', 'December 13, 2019', 'unread'),
(74, 4, 11, 33, 'order_message', '09:42: Dec 13, 2019', 'read'),
(75, 11, 4, 33, 'order_message', '10:18: Dec 13, 2019', 'unread'),
(76, 4, 11, 33, 'order_message', '10:22: Dec 13, 2019', 'unread'),
(77, 11, 4, 33, 'order_delivered', '10:12 Dec 13 2019', 'unread'),
(78, 4, 11, 33, 'order_revision', '10:37: Dec 13, 2019', 'unread'),
(79, 4, 11, 33, 'order_message', '10:57: Dec 13, 2019', 'unread'),
(80, 11, 4, 33, 'order_message', '11:00: Dec 13, 2019', 'read'),
(81, 4, 11, 33, 'order_message', '11:00: Dec 13, 2019', 'unread'),
(82, 11, 4, 33, 'order_delivered', '11:12 Dec 13 2019', 'read'),
(83, 4, 11, 33, 'order_completed', '11:05: Dec 13, 2019', 'unread'),
(84, 4, 11, 33, 'buyer_order_review', '11:12: Dec 13 2019', 'unread'),
(85, 4, 11, 35, 'order', 'December 14, 2019', 'unread'),
(86, 4, 11, 37, 'order', 'December 15, 2019', 'unread');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `order_number` text NOT NULL,
  `order_duration` text NOT NULL,
  `order_time` text NOT NULL,
  `order_date` text NOT NULL,
  `order_description` text NOT NULL,
  `seller_id` int(11) NOT NULL,
  `buyer_id` int(11) NOT NULL,
  `proposal_id` int(11) NOT NULL,
  `order_price` int(10) NOT NULL,
  `order_qty` int(10) NOT NULL,
  `order_fee` int(10) NOT NULL,
  `order_active` text NOT NULL,
  `order_status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `order_number`, `order_duration`, `order_time`, `order_date`, `order_description`, `seller_id`, `buyer_id`, `proposal_id`, `order_price`, `order_qty`, `order_fee`, `order_active`, `order_status`) VALUES
(1, '754152917456', '1 Day', 'Oct 02, 2019 02:46:29', 'September 28, 2019', '', 1, 4, 1, 1500, 1, 0, 'no', 'completed'),
(2, '754152917345', '1 Day', 'Oct 02, 2019 02:46:29', 'September 28, 2019', '', 2, 3, 2, 1500, 1, 0, 'no', 'completed'),
(3, '754152917754', '1 Day', 'Oct 02, 2019 02:46:29', 'September 28, 2019', '', 2, 4, 3, 1500, 1, 0, 'no', 'completed'),
(4, '75415291765', '1 Day', 'Oct 02, 2019 02:46:29', 'September 28, 2019', '', 2, 5, 3, 1500, 1, 0, 'no', 'completed'),
(6, '75415291796', '1 Day', 'Oct 02, 2019 02:46:29', 'September 28, 2019', '', 4, 7, 3, 1500, 1, 0, 'no', 'delivered'),
(7, '7541529179887', '5 Day', 'Oct 02, 2019 02:46:29', 'Sep 28, 2019', '', 4, 8, 2, 1500, 1, 0, 'no', 'cancelled'),
(8, '754152917098', '1 Day', 'Oct 06, 2019 02:46:29', 'October 05, 2019', '', 4, 6, 7, 1500, 1, 0, 'no', 'completed'),
(9, '7541529176', '1 Day', 'Oct 02, 2019 02:46:29', 'September 28, 2019', '', 4, 7, 7, 1500, 1, 0, 'no', 'completed'),
(10, '7541529173', '1 Day', 'Oct 02, 2019 02:46:29', 'September 28, 2019', '', 4, 3, 7, 1500, 1, 0, 'no', 'completed'),
(11, '754152917', '1 Day', 'Oct 08, 2019 02:46:29', 'September 28, 2019', '', 3, 4, 4, 1500, 1, 0, 'yes', 'pending'),
(12, '1465665216', '1 Day', 'Sep 14, 2019 02:46:29', 'September 28, 2019', '', 3, 4, 4, 1500, 1, 0, 'yes', 'pending'),
(13, '2059656713', '1 Day', 'Oct 20, 2019 02:46:29', 'September 28, 2019', '', 3, 4, 4, 1500, 1, 0, 'yes', 'pending'),
(14, '905903938', '1 Day', 'Oct 09, 2019 02:46:29', 'September 28, 2019', '', 3, 4, 4, 1500, 1, 0, 'yes', 'pending'),
(15, '692192648', '1 Day', 'Oct 04, 2019 12:46:29', 'September 28, 2019', '', 3, 4, 4, 1500, 1, 0, 'yes', 'pending'),
(16, '1354998742', '1 Day', 'Oct 02, 2019 05:46:29', 'September 28, 2019', '', 1, 4, 1, 1000, 1, 0, 'yes', 'pending'),
(17, '111571511', '7 Day', 'Oct 04, 2019 02:46:29', 'September 28, 2019', '', 3, 4, 4, 1500, 1, 0, 'no', 'cancelled'),
(18, '2066273524', '3 Days', 'Oct 02, 2019 02:46:29', 'September 29, 2019', '', 6, 4, 5, 701, 1, 0, 'yes', 'pending'),
(19, '116157722', '3 Days', 'Jan 01, 1970 01:00:00', 'October 09, 2019', '', 4, 3, 7, 501, 1, 100, 'no', 'completed'),
(20, '1698955868', '3 Days', 'Oct 14, 2019 12:36:44', 'October 10, 2019', 'I will do an ecommerce using php framework laravel 5.8 and above with vue js', 2, 4, 2, 550, 1, 0, 'no', 'cancelled'),
(21, '367582773', '1 Day', 'Oct 18, 2019 11:43:11', 'October 17, 2019', 'do as you said\r\n', 4, 4, 9, 700, 1, 0, 'yes', 'pending'),
(22, '243717578', '1 Day', 'Oct 18, 2019 11:44:08', 'October 17, 2019', 'do as you said\r\n', 4, 4, 9, 700, 1, 0, 'yes', 'pending'),
(23, '25490569', '7 Days', 'Oct 25, 2019 05:44:17', 'October 18, 2019', '', 4, 3, 6, 2000, 1, 0, 'yes', 'pending'),
(24, '1394148392', '3 Days', 'Oct 21, 2019 10:25:29', 'October 18, 2019', '', 4, 3, 9, 1500, 1, 0, 'no', 'cancelled'),
(25, '76210136', '7 Days', 'Nov 06, 2019 03:47:11', 'October 30, 2019', '', 4, 7, 8, 500, 1, 0, 'no', 'completed'),
(26, '1789105730', '7 Days', 'Nov 06, 2019 04:42:21', 'October 30, 2019', '', 4, 1, 6, 2000, 1, 0, 'no', 'completed'),
(27, '1952730924', '1 Day', 'Dec 06, 2019 10:17:23', 'December 05, 2019', '', 1, 4, 1, 1000, 1, 0, 'no', 'completed'),
(28, '945541262', '7 Days', 'Dec 13, 2019 08:05:48', 'December 06, 2019', '', 4, 11, 6, 2000, 1, 0, 'no', 'completed'),
(29, '1853071514', '1 Day', 'Dec 07, 2019 09:56:30', 'December 06, 2019', '', 3, 4, 4, 1500, 1, 0, 'yes', 'progress'),
(30, '363704359', '1 Day', 'Dec 11, 2019 07:04:58', 'December 10, 2019', '', 1, 4, 1, 1000, 1, 100, 'yes', 'pending'),
(31, '1311236749', '7 Days', 'Dec 17, 2019 04:01:10', 'December 10, 2019', '', 2, 4, 2, 550, 1, 100, 'yes', 'pending'),
(32, '997078383', '7 Days', 'Dec 17, 2019 05:42:19', 'December 10, 2019', '', 2, 3, 2, 550, 1, 100, 'yes', 'pending'),
(33, '1500530261', '7 Days', 'Dec 20, 2019 07:42:02', 'December 13, 2019', '', 4, 11, 8, 501, 1, 0, 'no', 'completed'),
(34, '1179988920', '1 Day', 'Dec 14, 2019 05:50:57', 'December 13, 2019', '', 3, 11, 3, 501, 1, 0, 'yes', 'pending'),
(35, '228261784', '3 Days', 'Dec 17, 2019 10:13:40', 'December 14, 2019', '', 4, 11, 5, 1000, 1, 0, 'yes', 'pending'),
(36, '1697666283', '3 Days', 'Dec 18, 2019 08:58:53', 'December 15, 2019', '', 4, 11, 5, 1000, 1, 100, 'yes', 'pending'),
(37, '636384992', '3 Days', 'Dec 18, 2019 08:59:39', 'December 15, 2019', '', 4, 11, 5, 1000, 1, 100, 'yes', 'pending'),
(38, '1592373677', '7 Days', 'Dec 22, 2019 09:00:01', 'December 15, 2019', '', 2, 11, 2, 551, 1, 0, 'yes', 'pending');

-- --------------------------------------------------------

--
-- Table structure for table `order_conversations`
--

CREATE TABLE `order_conversations` (
  `c_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `file` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `reason` text NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_conversations`
--

INSERT INTO `order_conversations` (`c_id`, `order_id`, `sender_id`, `message`, `file`, `date`, `reason`, `status`) VALUES
(1, 17, 4, 'Hi!, kimonyoski send your...send me your work!', 'home-header-background.jpg', '04:43: Oct 03, 2019', '', 'message'),
(2, 17, 3, 'it oky', '', '12:08: Oct 04, 2019', '', 'message'),
(3, 17, 4, 'thank you', '', '12:11: Oct 04, 2019', '', 'message'),
(4, 17, 3, 'i got your message', '', '12:15: Oct 04, 2019', '', 'message'),
(5, 17, 4, 'good this are now coming', '', '12:16: Oct 04, 2019', '', 'message'),
(6, 17, 3, 'bey', '', '12:23: Oct 04, 2019', '', 'message'),
(7, 17, 3, 'this is the result of what you consulted me', 'home-header-background.jpg', '01:10 Oct 04 2019', '', 'message'),
(8, 17, 4, 'yes i  have seen the error i will correct ', '', '09:31: Oct 04, 2019', '', 'revision'),
(9, 17, 3, 'Please cancel this order', '', '12:08: Oct 05, 2019', 'Buyer is not Responding', 'decline_cancellation_request'),
(10, 17, 4, 'thank you have accepted cancellation', '', '07:28: Oct 05, 2019', 'Expert/Seller tells me to cancel this order.', 'accept_cancellation_request'),
(11, 7, 8, 'Hi Daniel, i have given this order mistakenly... please cancel! ', 'threat-image.jpg', '08:15: Oct 05, 2019', '', 'message'),
(12, 7, 8, 'So i have given this order mistakenly Please cancel it', '', '08:35: Oct 05, 2019', 'Expert/Seller tells me to cancel this order.', 'decline_cancellation_request'),
(13, 7, 0, '', '', '', '', 'cancelled_by_customer_support'),
(14, 8, 6, 'Work will be done ', 'ATTACHMENT REPORT FORMAT.pdf', '10:22: Oct 05, 2019', '', 'message'),
(15, 8, 4, 'This is the result of your work', 'ATTACHMENT REPORT FORMAT.pdf', '11:10 Oct 05 2019', '', 'message'),
(16, 19, 3, 'just the way it is plz', 'no-image.jpg', '12:08: Oct 10, 2019', '', 'message'),
(17, 19, 4, 'Hi sir This is your result!,,, thank you', 'no-video.jpg', '12:10: Oct 10, 2019', '', 'message'),
(18, 19, 3, 'thank you', '', '12:11: Oct 10, 2019', '', 'message'),
(19, 19, 4, 'you order is very ok now thank you', '', '12:10 Oct 10 2019', '', 'message'),
(20, 20, 4, 'complete as you can', '', '01:36: Oct 11, 2019', '', 'message'),
(21, 24, 3, 'do the job bro', '', '11:25: Oct 18, 2019', '', 'message'),
(26, 20, 4, 'Order Cancellation By Customer Support', '', '11:46: Oct 27, 2019', '', 'cancelled_by_customer_support'),
(27, 25, 7, 'due as expected', 'wordpress-3.jpg', '05:47: Oct 30, 2019', '', 'message'),
(28, 25, 4, 'thanks', '', '05:47: Oct 30, 2019', '', 'message'),
(29, 25, 4, 'THIS IS THE RESULT OF YOUR WORK!', 'wordpress-3.jpg', '06:10 Oct 30 2019', '', 'message'),
(30, 26, 1, 'please,,, as usual ', 'andorid-2.jpg', '06:42: Oct 30, 2019', '', 'message'),
(31, 26, 4, 'FINAL FINE RESULT IS HEAR', 'wordpress-2.jpg', '06:10 Oct 30 2019', '', 'message'),
(32, 27, 1, 'send', '', '12:16: Dec 06, 2019', '', 'message'),
(33, 27, 4, 'sawa', '', '12:17: Dec 06, 2019', '', 'message'),
(34, 27, 1, 'have done the work', 'irm1.PNG', '12:12 Dec 06 2019', '', 'message'),
(35, 28, 11, 'start working now sir example below', 'adcReport.docx', '10:05: Dec 06, 2019', '', 'message'),
(36, 28, 4, 'thank i am sending the result....', 'Advantage of Resource Management Process.docx', '10:12: Dec 06, 2019', '', 'message'),
(37, 28, 4, 'test....the ', 'decisionTreeIssue.PNG', '10:12 Dec 06 2019', '', 'message'),
(38, 29, 4, 'get start sir,', '', '11:56: Dec 06, 2019', '', 'message'),
(39, 24, 3, 'Order Cancellation By Customer Support', '', '03:15: Dec 06, 2019', '', 'cancelled_by_customer_support'),
(40, 33, 11, 'Hi, there i want a gray skirt media waist like 32 and length of 35. make sure that the product has quality material. be conscience with time .Attache is the color ', 'writting.jpg', '09:42: Dec 13, 2019', '', 'message'),
(41, 33, 4, 'Thank you for your instruction order will be delivered within time see more sample attached', 'translate-1.jpg', '10:18: Dec 13, 2019', '', 'message'),
(42, 33, 11, 'Thank you eagerly waiting for that design daniel.', '', '10:22: Dec 13, 2019', '', 'message'),
(43, 33, 4, 'Hello, i have followed your instruction and now this is the end result attached.confirm so that i deliver.', 'wordpress-2.jpg', '10:12 Dec 13 2019', '', 'message'),
(44, 33, 11, 'Please make sure that the color much ', '', '10:37: Dec 13, 2019', '', 'revision'),
(45, 33, 11, 'Sorry for that am making change right now', '', '10:57: Dec 13, 2019', '', 'message'),
(46, 33, 4, 'Sorry am working on improvement right away.', '', '11:00: Dec 13, 2019', '', 'message'),
(47, 33, 11, 'Thank you', '', '11:00: Dec 13, 2019', '', 'message'),
(48, 33, 4, 'Final fine result is here', 'videosales-1.png', '11:12 Dec 13 2019', '', 'message');

-- --------------------------------------------------------

--
-- Table structure for table `payment_settings`
--

CREATE TABLE `payment_settings` (
  `id` int(11) NOT NULL,
  `commission_percentage` int(11) NOT NULL,
  `days_before_withdraw` int(11) NOT NULL,
  `withdrawal_limit` int(11) NOT NULL,
  `featured_fee` int(11) NOT NULL,
  `featured_duration` int(11) NOT NULL,
  `processing_fee` int(11) NOT NULL,
  `enable_paypal` text NOT NULL,
  `paypal_email` text NOT NULL,
  `paypal_currency_code` text NOT NULL,
  `paypal_api_username` text NOT NULL,
  `paypal_api_password` text NOT NULL,
  `paypal_api_signature` text NOT NULL,
  `paypal_app_id` text NOT NULL,
  `paypal_sandbox` text NOT NULL,
  `enable_stripe` text NOT NULL,
  `stripe_secret_key` text NOT NULL,
  `stripe_publishable_key` text NOT NULL,
  `stripe_currency_code` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_settings`
--

INSERT INTO `payment_settings` (`id`, `commission_percentage`, `days_before_withdraw`, `withdrawal_limit`, `featured_fee`, `featured_duration`, `processing_fee`, `enable_paypal`, `paypal_email`, `paypal_currency_code`, `paypal_api_username`, `paypal_api_password`, `paypal_api_signature`, `paypal_app_id`, `paypal_sandbox`, `enable_stripe`, `stripe_secret_key`, `stripe_publishable_key`, `stripe_currency_code`) VALUES
(1, 25, 10, 100, 100, 2, 100, 'yes', 'sb-o77e5176870@business.example.com', 'USD', 'sb-o77e5176870_api1.business.example.com', '65YH76W75JVYBHHD', 'A1VIC6K-zpcLy1hGXhUwUwd0OU1zAbi3gX.n8HkZyMhKJLndcDpSkimf', 'APP-80W284485P519543T', 'on', 'yes', 'sk_test_BggoyqKvTaz6N54pPfcBnI7h00zjEl5QE0', 'pk_test_xW2MvOuPFwSWtzhW3jAcBbCj00Jku3piKx', 'USD');

-- --------------------------------------------------------

--
-- Table structure for table `proposals`
--

CREATE TABLE `proposals` (
  `proposal_id` int(10) NOT NULL,
  `proposal_title` text NOT NULL,
  `proposal_url` text NOT NULL,
  `proposal_cat_id` int(10) NOT NULL,
  `proposal_child_id` int(10) NOT NULL,
  `proposal_price` int(100) NOT NULL,
  `proposal_img1` text NOT NULL,
  `proposal_img2` text NOT NULL,
  `proposal_img3` text NOT NULL,
  `proposal_img4` text NOT NULL,
  `proposal_video` text NOT NULL,
  `proposal_desc` text NOT NULL,
  `buyer_instruction` text NOT NULL,
  `proposal_tags` text NOT NULL,
  `proposal_featured` text NOT NULL,
  `proposal_seller_id` int(11) NOT NULL,
  `delivery_id` int(10) NOT NULL,
  `level_id` int(10) NOT NULL,
  `language_id` int(10) NOT NULL,
  `proposal_rating` text NOT NULL,
  `proposal_views` text NOT NULL,
  `proposal_status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `proposals`
--

INSERT INTO `proposals` (`proposal_id`, `proposal_title`, `proposal_url`, `proposal_cat_id`, `proposal_child_id`, `proposal_price`, `proposal_img1`, `proposal_img2`, `proposal_img3`, `proposal_img4`, `proposal_video`, `proposal_desc`, `buyer_instruction`, `proposal_tags`, `proposal_featured`, `proposal_seller_id`, `delivery_id`, `level_id`, `language_id`, `proposal_rating`, `proposal_views`, `proposal_status`) VALUES
(1, 'Cooking Pot', 'proposal-url-1', 2, 20, 1000, 'youtube-seo-1.jpg', 'youtube-seo-2.jpg', 'youtube-seo-3.jpg', 'youtube-seo-4.jpg', 'youtube-seo-video.mp4', 'Hey, thanks for checking my proposal. If you are finding an effective video promotion service then you are in the right place.How does it work?.I have some special tactics and ideas to make your video viral on youtube. I have several facebook fan pages based on different niches such as movies, music, arts, gaming, beauty care, health, bodybuilding, technology, cooking, lifestyle, kids care, etc. I will share your video there for driving quality and targeted traffic. Like If you have a video on technology then the video will be definitely shared on the technology page. So the chances of natural user engagements and traffics are very sure.', 'Please send your youtube account URL and sme of the promotional videos ', 'youtube,seo,viral,promotion', 'yes', 1, 1, 2, 1, '4', '35', 'active'),
(2, 'Suits', 'proposal-url-2', 6, 56, 550, 'web-development.jpg', 'web-development-2.png', '', '', '', 'We are encouraging you to join our community, we embrace creativity and concentration of the fine concept! Come with will and we provide staring wheel', 'Thank you, show case before you begun the service', 'web application,build website, codeigniter,php', 'yes', 2, 4, 2, 2, '5', '27', 'active'),
(3, 'Fork Jembe', 'proposal-url-3', 1, 1, 500, 'logo-1.jpg', 'logo-2.jpg', '', '', '', 'We are encouraging you to join our community, we embrace creativity and concentration of the fine concept! Come with will and we provide staring wheel', '', 'custom logo,typographic,design', 'yes', 3, 1, 2, 1, '5', '88', 'active'),
(4, 'Dinning Table', 'proposal-url-4', 4, 41, 1500, 'videosales-1.png', 'videosales-2.jpg', 'videosales-3jpg', '', '', 'We are encouraging you to join our community, we embrace creativity and concentration of the fine concept! Come with will and we provide staring wheel', 'Please send your full featured video about yourself ', 'explainer video,whiteboard animation,sales,video Animated video, video marketing', 'yes', 3, 1, 2, 1, '0', '92', 'active'),
(5, 'black Shoe', 'proposal-url-5', 5, 49, 1000, 'voice-over-1.jpg', '', '', '', 'voiceover.mp4', 'We are encouraging you to join our community, we embrace creativity and concentration of the fine concept! Come with will and we provide staring wheel', '', 'audio, voice,voiceover,swahili', 'yes', 4, 2, 1, 2, '0', '75', 'active'),
(6, ' Men Trouser', 'proposal-url-6', 6, 61, 2000, 'andorid-1.jpg', 'android-2.jpg', '', '', '', 'We are encouraging you to join our community, we embrace creativity and concentration of the fine concept! Come with will and we provide staring wheel', 'Please let me see the full functional relevant application  system that you have developed', 'android development,android app', 'yes', 4, 4, 2, 2, '3', '3', 'active'),
(7, 'Comfort Sit', 'proposal-url-7', 4, 43, 500, 'video-editing-1.jpg', 'video-editing-2.jpg', 'video-editing-3.jpg', 'video-editing-4.jpg', 'video-editing-video.mp4', '<p>Hi!, there i am a professinal product and post video editor with a domain backgrounfd for over five years and edit basically as a passion and do it to my level best for my client Hi!, there i am a professinal product and post video editor with a domain backgrounfd for over five years and edit basically as a passion and do it to my level best for my client</p>', '									Please ensure that you provide all the relanvant materials 								', 'unity, android,ios,gmes,app', 'no', 4, 2, 1, 2, '5', '10', 'active'),
(8, 'Fitting Skirt', 'proposal-url-8', 6, 11, 500, 'wordpress-customization-1.jpg', '', '', '', 'wordpress-customization.mp4', '<p>We are encouraging you to join our community, we embrace creativity and concentration of the fine concept! Come with will and we provide staring wheel</p>', '									Please let me see the full functional system that you have developed								', 'wordpress blog,complete wordpress, wordpress website', 'yes', 4, 4, 1, 2, '4', '7', 'active'),
(9, 'Christmas Shirts', 'proposal-url-9', 6, 56, 1500, 'php-script.jpg', '', '', '', '', 'We are encouraging you to join our community, we embrace creativity and concentration of the fine concept! Come with will and we provide staring wheel', '', 'html,css,php.javascript,laravel', 'no', 4, 2, 2, 2, '0', '7', 'pause');

-- --------------------------------------------------------

--
-- Table structure for table `proposal_modifications`
--

CREATE TABLE `proposal_modifications` (
  `modification_id` int(11) NOT NULL,
  `proposal_id` int(11) NOT NULL,
  `modification_message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `proposal_modifications`
--

INSERT INTO `proposal_modifications` (`modification_id`, `proposal_id`, `modification_message`) VALUES
(1, 11, 'Please change the image since it is conflicting');

-- --------------------------------------------------------

--
-- Table structure for table `purchases`
--

CREATE TABLE `purchases` (
  `purchase_id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `date` text NOT NULL,
  `method` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchases`
--

INSERT INTO `purchases` (`purchase_id`, `seller_id`, `order_id`, `amount`, `date`, `method`) VALUES
(1, 4, 14, 1500, 'September 28, 2019', 'shopping_balance'),
(2, 4, 15, 1500, 'September 28, 2019', 'shopping_balance'),
(3, 4, 16, 1000, 'September 28, 2019', 'shopping_balance'),
(4, 4, 17, 1500, 'September 28, 2019', 'shopping_balance'),
(5, 4, 18, 701, 'September 29, 2019', 'stripe'),
(6, 4, 17, 1500, 'October 05, 2019', 'order_cancellation'),
(7, 3, 19, 501, 'October 09, 2019', 'paypal'),
(8, 4, 20, 550, 'October 10, 2019', 'shopping_balance'),
(9, 4, 22, 700, 'October 17, 2019', 'shopping_balance'),
(16, 4, 20, 550, 'October 27, 2019', 'order_cancellation'),
(17, 7, 25, 500, 'October 30, 2019', 'shopping_balance'),
(18, 1, 26, 2000, 'October 30, 2019', 'shopping_balance'),
(19, 4, 27, 1000, 'December 05, 2019', 'shopping_balance'),
(20, 11, 28, 2000, 'December 06, 2019', 'shopping_balance'),
(21, 4, 29, 1500, 'December 06, 2019', 'shopping_balance'),
(22, 3, 24, 1500, 'December 06, 2019', 'order_cancellation'),
(23, 4, 30, 1000, 'December 10, 2019', 'shopping_balance'),
(24, 3, 32, 550, 'December 10, 2019', 'shopping_balance'),
(25, 11, 34, 501, 'December 13, 2019', 'shopping_balance'),
(26, 11, 35, 1000, 'December 14, 2019', 'shopping_balance'),
(27, 11, 37, 1000, 'December 15, 2019', 'shopping_balance');

-- --------------------------------------------------------

--
-- Table structure for table `recent_proposals`
--

CREATE TABLE `recent_proposals` (
  `recent_id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `proposal_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recent_proposals`
--

INSERT INTO `recent_proposals` (`recent_id`, `seller_id`, `proposal_id`) VALUES
(51, 4, 10),
(54, 4, 0),
(64, 4, 5),
(76, 3, 7),
(79, 3, 6),
(80, 3, 9),
(86, 3, 8),
(90, 7, 8),
(91, 1, 6),
(104, 12, 6),
(105, 12, 9),
(111, 4, 1),
(113, 4, 2),
(114, 3, 2),
(115, 4, 4),
(117, 4, 3),
(118, 3, 5),
(119, 11, 6),
(127, 11, 7),
(130, 11, 8),
(138, 11, 9),
(142, 11, 5),
(144, 11, 3),
(147, 11, 2),
(149, 7, 5);

-- --------------------------------------------------------

--
-- Table structure for table `referrals`
--

CREATE TABLE `referrals` (
  `refarral_id` int(10) NOT NULL,
  `seller_id` int(10) NOT NULL,
  `referred_id` int(10) NOT NULL,
  `comission` int(10) NOT NULL,
  `date` text NOT NULL,
  `ip` varchar(255) NOT NULL,
  `status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `referrals`
--

INSERT INTO `referrals` (`refarral_id`, `seller_id`, `referred_id`, `comission`, `date`, `ip`, `status`) VALUES
(1, 4, 5, 100, 'October 11 2019', '172.0.0.1', 'approved'),
(2, 4, 6, 100, 'October 11 2019', '127.0.0.1', 'declined'),
(3, 4, 7, 100, 'October 11 2019', '127.0.0.1', 'pending'),
(4, 4, 9, 100, 'October 11 2019', '127.0.0.1', 'declined');

-- --------------------------------------------------------

--
-- Table structure for table `revenues`
--

CREATE TABLE `revenues` (
  `revenue_id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `date` text NOT NULL,
  `end_date` text NOT NULL,
  `status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `revenues`
--

INSERT INTO `revenues` (`revenue_id`, `seller_id`, `order_id`, `amount`, `date`, `end_date`, `status`) VALUES
(1, 4, 8, 1200, 'Oct 05 2019', 'Oct 09, 2019 02:46:29', 'cleared'),
(2, 4, 26, 1500, 'November 09, 2019', 'November 09, 2019 04:43:26', 'cleared'),
(3, 1, 27, 750, 'December 15, 2019', 'December 15, 2019 09:20:19', 'pending'),
(4, 4, 28, 1500, 'December 16, 2019', 'December 16, 2019 07:22:04', 'pending'),
(5, 4, 33, 376, 'December 23, 2019', 'December 23, 2019 08:05:22', 'pending');

-- --------------------------------------------------------

--
-- Table structure for table `section_boxes`
--

CREATE TABLE `section_boxes` (
  `box_id` int(11) NOT NULL,
  `box_title` text NOT NULL,
  `box_desc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `section_boxes`
--

INSERT INTO `section_boxes` (`box_id`, `box_title`, `box_desc`) VALUES
(1, 'Your Satisfaction', 'Whatever you need to be done, we do it accordingly no matter the budget '),
(2, 'Your Timeline', 'Whatever you need to be done, we do it accordingly within Time limit '),
(3, 'Your safety', 'Whatever you need to be done, we do it accordingly and with money safety ');

-- --------------------------------------------------------

--
-- Table structure for table `sellers`
--

CREATE TABLE `sellers` (
  `seller_id` int(11) NOT NULL,
  `seller_name` varchar(255) NOT NULL,
  `seller_user_name` varchar(255) NOT NULL,
  `seller_email` text NOT NULL,
  `seller_paypal_email` text NOT NULL,
  `seller_pass` text NOT NULL,
  `seller_image` text NOT NULL,
  `seller_country` text NOT NULL,
  `seller_headline` text NOT NULL,
  `seller_about` text NOT NULL,
  `seller_level` int(10) NOT NULL,
  `seller_language` int(11) NOT NULL,
  `seller_recent_delivery` text NOT NULL,
  `seller_rating` int(10) NOT NULL,
  `seller_offers` int(10) NOT NULL,
  `seller_referral` int(10) NOT NULL,
  `seller_ip` varchar(255) NOT NULL,
  `seller_verification` text NOT NULL,
  `seller_vacation` text NOT NULL,
  `seller_register_date` text NOT NULL,
  `seller_status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sellers`
--

INSERT INTO `sellers` (`seller_id`, `seller_name`, `seller_user_name`, `seller_email`, `seller_paypal_email`, `seller_pass`, `seller_image`, `seller_country`, `seller_headline`, `seller_about`, `seller_level`, `seller_language`, `seller_recent_delivery`, `seller_rating`, `seller_offers`, `seller_referral`, `seller_ip`, `seller_verification`, `seller_vacation`, `seller_register_date`, `seller_status`) VALUES
(1, 'Emmanuel Juma', 'Fixmywebsitebio', 'fixmywebsite@gmail.com', 'sb-odaqk273082@personal.example.com', '$2y$10$KCqEpGMQH/vTCOz8I7dvb.MFFmdO9sV6c3cpRP/38FpItovBbogaK', 'avatar2.jpg', 'Kenya', 'Activation may also occur when a consumer clicks through from digital media including display ads', ' Offer occurs by printing, registering to device, saving to a card and/or digitally displaying coupons, such as via a mobile device. Activation of an Offer may also occur when a consumer registers to claim a rebate', 2, 1, 'December 06, 2019', 100, 10, 9876545, '127.0.0.1', 'ok', 'off', 'January 20, 2019', 'away'),
(2, 'Edwin Otemba ', 'mr_digimarket', 'mr_digimarket@gmail.com', 'sb-odaqk273082@personal.example.com', '$2y$10$KCqEpGMQH/vTCOz8I7dvb.MFFmdO9sV6c3cpRP/38FpItovBbogaK', 'miss_digimarket.jpg', 'Nigeria', 'Activation may also occur when a consumer clicks through from digital media including display ads', ' Offer occurs by printing, registering to device, saving to a card and/or digitally displaying coupons, such as via a mobile device. Activation of an Offer may also occur when a consumer registers to claim a rebate', 3, 2, 'September 4, 2019', 98, 10, 45678765, '127.0.0.1', 'ok', 'off', 'February 1, 2019', 'away'),
(3, 'juma nariakho', 'Kimonyoski', 'kimonyoski@gmail.com', 'sb-odaqk273082@personal.example.com', '$2y$10$KCqEpGMQH/vTCOz8I7dvb.MFFmdO9sV6c3cpRP/38FpItovBbogaK', 'juma.jpg', 'Kenya', 'Activation may also occur when a consumer clicks through from digital media including display ads', ' Offer occurs by printing, registering to device, saving to a card and/or digitally displaying coupons, such as via a mobile device. Activation of an Offer may also occur when a consumer registers to claim a rebate', 4, 1, 'August 20,2019', 100, 9, 6543456, '127.0.0.1', 'ok', 'off', 'March 10, 2019', 'away'),
(4, 'Maisha maina', 'Daniel', 'simplelearn.1818@gmail.com', 'sb-odaqk273082@personal.example.com', '$2y$10$FhFpJPXFeyG0NtT1RPgcHejPuEBOlwKJNE3sZPhFaFR7q3Mo4htl2', 'admin.png', 'nigeria', 'Activation may also occur when a consumer clicks through from digital media including display ads', ' Offer occurs by printing, registering to device, saving to a card and/or digitally displaying coupons, such as via a mobile device. Activation of an Offer may also occur when a consumer registers to claim a rebate', 4, 2, 'December 13, 2019', 100, 8, 87654, '127.0.0.1', 'ok', 'off', 'April 20, 2019', 'online'),
(5, 'JaneJuri', 'Njeri', 'njeri@gmail.com', 'sb-odaqk273082@personal.example.com', '$2y$10$KCqEpGMQH/vTCOz8I7dvb.MFFmdO9sV6c3cpRP/38FpItovBbogaK', 'avatar.jpg', 'Uganda', 'Activation may also occur when a consumer clicks through from digital media including display ads', ' Offer occurs by printing, registering to device, saving to a card and/or digitally displaying coupons, such as via a mobile device. Activation of an Offer may also occur when a consumer registers to claim a rebate', 1, 1, 'none', 100, 1, 765430, '127.0.0.1', '17752760453', 'off', 'May 22,2019', 'away'),
(6, 'MilnerMore', 'Academind', 'academind@gmail.com', 'sb-odaqk273082@personal.example.com', '$2y$10$KCqEpGMQH/vTCOz8I7dvb.MFFmdO9sV6c3cpRP/38FpItovBbogaK', 'user.png', 'Kenya', 'Activation may also occur when a consumer clicks through from digital media including display ads', ' Offer occurs by printing, registering to device, saving to a card and/or digitally displaying coupons, such as via a mobile device. Activation of an Offer may also occur when a consumer registers to claim a rebate', 1, 1, 'none', 100, 1, 987698, '127.0.0.1', 'ok', 'off', 'June 3, 2019', 'away'),
(7, 'MwasiMo', 'Joseph', '	joesoftmwai@gmail.com', 'sb-odaqk273082@personal.example.com', '$2y$10$xWNYoUVUD7RAirJCcHExS.DyyCoGq0zvZov3vCnSEVI0STTPEHBmO', 'avater.jpg', 'Kenya', 'Activation may also occur when a consumer clicks through from digital media including display ads', ' Offer occurs by printing, registering to device, saving to a card and/or digitally displaying coupons, such as via a mobile device. Activation of an Offer may also occur when a consumer registers to claim a rebate', 1, 1, 'none', 100, 2, 5678876, '127.0.0.1', 'ok', 'off', 'July 20, 2019', 'away'),
(8, 'kenya1', 'mwac', 'mwac@gmail.com', 'sb-odaqk273082@personal.example.com', '$2y$10$KCqEpGMQH/vTCOz8I7dvb.MFFmdO9sV6c3cpRP/38FpItovBbogaK', 'user1.png', 'America', 'Activation may also occur when a consumer clicks through from digital media including display ads', ' Offer occurs by printing, registering to device, saving to a card and/or digitally displaying coupons, such as via a mobile device. Activation of an Offer may also occur when a consumer registers to claim a rebate', 1, 2, 'none', 100, 1, 4567865, '127.0.0.1', 'ok', 'on', 'August 30, 2019', 'away'),
(9, 'traversy', 'traversy', 'traversy@gmail.com', 'sb-odaqk273082@personal.example.com', '$2y$10$KCqEpGMQH/vTCOz8I7dvb.MFFmdO9sV6c3cpRP/38FpItovBbogaK', '', '', '', '', 1, 4, 'none', 100, 10, 281248835, '127.0.0.1', 'ok', 'off', 'September 22, 2019', 'block-ban'),
(10, 'Brand', 'Brand', 'brand@gmail.com', 'sb-odaqk273082@personal.example.com', '$2y$10$KCqEpGMQH/vTCOz8I7dvb.MFFmdO9sV6c3cpRP/38FpItovBbogaK', '', '', '', '', 1, 3, 'none', 100, 10, 79152487, '127.0.0.1', '1775276045', 'off', 'September 22, 2019', 'away'),
(11, 'Gili shy', 'Gili', 'simpledaniel.1818@gmail.com', '', '$2y$10$xWNYoUVUD7RAirJCcHExS.DyyCoGq0zvZov3vCnSEVI0STTPEHBmO', 'avater.jpg', '', '', '', 1, 0, 'none', 100, 10, 626225598, '127.0.0.1', 'ok', 'off', 'December 06, 2019', 'away'),
(12, 'emmmanuel', 'emmanuel', 'emmanuelnaliakho@gmail.com', '', '$2y$10$hdxjuP3wq8FhIWnCYq4u2eFwudZRYCICVu/DCJP3K8mIHnOUbUmK6', 'play_icon.png', 'kenya', 'pyschology writer', 'professor in sociology and psychology', 1, 1, 'none', 100, 10, 2140827309, '127.0.0.1', 'ok', 'off', 'December 06, 2019', 'online');

-- --------------------------------------------------------

--
-- Table structure for table `seller_accounts`
--

CREATE TABLE `seller_accounts` (
  `account_id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `withdrawn` int(11) NOT NULL,
  `current_balance` int(11) NOT NULL,
  `used_purchases` int(11) NOT NULL,
  `pending_cleanrance` int(11) NOT NULL,
  `month_earnings` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `seller_accounts`
--

INSERT INTO `seller_accounts` (`account_id`, `seller_id`, `withdrawn`, `current_balance`, `used_purchases`, `pending_cleanrance`, `month_earnings`) VALUES
(1, 1, 500, 15400, 3500, 750, 7750),
(2, 2, 600, 8500, 1500, 0, 4000),
(3, 3, 300, 11500, 2300, 0, 6000),
(4, 4, 700, 3000, 9050, 252, 11152),
(5, 5, 650, 1500, 1500, 0, 5000),
(6, 6, 300, 6500, 1500, 0, 8000),
(7, 7, 500, 500, 1001, 0, 2000),
(8, 8, 350, 10500, 1500, 0, 6500),
(9, 9, 450, 1500, 1500, 0, 7500),
(10, 10, 630, 1500, 1500, 160, 9000),
(11, 11, 0, 5500, 4500, 0, 0),
(12, 12, 0, 10000, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `seller_languages`
--

CREATE TABLE `seller_languages` (
  `language_id` int(11) NOT NULL,
  `language_title` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `seller_languages`
--

INSERT INTO `seller_languages` (`language_id`, `language_title`) VALUES
(1, 'English'),
(2, 'French'),
(3, 'Spanish'),
(4, 'KIswahili');

-- --------------------------------------------------------

--
-- Table structure for table `seller_levels`
--

CREATE TABLE `seller_levels` (
  `level_id` int(11) NOT NULL,
  `level_title` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `seller_levels`
--

INSERT INTO `seller_levels` (`level_id`, `level_title`) VALUES
(1, 'New Seller'),
(2, 'Level One'),
(3, 'Level Two'),
(4, 'Top Rated');

-- --------------------------------------------------------

--
-- Table structure for table `seller_reviews`
--

CREATE TABLE `seller_reviews` (
  `review_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `review_seller_id` int(11) NOT NULL,
  `seller_rating` int(11) NOT NULL,
  `seller_review` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `seller_reviews`
--

INSERT INTO `seller_reviews` (`review_id`, `order_id`, `review_seller_id`, `seller_rating`, `seller_review`) VALUES
(2, 2, 2, 2, 'Thank You buyer'),
(3, 3, 2, 2, 'Thank you Will Try'),
(4, 6, 2, 5, 'super Buyer'),
(5, 8, 4, 4, 'Good experience with you sir'),
(6, 25, 4, 4, 'Thank you best buyer'),
(7, 26, 4, 3, 'Thank you point noted'),
(8, 27, 1, 4, 'Appreciated... sir'),
(9, 28, 4, 5, 'Interesting bro!'),
(10, 33, 4, 5, 'wow!...honest buyer');

-- --------------------------------------------------------

--
-- Table structure for table `seller_skills`
--

CREATE TABLE `seller_skills` (
  `skill_id` int(10) NOT NULL,
  `skill_title` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `seller_skills`
--

INSERT INTO `seller_skills` (`skill_id`, `skill_title`) VALUES
(1, 'Html'),
(2, 'Css'),
(3, 'Javascript'),
(4, 'Jquery'),
(5, 'PHP 7.2');

-- --------------------------------------------------------

--
-- Table structure for table `send_offers`
--

CREATE TABLE `send_offers` (
  `offer_id` int(11) NOT NULL,
  `request_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `proposal_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `delivery_time` text NOT NULL,
  `amount` text NOT NULL,
  `status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `send_offers`
--

INSERT INTO `send_offers` (`offer_id`, `request_id`, `sender_id`, `proposal_id`, `description`, `delivery_time`, `amount`, `status`) VALUES
(1, 2, 2, 2, 'I will do an ecommerce using php framework laravel 5.8 and above with vue js', '3 Days', '550', 'send'),
(2, 2, 8, 10, 'I will develop a well customized laravel website of any Tier', '3 Days', '1500', 'active'),
(3, 4, 3, 3, 'i will design as you said ', '1 Day', '650', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `skills_relation`
--

CREATE TABLE `skills_relation` (
  `relation_id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `skill_id` int(11) NOT NULL,
  `skill_level` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `skills_relation`
--

INSERT INTO `skills_relation` (`relation_id`, `seller_id`, `skill_id`, `skill_level`) VALUES
(1, 3, 1, 'Biginner'),
(2, 4, 2, 'Intermediate'),
(3, 4, 3, 'Expert');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `slide_id` int(10) NOT NULL,
  `slide_name` text NOT NULL,
  `slide_desc` text NOT NULL,
  `slide_image` text NOT NULL,
  `slide_url` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`slide_id`, `slide_name`, `slide_desc`, `slide_image`, `slide_url`) VALUES
(2, 'Perfect ORDER Delivery Mechanism ', 'We have made a special team of expertise belonging on different professions to serve you as a free lancers.So keep getting our services for your needs', 'image-2.jpg', 'http://127.0.0.1:8090/juakali'),
(3, 'Best and secure work place', 'We have made a special team of expertise belonging on different professions to serve you as a free lancers.So keep getting our services for your needs', 'image-3.jpg', 'http://127.0.0.1:8090/juakali'),
(4, 'Well organised communications', 'We have made a special team of expertise belonging on different professions to serve you as a free lancers.So keep getting our services for your needs', 'image-4.jpg', 'http://127.0.0.1:8090/juakali'),
(5, 'Web development and designer', 'This is an interesting topic that focus on system actualization, however the complexity must be reduced for security', 'image-5.jpg', 'http://127.0.0.1:8090/juakali');

-- --------------------------------------------------------

--
-- Table structure for table `support_tickets`
--

CREATE TABLE `support_tickets` (
  `ticket_id` int(11) NOT NULL,
  `enquiry_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `subject` text NOT NULL,
  `message` text NOT NULL,
  `order_number` text NOT NULL,
  `order_rule` text NOT NULL,
  `attachment` text NOT NULL,
  `date` text NOT NULL,
  `status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `support_tickets`
--

INSERT INTO `support_tickets` (`ticket_id`, `enquiry_id`, `sender_id`, `subject`, `message`, `order_number`, `order_rule`, `attachment`, `date`, `status`) VALUES
(1, 1, 4, 'need order support', 'Please help me resolve the error', '456789', '', '', '04:54 Sep 26, 2019', 'open'),
(2, 3, 4, 'System files', 'need for registry file support', '2345', '', '', '08:30 Sep 26, 2019', 'open'),
(3, 1, 4, 'help now', 'this thing of yours is not working how will it be supported', '1234567', '', '', '09:09 Sep 26, 2019', 'open'),
(4, 2, 4, 'Am happy', 'This project is working very ', '2345', 'Buyer', 'miss_digimarket.jpg', '09:46 Sep 26, 2019', 'closed'),
(5, 1, 4, 'Good service', 'This is the way out', '12345', 'Buyer', '', '09:51 Sep 26, 2019', 'open');

-- --------------------------------------------------------

--
-- Table structure for table `terms`
--

CREATE TABLE `terms` (
  `term_id` int(11) NOT NULL,
  `term_title` text NOT NULL,
  `term_link` text NOT NULL,
  `term_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `terms`
--

INSERT INTO `terms` (`term_id`, `term_title`, `term_link`, `term_description`) VALUES
(1, 'Terms And Conditions', 'terms', 'The parties may negotiate WOs under which Company will deliver Offers provided by Advertiser and/or Agency, and/or will provide Services. “Activation” of an Offer occurs by printing, registering to device, saving to a card and/or digitally displaying coupons, such as via a mobile device. Activation of an Offer may also occur when a consumer registers to claim a rebate, or Company registers the consumer for a rebate on behalf of the Advertiser. Activation may also occur when a consumer clicks through from digital media including display ads and promotional links'),
(2, 'Refund Policy', 'refund', 'The parties may negotiate WOs under which Company will deliver Offers provided by Advertiser and/or Agency, and/or will provide Services. “Activation” of an Offer occurs by printing, registering to device, saving to a card and/or digitally displaying coupons, such as via a mobile device. Activation of an Offer may also occur when a consumer registers to claim a rebate, or Company registers the consumer for a rebate on behalf of the Advertiser. Activation may also occur when a consumer clicks through from digital media including display ads and promotional links'),
(3, 'Price & Promotions Policy ', 'pricing', '<p>The parties may negotiate WOs under which Company will deliver Offers provided by Advertiser and/or Agency, and/or will provide Services. ï¿½Activationï¿½ of an Offer occurs by printing, registering to device, saving to a card and/or digitally displaying coupons, such as via a mobile device. Activation of an Offer may also occur when a consumer registers to claim a rebate, or Company registers the consumer for a rebate on behalf of the Advertiser. Activation may also occur when a consumer clicks through from digital media including display ads and promotional links</p>');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adds`
--
ALTER TABLE `adds`
  ADD PRIMARY KEY (`add_id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `buyer_requests`
--
ALTER TABLE `buyer_requests`
  ADD PRIMARY KEY (`request_id`);

--
-- Indexes for table `buyer_reviews`
--
ALTER TABLE `buyer_reviews`
  ADD PRIMARY KEY (`review_id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`cart_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `categories_childs`
--
ALTER TABLE `categories_childs`
  ADD PRIMARY KEY (`child_id`);

--
-- Indexes for table `contact_support`
--
ALTER TABLE `contact_support`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`coupon_id`);

--
-- Indexes for table `delivery_times`
--
ALTER TABLE `delivery_times`
  ADD PRIMARY KEY (`delivery_id`);

--
-- Indexes for table `enquiry_types`
--
ALTER TABLE `enquiry_types`
  ADD PRIMARY KEY (`enquiry_id`);

--
-- Indexes for table `favorites`
--
ALTER TABLE `favorites`
  ADD PRIMARY KEY (`favorite_id`);

--
-- Indexes for table `featured_proposals`
--
ALTER TABLE `featured_proposals`
  ADD PRIMARY KEY (`featured_id`);

--
-- Indexes for table `footer_links`
--
ALTER TABLE `footer_links`
  ADD PRIMARY KEY (`link_id`);

--
-- Indexes for table `general_settings`
--
ALTER TABLE `general_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hide_seller_messages`
--
ALTER TABLE `hide_seller_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_section`
--
ALTER TABLE `home_section`
  ADD PRIMARY KEY (`section_id`);

--
-- Indexes for table `inbox_messages`
--
ALTER TABLE `inbox_messages`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `inbox_sellers`
--
ALTER TABLE `inbox_sellers`
  ADD PRIMARY KEY (`inbox_seller_id`);

--
-- Indexes for table `language_relation`
--
ALTER TABLE `language_relation`
  ADD PRIMARY KEY (`relation_id`);

--
-- Indexes for table `messages_offers`
--
ALTER TABLE `messages_offers`
  ADD PRIMARY KEY (`offer_id`);

--
-- Indexes for table `my_buyers`
--
ALTER TABLE `my_buyers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `my_sellers`
--
ALTER TABLE `my_sellers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`notification_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `order_conversations`
--
ALTER TABLE `order_conversations`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `payment_settings`
--
ALTER TABLE `payment_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `proposals`
--
ALTER TABLE `proposals`
  ADD PRIMARY KEY (`proposal_id`);

--
-- Indexes for table `proposal_modifications`
--
ALTER TABLE `proposal_modifications`
  ADD PRIMARY KEY (`modification_id`);

--
-- Indexes for table `purchases`
--
ALTER TABLE `purchases`
  ADD PRIMARY KEY (`purchase_id`);

--
-- Indexes for table `recent_proposals`
--
ALTER TABLE `recent_proposals`
  ADD PRIMARY KEY (`recent_id`);

--
-- Indexes for table `referrals`
--
ALTER TABLE `referrals`
  ADD PRIMARY KEY (`refarral_id`);

--
-- Indexes for table `revenues`
--
ALTER TABLE `revenues`
  ADD PRIMARY KEY (`revenue_id`);

--
-- Indexes for table `section_boxes`
--
ALTER TABLE `section_boxes`
  ADD PRIMARY KEY (`box_id`);

--
-- Indexes for table `sellers`
--
ALTER TABLE `sellers`
  ADD PRIMARY KEY (`seller_id`);

--
-- Indexes for table `seller_accounts`
--
ALTER TABLE `seller_accounts`
  ADD PRIMARY KEY (`account_id`);

--
-- Indexes for table `seller_languages`
--
ALTER TABLE `seller_languages`
  ADD PRIMARY KEY (`language_id`);

--
-- Indexes for table `seller_levels`
--
ALTER TABLE `seller_levels`
  ADD PRIMARY KEY (`level_id`);

--
-- Indexes for table `seller_reviews`
--
ALTER TABLE `seller_reviews`
  ADD PRIMARY KEY (`review_id`);

--
-- Indexes for table `seller_skills`
--
ALTER TABLE `seller_skills`
  ADD PRIMARY KEY (`skill_id`);

--
-- Indexes for table `send_offers`
--
ALTER TABLE `send_offers`
  ADD PRIMARY KEY (`offer_id`);

--
-- Indexes for table `skills_relation`
--
ALTER TABLE `skills_relation`
  ADD PRIMARY KEY (`relation_id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`slide_id`);

--
-- Indexes for table `support_tickets`
--
ALTER TABLE `support_tickets`
  ADD PRIMARY KEY (`ticket_id`);

--
-- Indexes for table `terms`
--
ALTER TABLE `terms`
  ADD PRIMARY KEY (`term_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adds`
--
ALTER TABLE `adds`
  MODIFY `add_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `buyer_requests`
--
ALTER TABLE `buyer_requests`
  MODIFY `request_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `buyer_reviews`
--
ALTER TABLE `buyer_reviews`
  MODIFY `review_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `cart_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `categories_childs`
--
ALTER TABLE `categories_childs`
  MODIFY `child_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `contact_support`
--
ALTER TABLE `contact_support`
  MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `coupon_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `delivery_times`
--
ALTER TABLE `delivery_times`
  MODIFY `delivery_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `enquiry_types`
--
ALTER TABLE `enquiry_types`
  MODIFY `enquiry_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `favorites`
--
ALTER TABLE `favorites`
  MODIFY `favorite_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `featured_proposals`
--
ALTER TABLE `featured_proposals`
  MODIFY `featured_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `footer_links`
--
ALTER TABLE `footer_links`
  MODIFY `link_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `general_settings`
--
ALTER TABLE `general_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `hide_seller_messages`
--
ALTER TABLE `hide_seller_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `home_section`
--
ALTER TABLE `home_section`
  MODIFY `section_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `inbox_messages`
--
ALTER TABLE `inbox_messages`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `inbox_sellers`
--
ALTER TABLE `inbox_sellers`
  MODIFY `inbox_seller_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `language_relation`
--
ALTER TABLE `language_relation`
  MODIFY `relation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `messages_offers`
--
ALTER TABLE `messages_offers`
  MODIFY `offer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `my_buyers`
--
ALTER TABLE `my_buyers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `my_sellers`
--
ALTER TABLE `my_sellers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `notification_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `order_conversations`
--
ALTER TABLE `order_conversations`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `payment_settings`
--
ALTER TABLE `payment_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `proposals`
--
ALTER TABLE `proposals`
  MODIFY `proposal_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `proposal_modifications`
--
ALTER TABLE `proposal_modifications`
  MODIFY `modification_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `purchases`
--
ALTER TABLE `purchases`
  MODIFY `purchase_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `recent_proposals`
--
ALTER TABLE `recent_proposals`
  MODIFY `recent_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=150;

--
-- AUTO_INCREMENT for table `referrals`
--
ALTER TABLE `referrals`
  MODIFY `refarral_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `revenues`
--
ALTER TABLE `revenues`
  MODIFY `revenue_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `section_boxes`
--
ALTER TABLE `section_boxes`
  MODIFY `box_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sellers`
--
ALTER TABLE `sellers`
  MODIFY `seller_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `seller_accounts`
--
ALTER TABLE `seller_accounts`
  MODIFY `account_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `seller_languages`
--
ALTER TABLE `seller_languages`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `seller_levels`
--
ALTER TABLE `seller_levels`
  MODIFY `level_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `seller_reviews`
--
ALTER TABLE `seller_reviews`
  MODIFY `review_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `seller_skills`
--
ALTER TABLE `seller_skills`
  MODIFY `skill_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `send_offers`
--
ALTER TABLE `send_offers`
  MODIFY `offer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `skills_relation`
--
ALTER TABLE `skills_relation`
  MODIFY `relation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `slide_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `support_tickets`
--
ALTER TABLE `support_tickets`
  MODIFY `ticket_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `terms`
--
ALTER TABLE `terms`
  MODIFY `term_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
