<?php 
session_start();
include("includes/db.php");
if (!isset($_SESSION['seller_user_name'])) {
	echo "<script>window.open('login.php','_self');</script>";
}
//get the buyer/user deals to create the session
$login_seller_user_name=$_SESSION['seller_user_name'];
$select_login_seller="SELECT * from sellers where seller_user_name='$login_seller_user_name'";
$run_login_seller=mysqli_query($con,$select_login_seller);
$row_login_seller=mysqli_fetch_array($run_login_seller);
$login_seller_id=$row_login_seller['seller_id'];

//for the payment processing
$get_payment_setting="SELECT * from payment_settings";
$run_payment_setting=mysqli_query($con,$get_payment_setting);
$row_payament_setting=mysqli_fetch_array($run_payment_setting);
// $processing_fee=$row_payament_setting['processing_fee'];


//select from the cart details
$select_cart="SELECT * from cart where seller_id='$login_seller_id'";
$run_cart=mysqli_query($con,$select_cart);
$count_cart=mysqli_num_rows($run_cart);
if (isset($_GET['remove_proposal'])) {
$proposal_id=$_GET['remove_proposal'];	
$delete_cart_proposal="DELETE  from cart where proposal_id='$proposal_id' AND seller_id='$login_seller_id'";
$run_delete_cart_proposal=mysqli_query($con,$delete_cart_proposal);
if ($run_delete_cart_proposal) {
echo "<script>window.open('cart.php','_self');</script>";	
}

}

 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	<title>Shopping Cart</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="Jua Kali Mall">
	<link href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100" rel="stylesheet" >
	<link rel="stylesheet" href="styles/bootstrap.min.css">
	<link rel="stylesheet" href="styles/style.css">
	<link rel="stylesheet" href="styles/category_nav_style.css">
	<!-- Custome css from the user -->
	<link rel="stylesheet" href="styles/custom.css">
	<link rel="stylesheet" href="font-awesome/css/all.min.css">
	<script src="js/jquery.slim.min.js"></script>
</head>
<body>
<div class="preloader d-flex justify-content-center align-items-center">
    <img src="images/loadjuakali.gif" alt="the preloader"><br>
    <p>Loading...Please wait</p>
  </div>		
<?php include("includes/header.php");?>
<div class="container mt-5 mb-3">
	<div class="row">
		<div class="col-md-12">
			<div class="card mb-3">
				<div class="card-body">
					<h5 class="float-left"> Your Cart (<?php echo($count_cart); ?>)</h5>
					<h5 class="float-right">
					<a href="index.php">Keep Shopping</a>
					</h5>
				</div>
			</div>
		</div><!--col-md-12 ends-->
	</div>
	<div class="row" id="cart-show">
		<div class="col-md-7">
			<div class="card mb-3">
				<div class="card-body">
<?php 
$total=0;

while ($row_cart =mysqli_fetch_array($run_cart)) {
$proposal_id=$row_cart['proposal_id'];	
$proposal_price=$row_cart['proposal_price'];	
$proposal_qty=$row_cart['proposal_qty'];	

$select_proposal="SELECT * from proposals where proposal_id='$proposal_id'";
$run_proposal=mysqli_query($con,$select_proposal);
$row_proposals=mysqli_fetch_array($run_proposal);
$proposal_title=$row_proposals['proposal_title'];
$proposal_url=$row_proposals['proposal_url'];
$proposal_img1=$row_proposals['proposal_img1'];

$sub_total=$proposal_price * $proposal_qty;
$total +=$sub_total;


 ?>					
<div class="cart-proposal">
<div class="row">
<div class="col-lg-3 mb-2">
<a href="proposals/<?php echo $proposal_url; ?>">
<img src="proposals/proposal_files/<?php echo($proposal_img1); ?>" class="img-fluid">
</a>
</div><!--col-lg-3 mb-2-->
<div class="col-lg-9">
<a href="proposals/<?php echo($proposal_url); ?>">
<h6><?php echo($proposal_title); ?></h6>
</a>
<a href="cart.php?remove_proposal=<?php echo($proposal_id); ?>" class="remove-link text-danger" onclick="return confirm('Do You Want To Delete This product?')"><i class="fa fa-trash-alt"></i> Remove</a>
</div>
</div>
<hr>
<h6 class="clearfix">Proposal Quantity <strong class="float-right price ml-2 mt-2">Ksh <?php echo($sub_total); ?></strong>
<input type="text" name="quantity" class="float-right form-control quantity" data-proposal_id="<?php echo($proposal_id); ?>" value="<?php echo($proposal_qty); ?>">
</h6>
<hr>
</div><!--cart-proposal end (single)-->
<?php } ?>					
<h3 class="float-right">Total: Ksh <?php echo($total); ?></h3>
</div>
</div>
</div><!--col-md-7 ends-->
<div class="col-md-5">
<div class="card">
<div class="card-body cart-order-details">
<p>Cart Subtotal <span class="float-right">Ksh <?php echo($total); ?> </span></p>
<hr>
<p>Apply Coupon code</p>
<form class="input-group" method="post">
<input type="text" name="code" class="form-control apply-disabled" placeholder="Enter Coupon Code!">
<span class="input-group-append">
<button class="input-group-text btn btn-success" name="coupon_submit" type="submit">Apply</button></span>
</form>
<?php if (!isset($_GET['coupon_applied'])) {?>
<p class="coupon-response"></p>

<?php }else{ ?>
<p class="coupon-response p-2 mt-3 bg-success text-white">
	Your Coupon Has Been Applied!
</p>
<?php } ?>
<?php 
if (isset($_POST['coupon_submit'])) {
$coupon_code=$_POST['code'];
if (!empty($coupon_code)) {
$select_coupon="SELECT * from coupons where coupon_code='$coupon_code'";
$run_coupon=mysqli_query($con,$select_coupon);
$count_coupon=mysqli_num_rows($run_coupon);	
if ($count_coupon == 1) {
	$row_coupon=mysqli_fetch_array($run_coupon);
	$coupon_proposal=$row_coupon['proposal_id'];
	$coupon_limit=$row_coupon['coupon_limit'];
	$coupon_used=$row_coupon['coupon_used'];
	$coupon_price=$row_coupon['coupon_price'];
	
	if ($coupon_limit <= $coupon_used) {
echo "
<script>
$('.coupon-response').html('Your Coupon Code Has Been Expired.').attr('class','coupon-response p-2 mt-3 bg-danger text-white');
</script>

	";		
	}else{
		$select_cart="SELECT * from cart where proposal_id='$coupon_proposal' AND seller_id='$login_seller_id' ";
		$run_cart=mysqli_query($con,$select_cart);
		$count_cart=mysqli_num_rows($run_cart);
		if ($count_cart==1) {
	$update_coupon="UPDATE coupons set coupon_used=coupon_used + 1 where coupon_code='$coupon_code'";
	$run_update_coupon=mysqli_query($con,$update_coupon);
	$update_cart=" UPDATE cart set proposal_price='$coupon_price' where proposal_id='$coupon_proposal'";
	$run_update_cart=mysqli_query($con,$update_cart);
	echo "
	<script>
window.open('cart.php?coupon_applied','_self');
	</script>";
		}else{
		echo "
<script>
$('.coupon-response').html('Your Coupon Code is Not Right For Product.').attr('class','coupon-response p-2 mt-3 bg-danger text-white');
</script>

	";
		}
	}
}else{

	echo "
<script>
$('.coupon-response').html('Your Coupon Code is Not Valid.').attr('class','coupon-response p-2 mt-3 bg-danger text-white');
</script>

	";
}

	}	
}

 ?>

<hr>
<!-- <p>Processing Fee <span class="float-right">Ksh <?php// echo($processing_fee); ?></span></p> -->
<hr>
<p>Total <span class="font-weight-bold float-right">Ksh <?php echo($total);// + $processing_fee); ?></span></p>
<hr>
<?php if ($count_cart==0) {?>
<h5 class="text-center">You have NO Product in the Cart!</h5>
<?php }else{ ?>	
<a href="cart_payment_options.php" class="btn btn-lg btn-success btn-block">Proceed To Payment
</a>
<?php } ?>
</div>
</div>
</div><!--end of col-md-5 summary column-->
	</div><!--end of cart-show-->
</div><!--ends of the cart container-->
<?php include("includes/footer.php");?>
<script>
//js yo make price increase effective (under coupon page)	
$(document).ready(function(){
$(document).on('keyup','.quantity',function(){
var seller_id="<?php echo($login_seller_id); ?>";	
var proposal_id=$(this).data("proposal_id");	
var quantity=$(this).val();	
if (quantity !="") {
	$.ajax({
url: "change_quantity.php",
method: "POST",
data: {seller_id: seller_id, proposal_id:proposal_id, proposal_qty:quantity },
success:function(data){
	$("#cart-show").load("cart_show.php");
}
	});
}
});
});	

</script>
</body>
</html>