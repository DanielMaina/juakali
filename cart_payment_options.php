<?php 
session_start();
include("includes/db.php");
if (!isset($_SESSION['seller_user_name'])) {
	echo "<script>window.open('login.php','_self');</script>";
}
//for the payment processing
$get_payment_setting="SELECT * from payment_settings";
$run_payment_setting=mysqli_query($con,$get_payment_setting);
$row_payament_setting=mysqli_fetch_array($run_payment_setting);
$processing_fee=$row_payament_setting['processing_fee'];
$enable_paypal=$row_payament_setting['enable_paypal'];
$paypal_email=$row_payament_setting['paypal_email'];
$paypal_currency_code=$row_payament_setting['paypal_currency_code'];
$paypal_sandbox=$row_payament_setting['paypal_sandbox'];

if ($paypal_sandbox =="on") {
$paypal_url="https://www.sandbox.paypal.com/cgi-bin/webscr";
}elseif ($paypal_sandbox=="off") {
$paypal_url="https://www.paypal.com/cgi-bin/webscr";
}
//setting backend for stript
$enable_stripe=$row_payament_setting['enable_stripe'];
//a copy get id and username/email... 
$login_seller_user_name=$_SESSION['seller_user_name'];
$select_login_seller="SELECT * from sellers where seller_user_name='$login_seller_user_name'";
$run_login_seller=mysqli_query($con,$select_login_seller);
$row_login_seller=mysqli_fetch_array($run_login_seller);
$login_seller_id=$row_login_seller['seller_id'];
$login_seller_email=$row_login_seller['seller_email'];

//to update the seller account amount 
$get_seller_accounts="SELECT * from seller_accounts where seller_id='$login_seller_id'";
$run_seller_accounts=mysqli_query($con,$get_seller_accounts);
$row_seller_accounts=mysqli_fetch_array($run_seller_accounts);
$current_balance=$row_seller_accounts['current_balance'];

//select from the cart details
$select_cart="SELECT * from cart where seller_id='$login_seller_id'";
$run_cart=mysqli_query($con,$select_cart);
$count_cart=mysqli_num_rows($run_cart);

$sub_total=0;
while ($row_cart=mysqli_fetch_array($run_cart)) {
$proposal_price=$row_cart['proposal_price'];
$proposal_qty=$row_cart['proposal_qty'];
$cart_total=$proposal_price*$proposal_qty;
$sub_total +=$cart_total;
$total=$sub_total + $processing_fee;

}


?>

<!DOCTYPE html>
<html lang="en">
<head>
	
	<title>Pay||now</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="Juakali Mall">
	<link href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100" rel="stylesheet" >
	<link rel="stylesheet" href="styles/bootstrap.min.css">
	<link rel="stylesheet" href="styles/style.css">
	<link rel="stylesheet" href="styles/category_nav_style.css">
	<!-- Custome css from the user -->
	<link rel="stylesheet" href="styles/custom.css">
	<link rel="stylesheet" href="font-awesome/css/all.min.css">
	<link rel="stylesheet" href="styles/owl.carousel.css">
	<link rel="stylesheet" href="styles/owl.theme.default.css">
	 <script src="https://checkout.stripe.com/v2/checkout.js"></script>
	<script src="js/jquery.slim.min.js"></script>
	
</head>
<body>
<div class="preloader d-flex justify-content-center align-items-center">
    <img src="images/loadjuakali.gif" alt="the preloader"><br>
    <p>Loading...Please wait</p>
  </div>		
<?php include("includes/header.php");?>
<div class="container mt-5 mb-5">
	<div class="row">
		<div class="col-md-12">
			<div class="card mb-3">
				<div class="card-body">
					<h5 class="float-left">Your Cart (<?php echo $count_cart; ?>)</h5>
					<h5 class="float-right">
						<a href="index.php">Keep Shopping</a>
					</h5>
				</div>
			</div>
		</div><!--end of the first row-->
	</div>
	<div class="row"> 
			<div class="col-md-7">
				<div class="row">
<?php 
if ($current_balance >= $sub_total) { ?>					
<div class="col-md-12 mb-3">
<div class="card payment-options">
<div class="card-header">
<h5>Avaible Shopping Balance</h5>
</div>
<div class="card-body">
<div class="row">
<div class="col-1">
<input type="radio" name="method" id="shopping-balance" class="form-control radio-input" checked>
</div>
<div class="col-11">
<p class="lead mt-2">Balance-<?php echo($login_seller_user_name); ?> <span class="text-success font-weight-bold">Ksh <?php echo($current_balance); ?></span>
</p>
</div>
</div>
</div>
</div><!--payment and card ends-->
</div>
<?php } ?>	
<!-- add payment onDelivery -->
<div class="col-md-12 mb-3">
<div class="card payment-options">
<div class="card-header">
<h5>You Can Pay On Delivery</h5>
</div>
<div class="card-body">
<div class="row">
<div class="col-1">
<input type="radio" name="method" id="pay-delivery" class="form-control radio-input">
</div>
<div class="col-11">
<p class="lead mt-2"><span class="text-info"><?php echo strtoupper($login_seller_user_name); ?></span> You Can Pay Once Product Delivered<span class="text-success font-weight-bold"> Ksh <?php echo($sub_total); ?></span>
</p>
</div>
</div>
</div>
</div><!--payment and card ends-->
</div>
<!-- add payment onDelivery end -->
<div class="col-md-12 mb-3">
<div class="card payment-options">
<div class="card-header">
<div class="h5">Payment Option</div>
</div>
<div class="card-body">
<?php
//paypal payment  setting
 if ($enable_paypal=="yes") { ?>								
<div class="row">
<div class="col-1">
<input type="radio" name="method" id="paypal" class="form-control radio-input" <?php 
if ($current_balance < $sub_total) {echo "checked";}
 ?>>
</div>
<div class="col-11">
<img src="images/paypal.png" height="50" class="ml-2 width-xs-100">	 
</div>
</div>
<?php } ?>	

<?php 
//code for the stripe payment setup
if ($enable_stripe=="yes") { ?>	
<?php if ($enable_paypal=="yes") { ?>						
<hr>
	<?php }  ?>							
<div class="row">
<div class="col-1">
<input type="radio" name="method" id="credit-card" class="form-control radio-input"
<?php 
if ($current_balance < $sub_total) {
if ($enable_paypal=="no") {
	echo "checked";
	}	
}
 ?>
>
</div>
<div class="col-11">
<img src="images/credit_cards.jpg" height="50" class="ml-2 width-xs-100">	 
</div>
</div>
<?php } ?>								
</div>
</div>
</div><!--col-md-12 mb-3 ends-->
</div><!--end of the inner row-->
</div><!--end of col-md-7 ends-->
<div class="col-md-5">
<div class="card">
<div class="card-body cart-order-details">
<p>Cart Subtotal <span class="float-right">Ksh <?php echo $sub_total; ?></span></p>
<hr>
<p class="processing-fee">Processing Fee <span class="float-right">Ksh <?php echo ($processing_fee); ?></span></p><hr class="processing-fee">
<p>Total <span class="float-right font-weight-bold total-price">Ksh <?php echo $total; ?></span></p>
<hr> 
<!--OnDelivery form data -->
<form action="shopping_balance.php" method="post" id="pay-delivery-form">
<input type="hidden" name="amount" value="<?php echo $sub_total; ?>">
<button type="submit" name="cart_submit_order" class="btn btn-lg btn-success btn-block" onclick="return confirm('You will pay your Order Once Delivered!')">
	Pay On Delivery
</button>
</form>

<!--OnDelivery form data ends-->

<?php if ($current_balance >=$sub_total) { ?>						
<form action="shopping_balance.php" method="post" id="shopping-balance-form">
<input type="hidden" name="amount" value="<?php echo $sub_total; ?>">
<button type="submit" name="cart_submit_order" class="btn btn-lg btn-success btn-block" onclick="return confirm('Do you Really want to Order Product from your shopping Balance?')">
	Pay with Shopping Balance
</button>
</form><!--shopping-balance-form ends-->
<?php } ?>						
<br>
<?php if ($enable_paypal=="yes") { ?>						
<form action="<?php echo($paypal_url); ?>" method="post" id="paypal-form">
							<input type="hidden" name="cmd" value="_cart">
							<input type="hidden" name="upload" value="<?php echo($processing_fee); ?>">
							<input type="hidden" name="handling_cart" value="1">
							<input type="hidden" name="business" value="<?php echo($paypal_email); ?>">
							<input type="hidden" name="currency_code" value="<?php echo($paypal_currency_code); ?>">
							<input type="hidden" name="cancel_return" value="<?php echo($site_url); ?>/cart_payment_options.php">
							<input type="hidden" name="return" value="<?php echo($site_url); ?>/paypal_order.php?cart_seller_id=<?php echo($login_seller_id); ?>">
<?php 
$i=0;

//select from the cart details
$select_cart="SELECT * from cart where seller_id='$login_seller_id'";
$run_cart=mysqli_query($con,$select_cart);

while ($row_cart =mysqli_fetch_array($run_cart)) {
$proposal_id=$row_cart['proposal_id'];	
$proposal_price=$row_cart['proposal_price'];	
$proposal_qty=$row_cart['proposal_qty'];	

$select_proposal="SELECT * from proposals where proposal_id='$proposal_id'";
$run_proposal=mysqli_query($con,$select_proposal);
$row_proposals=mysqli_fetch_array($run_proposal);
$proposal_title=$row_proposals['proposal_title'];
$i++;
 ?>

<input type="hidden" name="item_name_<?php echo($i); ?>" value="<?php echo($proposal_title); ?>">
<input type="hidden" name="item_number_<?php echo($i); ?>" value="<?php echo($i); ?>">
<input type="hidden" name="amount_<?php echo($i); ?>" value="<?php echo($proposal_price); ?>">
<input type="hidden" name="quantity_<?php echo($i); ?>" value="<?php echo($proposal_qty); ?>">
<?php } ?>
<button type="submit" class="btn btn-lg btn-success btn-block">Pay with Paypal</button>
<p class="alert alert-danger"><strong>Alert!</strong> Will Be Redirected To PayPal Account</p>
</form><!---paypal-form ends-->

<?php } ?>	


<?php 
if ($enable_stripe== "yes") { ?>
<?php 
include("stripe_config.php");
$stripe_total_amounts=$total*100;
$stripe_total_amount=$stripe_total_amounts/100;

 ?>
<form action="cart_charge.php" method="post" id="credit-card-form">
<input type="hidden" name="amount" value="<?php echo($stripe_total_amount); ?>">
<input 
type="submit"
class="btn btn-lg btn-success btn-block stripe-submit" 
data-key="<?php echo($stripe['publishable_key']); ?>"
value="Pay With Credit Card" 
data-amount="<?php echo($stripe_total_amount); ?>"
data-currency="<?php echo($stripe['currency_code']); ?>"
data-email="<?php echo($login_seller_email); ?>"
data-name="JuakaliMall"
data-image="images/logo_transparent.png"
data-description="All cart Product Payment"
data-allow-remember-me="false">
<script>
			 $(document).ready(function() {
	            $('.stripe-submit').on('click', function(event) {
	                event.preventDefault();
	                var $button = $(this),
	                    $form = $button.parents('form');
	                var opts = $.extend({}, $button.data(), {
	                    token: function(result) {
	                        $form.append($('<input>').attr({ type: 'hidden', name: 'stripeToken', value: result.id })).submit();
	                    }
	                });
	                StripeCheckout.open(opts);
	            });
	        });
</script>
</form>
<?php } ?>
</div>
</div>
</div><!--col-md-5 ends-->
</div><!--end of the second row-->
</div><!--ends of the container mt-5 mb-5-->
<script>
	$(document).ready(function(){
		
	<?php if ($current_balance >=$sub_total) { ?>	
		$('.total-price').html('Ksh <?php echo($sub_total); ?>');
		$('.processing-fee').hide();
		$('#pay-delivery-form').hide();

	<?php }else{ ?>

$('.total-price').html('Ksh <?php echo($total); ?>');
$('.processing-fee').show();
$('#pay-delivery-form').hide();

<?php } ?>
<?php if ($current_balance >= $sub_total) { ?>

		$('#paypal-form').hide();
		$('#credit-card-form').hide();
		$('#pay-delivery-form').hide();

<?php }else{ ?>

  $('#shopping-balance-form').hide();

<?php } ?>
<?php if ($current_balance < $sub_total) { ?>

<?php if ($enable_paypal == "yes") { ?>

<?php }else{ ?>

$('#paypal-form').hide();

<?php } ?>
<?php } ?>
<?php if ($current_balance < $sub_total) { ?>
<?php if ($enable_stripe == "yes") { ?>
<?php if ($enable_paypal=="yes") { ?>

$('#credit-card-form').hide();

<?php }else{ ?>

<?php } ?>
<?php } ?>	
<?php } ?>

		$('#shopping-balance').click(function(){
			$('.col-md-5 .card br').show();
			$('.total-price').html('Ksh <?php echo($sub_total); ?>');
			$('.processing-fee').hide();
			$('#credit-card-form').hide();
			$('#paypal-form').hide();
			$('#shopping-balance-form').show();
			$('#pay-delivery-form').hide();
		});


		$('#paypal').click(function(){
			$('.col-md-5 .card br').hide();
			$('.total-price').html('Ksh <?php echo($total); ?>');
			$('.processing-fee').show();
			$('#credit-card-form').hide();
			$('#paypal-form').show();
			$('#shopping-balance-form').hide();
			$('#pay-delivery-form').hide();
		});


		$('#credit-card').click(function(){
			$('.col-md-5 .card br').hide();
			$('.total-price').html('Ksh <?php echo($total); ?>');
			$('.processing-fee').show();
			$('#credit-card-form').show();
			$('#paypal-form').hide();
			$('#shopping-balance-form').hide();
			$('#pay-delivery-form').hide();
		});

		//addedToPayOnDelivery
		$('#pay-delivery').click(function(){
			$('.col-md-5 .card br').show();
			$('.total-price').html('Ksh <?php echo($sub_total); ?>');
			$('.processing-fee').hide();
			$('#credit-card-form').hide();
			$('#paypal-form').hide();
			$('#shopping-balance-form').hide();
			$('#pay-delivery-form').show();
		});


	});
</script>
<?php include("includes/footer.php");?>
</body>
</html>