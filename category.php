<?php
session_start();
include("includes/db.php");
include("functions/functions.php");
if (isset($_GET['cat_id'])) {
	unset($_SESSION['cat_child_id']);
	$cat_id=$_GET['cat_id'];
	$_SESSION['cat_id']=$cat_id;

}
if (isset($_GET['cat_child_id'])) {
        unset($_SESSION['cat_id']);
		$cat_child_id=$_GET['cat_child_id'];
		$_SESSION['cat_child_id']=$cat_child_id;
	}	
?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php
if (isset($_SESSION['cat_id'])) {
 $cat_id=$_SESSION['cat_id'];
 $get_cats="SELECT * from categories  where cat_id='$cat_id'";
 $run_cats=mysqli_query($con,$get_cats);
 $row_cats=mysqli_fetch_array($run_cats);
 $cat_title=$row_cats['cat_title'];
 $cat_desc=$row_cats['cat_desc'];
 ?>
	
<title>JuakaliMall / <?php echo $cat_title; ?></title>
<meta name="description" content="<?php echo $cat_desc ; ?>">
<?php } ?>


<?php
if (isset($_SESSION['cat_child_id'])) {
 $cat_child_id=$_SESSION['cat_child_id'];
 $get_child_cats="SELECT * from categories_childs where child_id='$cat_child_id'";
 $run_child_cats=mysqli_query($con,$get_child_cats);
 $row_child_cats=mysqli_fetch_array($run_child_cats);
 $child_title=$row_child_cats['child_title'];
 $child_desc=$row_child_cats['child_desc'];

 ?>
	
<title>JuakaliMall / <?php echo $child_title; ?></title>
<meta name="description" content="<?php echo $child_desc ; ?>">
<?php } ?>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="JuakaliMall">
	<link href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100" rel="stylesheet" >
	<link rel="stylesheet" href="styles/bootstrap.min.css">
	<link rel="stylesheet" href="styles/style.css">
	<link rel="stylesheet" href="styles/category_nav_style.css">
	<!-- Custome css from the user -->
	<link rel="stylesheet" href="styles/custom.css">
	<link rel="stylesheet" href="font-awesome/css/all.min.css">
	<script src="js/jquery.slim.min.js"></script>
</head>
<body class="bg-white">
<div class="preloader d-flex justify-content-center align-items-center">
    <img src="images/loadjuakali.gif" alt="the preloader"><br>
    <p>Loading...Please wait</p>
  </div>	
<?php include("includes/header.php");?>

<div class="container-fluid mt-5">
	<div class="row">
	<div class="col-md-12">
<center>
<?php
if (isset($_SESSION['cat_id'])) {
 $cat_id=$_SESSION['cat_id'];
 $get_cats="SELECT* from categories  where cat_id='$cat_id'";
 $run_cats=mysqli_query($con, $get_cats);
 $row_cats=mysqli_fetch_array($run_cats);
 $cat_title=$row_cats['cat_title'];
 $cat_desc=$row_cats['cat_desc'];
 ?>	
<h1><?php echo $cat_title;?></h1>
<p class="lead"><?php echo $cat_desc;?></p>

<?php }?>

<?php
if (isset($_SESSION['cat_child_id'])) {
 $cat_child_id=$_SESSION['cat_child_id'];
 $get_child_cats="SELECT * from categories_childs where child_id ='$cat_child_id'";
 $run_child_cats=mysqli_query($con,$get_child_cats);
 $row_child_cats=mysqli_fetch_array($run_child_cats);
 $child_title=$row_child_cats['child_title'];
 $child_desc=$row_child_cats['child_desc'];

 ?>

<h1><?php echo $child_title;?></h1>
<p class="lead"><?php echo $child_desc;?></p>

<?php }?>
</center>
<hr>	
</div>
</div>
<div class="row mt-3">
<div class="col-lg-3 col-md-4 col-sm-12 display-small-none">
<?php include("includes/category_sidebar.php");?>
</div><!--col-lg-3 col-md-4 col-sm-12 ends-->
<div class="col-lg-9 col-md-8 col-sm-12">
<div class="row flex-wrap" id="category_proposals">
<?php get_category_proposals(); ?>
</div>
<div id="wait" class="loader"></div>
<br><br>
<div class="row justify-content-center">
<nav>
<ul class="pagination" id="category_pagination">
<?php get_category_pagination(); ?>
</ul>
</nav>
</div>
</div><!--col-lg-9 col-md-8 col-sm-12 end-->
</div><!--row mt-3-->
</div>
<?php include("includes/footer.php");?>
<script>
function get_category_proposals(){
var sPath = ''; 
var aInputs = $('li').find('.get_online_sellers');
var aKeys   = Array();
var aValues = Array();
iKey = 0;
$.each(aInputs,function(key,oInput){
if(oInput.checked){
aKeys[iKey] =  oInput.value
};
iKey++;
});
if(aKeys.length>0){
var sPath = '';
for(var i = 0; i < aKeys.length; i++){
sPath = sPath + 'online_sellers[]=' + aKeys[i]+'&';
}
}
var aInputs = Array();
var aInputs = $('li').find('.get_delivery_time');
var aKeys   = Array();
var aValues = Array();
iKey = 0;
$.each(aInputs,function(key,oInput){
if(oInput.checked){
aKeys[iKey] =  oInput.value
};
iKey++;
});
if(aKeys.length>0){
for(var i = 0; i < aKeys.length; i++){
sPath = sPath + 'delivery_time[]=' + aKeys[i]+'&';
}
}
var aInputs = Array();
var aInputs = $('li').find('.get_seller_level');
var aKeys   = Array();
var aValues = Array();
iKey = 0;
$.each(aInputs,function(key,oInput){
if(oInput.checked){
aKeys[iKey] =  oInput.value
};
iKey++;
});
if(aKeys.length>0){
for(var i = 0; i < aKeys.length; i++){
sPath = sPath + 'seller_level[]=' + aKeys[i]+'&';
}
}
var aInputs = Array();
var aInputs = $('li').find('.get_seller_language');
var aKeys   = Array();
var aValues = Array();
iKey = 0;
$.each(aInputs,function(key,oInput){
if(oInput.checked){
aKeys[iKey] =  oInput.value
};
iKey++;
});
if(aKeys.length>0){
for(var i = 0; i < aKeys.length; i++){
sPath = sPath + 'seller_language[]=' + aKeys[i]+'&';
}
}		
$('#wait').addClass("loader");		
$.ajax({  
url:"category_load.php",  
method:"POST",  
data: sPath+'zAction=get_category_proposals',  
success:function(data){
$('#category_proposals').html('');  
$('#category_proposals').html(data);
$('#wait').removeClass("loader");
}  
});							  
$.ajax({  
url:"category_load.php",  
method:"POST",  
data: sPath+'zAction=get_category_pagination',  
success:function(data){  
$('#category_pagination').html('');  
$('#category_pagination').html(data); 
}  
});
}
$('.get_online_sellers').click(function(){ 
get_category_proposals(); 
});
$('.get_delivery_time').click(function(){ 
get_category_proposals(); 
}); 
$('.get_seller_level').click(function(){ 
get_category_proposals(); 
}); 
$('.get_seller_language').click(function(){ 
get_category_proposals(); 
});

	
</script>


<script>
$(document).ready(function(){
	$(".get_delivery_time").click(function(){
		if($('.get_delivery_time:checked').length > 0){
			$(".clear_delivery_time").show();
		}else{
			$(".clear_delivery_time").hide();
		}
	})//end hide/show delivery time...
	$(".get_seller_level").click(function(){
		if($(".get_seller_level:checked").length > 0){
			$(".clear_seller_level").show();
		}else{
			$(".clear_seller_level").hide();
		}
	})//end hide /show seller levels
	$(".get_seller_language").click(function(){
		if ($(".get_seller_language").length > 0) {
			$(".clear_seller_language").show();
		}else{
			$(".clear_seller_language").hide();
		}
	});//end of hide/show seller language
$(".clear_delivery_time").click(function(){
 $(".clear_delivery_time").hide();
});//hide clear filter time...
$(".clear_seller_level").click(function(){
 $(".clear_seller_level").hide();
});//hide clear filter level...
$(".clear_seller_language").click(function(){
 $(".clear_seller_language").hide();
});//hide clear filter language...

});
function clearDelivery(){
	$('.get_delivery_time').prop('checked',false);
	get_category_proposals();
}
function clearLevel(){
	$('.get_seller_level').prop('checked',false);
	get_category_proposals();
}
function clearLanguage(){
	$('.get_seller_language').prop('checked', false);
	get_category_proposals();
}

</script>
</body>
</html>