<?php
session_start();
include("includes/db.php");
if (isset($_SESSION['seller_user_name'])) {
echo "<script>
window.open('indxe.php','_self');
</script>";
}

$code=$_GET['code'];
$select_seller="SELECT * from sellers where seller_pass='$code'";
$run_seller=mysqli_query($con,$select_seller);

$count_seller=mysqli_num_rows($run_seller);
if ($count_seller==0) {
echo "
 <script>
alert('YOUR Change Password Link is Invalid.');
window.open('index.php','_self');
 </script>
";
}

$row_seller=mysqli_fetch_array($run_seller);
$seller_id=$row_seller['seller_id'];
$seller_user_name=$row_seller['seller_user_name'];
?>



<!DOCTYPE html>
<html lang="en">
<head>
	
	<title>Change Password</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="JuaKali Mall">
	<link href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100" rel="stylesheet" >
	<link rel="stylesheet" href="styles/bootstrap.min.css">
	<link rel="stylesheet" href="styles/style.css">
	<link rel="stylesheet" href="styles/category_nav_style.css">
	<!-- Custome css from the user -->
	<link rel="stylesheet" href="styles/custom.css">
	<link rel="stylesheet" href="font-awesome/css/all.min.css">
	<script src="js/jquery.slim.min.js"></script>
</head>
<body>
<?php include("includes/header.php");?>
	<div class="container">
		<div class="row">
			<div class="col-md-12 mt-5 mb-5">
				<div class="card change-pass">
					<div class="card-header text-center">
						<h3>Dear <?php echo $seller_user_name ; ?> , Change Password</h3>
					</div>
					<div class="card-body d-flex justify-content-center">
					<form action="" method="post" class="col-md-8">
						<div class="form-group">
							<label>Enter Your New Password</label>
							<div class="input-group">
								<span class="input-group-prepend">
									<span class="input-group-text">
									<i class="fa fa-check tick1 text-success"></i>
									<i class="fa fa-times cross1 text-danger"></i>
								</span>
								</span>
								<input type="password" name="new_pass" id="password" class="form-control" required>
								<span class="input-group-append">
									<div class="input-group-text" id="meter_wrapper">
										<span id="pass_type"></span><div id="meter"></div>
									</div>
								</span>
							</div>
						</div>
						<div class="form-group">
							<label>Re-Enter Your Password</label>
							<div class="input-group">
								<span class="input-group-prepend">
									<span class="input-group-text">
									<i class="fa fa-check tick2 text-success"></i>
									<i class="fa fa-times cross2 text-danger"></i>
								</span>
								</span>
								<input type="password" name="new_pass_again" id="confirm_password" class="form-control" required>
							</div>
						</div>
						<div class="text-center">
							<button type="submit" name="submit" class="btn btn-primary">
								<i class="fa fa-user-md"></i>Change Password
							</button>
						</div>
					</form>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
if (isset($_POST['submit'])) {
$new_pass=$_POST['new_pass'];
$new_pass_again=$_POST['new_pass_again'];
if ($new_pass !=$new_pass_again) {
	echo "<script>
alert('Your New Password Does Not MATCH,TRY again!');
	</script>";
}else{
	$ecnrypted_password=password_hash($new_pass, PASSWORD_DEFAULT);
	$update_password="UPDATE sellers set seller_pass='$ecnrypted_password' where seller_id='$seller_id' ";
	$run_password=mysqli_query($con, $update_password);
	if ($run_password) {
		echo "<script>
alert('New Password Has been Successfully Changed!');
window.open('login.php','_self');
		</script>";
	}
}

	}
?>

<?php include("includes/footer.php");?>
<script>

$(document).ready(function(){
$("#password").keyup(function(){
check_pass();
});
});

function check_pass() {
var val = document.getElementById("password").value;
var meter = document.getElementById("meter");
var no=0;
if(val!=""){
// If the password length is less than or equal to 6
if(val.length<=6)no=1;
// If the password length is greater than 6 and contain any lowercase alphabet or any number or any special character
if(val.length>6 && (val.match(/[a-z]/) || val.match(/\d+/) || val.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)))no=2;
// If the password length is greater than 6 and contain alphabet,number,special character respectively
if(val.length>6 && ((val.match(/[a-z]/) && val.match(/\d+/)) || (val.match(/\d+/) && val.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)) || (val.match(/[a-z]/) && val.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/))))no=3;
// If the password length is greater than 6 and must contain alphabets,numbers and special characters
if(val.length>6 && val.match(/[a-z]/) && val.match(/\d+/) && val.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/))no=4;
if(no==1){
$("#meter").animate({width:'50px'},300);
meter.style.backgroundColor="red";
document.getElementById("pass_type").innerHTML="Very Weak";
}
if(no==2){
$("#meter").animate({width:'100px'},300);
meter.style.backgroundColor="#F5BCA9";
document.getElementById("pass_type").innerHTML="Weak";
}
if(no==3){
$("#meter").animate({width:'150px'},300);
meter.style.backgroundColor="#FF8000";
document.getElementById("pass_type").innerHTML="Good";
}
if(no==4){
$("#meter").animate({width:'200px'},300);
meter.style.backgroundColor="#00FF40";
document.getElementById("pass_type").innerHTML="Strong";
}
}
else{
meter.style.backgroundColor="";
document.getElementById("pass_type").innerHTML="";
}
}

</script>

<script>
$(document).ready(function(){
$('.tick1').hide();
$('.cross1').hide();
$('.tick2').hide();
$('.cross2').hide();
$('#confirm_password').focusout(function(){
var password = $('#password').val();
var confirmPassword = $('#confirm_password').val();
if(password == confirmPassword){
$('.tick1').show();
$('.cross1').hide();
$('.tick2').show();
$('.cross2').hide();
}
else{
$('.tick1').hide();
$('.cross1').show();
$('.tick2').hide();
$('.cross2').show();
}
});
});
</script>


</body>
</html>