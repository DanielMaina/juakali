<?php 
session_start();
include("includes/db.php");
if (!isset($_SESSION['seller_user_name'])) {
	echo "<script>window.open('login.php','_self');</script>";
}

if (!isset($_POST['add_order']) and !isset($_POST['add_cart']) and !isset($_POST['coupon_submit'])) {
	echo "<script>window.open('index.php','_self');</script>";
}

 ?>

<?php 
//for the payment processing
$get_payment_setting="SELECT * from payment_settings";
$run_payment_setting=mysqli_query($con,$get_payment_setting);
$row_payament_setting=mysqli_fetch_array($run_payment_setting);
$processing_fee=$row_payament_setting['processing_fee'];
$enable_paypal=$row_payament_setting['enable_paypal'];
$paypal_email=$row_payament_setting['paypal_email'];
$paypal_currency_code=$row_payament_setting['paypal_currency_code'];
$paypal_sandbox=$row_payament_setting['paypal_sandbox'];

if ($paypal_sandbox =="on") {
$paypal_url="https://www.sandbox.paypal.com/cgi-bin/webscr";
}elseif ($paypal_sandbox=="off") {
$paypal_url="https://www.paypal.com/cgi-bin/webscr";
}
//setting backend for stript
$enable_stripe=$row_payament_setting['enable_stripe'];
//a copy get id and username/email... 
$login_seller_user_name=$_SESSION['seller_user_name'];
$select_login_seller="SELECT * from sellers where seller_user_name='$login_seller_user_name'";
$run_login_seller=mysqli_query($con,$select_login_seller);
$row_login_seller=mysqli_fetch_array($run_login_seller);
$login_seller_id=$row_login_seller['seller_id'];
$login_seller_email=$row_login_seller['seller_email'];

 ?>


 <?php 
if (isset($_POST['add_order']) or isset($_POST['coupon_submit'])){
$proposal_id=$_POST['proposal_id'];
$proposal_qty=$_POST['proposal_qty'];

$get_proposals="SELECT * from proposals where proposal_id='$proposal_id'";
$run_proposals=mysqli_query($con,$get_proposals);
$row_proposals=mysqli_fetch_array($run_proposals);
$proposal_price=$row_proposals['proposal_price'];
$proposal_title=$row_proposals['proposal_title'];
$proposal_url=$row_proposals['proposal_url'];
$proposal_img1=$row_proposals['proposal_img1'];

$sub_total=$proposal_price*$proposal_qty;

$total=$processing_fee+$sub_total;

$get_seller_accounts="SELECT * from seller_accounts where seller_id='$login_seller_id'";
$run_seller_accounts=mysqli_query($con,$get_seller_accounts);
$row_seller_accounts=mysqli_fetch_array($run_seller_accounts);
$current_balance=$row_seller_accounts['current_balance'];

  ?>

<!DOCTYPE html>
<html lang="en">
<head>
	
	<title>JuaKali||Order Details</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="juakali quality product">
	<meta name="keywords" content="product meterial">
	<meta name="author" content="JuaKali Mall">
	<link href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100" rel="stylesheet" >
	<link rel="stylesheet" href="styles/bootstrap.min.css">
	<link rel="stylesheet" href="styles/style.css">
	<link rel="stylesheet" href="styles/category_nav_style.css">
	<!-- Custome css from the user -->
	<link rel="stylesheet" href="styles/custom.css">
	<link rel="stylesheet" href="font-awesome/css/all.min.css">
	<link rel="stylesheet" href="styles/owl.carousel.css">
	<link rel="stylesheet" href="styles/owl.theme.default.css">
	 <script src="https://checkout.stripe.com/v2/checkout.js"></script>
	<script src="js/jquery.slim.min.js"></script>
</head>
<body>
<div class="preloader d-flex justify-content-center align-items-center">
    <img src="images/loadjuakali.gif" alt="the preloader"><br>
    <p>Loading...Please wait</p>
  </div>
<?php include("includes/header.php");?>
<div class="container mt-5 mb-5">
<div class="row">
<div class="col-md-7">
<div class="row">	
<?php 
if ($current_balance >=$sub_total) { ?>	

<div class="col-md-12 mb-3">
<div class="card payment-options">
<div class="card-header">
<h5>Avaible Shopping Balance</h5>
</div>
<div class="card-body">
<div class="row">
<div class="col-1">
<input type="radio" name="method" id="shopping-balance" class="form-control radio-input" checked>
</div>
<div class="col-11">
<p class="lead mt-2">Personal Balance - <?php echo $login_seller_user_name; ?> <span class="text-success font-weight-bold">Ksh <?php echo($current_balance); ?></span></p>
</div>
</div>
</div>
</div><!--payment and card ends-->
</div><!--col-md-12 mb-3 ends-->
<?php } ?>
<!-- add payment onDelivery -->
<div class="col-md-12 mb-3">
<div class="card payment-options">
<div class="card-header">
<h5>You Can Pay On Delivery</h5>
</div>
<div class="card-body">
<div class="row">
<div class="col-1">
<input type="radio" name="method" id="pay-delivery" class="form-control radio-input">
</div>
<div class="col-11">
<p class="lead mt-2"><span class="text-info"><?php echo strtoupper($login_seller_user_name); ?></span> You Can Pay Once Product Delivered<span class="text-success font-weight-bold"> Ksh <?php echo($sub_total); ?></span>
</p>
</div>
</div>
</div>
</div><!--payment and card ends-->
</div>
<!-- add payment onDelivery end -->
<div class="col-md-12 mb-3">
<div class="card payment-options">
<div class="card-header">
<div class="h5">Payment Option</div>
</div>
<div class="card-body">
<?php if ($enable_paypal=="yes") { ?>	
<div class="row">
<div class="col-1">
<input type="radio" name="method" id="paypal" class="form-control radio-input"
<?php 
if ($current_balance < $sub_total) {
	echo "checked";
}
 ?>
 >
</div>
<div class="col-11">
<img src="images/paypal.png" height="50" class="ml-2 width-xs-100">	 
</div>
</div>
<?php } ?>
<?php if ($enable_stripe=="yes") { ?>
<?php if ($enable_paypal=="yes") { ?>	
<hr>
<?php } ?>
<div class="row">
<div class="col-1">
<input type="radio" name="method" id="credit-card" class="form-control radio-input"
<?php if ($current_balance< $sub_total) {
	if ($enable_paypal=="no") {echo "checked";	}
} ?>
>
</div>
<div class="col-11">
<img src="images/credit_cards.jpg" height="50" class="ml-2 width-xs-100">	 
</div>
</div>
<?php } ?>
</div>
</div>
</div><!--col-md-12 mb-3 ends-->
</div><!--end of the inner row-->
</div><!--end of col-md-7 ends-->
<div class="col-md-5">
<div class="card checkout-details">
<div class="card-header">
<h5>
<i class="far fa-money-bill-alt"></i>&nbsp;Order Summary
</h5>
</div>
<div class="card-body">
<div class="row">
<div class="col-md-4 mb-3">
<img src="proposals/proposal_files/<?php echo($proposal_img1); ?>" class="img-fluid">
</div>
<div class="col-md-8">
<h5><?php echo($proposal_title); ?></h5>
</div>
</div>
<hr>
<h6>Proposal Price: <span class="float-right proposal_price">Ksh <?php echo($proposal_price); ?></span></h6>
<hr>
<h6>Proposal Quatity: <span class="float-right"><?php echo($proposal_qty); ?></span></h6>
<hr>
<h6 class="processing-fee">Processing Fee: <span class="float-right">Ksh <?php echo($processing_fee); ?></span></h6>
<hr class="processing-fee">
<h6>Apply Coupon Code:</h6>
<form method="post" class="input-group">
<input type="hidden" name="proposal_id" value="<?php echo($proposal_id); ?>">
<input type="hidden" name="proposal_qty" value="<?php echo(
$proposal_qty); ?>">
<input type="text" name="code" value="" class="form-control apply-disabled" placeholder="Enter coupon Code">
<span class="input-group-append">
<button type="submit" class=" input-group-text btn btn-success" name="coupon_submit">Apply</button></span>
</form>
<p class="coupon-response"></p>
<?php if (isset($_POST['code'])) {
 $coupon_code=$_POST['code'];
 if (!empty($coupon_code)) {
 	$select_coupon="SELECT * from coupons where proposal_id='$proposal_id' AND coupon_code='$coupon_code'";
 	$run_coupon=mysqli_query($con,$select_coupon);
 	$count_coupon=mysqli_num_rows($run_coupon);
if ($count_coupon == 1) {
$row_coupon=mysqli_fetch_array($run_coupon);
$coupon_limit=$row_coupon['coupon_limit'];	
$coupon_used=$row_coupon['coupon_used'];	
$coupon_price=$row_coupon['coupon_price'];	

if ($coupon_limit <= $coupon_used) {
echo "
<script>
$('.coupon-response').html('Your Coupon CODE Has Been Expired!').attr('class','coupon-response mt-2 p-2 bg-danger text-white');
</script>
";		
}else{
$update_coupon="UPDATE coupons set coupon_used=coupon_used+1 where proposal_id='$proposal_id' AND coupon_code='$coupon_code'";
$run_update=mysqli_query($con,$update_coupon);

$proposal_price=$coupon_price;
$proposal_qty=$_POST['proposal_qty'];
$sub_total=$proposal_price*$proposal_qty;
$total=$processing_fee+$sub_total;
echo "
<script>
$('.proposal_price').html('Ksh $proposal_price');
$('.coupon-response').html('Your Coupon CODE Has Been Applied!').attr('class','coupon-response mt-2 p-2 bg-success text-white');
</script>
";
}	
}else{
echo "

<script>
$('.coupon-response').html('Your Coupon Code Is not Valid!').attr('class','coupon-response mt-2 p-2 bg-danger text-white');
</script>
";	
}



 }

} ?>
<hr><h5 class="coupon-response">
Proposal Total : <span class="float-right total-price">Ksh <?php echo($total); ?></span>
</h5>
<hr>
<!--OnDelivery form data -->
<form action="shopping_balance.php" method="post" id="pay-delivery-form">
<input type="hidden" name="amount" value="<?php echo $sub_total; ?>">
<button type="submit" name="cart_submit_order" class="btn btn-lg btn-success btn-block" onclick="return confirm('You will pay your Order Once Delivered!')">
	Pay On Delivery
</button>
</form>

<!--OnDelivery form data ends-->

<?php 
if ($current_balance>=$sub_total) { ?>
<form action="shopping_balance.php" method="post" id="shopping-balance-form">
<input type="hidden" name="proposal_id" value="<?php echo($proposal_id); ?>">
<input type="hidden" name="proposal_qty" value="<?php echo($proposal_qty); ?>">
<input type="hidden" name="amount" value="<?php echo(
$sub_total); ?>">
<button name="checkout_submit_order" type="submit" class="btn btn-lg btn-success btn-block" onclick="return confirm('Do you Really want to order this proposal from your shopping Balance?')">Pay With Shopping Balance</button> 
</form>
<?php } ?>
<br>
<?php if ($enable_paypal=="yes") { ?>
<form action="<?php echo($paypal_url); ?>" method="post" id="paypal-form">
<input type="hidden" name="cmd" value="_xclick">
<input type="hidden" name="business" value="<?php echo($paypal_email); ?>">
<input type="hidden" name="tax" value="<?php echo($processing_fee);?>">
<input type="hidden" name="currency_code" value="<?php echo($paypal_currency_code); ?>">
<input type="hidden" name="cancel_return" value="<?php echo($site_url); ?>/proposals/<?php echo($proposal_url); ?>">
<input type="hidden" name="return" value="<?php echo($site_url); ?>/paypal_order.php?checkout_seller_id=<?php echo($login_seller_id); ?>&proposal_id=<?php echo($proposal_id); ?>&proposal_qty=<?php echo($proposal_qty); ?>&proposal_price=<?php echo($sub_total); ?>">
<input type="hidden" name="item_name" value="<?php echo($proposal_title); ?>">
<input type="hidden" name="item_number" value="1">
<input type="hidden" name="amount" value="<?php echo($proposal_price); ?>">
<input type="hidden" name="quantity" value="<?php echo($proposal_qty); ?>">
<button type="submit" name="submit" class="btn btn-lg btn-success btn-block">Pay with Paypal</button>
<p class="alert alert-danger"><strong>Alert!</strong> Will Be Redirected To PayPal Account</p>
</form><!--paypal-form ends-->
<?php } ?>

<?php if ($enable_stripe == "yes") { ?>

<?php 
include("stripe_config.php");
$stripe_total_amount=$total *100;
$stripe_total_amount=$stripe_total_amount/100;

 ?>	
<form action="checkout_charge.php" method="post" id="credit-card-form">
<input type="hidden" name="proposal_id" value="<?php echo($proposal_id); ?>">
<input type="hidden" name="proposal_qty" value="<?php echo($proposal_qty); ?>">
<input type="hidden" name="proposal_price" value="<?php echo($proposal_price); ?>">
<input type="hidden" name="amount" value="<?php echo($stripe_total_amount); ?>">
<input 
type="submit"
class="btn btn-lg btn-success btn-block stripe-submit"
value="Pay With Credit card"
data-key="<?php echo $stripe['publishable_key']; ?>"
data-amount="<?php echo($stripe_total_amount); ?>"
data-currency="<?php echo $stripe['currency_code']; ?>"
data-email="<?php echo($login_seller_email); ?>"
data-image="images/logo_transparent.png"
data-name="JuaKaliMall"
data-description="<?php echo($proposal_title); ?>"
data-allow-remember-me="false" 
>
<script>
			 $(document).ready(function() {
	            $('.stripe-submit').on('click', function(event) {
	                event.preventDefault();
	                var $button = $(this),
	                    $form = $button.parents('form');
	                var opts = $.extend({}, $button.data(), {
	                    token: function(result) {
	                        $form.append($('<input>').attr({ type: 'hidden', name: 'stripeToken', value: result.id })).submit();
	                    }
	                });
	                StripeCheckout.open(opts);
	            });
	        });
							</script>
						</form>
<?php } ?>						

					</div>
				</div>
			</div><!--ends of the col-md-5-->
	</div>
</div>
<script>
	$(document).ready(function(){

	<?php if ($current_balance >=$sub_total) { ?>

		$('.total-price').html('Ksh <?php echo($sub_total); ?>');
   $('.processing-fee').hide();
   $('#pay-delivery-form').hide();
	<?php }else{ ?>

	$('.total-price').html('Ksh <?php echo($total); ?>');
	 $('.processing-fee').show();
	 $('#pay-delivery-form').hide();

	<?php } ?>

		//code to modifie the payment button strt
<?php if ($current_balance >= $sub_total) { ?>

		$('#paypal-form').hide();
		$('#credit-card-form').hide();
		$('#pay-delivery-form').hide();

<?php }else{ ?>

  $('#shopping-balance-form').hide();

<?php } ?>
<?php if ($current_balance < $sub_total) { ?>

<?php if ($enable_paypal == "yes") { ?>
<?php }else{ ?>

$('#paypal-form').hide();

<?php } ?>
<?php } ?>
<?php if ($current_balance < $sub_total) { ?>
<?php if ($enable_stripe == "yes") { ?>
<?php if ($enable_paypal=="yes") { ?>

$('#credit-card-form').hide();

<?php }else{ ?>

<?php } ?>
<?php } ?>	
<?php } ?>
		//code to modifie the payment button ends

		$('#shopping-balance').click(function(){
			$('.col-md-5 .card br').show();
			$('.total-price').html('Ksh <?php echo($sub_total); ?>');
			$('.processing-fee').hide();
			$('#credit-card-form').hide();
			$('#paypal-form').hide();
			$('#shopping-balance-form').show();
			$('#pay-delivery-form').hide();
		});


		$('#paypal').click(function(){
			$('.col-md-5 .card br').hide();
			$('.total-price').html('Ksh <?php echo($total); ?>');
			$('.processing-fee').show();
			$('#credit-card-form').hide();
			$('#paypal-form').show();
			$('#shopping-balance-form').hide();
			$('#pay-delivery-form').hide();
		});


		$('#credit-card').click(function(){
			$('.col-md-5 .card br').hide();
			$('.total-price').html('Ksh <?php echo($total); ?>');
			$('.processing-fee').show();
			$('#credit-card-form').show();
			$('#paypal-form').hide();
			$('#shopping-balance-form').hide();
			$('#pay-delivery-form').hide();
		});

		//addedToPayOnDelivery
		$('#pay-delivery').click(function(){
			$('.col-md-5 .card br').show();
			$('.total-price').html('Ksh <?php echo($sub_total); ?>');
			$('.processing-fee').hide();
			$('#credit-card-form').hide();
			$('#paypal-form').hide();
			$('#shopping-balance-form').hide();
			$('#pay-delivery-form').show();
		});


	});
</script>
<?php include("includes/footer.php");?>
</body>
</html>
<?php 
}elseif (isset($_POST['add_cart'])) {
$proposal_id=$_POST['proposal_id'];
$proposal_qty=$_POST['proposal_qty'];
$select_proposal="SELECT * from proposals where proposal_id='$proposal_id'";
$run_proposal=mysqli_query($con,$select_proposal);
$row_proposal=mysqli_fetch_array($run_proposal);
$proposal_price=$row_proposal['proposal_price'];
$proposal_url=$row_proposal['proposal_url'];
$select_cart="SELECT * from cart where seller_id='$login_seller_id' AND proposal_id='$proposal_id'";
	$run_cart=mysqli_query($con,$select_cart);
	$count_cart=mysqli_num_rows($run_cart);
if ($count_cart ==1) {
echo "
<script>
alert('This Product Is Already Added In Cart');
window.open('proposals/$proposal_url','_self');
</script>
";		
}else{
$insert_cart="INSERT INTO cart(seller_id,proposal_id,proposal_price,proposal_qty)values('$login_seller_id','$proposal_id','$proposal_price','$proposal_qty')";
$run_insert_cart=mysqli_query($con,$insert_cart);
echo "
<script>
window.open('proposals/$proposal_url','_self');
</script>
";
}


}

 ?>
