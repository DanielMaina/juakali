
<?php
session_start();
include("includes/db.php");
if (isset($_SESSION['seller_user_name'])) {

$login_seller_user_name=$_SESSION['seller_user_name'];
$select_login_seller="select * from sellers where seller_user_name='$login_seller_user_name'";
$run_login_seller=mysqli_query($con,$select_login_seller);
$row_login_seller=mysqli_fetch_array($run_login_seller);

$login_seller_id=$row_login_seller['seller_id'];
$login_seller_email=$row_login_seller['seller_email'];
$login_seller_user_name=$row_login_seller['seller_user_name'];


}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	<title>Customer support</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="jua kali ptoduct agriculture domestic cloth shoe kitchen">
	<meta name="keywords" content="juakali products utensils bulding construction">
	<meta name="author" content="JuaKali Mall">
	<link href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100" rel="stylesheet" >
	<link rel="stylesheet" href="styles/bootstrap.min.css">
	<link rel="stylesheet" href="styles/style.css">
	<link rel="stylesheet" href="styles/category_nav_style.css">
	<!-- Custome css from the user -->
	<link rel="stylesheet" href="styles/custom.css">
	<link rel="stylesheet" href="font-awesome/css/all.min.css">
	<link rel="stylesheet" href="styles/owl.carousel.css">
	<link rel="stylesheet" href="styles/owl.theme.default.css">
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
	<script src="js/jquery.slim.min.js"></script>
</head>
<body>
<?php include("includes/header.php");?>
<div class="container pt-2">
<div class="row">
<div class="col-md-12 mt-4">
<?php 
 if (!isset($_SESSION['seller_user_name'])) {

 ?>
<div class="alert alert-warning rounded-0">
<p class="lead mt-1 mb-1">
<strong>Warning!</strong>
You cannot submit a support request without logging in to the system.!
</p>
</div><!--ends alert alert-warning-->
<?php }?>
</div>
</div>
<div class="row">
<div class="col-md-12">
<div class="card">
<?php
$get_contact_support="select * from contact_support";
$run_contact_support=mysqli_query($con,$get_contact_support);
$row_contact_support=mysqli_fetch_array($run_contact_support);
$contact_heading=$row_contact_support['contact_heading'];
$contact_desc=$row_contact_support['contact_desc'];
 ?>	
<div class="card-header text-center">
<h2><?php echo $contact_heading; ?></h2>
<p class="text-muted">
<?php  echo $contact_desc; ?>
</p>
</div><!--card-header text-center ends-->
<div class="card-body">
	<center>
<form action="contact.php" method="post" class="col-md-8 contact-form" enctype="multipart/form-data">
<div class="form-group">
<label class="float-left">Select Enquiry Type</label>
<select name="enquiry_type" class="form-control select_tag" required>
<option value="" url="contact.php">Select Enquiry</option>
<?php
if (isset($_GET['enquiry_id'])) {
	$enquiry_id=$_GET['enquiry_id'];
	$get_enquiry_type="select * from enquiry_types where enquiry_id='$enquiry_id'";
	$run_enquiry_type=mysqli_query($con,$get_enquiry_type);
	$row_enquiry_type=mysqli_fetch_array($run_enquiry_type);
	$enquiry_title=$row_enquiry_type['enquiry_title'];

	 echo "
   <option value='$enquiry_id' url='contact.php?enquiry_id=$enquiry_id' selected> 
   $enquiry_title
   </option>
	 ";
}
if (isset($_GET['enquiry_id'])) {
$get_enquiry_type="select * from enquiry_types where not enquiry_id='$enquiry_id'";
}else{
$get_enquiry_type="select * from enquiry_types";
}
$run_enquiry_type=mysqli_query($con,$get_enquiry_type);
while ($row_enquiry_types=mysqli_fetch_array($run_enquiry_type)) {
	$enquiry_id=$row_enquiry_types['enquiry_id'];
	$enquiry_title=$row_enquiry_types['enquiry_title'];

	 echo "
   <option value='$enquiry_id' url='contact.php?enquiry_id=$enquiry_id'>$enquiry_title</option>
	 ";
}
?>
</select>
</div><!--form-group ends-->
<?php
if (isset($_GET['enquiry_id'])) {
	

?>
<div class="form-group">
<label class="float-left">Subject *</label>
<input type="text" class="form-control" name="subject" required>
</div><!--form-group ends-->
<div class="form-group">
<label class="float-left">Message *</label>
<textarea name="message" class="form-control" cols="30" rows="10"></textarea>
</div><!--form-group ends-->
<?php
if ($_GET['enquiry_id']==1 or $_GET['enquiry_id']==2) { ?>
<div class="form-group">
<label class="float-left">Order Number *</label>
<input type="text" class="form-control" name="order_number" required>
</div><!--form-group ends-->
<div class="form-group">
<label class="float-left">Order Rule *</label>
<select name="order_rule" class="form-control" required>
<option value="" class="hidden">Select Order Rule</option>
<option>Buyer</option>
<option>Seller</option>
</select>
</div><!--form-group ends-->
<?php } ?>
<div class="form-group">
<label class="float-left">Attachment</label>
<input type="file" name="file" class="form-control">
</div><!--form-group ends-->
<div class="form-group">
<label>Please Verify that you are human</label>
<div class="g-recaptcha" data-sitekey="6LdGR7cUAAAAAKTkb4YA4uArLVsS8YttrG_CSMQ-"></div>
</div><!--form-group ends-->
<div class="text-center">
<button class="btn btn-primary btn-lg" type="submit" name="submit">
<i class="fa fa-user-md"></i> Send Message
</button>
</div><!--text-center ends-->
<?php }?>
</form>
</center>
</div>
</div>
</div><!--col-md-12 ends-->
</div>
</div>
<?php

if (isset($_POST['submit'])) {
	if (!isset($_SESSION['seller_user_name'])) {
	 	echo "
<script>alert('You cannot Submit a Support Request, Without Logging into the System!');</script>
	 	
	 <script>window.open('login.php','_self');</script>
	 ";	
exit();
}else{
$secret_key="6LdGR7cUAAAAAMZRNuUM0vMjVLpN9he53GhGZReO";
$response=$_POST['g-recaptcha-response'];
$remote_ip=$_SERVER['REMOTE_ADDR'];
$url=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret_key&response=$response&remoteip=$remote_ip");
$result=json_decode($url,TRUE);
if ($result["success"] == 1) {
$enquiry_type=mysqli_real_escape_string($con,$_POST['enquiry_type']);	
$subject=mysqli_real_escape_string($con,$_POST['subject']);	
$message=mysqli_real_escape_string($con,$_POST['message']);	
if($enquiry_type == 1 or $enquiry_type == 2) {
$order_number=mysqli_real_escape_string($con,$_POST['order_number']);	
$order_rule=mysqli_real_escape_string($con,$_POST['order_rule']);	
}else{
$order_number="";

$order_rule="";

}
$file=$_FILES['file']['name'];
$file_tmp=$_FILES['file']['tmp_name'];	
move_uploaded_file($file_tmp,"ticket_files/$file");
$date=date("h:i M d, Y");
$insert_support_ticket="INSERT INTO support_tickets (enquiry_id,sender_id,subject,message,order_number,order_rule,attachment,date,status) values('$enquiry_type','$login_seller_id','$subject','$message','$order_number','$order_rule','$file','$date','open')";
$run_select_ticket=mysqli_query($con,$insert_support_ticket);
if ($run_contact_support) {
	$select_enquiry_type="select * from enquiry_types where enquiry_id='$enquiry_type'";
	$run_enquiry_type=mysqli_query($con,$select_enquiry_type);
	$row_enquiry_type=mysqli_fetch_array($run_enquiry_type);
	$enquiry_title =$row_enquiry_type['enquiry_title'];
//Send Mail to  Admin...
if (!empty($file)) {
$email_message="

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style>

.container{
	background:rgb(238,238,238);
	padding:80px;

}
.box{
	background:#fff;
	margin:0px 0px 30px;
	padding:8px 20px 20px;
	border:1px solid #e6e6e6;
	box-shandow:0px 1px 5px rgba(0,0,0,0.1);
}
.table{
max-width:100%;
background-color:#fff;
margin-bottom:;	
}
.table thead tr th{
border:1px solid #ddd;
font-weight:bolder;
padding:10px;	
}

.table tbody tr td{
	border:1px solid #ddd;
	padding:10px;
}   

hr{
	margin-top:20px;
	margin-botton:20px;
	border:1px solid #eee;
}
.logo{
	height:50px;
	width:50px;
	border-radius:50%;

}

	</style>
</head>
<body>
<div class='container'>
<div class='box'>
<center>
<button class='logo'> <img src='$site_url/images/pinterest_profile_image.png'></button>
<h2>Hello JuaKali Administrator</h2>
<h2>This message Has been sent from Submit a Request Form</h2>
<hr>
<table class='table'>
<thead>
<tr>
<th>Enquiry Type</th>
<th>Email Address</th>
<th>Subject</th>
<th>Message </th>
<th>Attachment</th>
<th>Sender UserName</th> 
</tr>
</thead>
<tbody>
<tr>
<td>$enquiry_title</td>
<td>$login_seller_email</td>
<td>$subject</td>
<td>$message</td>
<td>$file</td>
<td>$login_seller_user_name</td>
</tr>
</tbody>
</table>
</center>
</div>
</div>
</body>
</html>

";//end of the mail text...
}else{
$email_message="

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style>

.container{
	background:rgb(238,238,238);
	padding:80px;

}
.box{
	background:#fff;
	margin:0px 0px 30px;
	padding:8px 20px 20px;
	border:1px solid #e6e6e6;
	box-shandow:0px 1px 5px rgba(0,0,0,0.1);
}
.table{
max-width:100%;
background-color:#fff;
margin-bottom:;	
}
.table thead tr th{
border:1px solid #ddd;
font-weight:bolder;
padding:10px;	
}

.table tbody tr td{
	border:1px solid #ddd;
	padding:10px;
}  
hr{
	margin-top:20px;
	margin-botton:20px;
	border:1px solid #eee;
}
.logo{
	height:50px;
	width:50px;
	border-radius:50%;

}

	</style>
</head>
<body>
<div class='container'>
<div class='box'>
<center>
<button class='logo'> <img src='$site_url/images/pinterest_profile_image.png'></button>
<h2>Hello JuakaliMall Administrator</h2>
<h2>This message Has been sent from Submit a Request Form</h2>
<hr>
<table class='table'>
<thead>
<tr>
<th>Enquiry Type</th>
<th>Email Address</th>
<th>Subject</th>
<th>Message </th>
<th>Sender UserName</th> 
</tr>
</thead>
<tbody>
<tr>
<td>$enquiry_title</td>
<td>$login_seller_email</td>
<td>$subject</td>
<td>$message</td>
<td>$login_seller_user_name</td>
</tr>
</tbody>
</table>
</center>
</div>
</div>
</body>
</html>

";//end of the mail text...	
}	
$headers ="From: Juakali Mall\r\n";
$headers .="Reply-To: $login_seller_email\r\n";
$headers .="content-type: text/html\r\n";

$get_cantact_support="SELECT * from contact_support";
$run_contact_support=mysqli_query($con,$get_cantact_support);
$row_contact_support=mysqli_fetch_array($run_contact_support);
$contact_email=$row_contact_support['contact_email'];
mail($contact_email, $subject, $email_message,$headers);
//Send mail to the ADMIN ENDS


//send email to sender

$subject="Welcome to JuaKali Mall Dear $login_seller_user_name";
$message="
<!DOCTYPE html>
<html>
<head>
<title></title>
<style>

.container{
	background:rgb(238,238,238);
	padding:80px;

}
.box{
	background:#fff;
	margin:0px 0px 30px;
	padding:8px 20px 20px;
	border:1px solid #e6e6e6;
	box-shandow:0px 1px 5px rgba(0,0,0,0.1);
}  
hr{
	margin-top:20px;
	margin-botton:20px;
	border:1px solid #eee;
}

.lead{
	font-size:10px;

}

	</style>
</head>
<body class='container'>
<div class='box'>
<center>
<img src='$site_url/images/pinterest_profile_image.png' width='100'>
<h2> Hi! $login_seller_user_name, Welcom To Juakali Mall</h2>
<p class='lead'>Thank You For Contacting us!</p>
<hr>
<p class='lead'>We shall Contact You Soon, Thank Your for using Customer Support!</p>
</center>
</div>	
</body>
</html>


";
 
$headers="From: JuakaliMall\r\n";
$headers .="Reply-To: $contact_email\r\n";
$headers .="content-type: text/html\r\n";
mail($login_seller_email, $subject, $message,$headers);
echo "
<script>
alert('Your Message Has been Successfully Sent, We shall Contact Your Soon');

</script>
";
}
}else{
echo "
<script>alert('Please Select Captcha, And Try Again');</script>
";
}
} 
}
?>
<?php include("includes/footer.php");?>
<script>
	$(document).ready(function(){
		$(".select_tag").change(function(){
url=$(".select_tag option:selected").attr('url');
window.location.href=url;
		});
	});
</script>
</body>
</html>