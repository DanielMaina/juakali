<?php 
session_start();
include("../includes/db.php");
if (!isset($_SESSION['seller_user_name'])) {
echo "<script>window.open('../login.php','_self');</script>";
}
//get the buyer/user details to create the session
$login_seller_user_name=$_SESSION['seller_user_name'];
$select_login_seller="SELECT * from sellers where seller_user_name='$login_seller_user_name'";
$run_login_seller=mysqli_query($con,$select_login_seller);
$row_login_seller=mysqli_fetch_array($run_login_seller);
$login_seller_id=$row_login_seller['seller_id'];

 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	<title><?php echo $login_seller_user_name; ?>/ conversation</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="JuaKaliMall">
	<link href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100" rel="stylesheet" >
	<link rel="stylesheet" href="../styles/bootstrap.min.css">
	<link rel="stylesheet" href="../styles/style.css">
	<link rel="stylesheet" href="../styles/user_nav_style.css">
	<!-- Custome css from the user -->
	<link rel="stylesheet" href="../styles/custom.css">
	<link rel="stylesheet" href="../font-awesome/css/all.min.css">
	<script src="../js/jquery.slim.min.js"></script>
	<script src="https://checkout.stripe.com/v2/checkout.js"></script>
</head>
<body>
<?php include("../includes/user_header.php");?>
<div class="container">
<div class="row">
<?php 

$single_message_id=$_GET['single_message_id'];
$update_inbox_seller="UPDATE inbox_sellers set message_status='read' where receiver_id='$login_seller_id' AND message_status='unread' AND message_group_id='$single_message_id'";
$run_update_inbox_sellers=mysqli_query($con,$update_inbox_seller);
$update_inbox_messages="UPDATE inbox_messages set message_status='read' where message_receiver='$login_seller_id' AND message_status='unread' AND message_group_id='$single_message_id'";
$run_update_inbox_messages=mysqli_query($con,$update_inbox_messages);
$get_inbox_sellers="SELECT * from inbox_sellers where message_group_id='$single_message_id'";
$run_inbox_sellers=mysqli_query($con,$get_inbox_sellers); 
$row_inbox_sellers=mysqli_fetch_array($run_inbox_sellers);

$offer_id=$row_inbox_sellers['offer_id'];
$sender_id=$row_inbox_sellers['sender_id'];
$receiver_id=$row_inbox_sellers['receiver_id'];

if ($login_seller_id==$sender_id) {
$seller_id=$receiver_id;	
}else{
$seller_id=$sender_id;
}
$select_seller="SELECT * from sellers where seller_id='$seller_id'";
$run_seller=mysqli_query($con,$select_seller);
$row_seller=mysqli_fetch_array($run_seller);
$seller_image=$row_seller['seller_image'];
$seller_user_name=$row_seller['seller_user_name'];
$seller_level=$row_seller['seller_level'];
$seller_vacation=$row_seller['seller_vacation'];
$seller_recent_delivery=$row_seller['seller_recent_delivery'];
$seller_rating=$row_seller['seller_rating'];
$seller_status=$row_seller['seller_status'];

$get_seller_level="SELECT * from seller_levels where level_id='$seller_level'";
$run_seller_level=mysqli_query($con,$get_seller_level);
$row_seller_level=mysqli_fetch_array($run_seller_level);
$level_title=$row_seller_level['level_title'];


 ?>	

<div class="col-md-12 mb-5 mt-5">
<div class="row insert-message">
<div class="col-md-3">
<div class="row">
<div class="col-lg-5 col-md-12 text-center">
<?php 
if (!empty($seller_image)) { ?>
<img src="../user_images/<?php echo($seller_image); ?>" class="Seller-image">	
<?php }else{?>
<img src="../user_images/empty-image.png" class="Seller-image">
<?php } ?>	

<?php if ($seller_level==2) { ?>
<img src="../images/level_badge_1.png" class="seller-level-image">
<?php }elseif ($seller_level==3) {?>
<img src="../images/level_badge_2.png" class="seller-level-image">
<?php }elseif ($seller_level==4) { ?>
<img src="../images/level_badge_3.png" class="seller-level-image">
<?php } ?>	
	
</div><!--col-lg-5 col-md-12 text-cente ends-->
<div class="col-lg-7 col-md-12 mt-lg-0 mt-3 text-lg-left text-center">
<h4>
<a href="../user/<?php echo($seller_user_name); ?>" target="blank"><?php echo($seller_user_name); ?></a>	
</h4>
<p><?php echo($level_title); ?></p>

</div><!--col-lg-7 col-md-12 mt-lg-0 mt-3 text-lg-left text-cente ends-->
</div>	
</div><!--col-md-3 ends-->
<div class="col-md-9 responsive-border mt-lg-0 mt-3 text-md-left text-center">
<p class="p-style mt--md-0 mt-3">
<i class="fas fa-clock"></i>Recent Delivery <b>
<?php 
if ($seller_recent_delivery=="none") {
echo "No Order Derivered Recent!";	
}else{
echo($seller_recent_delivery);

}
?>

</b>	
</p>
<p class="p-style">
<i class="fa fa-comments"></i> Performance
<?php 
$sel_languages_relation="SELECT * from language_relation where seller_id='$seller_id'";
$run_languages_relation=mysqli_query($con,$sel_languages_relation);
$count_languages_relation=mysqli_num_rows($run_languages_relation);

if (!$count_languages_relation==0) {
while ($row_languages_relation=mysqli_fetch_array($run_languages_relation)) {
$language_id=$row_languages_relation['language_id'];	
$language_level=$row_languages_relation['language_level'];	
		
$get_seller_languages="SELECT * from seller_languages where language_id='$language_id'";
$run_seller_languages=mysqli_query($con,$get_seller_languages);
$row_seller_languages=mysqli_fetch_array($run_seller_languages);
$language_title=$row_seller_languages['language_title'];		
 ?>
<b><?php echo($language_title); ?> </b> (<?php echo($language_level); ?>)
</p>
<?php } } ?>	
<p class="p-style">
<i class="fa fa-smile"></i> Positive Ratings <b><?php echo($seller_rating); ?>%</b>	
</p>
</div><!--col-md-9 responsive-border mt-lg-0 mt-3 text-md-left text-center ends-->
</div><!--row insert-message ends-->	
</div><!--col-md-12 mb-5 mt-3 ends-->
<div class="col-md-12">
<div id="display-request">
<?php if(!empty($offer_id)){
// copy from view_offers.php
$select_offers="SELECT * from send_offers where offer_id='$offer_id'";
$run_offers=mysqli_query($con,$select_offers);
$row_offers=mysqli_fetch_array($run_offers);	
$request_id=$row_offers['request_id'];	
$proposal_id=$row_offers['proposal_id'];
$description=$row_offers['description'];	
$delivery_time=$row_offers['delivery_time'];	
$amount=$row_offers['amount'];

// a copy from view_offers.php
 $get_requests="SELECT * from buyer_requests where request_id='$request_id'";
$run_requests=mysqli_query($con,$get_requests);
$row_requests=mysqli_fetch_array($run_requests);
$request_id=$row_requests['request_id'];
$request_description=$row_requests['request_description'];

//a copy from view_offers.php
$select_proposals="SELECT * from proposals where proposal_id='$proposal_id'";
$run_proposals=mysqli_query($con,$select_proposals);
$row_proposals=mysqli_fetch_array($run_proposals);
$proposal_title =$row_proposals['proposal_title'];
?>	
<div class="request-div">
<h5>
THIS MESSAGE IS RELATED TO THE FOLLOWING REQUEST:
<span class="btn-close float-right">X</span>	
</h5>
<p>"<?php echo($request_description); ?>"</p>
<span class="arrow">
	View Offer <i class="fa fa-caret-down"></i>
</span>	
</div><!--request-div ends-->
<div class="offer-div">
<h5>
<?php echo($proposal_title); ?>
<span class="price float-right">Ksh <?php echo($amount); ?></span>	
</h5>	
<p><?php echo($description);?></p>
<p>
<strong><i class="fa fa-clock"></i> Delivery Time </strong><?php echo($delivery_time); ?>	
</p>
</div><!--offer-div ends-->	
<script>
//hide and show the custome alert format top...
$(".offer-div").hide();
$(".arrow").click(function(){
	$(".offer-div").slideToggle();
});	
$(".btn-close").click(function(){
	$(".request-div").fadeOut().remove();
	$(".offer-div").fadeOut().remove();
});
</script>
<?php } ?>
</div><!--display-request-->
<div id="display-messages" class="bg-white">

<?php include("display_messages.php")?>

</div><!--display-messages ends-->
<?php if ($seller_vacation=="on") {
	
 ?>
<div id="seller-vacation-div">
<h3>
Dear <?php echo $seller_user_name; ?>	
</h3>
<p class="lead">
You Cannot Send Message This Person is on Vacation.
</p>	
</div><!--seller-vacation-div ends-->
<?php }elseif($seller_vacation=="off"){ ?>
<div class="insert-message-box">
<div class="float-right">
<p class="text-mute mt-1">
<?php echo ucfirst($seller_user_name); ?> <span class="text-success"> <?php echo $seller_status; ?> </span>| Local Time <i class="fa fa-clock"></i>&nbsp;<?php date_default_timezone_set("africa/nairobi"); echo date("h:i A"); ?>
</p>	
</div><!--float-right ends-->	
<form id="insert-message-form" method="post">
	<textarea class="form-control mb-2" id="message" placeholder="Type Message here..." rows="5"></textarea>
	<button type="submit" class="btn btn-success ml-2 float-right">Send</button>
	<button type="button" id="send-offer" class="btn btn-success float-right">Send Offer</button>
</form>
<div class="clearfix"></div>
<p class="mt-lg-0 mt-2 mb-0">
	<span class="font-weight-bold">Accepted File Format:</span>
	jpeg,jpg,gif,png,mp4,mp3
</p>
<div class="form-row align-item-center message-attachment">
<label  class="h6 ml-2 mt-1">Attach File (Optional)</label>
<input type="file" id="file" class="form-control-file p-1 mb-2 mb-sm-0">
</div><!--form-row align-item-center message-attachment ends-->
</div><!--insert-message-box ends-->
<?php } ?>
</div><!--col-md-12-->	
</div><!--row ends-->	
</div><!--container ends-->
<div id="upload_file_div"></div>
<div id="accept-offer-div"></div>
<div id="send-offer-div"></div>
<script>
$(document).ready(function(){
$("#send-offer").click(function(){

	receiver_id="<?php echo($seller_id); ?>";
	message=$("#message").val();
	file=$("#file").val();
	if (file=="") {
   message_file=file;
	}else{
 message_file=document.getElementById("file").files[0].name;
	}
	$.ajax({
    method: "POST",
    url: "send_offer_modal.php",
    data: {receiver_id: receiver_id, message: message, file:message_file}
	}).done(function(data){
		$("#send-offer-div").html(data);
	});
});
// to update the message base on ajax
single_message_id = <?php echo($single_message_id); ?>;

//a copy from order_deatils.php 
//jquery code to get the upload files
$(document).on('change','#file', function(){
var form_data = new FormData();
form_data.append("file", document.getElementById('file').files[0]);
$.ajax({
	url: "upload_file.php",
	method: "POST",
	data: form_data,
	contentType: false,
	cache: false,
	processData: false,

}).done(function(data){
$("#upload_file_div").empty();
$("#upload_file_div").append(data);
});
});//ends document.on

//code to upload the  message
$('#insert-message-form').submit(function(e){

e.preventDefault();
message=$('#message').val();
if (message == "") {

}else{
file=$('#file').val();
if (file=="") {
message_file=file;
}else{
message_file = document.getElementById("file").files[0].name;
}
$.ajax({
method: "POST",
url: "insert_inbox_message.php",
data: {single_message_id: single_message_id, message: message,file: message_file},
success: function(data){
	$('#message').val('');
	$('#file').val('');
}

});
}

});

setInterval(function(){
$.ajax({
method: "GET",
url: "display_messages.php",
data: {single_message_id: single_message_id}
})
.done(function(data){
$('#display-messages').empty();
$('#display-messages').append(data);
});


},1000);
});
</script>
<?php include("../includes/footer.php");?>
</body>
</html>