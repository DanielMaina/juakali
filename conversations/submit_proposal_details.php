<?php 
@session_start();
include("../includes/db.php");
if (!isset($_SESSION['seller_user_name'])) {
echo "<script>window.open('../login.php','_self');</script>";
}
//get the buyer/user details to create the session
$login_seller_user_name=$_SESSION['seller_user_name'];
$select_login_seller="SELECT * from sellers where seller_user_name='$login_seller_user_name'";
$run_login_seller=mysqli_query($con,$select_login_seller);
$row_login_seller=mysqli_fetch_array($run_login_seller);
$login_seller_id=$row_login_seller['seller_id'];

$proposal_id=$_POST['proposal_id'];
$message=$_POST['message'];
$file=$_POST['file'];
$receiver_id=$_POST['receiver_id'];
//copy from send_offer_modal.php

$select_proposals="SELECT * from proposals where proposal_id='$proposal_id'";
$run_proposals=mysqli_query($con,$select_proposals);
$row_porposals=mysqli_fetch_array($run_proposals);
$proposal_title=$row_porposals['proposal_title'];


 ?>
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title">Specify Your Product Details</h5>
<button class="close" data-dismiss="modal"><span>&times;</span></button>	
</div><!--modal-header ends-->
<div class="modal-body p-0">
<form id="proposal-details-form">
<div class="selected-proposal p-3">
<h5><?php echo $proposal_title; ?></h5>
<hr>
<input type="hidden" name="proposal_id" value="<?php echo $proposal_id; ?>">
<input type="hidden" name="receiver_id" value="<?php echo $receiver_id; ?>">
<input type="hidden" name="message" value="<?php echo $message; ?>">
<input type="hidden" name="file" value="<?php echo $file; ?>">
<div class="form-group">
<label class="font-weight-bold">Description :</label>
<textarea name="description" class="form-control" required>
	
</textarea>
</div>
<hr>
<div class="form-group">
			<label class="font-weight-bold">Delivery Time :</label>
			<select name="delivery_time" class="form-control float-right" id="">
				<option value="1 Day">1 Day</option>
				<option value="2 Day">2 Day</option>
				<option value="3 Day">3 Day</option>
			</select>
		</div>
		<hr>
		<div class="form-group">
			<label class="font-weight-bold">Total Offer Amount :</label>
			<div class="input-group float-right">
				<span class="input-group-prepend">
					<span class="input-group-text">Ksh</span>
				</span>
				<input type="number" name="amount" min="100" placeholder="100 Minimum" class="form-control">
			</div>
		</div>
	
	</div>
	<div class="modal-footer">
		<button class="btn btn-secondary" type="button" data-dismiss="modal" data-toggle="modal" data-target="#send-offer-modal">
			Back
		</button>
		<button class="btn btn-success" type="submit">Submit Offer</button>
	</div>
</form>	
</div><!--modal-body ends p-0-->	
</div><!--modal-content ends-->
<div id="insert_offer"></div>
	<script>
		$(document).ready(function(){
			$("#proposal-details-form").submit(function(event){
event.preventDefault();
$.ajax({
	method: "POST",
	url: "<?php echo($site_url); ?>/conversations/insert_offer.php",
	data : $('#proposal-details-form').serialize()
})
.done(function(data){
	$("#submit-proposal-details").modal('hide');
	$("#insert_offer").html(data);
});
			});
		});
	</script>
