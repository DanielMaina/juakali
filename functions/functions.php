<?php
if (isset($_SESSION['seller_user_name'])) {
$login_seller_user_name=$_SESSION['seller_user_name'];
$select_login_seller="SELECT * from sellers where seller_user_name='$login_seller_user_name'";
$run_login_seller=mysqli_query($con,$select_login_seller);
$row_login_seller=mysqli_fetch_array($run_login_seller);

$login_seller_id=$row_login_seller['seller_id'];

}
//get_search_proposal functions strt
function get_search_proposals(){
global $con;
global $login_seller_id;
$search_query=$_SESSION['search_query'];
$online_sellers=array();
$get_proposals="SELECT DISTINCT proposal_seller_id FROM proposals WHERE proposal_title LIKE '%$search_query%' AND proposal_status='active'";
$run_proposals=mysqli_query($con,$get_proposals);
while ($row_proposals=mysqli_fetch_array($run_proposals)){
	$proposal_seller_id=$row_proposals['proposal_seller_id'];
 
$select_seller="SELECT * FROM sellers WHERE seller_id='$proposal_seller_id'" ;
$run_seller=mysqli_query($con,$select_seller);
$row_seller=mysqli_fetch_array($run_seller);
$seller_status=$row_seller['seller_status'];
if ($seller_status == "online") {
	array_push($online_sellers,$proposal_seller_id);

	}	

 }//ends while loops
 $where_online=array();
 $where_cat=array();
 $where_delivery_time=array();
 $where_level=array();
 $where_language=array();
 if (isset($_REQUEST['online_sellers'])) {
 	foreach ($_REQUEST['online_sellers'] as $value) {
 		if ($value != 0) {
 			foreach ($online_sellers as $seller_id) {
 				$where_online [] ="proposal_seller_id=" . $seller_id;
 			}
 		}//end inner if
 	}//end foreach
 }//end of out if isset

 if (isset($_REQUEST['cat_id'])) {
 	foreach ($_REQUEST['cat_id'] as $value) {
 		if ($value != 0) {
 			$where_cat[]= "proposal_cat_id=" . $value; 
 		}//end inner if
 	}//end foreach
 }//ends of if isset

 if (isset($_REQUEST['delivery_time'])) {
 	foreach ($_REQUEST['delivery_time'] as $value) {
 		if ($value != 0) {
 			$where_delivery_time[]="delivery_id=" . $value; 
 		}//end inner if
 	}//end foreach
 }//ends of if isset

 if (isset($_REQUEST['seller_level'])) {
 	foreach ($_REQUEST['seller_level'] as $value) {
 		if ($value != 0) {
 			$where_level[]="level_id=" .$value; 
 		}//end inner if
 	}//end foreach
 }//ends of if isset

if (isset($_REQUEST['seller_language'])) {
 	foreach ($_REQUEST['seller_language'] as $value) {
 		if ($value != 0) {
 			$where_language[]="language_id=" . $value; 
 		}//end inner if
 	}//end foreach
 }//ends of if isset



$query_where="where proposal_title like '%$search_query%' AND  proposal_status='active'";
if (count($where_online)>0) {
$query_where .=" and (". implode(" or ",$where_online) . ")";
}

if (count($where_cat)>0) {
$query_where .=" and (". implode(" or ",$where_cat) . ")";
}
if (count($where_delivery_time)>0) {
$query_where .=" and (". implode(" or ",$where_delivery_time) . ")";
}
if (count($where_level)>0) {
$query_where .=" and (". implode(" or ",$where_level) . ")";
}
if (count($where_language)>0) {
$query_where .=" and (". implode(" or ",$where_language) . ")";
}
//code for pagination
$per_page=9;
if (isset($_GET['page'])) {

	$page = $_GET['page'];

}else{

	$page=1;

}
$start_from=($page-1) * $per_page;

$where_limit ="order by proposal_featured='yes' DESC LIMIT $start_from,$per_page";
$get_proposals = "select * from proposals " . $query_where . $where_limit;
$run_proposals=mysqli_query($con, $get_proposals);
$count_proposals=mysqli_num_rows($run_proposals);

if ($count_proposals==0) {
	echo "
   <div class='col-md-12'>
    <h1 class='alert alert-danger text-center mt-4'>Sorry! NO Result Found! Try Another Word</h1>
   </div>

	";
}
while ($row_proposals=mysqli_fetch_array($run_proposals)) {
  $proposal_id=$row_proposals['proposal_id'];
  $proposal_title=$row_proposals['proposal_title'];
  $proposal_price=$row_proposals['proposal_price'];
  $proposal_img1=$row_proposals['proposal_img1'];
  $proposal_video=$row_proposals['proposal_video'];
  $proposal_seller_id=$row_proposals['proposal_seller_id'];
  $proposal_rating=$row_proposals['proposal_rating'];
  $proposal_url=$row_proposals['proposal_url'];
  $proposal_featured=$row_proposals['proposal_featured'];
 if (empty($proposal_video)) {
$video_class="";
 }else{
$video_class="video-img";
 }
 $select_seller="select * from sellers where seller_id='$proposal_seller_id'";
 $run_seller=mysqli_query($con,$select_seller);
 $row_seller=mysqli_fetch_array($run_seller);
 $seller_user_name=$row_seller['seller_user_name'];

 $seller_buyer_reviews="select * from buyer_reviews where proposal_id='$proposal_id'";
 $run_buyer_reviews=mysqli_query($con,$seller_buyer_reviews);
 $count_reviews=mysqli_num_rows($run_buyer_reviews);

 $select_favorites="SELECT * from favorites where proposal_id='$proposal_id' AND seller_id='$login_seller_id'";
 $run_favorites=mysqli_query($con,$select_favorites);
 $count_favorites=mysqli_num_rows($run_favorites);
 if ($count_favorites == 0) {
  $show_favorite_id="favorite_$proposal_id";
  $show_favorite_class="favorite";
 }else{
    $show_favorite_id="unfavorite_$proposal_id";
  $show_favorite_class="favorited";
 }
?>

<div class="col-lg-4 col-md-6 col-sm-6">
<div class="proposal-div">
<div class="proposal_nav"><span class="float-left mt-2">
<strong class="ml-2 mr-1">By</strong><?php echo $seller_user_name; ?></span>
<span class="float-right mt-2">
<?php
for($proposal_i=0 ; $proposal_i < $proposal_rating; $proposal_i++){
	echo "<img src='images/user_rate_full.png' class='rating'>";
}
for($proposal_i=$proposal_rating ; $proposal_i < 5 ; $proposal_i++){
	echo "<img src='images/user_rate_blank.png' class='rating'>";

}
?>

<span class="ml-1 mr-2">(<?php echo $count_reviews ; ?>)</span>
</span>
<div class="clearfix mb-2"></div>
</div>
<a href="proposals/<?php echo $proposal_url;?>">
<hr class="m-0 p-0">
<img src="proposals/proposal_files/<?php echo $proposal_img1 ; ?>" class="resp-img" alt="proposal">
</a>
<div class="text">
<h4><a href="proposals/<?php echo $proposal_url ; ?>" class="<?php echo($video_class)?>">
<?php echo $proposal_title; ?>
</a></h4>
<hr>
<p class="buttons clearfix">
<?php
//for the purpose of creating dynamic favourite
 if (!isset($_SESSION['seller_user_name'])) { ?>	
<a href="login.php" class="favorite mt-2 float-left" data-toggle="tooltip" title="Add To Favorites"><i class="fa fa-heart fa-lg"></i></a>
<?php }else{ ?>

  <a href="#" id="<?php echo($show_favorite_id); ?>" class="<?php echo($show_favorite_class); ?> mt-2 float-left" data-toggle="tooltip" title="Add To Favorites"><i class="fa fa-heart fa-lg"></i></a>
<?php } ?>
<?php if (isset($_SESSION['seller_user_name'])) { ?>
<script>
$(document).on("click","#favorite_<?php echo $proposal_id ?>", function(event){
 event.preventDefault();
var seller_id="<?php echo($login_seller_id); ?>";
var proposal_id="<?php echo($proposal_id); ?>";
$.ajax({
type:"POST",
url: "includes/add_delete_favorite.php",
data: {seller_id: seller_id, proposal_id: proposal_id, favorite: "add_favorite"},
success:function(){
  $("#favorite_<?php echo $proposal_id; ?>").attr({
    id: "unfavorite_<?php echo $proposal_id?>", class: " favorited mt-2 float-left"
  });
}
});
});

//copy for the same favorite fanctionality
$(document).on("click","#unfavorite_<?php echo $proposal_id ?>", function(event){
 event.preventDefault();
var seller_id="<?php echo($login_seller_id); ?>";
var proposal_id="<?php echo($proposal_id); ?>";
$.ajax({
type:"POST",
url: "../includes/add_delete_favorite.php",
data: {seller_id: seller_id, proposal_id: proposal_id, favorite: "delete_favorite"},
success:function(){
  $("#unfavorite_<?php echo $proposal_id; ?>").attr({
    id: "favorite_<?php echo $proposal_id?>", class: "favorite mt-2 float-left"
  });
}
});
});
</script>
<?php } ?>
<span class="float-right">STARTING AT <strong class="price"><?php echo $proposal_price ; ?></strong></span>
</p>
</div>
<?php if ($proposal_featured =='yes') {
# hide/show the ribbon
?>
<div class="ribbon">
<div class="theribbon">Featured</div>
<div class="ribbon-background"></div>
</div>
<?php }?>
</div><!--ends proposal-div-->	
</div>

<?php
}//$row_proposal while loop ends



}//function ends
//get_search_proposal functions ends


//get_search_pagination functions strt
function get_search_pagination(){
global $con;
$search_query=$_SESSION['search_query'];
$online_sellers=array();
$get_proposals="SELECT DISTINCT proposal_seller_id FROM proposals WHERE proposal_title LIKE '%$search_query%' AND proposal_status='active'";
$run_proposals=mysqli_query($con,$get_proposals);
while ($row_proposals=mysqli_fetch_array($run_proposals)){
	$proposal_seller_id=$row_proposals['proposal_seller_id'];
 
$select_seller="SELECT * FROM sellers WHERE seller_id='$proposal_seller_id'" ;
$run_seller=mysqli_query($con,$select_seller);
$row_seller=mysqli_fetch_array($run_seller);
$seller_status=$row_seller['seller_status'];
if ($seller_status == "online") {
	array_push($online_sellers,$proposal_seller_id);

	}	

 }//ends while loops
 $where_online=array();
 $where_cat=array();
 $where_delivery_time=array();
 $where_level=array();
 $where_language=array();
 $where_path="";
 if (isset($_REQUEST['online_sellers'])) {
 	foreach ($_REQUEST['online_sellers'] as $value) {
 		if ($value != 0) {
 			foreach ($online_sellers as $seller_id) {
 				$where_online [] ="proposal_seller_id=" . $seller_id;
 			}
 			$where_path .="online_sellers[]=" . $value . "&";
 		}//end inner if
 	}//end foreach
 }//end of out if isset

 if (isset($_REQUEST['cat_id'])) {
 	foreach ($_REQUEST['cat_id'] as $value) {
 		if ($value != 0) {
 			$where_cat[]= "proposal_cat_id=" . $value;
 			$where_path .="cat_id[]=" .$value . "&";
 		}//end inner if
 	}//end foreach
 }//ends of if isset

 if (isset($_REQUEST['delivery_time'])) {
 	foreach ($_REQUEST['delivery_time'] as $value) {
 		if ($value != 0) {
 			$where_delivery_time[]="delivery_id=" . $value;
 			$where_path .="delivery_time[]=" . $value . "&"; 
 		}//end inner if
 	}//end foreach
 }//ends of if isset

 if (isset($_REQUEST['seller_level'])) {
 	foreach ($_REQUEST['seller_level'] as $value) {
 		if ($value != 0) {
 			$where_level[]="level_id=" .$value;
 			$where_path .="seller_level[]=" .$value . "&"; 
 		}//end inner if
 	}//end foreach
 }//ends of if isset

if (isset($_REQUEST['seller_language'])) {
 	foreach ($_REQUEST['seller_language'] as $value) {
 		if ($value != 0) {
 			$where_language[]="language_id=" . $value;
 			$where_path .="seller_language[]=" . $value . "&"; 
 		}//end inner if
 	}//end foreach
 }//ends of if isset



$query_where="where proposal_title like '%$search_query%' AND  proposal_status='active'";
if (count($where_online)>0) {
$query_where .=" and (". implode(" or ",$where_online) . ")";
}

if (count($where_cat)>0) {
$query_where .=" and (". implode(" or ",$where_cat) . ")";
}
if (count($where_delivery_time)>0) {
$query_where .=" and (". implode(" or ",$where_delivery_time) . ")";
}
if (count($where_level)>0) {
$query_where .=" and (". implode(" or ",$where_level) . ")";
}
if (count($where_language)>0) {
$query_where .=" and (". implode(" or ",$where_language) . ")";
}
//code for pagination
$per_page=9;
$get_proposals="SELECT * from proposals " . $query_where;
$run_proposals=mysqli_query($con, $get_proposals);
$count_proposals=mysqli_num_rows($run_proposals);
if ($count_proposals>0) {
	$total_pages=ceil($count_proposals/$per_page);
	echo "
   <li class='page-item'>
<a href='search.php?page=1&$where_path' class='page-link'>
First Page  
</a>
   </li>
	";
for ($i=1; $i<= $total_pages; $i++) { 
	if ($i == @$_GET['page']) {
$active="active";
	}else{
  $active="";
	}
echo "
   <li class='page-item $active'>
<a href='search.php?page=$i&$where_path' class='page-link'>
$i 
</a>
   </li>
	";
}

echo "
 <li class='page-item'>
<a href='search.php?page=$total_pages&$where_path' class='page-link'>
Last Page
</a>
 </li>
	";
}
}
//get_search_pagination functions ends

//function to create dynamic categories {category.php}
function get_category_proposals(){

global $con;
global $login_seller_id;
$online_sellers=array();

if (isset($_SESSION['cat_id'])) {
	$session_cat_id=$_SESSION['cat_id'];

$get_proposals="SELECT DISTINCT proposal_seller_id FROM proposals WHERE proposal_cat_id='$session_cat_id' AND proposal_status='active'";
}elseif(isset($_SESSION['cat_child_id'])){

$session_cat_child_id=$_SESSION['cat_child_id'];

$get_proposals="SELECT DISTINCT proposal_seller_id FROM proposals WHERE proposal_child_id='$session_cat_child_id' AND proposal_status='active'";
}

$run_proposals=mysqli_query($con,$get_proposals);
while ($row_proposals=mysqli_fetch_array($run_proposals)){
	$proposal_seller_id=$row_proposals['proposal_seller_id'];
 
$select_seller="SELECT * FROM sellers WHERE seller_id='$proposal_seller_id'" ;
$run_seller=mysqli_query($con,$select_seller);
$row_seller=mysqli_fetch_array($run_seller);
$seller_status=$row_seller['seller_status'];
if ($seller_status == "online") {
	array_push($online_sellers,$proposal_seller_id);

	}	

 }//ends while loops
 $where_online=array();
 
 $where_delivery_time=array();
 $where_level=array();
 $where_language=array();
 if (isset($_REQUEST['online_sellers'])) {
 	foreach ($_REQUEST['online_sellers'] as $value) {
 		if ($value != 0) {
 			foreach ($online_sellers as $seller_id) {
 				$where_online [] ="proposal_seller_id=" . $seller_id;
 			}
 		}//end inner if
 	}//end foreach
 }//end of out if isset

 if (isset($_REQUEST['delivery_time'])) {
 	foreach ($_REQUEST['delivery_time'] as $value) {
 		if ($value != 0) {
 			$where_delivery_time[]="delivery_id=" . $value; 
 		}//end inner if
 	}//end foreach
 }//ends of if isset

 if (isset($_REQUEST['seller_level'])) {
 	foreach ($_REQUEST['seller_level'] as $value) {
 		if ($value != 0) {
 			$where_level[]="level_id=" .$value; 
 		}//end inner if
 	}//end foreach
 }//ends of if isset

if (isset($_REQUEST['seller_language'])) {
 	foreach ($_REQUEST['seller_language'] as $value) {
 		if ($value != 0) {
 			$where_language[]="language_id=" . $value; 
 		}//end inner if
 	}//end foreach
 }//ends of if isset


if (isset($_SESSION['cat_id'])) {
	
$query_where="where proposal_cat_id='$session_cat_id' AND  proposal_status='active'";
}elseif (isset($_SESSION['cat_child_id'])) {
 $query_where="where proposal_child_id='$session_cat_child_id' AND  proposal_status='active'";	
}


if (count($where_online)>0) {
$query_where .=" and (". implode(" or ",$where_online) . ")";
}


if (count($where_delivery_time)>0) {
$query_where .=" and (". implode(" or ",$where_delivery_time) . ")";
}
if (count($where_level)>0) {
$query_where .=" and (". implode(" or ",$where_level) . ")";
}
if (count($where_language)>0) {
$query_where .=" and (". implode(" or ",$where_language) . ")";
}
//code for pagination
$per_page=9;

if (isset($_GET['page'])) {

	$page = $_GET['page'];

}else{

	$page=1;

}
$start_from=($page-1) * $per_page;

$where_limit ="order by proposal_featured='yes' DESC LIMIT $start_from,$per_page";
$get_proposals = "select * from proposals " . $query_where . $where_limit;
$run_proposals=mysqli_query($con, $get_proposals);
$count_proposals=mysqli_num_rows($run_proposals);

if ($count_proposals==0) {
  if (isset($_SESSION['cat_id'])) {
  	
  
	echo "
   <div class='col-md-12'>
    <h1 class='alert alert-danger text-center mt-4'>Sorry No! Product Found In This Category</h1>
   </div>

	";
 }elseif (isset($_SESSION['cat_child_id'])) {
 echo "
   <div class='col-md-12'>
    <h1 class='alert alert-danger text-center mt-4'>Sorry No Product Found In This Sub-Category</h1>
   </div>

	";	
 }
}
while ($row_proposals=mysqli_fetch_array($run_proposals)) {
  $proposal_id=$row_proposals['proposal_id'];
  $proposal_title=$row_proposals['proposal_title'];
  $proposal_price=$row_proposals['proposal_price'];
  $proposal_img1=$row_proposals['proposal_img1'];
  $proposal_video=$row_proposals['proposal_video'];
  $proposal_seller_id=$row_proposals['proposal_seller_id'];
  $proposal_rating=$row_proposals['proposal_rating'];
  $proposal_url=$row_proposals['proposal_url'];
  $proposal_featured=$row_proposals['proposal_featured'];
 if (empty($proposal_video)) {
$video_class="";
 }else{
$video_class="video-img";
 }
 $select_seller="select * from sellers where seller_id='$proposal_seller_id'";
 $run_seller=mysqli_query($con,$select_seller);
 $row_seller=mysqli_fetch_array($run_seller);
 $seller_user_name=$row_seller['seller_user_name'];

 $seller_buyer_reviews="select * from buyer_reviews where proposal_id='$proposal_id'";
 $run_buyer_reviews=mysqli_query($con,$seller_buyer_reviews);
 $count_reviews=mysqli_num_rows($run_buyer_reviews);

 $select_favorites="SELECT * from favorites where proposal_id='$proposal_id' AND seller_id='$login_seller_id'";
 $run_favorites=mysqli_query($con,$select_favorites);
 $count_favorites=mysqli_num_rows($run_favorites);
 if ($count_favorites == 0) {
  $show_favorite_id="favorite_$proposal_id";
  $show_favorite_class="favorite";
 }else{
    $show_favorite_id="unfavorite_$proposal_id";
  $show_favorite_class="favorited";
 }
?>

<div class="col-lg-4 col-md-6 col-sm-6">
<div class="proposal-div">
<div class="proposal_nav"><span class="float-left mt-2">
<strong class="ml-2 mr-1">By</strong><?php echo $seller_user_name; ?></span>
<span class="float-right mt-2">
<?php
for($proposal_i=0 ; $proposal_i < $proposal_rating; $proposal_i++){
	echo "<img src='images/user_rate_full.png' class='rating'>";
}
for($proposal_i=$proposal_rating ; $proposal_i < 5 ; $proposal_i++){
	echo "<img src='images/user_rate_blank.png' class='rating'>";

}
?>

<span class="ml-1 mr-2">(<?php echo $count_reviews ; ?>)</span>
</span>
<div class="clearfix mb-2"></div>
</div>
<a href="proposals/<?php echo $proposal_url;?>">
<hr class="m-0 p-0">
<img src="proposals/proposal_files/<?php echo $proposal_img1 ; ?>" class="resp-img" alt="proposal">
</a>
<div class="text">
<h4><a href="proposals/<?php echo $proposal_url ; ?>" class="<?php echo($video_class)?>">
<?php echo $proposal_title; ?>
</a></h4>
<hr>
<p class="buttons clearfix">
<?php
//for the purpose of creating dynamic favourite
 if (!isset($_SESSION['seller_user_name'])) { ?>	
<a href="login.php" class="favorite mt-2 float-left" data-toggle="tooltip" title="Add To Favorites"><i class="fa fa-heart fa-lg"></i></a>
<?php }else{ ?>
<a href="#" id="<?php echo($show_favorite_id); ?>" class="<?php echo($show_favorite_class); ?> mt-2 float-left" data-toggle="tooltip" title="Add To Favorites"><i class="fa fa-heart fa-lg"></i></a> 
<?php }?>
<span class="float-right">STARTING AT <strong class="price"><?php echo $proposal_price ; ?></strong></span>
</p>
</div>
<?php if ($proposal_featured =='yes') {
# hide/show the ribbon
?>
<div class="ribbon">
<div class="theribbon">Featured</div>
<div class="ribbon-background"></div>
</div>
<?php }?>
<?php if (isset($_SESSION['seller_user_name'])) { ?>
<script>
$(document).on("click","#favorite_<?php echo $proposal_id ?>", function(event){
 event.preventDefault();
var seller_id="<?php echo($login_seller_id); ?>";
var proposal_id="<?php echo($proposal_id); ?>";
$.ajax({
type:"POST",
url: "includes/add_delete_favorite.php",
data: {seller_id: seller_id, proposal_id: proposal_id, favorite: "add_favorite"},
success:function(){
  $("#favorite_<?php echo $proposal_id; ?>").attr({
    id: "unfavorite_<?php echo $proposal_id?>", class: " favorited mt-2 float-left"
  });
}
});
});

//copy for the same favorite fanctionality
$(document).on("click","#unfavorite_<?php echo $proposal_id ?>", function(event){
 event.preventDefault();
var seller_id="<?php echo($login_seller_id); ?>";
var proposal_id="<?php echo($proposal_id); ?>";
$.ajax({
type:"POST",
url: "includes/add_delete_favorite.php",
data: {seller_id: seller_id, proposal_id: proposal_id, favorite: "delete_favorite"},
success:function(){
  $("#unfavorite_<?php echo $proposal_id; ?>").attr({
    id: "favorite_<?php echo $proposal_id?>", class: "favorite mt-2 float-left"
  });
}
});
});
</script>
<?php } ?>
</div><!--ends proposal-div-->	
</div>

<?php
}//$row_proposal while loop ends
}//get_category_proposals ends

function get_category_pagination(){
global $con;

$online_sellers=array();

if (isset($_SESSION['cat_id'])) {
	$session_cat_id=$_SESSION['cat_id'];

$get_proposals="SELECT DISTINCT proposal_seller_id FROM proposals WHERE proposal_cat_id='$session_cat_id' AND proposal_status='active'";
}elseif(isset($_SESSION['cat_child_id'])){

$session_cat_child_id=$_SESSION['cat_child_id'];

$get_proposals="SELECT DISTINCT proposal_seller_id FROM proposals WHERE proposal_child_id='$session_cat_child_id' AND proposal_status='active'";
}


$run_proposals=mysqli_query($con,$get_proposals);
while ($row_proposals=mysqli_fetch_array($run_proposals)){
	$proposal_seller_id=$row_proposals['proposal_seller_id'];
 
$select_seller="SELECT * FROM sellers WHERE seller_id='$proposal_seller_id'" ;
$run_seller=mysqli_query($con,$select_seller);
$row_seller=mysqli_fetch_array($run_seller);
$seller_status=$row_seller['seller_status'];
if ($seller_status == "online") {
	array_push($online_sellers,$proposal_seller_id);

	}	

 }//ends while loops
 $where_online=array();
 $where_delivery_time=array();
 $where_level=array();
 $where_language=array();
 $where_path="";
 if (isset($_REQUEST['online_sellers'])) {
 	foreach ($_REQUEST['online_sellers'] as $value) {
 		if ($value != 0) {
 			foreach ($online_sellers as $seller_id) {
 				$where_online [] ="proposal_seller_id=" . $seller_id;
 			}
 			$where_path .="online_sellers[]=" . $value . "&";
 		}//end inner if
 	}//end foreach
 }//end of out if isset

 if (isset($_REQUEST['delivery_time'])) {
 	foreach ($_REQUEST['delivery_time'] as $value) {
 		if ($value != 0) {
 			$where_delivery_time[]="delivery_id=" . $value;
 			$where_path .="delivery_time[]=" . $value . "&"; 
 		}//end inner if
 	}//end foreach
 }//ends of if isset

 if (isset($_REQUEST['seller_level'])) {
 	foreach ($_REQUEST['seller_level'] as $value) {
 		if ($value != 0) {
 			$where_level[]="level_id=" .$value;
 			$where_path .="seller_level[]=" .$value . "&"; 
 		}//end inner if
 	}//end foreach
 }//ends of if isset

if (isset($_REQUEST['seller_language'])) {
 	foreach ($_REQUEST['seller_language'] as $value) {
 		if ($value != 0) {
 			$where_language[]="language_id=" . $value;
 			$where_path .="seller_language[]=" . $value . "&"; 
 		}//end inner if
 	}//end foreach
 }//ends of if isset

 

if (isset($_SESSION['cat_id'])) {
	
$query_where="where proposal_cat_id='$session_cat_id' AND  proposal_status='active'";
}elseif (isset($_SESSION['cat_child_id'])) {
 $query_where="where proposal_child_id='$session_cat_child_id' AND  proposal_status='active'";	
}


if (count($where_online)>0) {
$query_where .=" and (". implode(" or ",$where_online) . ")";
}

if (count($where_delivery_time)>0) {
$query_where .=" and (". implode(" or ",$where_delivery_time) . ")";
}
if (count($where_level)>0) {
$query_where .=" and (". implode(" or ",$where_level) . ")";
}
if (count($where_language)>0) {
$query_where .=" and (". implode(" or ",$where_language) . ")";
}
//code for pagination
$per_page=9;
$get_proposals="select * from proposals " . $query_where;
$run_proposals=mysqli_query($con, $get_proposals);
$count_proposals=mysqli_num_rows($run_proposals);
if ($count_proposals>0) {
	$total_pages=ceil($count_proposals/$per_page);
	echo "
   <li class='page-item'>
<a href='category.php?page=1&$where_path' class='page-link'>
First Page  
</a>
   </li>
	";
for ($i=1; $i<= $total_pages; $i++) { 
	if ($i == @$_GET['page']) {
$active="active";
	}else{
  $active="";
	}
echo "
   <li class='page-item $active'>
<a href='category.php?page=$i&$where_path' class='page-link'>
$i 
</a>
   </li>
	";
}

echo "
 <li class='page-item'>
<a href='category.php?page=$total_pages&$where_path' class='page-link'>
Last Page
</a>
 </li>
	";
}
}
?>