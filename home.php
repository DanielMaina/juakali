<div class="home-header mx-0 d-none d-lg-block">
<div class="row">
<div class="col-md-3 d-none d-lg-block ">
<div class="card text-dark">
<div class="card-body">
<h3>Top Categories</h3>
<div class="row" style="height: 420px; overflow-y:scroll;">
<?php
$get_categories="SELECT * from categories where cat_featured= 'yes'";
$run_categories=mysqli_query($con,$get_categories);
while ($row_categories=mysqli_fetch_array($run_categories)) {
	$cat_id=$row_categories['cat_id'];
	$cat_title=$row_categories['cat_title'];
	$cat_image=$row_categories['cat_image'];
?>
<div class="col-md-12">	
<div class="mobile-category py-2" style="border: none;">
<a href="category.php?cat_id=<?php echo $cat_id; ?>">
<div class="ml-2 category-picture">
<img src="cat_images/<?php echo $cat_image; ?>">
</div>
<div class="category-text">
<p class="category-title">
<strong><?php echo $cat_title; ?></strong>
</p>
</div>
</a></div>
<hr>
</div><!--end of a single col-md-12-->
<?php }?>	
	</div>
</div>	
</div>	
</div>
<div class="col-md-6 mx-0 mobile-view">	
<div class="">
<!-- <center> -->
<h3 class="mt-2">
<a href="#" class="btn btn-outline-success sign-in-sm-float-left float-left" data-toggle="modal" data-target="#register-modal">
<i class="fas fa-sign-in-alt"></i>&nbsp;Be Part of Us
</a><span class="hide-name-small pl-5">Jua Kali Mall center!</span><a class="btn btn-outline-info how-sm-float-right float-right" href="#" data-toggle="modal" data-target="#how-it-work">
How We Works</a></h3>
<form action="" method="POST" class="py-1">
<div class="input-group">
<input type="text" class="form-control input-lg" placeholder="Search Product" name="search_query" value="<?php echo @$_SESSION["search_query"]; ?>" required>
<span class="input-group-btn">
<button class="btn btn-success btn-md" type="submit" name="search">
<strong>Search</strong>
</button>
</span>
</div>
</form>	
<div class="row mx-0">
<div class="card text-dark  border-0">
<div class="card-body p-0 m-0">
<div id="myCarousel" class="carousel slide">
<ol class="carousel-indicators">
<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
<?php
$select_slides="SELECT * FROM slider";
$run_slides=mysqli_query($con,$select_slides);
$count_slides=mysqli_num_rows($run_slides);
$i=0;
$get_slides="SELECT * FROM slider LIMIT 1,$count_slides";
$run_slides=mysqli_query($con,$get_slides);
while ($row_slides=mysqli_fetch_array($run_slides)) {
$i++;
?>	
<li data-target="#myCarousel" data-slide-to="<?php echo($i);?>" ></li> 
<?php } ?>		
	</ol>
<div class="carousel-inner">
<?php
$get_slides="SELECT * FROM slider LIMIT 0,1";
$run_slides=mysqli_query($con,$get_slides);
while ($row_slides=mysqli_fetch_array($run_slides)) {
$slide_image=$row_slides['slide_image'];
$slide_name=$row_slides['slide_name'];
$slide_desc=$row_slides['slide_desc'];
$slide_url=$row_slides['slide_url'];
?>
<div class="carousel-item active">
			<a href="<?php echo($slide_url); ?>">
				<img src="slides_images/<?php echo($slide_image); ?>" class="img-fluid image-size-small" style="height: 100%;" >
				<div class="carousel-caption" style="background-color: rgba(0,0,0,0.7);">
					<h3 class="d-lg-block d-md-block d-none">
						<?php echo($slide_name); ?></h3>
						<p class="d-lg-block d-md-block d-none">
							<?php echo($slide_desc) ?>
						</p>
					
				</div>
			</a>
		</div><!--single carousel-->
<?php } ?>

<?php
$get_slides="SELECT * FROM slider LIMIT 1,$count_slides";
$run_slides=mysqli_query($con,$get_slides);
while ($row_slides=mysqli_fetch_array($run_slides)) {
$slide_image=$row_slides['slide_image'];
$slide_name=$row_slides['slide_name'];
$slide_desc=$row_slides['slide_desc'];
$slide_url=$row_slides['slide_url'];
?>
<div class="carousel-item ">
			<a href="<?php echo($slide_url); ?>">
				<img src="slides_images/<?php echo($slide_image); ?>" class="img-fluid image-size-small" style="height: 100%;" >
				<div class="carousel-caption" style="background-color: rgba(0,0,0,0.7);">
					<h3 class="d-lg-block d-md-block d-none">
						<?php echo($slide_name); ?></h3>
						<p class="d-lg-block d-md-block d-none">
							<?php echo($slide_desc) ?>
						</p>
					
				</div>
			</a>
		</div><!--single carousel-->
<?php } ?>				
</div><!--end of the inner carousel-->
<a href="#myCarousel" class="carousel-control-prev" data-slide="prev">
<span class="carousel-control-prev-icon"></span>
</a>
<a href="#myCarousel" class="carousel-control-next" data-slide="next">
<span class="carousel-control-next-icon"></span>
</a>
</div><!-- carousel slide ends-->
</div>	
</div>	
</div>		
<!-- </center> -->
</div>
</div>
<!-- end of carousel middle colunm -->
<div class="col-md-3 d-none d-lg-block">
<div class="card text-dark">
<div class="card-body">
<h3>Sold Recent</h3>
<div class="row" style="height: 420px; overflow-y:scroll;">
<?php 
$get_proposals="SELECT * from proposals where proposal_featured='yes' AND proposal_status='active'";
$run_proposals=mysqli_query($con,$get_proposals);
while ($row_proposals=mysqli_fetch_array($run_proposals)) {
  $proposal_id=$row_proposals['proposal_id'];
  $proposal_title=$row_proposals['proposal_title'];
  $proposal_img1=$row_proposals['proposal_img1'];
  $proposal_url=$row_proposals['proposal_url'];
 ?>	
<div class="col-md-12">
<div class="mobile-category py-2" style="border: none;">
<a href="proposals/<?php echo $proposal_url;?>">
<div class="ml-2 category-picture">
<img src="proposals/proposal_files/<?php echo $proposal_img1 ; ?>">
</div>
<div class="category-text">
<p class="category-title">
<strong><?php echo $proposal_title; ?></strong>
</p>
</div>
</a></div>
<hr>
</div><!--end of a single col-md-12-->
<?php } ?>
</div>
</div>
</div>
</div>
</div><!--End of header row-->
</div><!--End of header part-->
<!-- Handle small device view srt -->
<div class="container-fluid d-small-block d-none">
<div class="row">
<div class="col-12">
<div class="row">
<?php
$get_categories="SELECT * from categories";
$run_categories=mysqli_query($con,$get_categories);
while ($row_categories=mysqli_fetch_array($run_categories)) {
	$cat_id=$row_categories['cat_id'];
	$cat_title=$row_categories['cat_title'];
	$cat_image=$row_categories['cat_image'];
?>
<div class="col-4 table-grid-product" style="border:1px solid #E2DFDF">
<div class="row">
<div class="col-12">	
<a href="category.php?cat_id=<?php echo $cat_id; ?>">
<img src="cat_images/<?php echo $cat_image; ?>" height="75px" width="70px"></a>
<p><?php echo($cat_title); ?></p>
</div>
</div>
</div><!--col-md-4 table-grid-product ends-->
<?php } ?>	
</div><!-- row single product table design ends-->
</div>
</div><!--row ends-->	
</div>
<!-- Handle small device view enda -->
<!-- the categories section {for the featured products jobs} ..!srt-->
<!-- Top Featuered proposal -->
<div class="proposals-section">
<div class="container-fluid">
<div class="row">
<div class="col-md-12 flex-wrap">
<h2 class="ml-5">Top Featured Products</h2>
<div class="owl-carousel home-featured-carousel owl-theme">
<?php
$get_proposals="SELECT * from proposals where proposal_featured='yes' AND proposal_status='active'";
$run_proposals=mysqli_query($con,$get_proposals);
while ($row_proposals=mysqli_fetch_array($run_proposals)) {
  $proposal_id=$row_proposals['proposal_id'];
  $proposal_title=$row_proposals['proposal_title'];
  $proposal_price=$row_proposals['proposal_price'];
  $proposal_img1=$row_proposals['proposal_img1'];
  $proposal_video=$row_proposals['proposal_video'];
  $proposal_seller_id=$row_proposals['proposal_seller_id'];
  $proposal_rating=$row_proposals['proposal_rating'];
  $proposal_url=$row_proposals['proposal_url'];
 if (empty($proposal_video)) {
$video_class="";
 }else{
$video_class="video-img";
 }
 $select_seller="SELECT * from sellers where seller_id='$proposal_seller_id'";
 $run_seller=mysqli_query($con,$select_seller);
 $row_seller=mysqli_fetch_array($run_seller);
 $seller_user_name=$row_seller['seller_user_name'];

 $seller_buyer_reviews="SELECT * from buyer_reviews where proposal_id='$proposal_id'";
 $run_buyer_reviews=mysqli_query($con,$seller_buyer_reviews);
 $count_reviews=mysqli_num_rows($run_buyer_reviews);

 ?>	

<div class="proposal-div">
<div class="proposal_nav"><span class="float-left mt-2">
<strong class="ml-2 mr-1">By</strong><?php echo $seller_user_name; ?></span>
<span class="float-right mt-2">
<?php
for($proposal_i=0 ; $proposal_i < $proposal_rating; $proposal_i++){
	echo "<img src='images/user_rate_full.png' alt='rating' class='rating'>";
}
for($proposal_i=$proposal_rating ; $proposal_i < 5 ; $proposal_i++){
	echo "<img src='images/user_rate_blank.png' alt='rating' class='rating'>";

}
?>

<span class="ml-1 mr-2">(<?php echo $count_reviews ; ?>)</span>
</span>
<div class="clearfix mb-2"></div>
</div>
<a href="proposals/<?php echo $proposal_url;?>">
<hr class="m-0 p-0">
<img src="proposals/proposal_files/<?php echo $proposal_img1 ; ?>" class="resp-img" alt="proposal">
</a>
<div class="text">
<p><a href="proposals/<?php echo $proposal_url ; ?>" class="<?php echo $video_class; ?> text-dark">
<?php echo ucwords($proposal_title); ?>
</a>
<a title="Add To Cart" data-toggle="tooltip" class="text-danger float-right" href="proposals/<?php echo($proposal_url); ?>"><i class="fa fa-fw fa-lg fa-shopping-cart"></i></a>
</p>
<hr>
<p class="buttons clearfix">
<a href="#" class="favorite mt-2 float-left" data-toggle="tooltip" title="Add To Favorites"><i class="fa fa-heart fa-lg"></i></a>
<span class="float-right">START AT Kshs <strong class="price"><?php echo $proposal_price ; ?></strong></span>
</p>
</div>
<div class="ribbon">
<div class="theribbon">Featured</div>
<div class="ribbon-background"></div>
</div>
</div><!--ends proposal-div-->
<?php }?>
</div><!--owl-carousel home-featured-carousel wol-theme ends-->
</div><!--col-md-12 flex-wrap ends-->
</div>
</div>
</div><!-- Top Featuered proposal ends -->

<!-- Create Affordable Appliances srt -->
<div class="container-fluid">
<div class="card card-light">
<div class="card-header">
<h3 class="card-title">Affordable Applinces</h3>
</div>
<div class="card-body">
<div class="row">
<div class="col-md-3">
<?php 
$get_pro_image="SELECT * from proposals where proposal_status='active' ORDER BY 1 DESC";
$run_pro_image=mysqli_query($con,$get_pro_image);
$row_pro_image=mysqli_fetch_array($run_pro_image);
$proposal_img1=$row_pro_image['proposal_img1'];
 ?>	
<img class="hide-image-small" src="proposals/proposal_files/<?php echo($proposal_img1) ?>" height="247px" width="313px">	
</div>
<div class="col-md-9">
<div class="row">
<?php 
$get_affo_appl="SELECT * FROM proposals where proposal_status='active' LIMIT 0,8";
$run_affo_appl=mysqli_query($con,$get_affo_appl);
while($row_affo_appl=mysqli_fetch_array($run_affo_appl)) {
$proposal_title=$row_affo_appl['proposal_title'];
  $proposal_img1=$row_affo_appl['proposal_img1'];
  $proposal_url=$row_affo_appl['proposal_url'];
  $proposal_price=$row_affo_appl['proposal_price'];
 ?>
<div class="col-md-3 col-sm-2 table-grid-product py-3">
<div class="row">
<div class="col-4">	
<a class="text-info" href="proposals/<?php echo $proposal_url;?>">
<img src="proposals/proposal_files/<?php echo $proposal_img1; ?>" height="90px" width="80px"></a>
</div>
<div class="col-6 mx-auto">
<span class="text-dark"><?php echo($proposal_title); ?></span><br><br> 
<span class="text-info">Kshs <?php echo($proposal_price) ?></span><br>
<span><a class="btn btn-outline-info btn-sm btn-block" href="proposals/<?php echo $proposal_url;?>"><i class="fa fa-fw fa-lg fa-shopping-cart"></i></a></span>
</div>
</div>
</div><!--col-md-3 table-grid-product ends-->
<?php } ?>	
</div><!-- row single product table design ends-->
</div>
</div>
</div>	
</div>
</div><!--end of container-fluid-->
<!-- Create Affordable Appliances ends-->
<!-- GET THE NEW PRODUCT IN THE STORE... srts-->
<div class="container-fluid mt-2">
<div class="card card-light">
<div class="card-header">
<h3 class="card-title float-right">NEW IN MARKET</h3>
</div>	
<div class="card-body">
<div class="row">
<div class="col-md-3">
<?php 
$get_pro_image="SELECT * from proposals where proposal_status='active'";
$run_pro_image=mysqli_query($con,$get_pro_image);
$row_pro_image=mysqli_fetch_array($run_pro_image);
$proposal_img1=$row_pro_image['proposal_img1'];
 ?>		
<img class="hide-image-small"  src="proposals/proposal_files/<?php echo($proposal_img1); ?>" height="335px" width="313px">	
</div>	
<div class="col-md-9">
<div class="row">
<?php 
$get_new_pro="SELECT * FROM proposals where proposal_status='active'ORDER BY 1 DESC LIMIT 0,8";
$run_new_pro=mysqli_query($con,$get_new_pro);
while($row_new_pro=mysqli_fetch_array($run_new_pro)) {
$proposal_title=$row_new_pro['proposal_title'];
  $proposal_img1=$row_new_pro['proposal_img1'];
  $proposal_url=$row_new_pro['proposal_url'];
  $proposal_price=$row_new_pro['proposal_price'];
 ?>
<div class="col-md-3 table-grid-product border-dark py-2">
<div class="product-detail">
<div class="imgbox">
<img src="proposals/proposal_files/<?php echo($proposal_img1); ?>" width="100%" height="200px">		
</div><!--imgbox-->
<div class="detail">
<p><a class="text-dark" href="proposals/<?php echo $proposal_url;?>"><?php echo($proposal_title); ?></a><span class="new-price float-right">Kshs <?php echo $proposal_price; ?></span></p>

<label class="new-label">More</label>
<a href="proposals/<?php echo $proposal_url;?>" class="btn btn-success btn-block btn-sm"><i class="fa fa-fw fa-lg fa-shopping-cart"></i> Cart</a>
</div><!--detail ends-->
</div><!--Custome product-detail ends-->
<a href='proposals/<?php echo $proposal_url;?>' style='color:black;' class='label sale'>
  <div class='thelabel'>New</div>
  <div class='label-backgroud'></div>
 </a>	
</div><!-- single table-grid-product ends-->
 <?php } ?>
</div><!--row new product ends-->	
</div><!--col-md-9 ends-->
</div>
</div>
</div>
</div>
<!-- GET THE NEW PRODUCT IN THE STORE... ends-->
<!-- GET THE FEATUERED CATEGORY ACROSE SRT... -->
<div class="container-fluid mt-2">		
<div class="row">
<div class="card">
<div class="card-header">
<div class="col-md-12 text-center">
<h2>Explore our Featured Categories</h2>
<h4 class="text-muted">Get Inspired to purchase product</h4>
</div>
</div><div class="card-body">			
<div class="row">
<?php
$get_categories="SELECT * from categories where cat_featured= 'yes' ORDER BY 1 DESC LIMIT 0,8";
$run_categories=mysqli_query($con,$get_categories);
while ($row_categories=mysqli_fetch_array($run_categories)) {
	$cat_id=$row_categories['cat_id'];
	$cat_title=$row_categories['cat_title'];
	$cat_image=$row_categories['cat_image'];
	?>					
<div class="col-lg-3 col-md-4 col-sm-6">
<div class="category-item">
<a href="category.php?cat_id=<?php echo($cat_id); ?>">
<h6><?php echo($cat_title); ?></h6>
<img src="cat_images/<?php echo($cat_image); ?>" width="100%" height="auto">
</a>
</div>
</div>
<?php }?>					
</div><!--ends of the row-->
</div>
</div>
</div>
</div>
<!-- GET THE FEATUERED CATEGORY ACROSE END... -->
<!-- for the three box infor... -->

<section class="box-section">	
<div class="container">	
<div class="row">
<?php
$get_boxes="SELECT * FROM section_boxes";
$run_boxes=mysqli_query($con,$get_boxes);
while ($row_boxes=mysqli_fetch_array($run_boxes)) {
 $box_title=$row_boxes['box_title'];
 $box_desc=$row_boxes['box_desc'];
 ?>
<div class="col-md-4 mb-4 text-center">
<h3><?php echo $box_title; ?></h3>
<p><?php echo $box_desc; ?></p>
</div>
<?php }?>
</div>
</div>
</section>
<section class="platform-section container-fluid">
<div class="container">
<?php
  $get_section="SELECT * from  home_section";
  $run_section=mysqli_query($con,$get_section);
  $row_section=mysqli_fetch_array($run_section);
  $section_title=$row_section['section_title'];
  $section_short_desc=$row_section['section_short_desc'];
  $section_desc=$row_section['section_desc'];
  $section_button=$row_section['section_button'];
  $section_button_url=$row_section['section_button_url'];
  $section_image=$row_section['section_image'];
 ?>	
	<div class="row text-center">
		<div class="col-md-6 mb-3">
			<h2><?php echo $section_title; ?></h2>
			<h3 class="text-muted"><?php echo $section_short_desc; ?></h3><p>
			<?php echo $section_desc; ?></p>
			<button class="btn btn-success btn-lg" data-toggle="modal" data-target="#register-modal">Join Now!</button>
	<a href="<?php echo $section_button_url; ?>" class="btn btn-outline-primary btn-lg"><?php echo $section_button; ?></a> 
		</div>
		<div class="col-md-6 border-dark" style="height:200px;">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.725525034677!2d37.1387927142708!3d-0.3907733354138498!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x18287b20d073fd55%3A0x92ae2018a3538597!2sKaratina%20University%20(Main%20Campus)!5e0!3m2!1sen!2ske!4v1576002356616!5m2!1sen!2ske" width="100%" height="100%" frameborder="0" allowfullscreen=""></iframe>
			<!---<img src="images/<?php //echo $section_image; ?>" class="img-fluid" alt="platform-image">-->
		</div>
	</div>
</div>	
</section>
<!-- for how the system work popup..! srt -->
<div class="modal fade" id="how-it-work">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">How We Work</h5>
				<button type="button" class="close" data-dismiss=modal>
					<span>&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<h4 align="center">How to Sell/earn Money! </h4>
				<p><i class="fas fa-exclamation"></i> First create your profile for what you can offer/Product!</p>
				<p><i class="fas fa-exclamation"></i>Upload your Products, views by Featuring Them!</p>
				<p><i class="fas fa-exclamation"></i>Ensure that you Detail is Ok! for Payment And Clear Communication</p>
				<p><i class="fas fa-exclamation"></i> Once product purchased from You Will Receive/ deposited Your Money</p>
				<p><i class="fas fa-exclamation"></i>Deposited After Designated Days</p>
				<h4 align="center">How To Buy!</h4>
				<p><i class="fas fa-angle-double-right"></i>Explore Our Store! Ensure Logged In</p>
				<p><i class="fas fa-angle-double-right"></i> Identify the Product You Are Looking For!</p>
				<p><i class="fas fa-angle-double-right"></i>Add to Cart or Buy Now!</p>
				<p><i class="fas fa-angle-double-right"></i>Make Payment As Requested</p>
				<p><i class="fas fa-angle-double-right"></i> Wait for the Seller To communicate!</p>
				<p><i class="fas fa-angle-double-right"></i>Wait for Delivery</p>
				<br>
			</div>
		</div>
	</div>
</div>