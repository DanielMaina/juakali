
<?php
if (isset($_SESSION['cat_id'])) {
	$session_cat_id= $_SESSION['cat_id'];

}
if (isset($_SESSION['cat_child_id'])) {
	$session_cat_child_id= $_SESSION['cat_child_id'];
$get_child_cats="select * from categories_childs where child_id ='$session_cat_child_id'";
 $run_child_cats=mysqli_query($con,$get_child_cats);
 $row_child_cats=mysqli_fetch_array($run_child_cats);
 $child_parent_id=$row_child_cats['child_parent_id']; 	
}
//to prevent unchecking during checking...
$online_sellers=array();
$delivery_time=array();
$seller_level=array();
$seller_language=array();
if (isset( $_GET['online_sellers'])) {
 foreach ($_GET['online_sellers'] as $value) {
 	$online_sellers[$value] = $value;
 	 }
}
if (isset( $_GET['delivery_time'])) {
 foreach ($_GET['delivery_time'] as $value) {
 	$delivery_time[$value] = $value;
 	 }
}
if (isset( $_GET['seller_level'])) {
 foreach ($_GET['seller_level'] as $value) {
 	$seller_level[$value] = $value;
 	 }
}
if (isset( $_GET['seller_language'])) {
 foreach ($_GET['seller_language'] as $value) {
 	$seller_language[$value] = $value; }
}


?>
<div class="card border-primary mb-3">
	<div class="card-header bg-primary">
		<h3 class="h5 text-white">Categories</h3>
	</div>
<div class="card-body" style="height:300px; overflow-y: scroll;" >
<ul class="nav flex-column" id="proposal_category">
<?php
$get_cats="select * from categories";
$run_cats=mysqli_query($con,$get_cats);
while ($row_cats=mysqli_fetch_array($run_cats)) {

	$cat_id=$row_cats['cat_id'];
	$cat_title=$row_cats['cat_title'];
?>	
<li class="nav-item">
<span class="nav-link 
<?php 
 if($cat_id == @$_SESSION['cat_id']){echo "active";}
 if($cat_id == @$child_parent_id){echo "active";}
?>
">
<a href="category.php?cat_id=<?php echo $cat_id; ?>">
<?php echo $cat_title;?></a>
<a href="#" class="h5 text-success float-right" data-toggle="collapse" data-target="#cat_<?php echo $cat_id; ?>">
<i class="fa fa-arrow-circle-down"></i>
</a>
</span>
<ul id="cat_<?php echo $cat_id;?>" class="collapse">
<?php 
$get_child_cats="select * from categories_childs where child_parent_id='$cat_id'";
$run_child_cats=mysqli_query($con,$get_child_cats);
while ($row_child_cats=mysqli_fetch_array($run_child_cats)) {
$child_id=$row_child_cats['child_id'];
$child_title=$row_child_cats['child_title'];
?>	
<li><a class="nav-link 
<?php if($child_id == @$_SESSION['cat_child_id']){echo "active";}?>
	" href="category.php?cat_child_id=<?php echo $child_id; ?>"><?php echo $child_title; ?></a></li>
<?php }?>
</ul>
</li>
<?php } ?>
</ul>
</div>
</div>

<div class="card border-primary mb-3">
<div class="card-body pb-2 pt-3">
<ul class="nav flex-column">
<li class="nav-item checkbox checkbox-primary">
<label>
<input type="checkbox" value="1" class="get_online_sellers"
<?php 
if (isset($online_sellers["1"])) { echo "checked";}
?> >
<span>Show Online Seller</span>
</label>
</li>
</ul>
</div>
</div>
<div class="card border-primary mb-3">
	<div class="card-header bg-primary">
		<h3 class="flow-left text-white h5">Delivery Time</h3>
		<button class="btn btn-secondary btn-sm float-right clear_delivery_time clearlink" onclick="clearDelivery()">
			<i class="fas fa-times-circle"></i>Clear Filter
		</button>
	</div>
<div class="card-body">
<ul class="nav flex-column">
<?php 
if(isset($_SESSION['cat_id'])) {
$get_proposals="SELECT DISTINCT delivery_id from proposals where proposal_cat_id='$session_cat_id' AND proposal_status='active'";


 }elseif(isset($_SESSION['cat_child_id'])) {
 $get_proposals="SELECT DISTINCT delivery_id from proposals where proposal_child_id='$session_cat_child_id' AND proposal_status='active'";
 }
$run_proposals=mysqli_query($con,$get_proposals);
while ($row_proposals=mysqli_fetch_array($run_proposals)) {
	$delivery_id=$row_proposals['delivery_id'];

	$get_delivery_times="SELECT * from delivery_times where delivery_id='$delivery_id'";
	$run_delivery_times=mysqli_query($con,$get_delivery_times);
	$row_delivery_times=mysqli_fetch_array($run_delivery_times);
	$delivery_id=$row_delivery_times['delivery_id'];
	$delivery_title=$row_delivery_times['delivery_title'];
  
 ?>
<li class="nav-item checkbox checkbox-primary">
<label><input type="checkbox" value="<?php echo($delivery_id);?>" class="get_delivery_time"
	<?php 
if (isset($delivery_time[$delivery_id])) { echo "checked";}
?> >
<span><?php echo($delivery_title); ?></span></label>
</li>
<?php }?>
</ul>
</div>
</div>
<div class="card border-primary mb-3">
	<div class="card-header bg-primary">
		<h3 class="flow-left text-white h5">Expert Level</h3>
		<button class="btn btn-secondary btn-sm float-right clear_seller_level clearlink" onclick="clearLevel()">
			<i class="fas fa-times-circle"></i>Clear Filter
		</button>
	</div>
<div class="card-body">
<ul class="nav flex-column">
<?php 
if(isset($_SESSION['cat_id'])) {
$get_proposals="SELECT DISTINCT level_id from proposals where proposal_cat_id='$session_cat_id' AND proposal_status='active'";


 }elseif(isset($_SESSION['cat_child_id'])) {
 $get_proposals="SELECT DISTINCT level_id from proposals where proposal_child_id='$session_cat_child_id' AND proposal_status='active'";
 }
$run_proposals=mysqli_query($con,$get_proposals);
while ($row_proposals=mysqli_fetch_array($run_proposals)) {
	$level_id=$row_proposals['level_id'];
$select_seller_levels="SELECT * from seller_levels where level_id='$level_id'";	
$run_seller_levels=mysqli_query($con, $select_seller_levels);
$row_seller_levels=mysqli_fetch_array($run_seller_levels);
$level_id=$row_seller_levels['level_id'];
$level_title=$row_seller_levels['level_title'];

?>	
<li class="nav-item checkbox checkbox-primary">
<label><input type="checkbox" value="<?php echo($level_id)?>" class="get_seller_level"
	<?php 
if (isset($seller_levels[$level_id])) { echo "checked";}
?> >
<span><?php echo($level_title)?></span></label>
</li>
<?php }?>
</ul>
</div>
</div>
<div class="card border-primary mb-3">
	<div class="card-header bg-primary">
		<h3 class="flow-left text-white h5">Expert Performance</h3>
		<button class="btn btn-secondary btn-sm float-right clear_seller_language clearlink" onclick="clearLanguage()">
			<i class="fas fa-times-circle"></i>Clear Filter
		</button>
	</div>
<div class="card-body">
<ul class="nav flex-column">
<?php 
if(isset($_SESSION['cat_id'])) {
$get_proposals="SELECT DISTINCT language_id from proposals where proposal_cat_id='$session_cat_id' AND proposal_status='active'";


 }elseif(isset($_SESSION['cat_child_id'])) {
 $get_proposals="SELECT DISTINCT language_id from proposals where proposal_child_id='$session_cat_child_id' AND proposal_status='active'";
 }
$run_proposals=mysqli_query($con,$get_proposals);
while ($row_proposals=mysqli_fetch_array($run_proposals)) {
	$language_id=$row_proposals['language_id'];
$select_seller_languages="select * from seller_languages where language_id='$language_id'";
$run_seller_languages=mysqli_query($con,$select_seller_languages);
$row_seller_languages=mysqli_fetch_array($run_seller_languages);
$language_id=$row_seller_languages['language_id'];
$language_title=$row_seller_languages['language_title'];


?>	
<li class="nav-item checkbox checkbox-primary">
<label><input type="checkbox" value="<?php echo $language_id ?>" class="get_seller_language"
	<?php 
if (isset($seller_language[$language_id])) { echo "checked";}
?> >
<span><?php echo $language_title;?></span></label>
</li>
<?php }?>
</ul>
</div>
</div>