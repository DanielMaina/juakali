<div class="mt-2" id="footer">
<div class="container">
<div class="row">
<div class="col-md-3">
<h4><strong>CATEGORIES</strong></h4>
<ul>
<?php 
 $get_links="SELECT * from footer_links where link_section='categories'";
 $run_links=mysqli_query($con,$get_links);
while ($row_links=mysqli_fetch_array($run_links)) {
 $link_title=$row_links['link_title'];
 $link_url=$row_links['link_url'];
?>	
<li><a href="<?php echo $link_url ;?>"><?php echo $link_title ;?></a></li>
<?php }?>		
</ul>
</div>
<div class="col-md-3">
<h4><strong>ABOUT US</strong></h4>
<ul>
<?php 
 $get_links="SELECT * from footer_links where link_section='about'";
 $run_links=mysqli_query($con,$get_links);
while ($row_links=mysqli_fetch_array($run_links)) {
 $link_title=$row_links['link_title'];
 $link_url=$row_links['link_url'];
?>	
<li><a href="<?php echo $link_url ;?>"><?php echo $link_title ;?></a></li>
<?php }?>	
				</ul>
			</div>
<div class="col-md-3">
<h4><strong>SUPPORT</strong></h4>
<ul>
<?php 
 $get_links="SELECT * from footer_links where link_section='Support'";
 $run_links=mysqli_query($con,$get_links);
while ($row_links=mysqli_fetch_array($run_links)) {
 $link_title=$row_links['link_title'];
 $link_url=$row_links['link_url'];
?>	
<li><a href="<?php echo $link_url ;?>"><?php echo $link_title ;?></a></li>
<?php }?>				
</ul>
</div>
<div class="col-md-3">
<h4><strong>FOLLOW US</strong></h4>
<ul>
<?php 
 $get_links="SELECT * from footer_links where link_section='follow'";
 $run_links=mysqli_query($con,$get_links);
while ($row_links=mysqli_fetch_array($run_links)) {
 $link_title=$row_links['link_title'];
 $link_url=$row_links['link_url'];
?>	
<li><a href="<?php echo $link_url ;?>"><?php echo $link_title ;?></a></li>
<?php }?>	
				</ul>
			</div>
		</div>
	</div>
</div>
<div id="copyright">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<p class="text-lg-left text-center font-weight-bold">
					&copy;Jua kali Mall services LLC. All Right Reserved!
				</p>
			</div>
			<div class="col-md-6">
				<p class="text-lg-right text-center">Developed In <i class="fa fa-heart fa-lg"></i> By Jua Kali</p>
			</div>
		</div>
	</div>
</div>




<script src="<?php echo $site_url;?>/js/jquery.sticky.js"></script>
<script src="<?php echo $site_url;?>/font-awesome/js/all.min.js"></script>
<script src="<?php echo $site_url;?>/js/proper.js"></script>
<script src="<?php echo $site_url;?>/js/bootstrap.min.js"></script>	
<script src="<?php echo $site_url;?>/js/owl.carousel.min.js"></script>
<script src="<?php echo $site_url;?>/js/custom.js"></script>
