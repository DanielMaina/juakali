<div class="card rounded-0 mb-3">
	<div class="card-body">
		<h3> Hi!, <?php echo $login_seller_name; ?></h3>
		<p>Requst for a product you are looking at...</p>
		<a href="requests/post_request.php" class="btn btn-success btn-block" target="blank">Post A Request</a>
	</div>
</div>
<?php 
$select_proposals="SELECT * from proposals WHERE proposal_seller_id='$login_seller_id'";
$run_hidder=mysqli_query($con,$select_proposals);
$count_hidder=mysqli_num_rows($run_hidder);

if ($count_hidder > 0) {

 ?>
<div class="card rounded-0 mb-3">
	<div class="card-header">
		<h5>BUY IT AGAIN</h5>
	</div>
	<div class="card-body">
<?php
$select_orders="SELECT DISTINCT proposal_id from orders where buyer_id='$login_seller_id' AND order_status='completed' order by 1 DESC LIMIT 0,4";
$run_orders=mysqli_query($con,$select_orders);
while ($row_orders=mysqli_fetch_array($run_orders)) {
$proposal_id=$row_orders['proposal_id'];

$get_proposals="SELECT * from proposals where proposal_id='$proposal_id' AND proposal_status='active'";
$run_proposals=mysqli_query($con, $get_proposals);
$row_proposals=mysqli_fetch_array($run_proposals);
$proposal_title=$row_proposals['proposal_title'];
$proposal_img1=$row_proposals['proposal_img1'];
$proposal_url=$row_proposals['proposal_url'];
?>		
<div class="row mb-3">
<img src="proposals/proposal_files/<?php echo($proposal_img1);?>" alt="" class="col-lg-4 col-md-12 col-sm-4 col-4 img-fluid user-home-img-responsive">
<p class="col-lg-8 col-md-12 col-sm-8 col-8 user-home-title-responsive">
<a href="proposals/<?php echo $proposal_url; ?>"><?php echo $proposal_title; ?></a>
			</p>
		</div>
<?php } ?>
	</div>
</div><!--card rounded-0 mb-3 buy again ends-->
<?php } ?>
<div class="card rounded-0 mb-3">
	<div class="card-header">
		<h5>RECENTLY VIEWED</h5>
	</div>
	<div class="card-body">
<?php 

$select_recent="SELECT * from recent_proposals where seller_id='$login_seller_id'order by 1 DESC LIMIT 0,4";
$run_recent=mysqli_query($con,$select_recent);
while ($row_recent=mysqli_fetch_array($run_recent)) {
$proposal_id=$row_recent['proposal_id'];
 
$get_proposals="SELECT * from proposals where proposal_id='$proposal_id' AND proposal_status='active'";
$run_proposals=mysqli_query($con, $get_proposals);
$row_proposals=mysqli_fetch_array($run_proposals);
$proposal_title=$row_proposals['proposal_title'];
$proposal_img1=$row_proposals['proposal_img1'];
$proposal_url=$row_proposals['proposal_url'];
 ?>		
<div class="row mb-3">
<img src="proposals/proposal_files/<?php echo($proposal_img1) ?>" alt="" class="col-lg-4 col-md-12 col-sm-4 col-4 img-fluid user-home-img-responsive">
<p class="col-lg-8 col-md-12 col-sm-8 col-8 user-home-title-responsive">
<a href="proposals/<?php echo $proposal_url; ?>"><?php echo($proposal_title) ?></a>
</p>
</div>
<?php } ?>		
	</div>
</div><!--card rounded-0 mb-3 buy again ends-->