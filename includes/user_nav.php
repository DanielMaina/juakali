<div class="mp-box mp-box-white notop d-lg-block d-none">
	<div class="box-row">
		<ul class="main-cat-list active">
			<li>
				<a href="<?php echo($site_url); ?>/dashboard.php">Dashboard</a>
			</li>
			<li><a href="#">Selling <i class="fa fa-fw fa-caret-down"></i></a>
<div class="menu-cont">
	<ul>
		<li><a href="<?php echo($site_url); ?>/selling_orders.php">Orders</a></li>
		<li><a href="<?php echo($site_url); ?>/proposals/view_proposals.php">View Products</a></li>
		<li><a href="<?php echo($site_url); ?>/requests/buyer_requests.php">Buyer Requests</a></li>
		<li><a href="<?php echo($site_url); ?>/revenue.php">Revenues</a></li>
	</ul><!--inner ul for dropdown ul ends-->
</div>
			</li>
			<li><a href="#">Buying <i class="fa fa-fw fa-caret-down"></i></a>
<div class="menu-cont">
	<ul>
		<li><a href="<?php echo($site_url); ?>/buying_orders.php">Orders</a></li>
		<li><a href="<?php echo($site_url); ?>/purchases.php">Purchases</a></li>
		<li><a href="<?php echo($site_url); ?>/favourites.php">Favourites</a></li>
	</ul><!--inner ul for dropdown ul ends-->
</div>
			</li>
			<li><a href="#">Requests<i class="fa fa-fw fa-caret-down"></i></a>
<div class="menu-cont">
	<ul>
		<li><a href="<?php echo($site_url); ?>/requests/manage_requests.php">Manage Requests</a></li>
		<li><a href="<?php echo($site_url); ?>/requests/post_request.php">Post A Request</a></li>
	</ul><!--inner ul for dropdown ul ends-->
</div>
			</li>
			<li><a href="#">Contact <i class="fa fa-fw fa-caret-down"></i></a>
<div class="menu-cont">
	<ul>
		<li><a href="<?php echo($site_url); ?>/manage_contacts.php?my_buyers">My Buyers</a></li>
		<li><a href="<?php echo($site_url); ?>/manage_contacts.php?my_sellers">My Sellers</a></li>
	</ul><!--inner ul for dropdown ul ends-->
</div>
			</li>
<?php if ($enable_referrals=="yes") {
	 ?>			
			<li>
				<a href="<?php echo($site_url); ?>/my_referrals.php">My Referrals</a>
			</li>
<?php } ?>			
			<li>
				<a href="<?php echo($site_url); ?>/conversations/inbox.php">Inbox</a>
			</li>
			<li>
				<a href="<?php echo($site_url); ?>/<?php echo($_SESSION['seller_user_name']); ?>">My Profile</a>
			</li>
			<li>
				<a href="<?php echo($site_url); ?>/settings.php">Settings</a>
			</li>
		</ul>
	</div><!--box-row end-->
</div><!--mb-box mp-box-white notop d-lg-block d-none end-->






