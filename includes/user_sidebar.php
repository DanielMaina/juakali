<div class="card user-sidebar rounded-0">
<div class="card-body">
<h3>Description</h3>
<p>
   <?php echo($seller_about);?>  
</p>
<hr class="card-hr">
<h3>Task Performance</h3>
<?php
if (isset($_SESSION['seller_user_name'])) {  ?>
<?php if ($login_seller_user_name == $seller_user_name) { ?>	
<ul class="list-unstyled"> 
<li class="mb-4 clearfix">

 <button data-toggle="collapse" data-target="#language" class="btn btn-success float-right d-none">Add New</button>
 </li>
<div id="language" class="collapse form-style mb-2">
 <form method="post">
<div class="form-group">
<select class="form-control" name="language_id">
<option class="hidden">Select Performance</option>
<?php 
$get_languages="SELECT * from seller_languages";
$run_languages=mysqli_query($con,$get_languages);
while ($row_languages=mysqli_fetch_array($run_languages)) {
$language_id=$row_languages['language_id']	;
$language_title=$row_languages['language_title'];

 ?>
<option value="<?php echo($language_id); ?>"><?php echo($language_title); ?></option>
<?php } ?>
 </select>
 </div><!--form-group ends-->
 <div class="form-group">
<select class="form-control" name="language_level">
<option class="hidden">Select Level</option>
<option >Basic</option>
<option >Fluent</option>
<option >Convesational</option>
<option >Native or Bilingual</option>
</select>
</div><!--form-group ends-->
<div class="text-center">
<button type="button" data-toggle="collapse" data-target="#language" class="btn btn-secondary">Cancel</button>
 <button type="submit" name="insert_language" class="btn btn-success">Add</button>
</div><!--text-center ends-->
</form>
<?php 
if (isset($_POST['insert_language'])) {
$language_id=$_POST['language_id'];	
$language_level=$_POST['language_level'];

$insert_language="INSERT INTO language_relation (seller_id, language_id,language_level) values('$seller_id','$language_id','$language_level')";	
$run_language=mysqli_query($con,$insert_language);


}
 ?>
</div><!--collapse form-style mb-2 ends-->
</ul><!--list-unstyled ends-->
<?php } ?>
<?php } ?>

<ul class="list-unstyled mt-3">
<?php 
$select_languages_relation="SELECT * from language_relation where seller_id='$seller_id'";
$run_languages_relation=mysqli_query($con,$select_languages_relation);
while($row_languages_relation=mysqli_fetch_array($run_languages_relation)) {
$relation_id=$row_languages_relation['relation_id'];
$language_id=$row_languages_relation['language_id'];
$language_level=$row_languages_relation['language_level'];
$get_language="SELECT * from seller_languages where language_id='$language_id'";
$run_language=mysqli_query($con,$get_language);
$row_language=mysqli_fetch_array($run_language);
$language_title=$row_language['language_title'];
?>
<li class="card-li mb-1">
<?php echo($language_title); ?> - <span class="text-muted"><?php echo($language_level); ?></span>
<?php if (isset($_SESSION['seller_user_name'])) { ?>
<?php if ($login_seller_user_name==$seller_user_name) { ?>
<a class="d-none" href="user.php?delete_language=<?php echo($relation_id); ?>"><i class="fa fa-trash text-danger"></i></a>
<?php } ?>

<?php } ?>
</li>
<?php } ?>
</ul>




<hr class="card-hr">
<h3>Skill</h3>
<?php if (isset($_SESSION['seller_user_name'])) { ?>
<?php if ($login_seller_user_name==$seller_user_name) { ?>	
<ul class="list-unstyled"> 
<li class="mb-4 clearfix">

<button data-toggle="collapse" data-target="#add_skill" class="btn btn-success float-right">Add New</button>
</li>
<div id="add_skill" class="collapse form-style mb-2">
<form method="post">
<div class="form-group">
<select class="form-control" name="skill_id">
<option class="hidden">Select Skill</option>
<?php 
$get_skills="SELECT * from seller_skills";
$run_skills=mysqli_query($con,$get_skills);
while ($row_skills =mysqli_fetch_array($run_skills)) {

$skill_id=$row_skills['skill_id'];
$skill_title=$row_skills['skill_title'];
 ?>
<option value="<?php echo($skill_id); ?>"><?php echo $skill_title; ?></option>
<?php } ?>
 </select>
 </div><!--form-group ends-->
<div class="form-group">
 <select class="form-control" name="skill_level">
 <option class="hidden">Select Level</option>
 <option value="Beginner" >Beginner</option>
 <option value="Ingtermediate" >Intermediate</option>
  <option value="Expert">Expert</option>
 </select>
</div><!--form-group ends-->
<div class="text-center">
 <button type="button" data-toggle="collapse" data-target="#add_skill" class="btn btn-secondary">Cancel</button>
 <button type="submit" name="insert_skill" class="btn btn-success">Add</button>
 </div><!--text-center ends-->
 </form>
 <?php 
if (isset($_POST['insert_skill'])) {
	$skill_id=$_POST['skill_id'];

	$skill_level=$_POST['skill_level'];

$insert_skill="INSERT INTO skills_relation (seller_id,skill_id,skill_level) values('$seller_id','$skill_id','$skill_level')";
$run_skill=mysqli_query($con,$insert_skill);
}
  ?>
 </div><!--collapse form-style mb-2 ends-->
</ul>
<?php } ?>
<?php } ?>
<ul class="list-unstyled mt-3">
<?php
$select_skills_relation="SELECT * from skills_relation where seller_id='$seller_id'";
$run_skills_relation=mysqli_query($con,$select_skills_relation);
while($row_skills_relation=mysqli_fetch_array($run_skills_relation)){
     $relation_id=$row_skills_relation['relation_id'];
     $skill_id=$row_skills_relation['skill_id'];
     $skill_level=$row_skills_relation['skill_level'];
  
  $get_skill="SELECT * from seller_skills where skill_id='$skill_id'"; 
  $run_skill=mysqli_query($con,$get_skill);
  $row_skll=mysqli_fetch_array($run_skill);
  $skill_title=$row_skll['skill_title'];
?>
<li class="card-li mb-1">
<?php echo($skill_title); ?> - <span class="text-muted"><?php echo($skill_level); ?></span>
<?php if (isset($_SESSION['seller_user_name'])) { ?>
<?php if ($login_seller_user_name==$seller_user_name) { ?>
<a href="user.php?delete_skill=<?php echo($relation_id); ?>"><i class="fa fa-trash text-danger"></i></a>
<?php } ?>
<?php }?>
</li>
<?php } ?>
</ul>
</div>
</div>