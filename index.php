<?php
session_start();
include("includes/db.php");
//get the key site setting dynamicaly....
$get_general_settings="SELECT * from general_settings";
$run_general_settings=mysqli_query($con,$get_general_settings);
$row_general_settings=mysqli_fetch_array($run_general_settings);
$site_title =$row_general_settings["site_title"];
$site_desc =$row_general_settings["site_desc"];
$site_keywords =$row_general_settings["site_keywords"];
$site_author =$row_general_settings["site_author"];
?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	<title><?php echo $site_title;?></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="<?php echo $site_desc;?>">
	<meta name="keywords" content="<?php echo $site_keywords;?>">
	<meta name="author" content="<?php echo $site_author;?>">
	<link href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100" rel="stylesheet" >
	<link rel="stylesheet" href="styles/bootstrap.min.css">
	<link rel="stylesheet" href="styles/style.css">
	<link rel="stylesheet" href="styles/category_nav_style.css">
	<!-- Custome css from the user -->
	<link rel="stylesheet" href="styles/custom.css">
	<link rel="stylesheet" href="font-awesome/css/all.min.css">
	<link rel="stylesheet" href="styles/owl.carousel.css">
	<link rel="stylesheet" href="styles/owl.theme.default.css">
	<script src="js/jquery.slim.min.js"></script>
</head>
<body>
 <div class="preloader d-flex justify-content-center align-items-center">
    <img src="images/loadjuakali.gif" alt="the preloader"><br>
    <p>Loading...Please wait</p>
  </div> 	
<?php include("includes/header.php");?>
	
<?php 
if (!isset($_SESSION['seller_user_name'])) {
include("home.php");	
}else{
include("user_home.php");
}
?>
<?php include("includes/footer.php");?>
</body>
</html>