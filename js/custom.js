window.addEventListener('load', () => document.querySelector('.preloader').classList.add('hidepreloader'))
$(document).ready(function(){
$(".notop").sticky({
	zIndex:1000,
	className:'stickytop'
});	
//for the customized owl carousel while not logged
$(".home-featured-carousel").owlCarousel({
	items:4,
	margin:30,
	stagePadding:10,
	autoplay:true,
	autoplaySpeed:1000,
	responsiveClass:true,
	responsive:{
		0:{
			items:1,
		},
		480:{
			items:1,
		},
		600:{
			items:3,
		},
		768:{
			items:5,
		},
		1000:{
			items:5
		}
	}

});
$('[data-toggle="tooltip"]').tooltip();
// inside the logindropdown prevent propagation during toggle
$(document).on('click','.dropdown-menu',function(event){
	event.stopPropagation();
});
//hide the submenu while next/others are clicked
$(".dropdown-menu .dropdown-item.dropdown-toggle").click(function(){
	$(".collapse.dropdown-submenu").collapse('hide');
});
//for the owl carousel logged in user
$(".user-home-featured-carousel").owlCarousel({
	items:3,
	margin:30,
	stagePadding:20,
	autoplay:true,
	autoplaySpeed:1000,
	responsive:{
		0:{
			items:1,
		},
		480:{
			items:1,
		},
		600:{
			items:2,
		},
		1000:{
			items:3,
		}
	}
});
});