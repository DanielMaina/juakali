<?php
session_start();
include("includes/db.php");
if (isset($_SESSION['seller_user_name'])) {
  echo "
<script>window.open('index.php','_self');</script>
  ";
 }
?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	<title>Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="Juakali Mall">
	<link href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100" rel="stylesheet" >
	<link rel="stylesheet" href="styles/bootstrap.min.css">
	<link rel="stylesheet" href="styles/style.css">
	<link rel="stylesheet" href="styles/category_nav_style.css">
	<!-- Custome css from the user -->
	<link rel="stylesheet" href="styles/custom.css">
	<link rel="stylesheet" href="font-awesome/css/all.min.css">
	<script src="js/jquery.slim.min.js"></script>
</head>
<body>
<?php include("includes/header.php");?>
<div class="body-login">
<div class="container-login ">
	<div class="form">
		<h2>Login To Jua Kali Mall</h2>
		<form action="" method="POST">
			<div class="inputBox">
				<input type="text" name="seller_user_name" placeholder="UserName">
			</div>
			<div class="inputBox">
				<input type="password" name="seller_pass" placeholder="Password">
			</div>
			<div class="inputBox">
				<input type="submit" name="logged_id" value="LogIn">
			</div>
			<p>Forget Password? <a href="#" data-toggle="modal" data-target="#forgot-modal">Click Here</a></p>
		</form>
	</div>
</div>
</div>
<?php

//end od the user/seller login
if (isset($_POST['logged_id'])) {
$seller_user_name=mysqli_real_escape_string($con, $_POST['seller_user_name']);
$seller_pass=mysqli_real_escape_string($con, $_POST['seller_pass']);
$select_seller="SELECT * from sellers where seller_user_name='$seller_user_name' AND NOT seller_status='deactivated'";
$run_seller=mysqli_query($con,$select_seller);
$row_seller=mysqli_fetch_array($run_seller);
$hashed_password=$row_seller['seller_pass'];
$seller_status=$row_seller['seller_status'];
$decrypt_password=password_verify($seller_pass, $hashed_password);

if ($decrypt_password==0) {
	echo "
 <script>alert('Pasword or Username is incorrect');</script>
	";
	}else{
if($seller_status=="block-ban"){
echo "
<script>
alert('You Have Been Blocked By the Admin Please Contact Customer Care!');
window.open('$site_url/index.php','_self');
</script>

";
}else{		
$select_seller="SELECT * from sellers where seller_user_name='$seller_user_name' AND seller_pass='$hashed_password'";
$run_seller=mysqli_query($con,$select_seller);
if ($run_seller) {
$_SESSION['seller_user_name']=$seller_user_name;
$update_seller_status="UPDATE sellers set seller_status='online', seller_ip=$ip  where seller_user_name='$seller_user_name' AND seller_pass='$hashed_password' ";
$run_seller_status=mysqli_query($con,$update_seller_status);
echo "
<script>
alert('You are Logged InTo Your Account!');
window.open('$site_url/index.php','_self');
</script>

";

		}
	 }
	}
}
?>
<?php include("includes/footer.php");?>
</body>
</html>