<?php
session_start();
include("includes/db.php");

?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	<title>Categories</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="JuaKali Mall">
	<link href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100" rel="stylesheet" >
	<link rel="stylesheet" href="styles/bootstrap.min.css">
	<link rel="stylesheet" href="styles/style.css">
	<link rel="stylesheet" href="styles/category_nav_style.css">
	<!-- Custome css from the user -->
	<link rel="stylesheet" href="styles/custom.css">
	<link rel="stylesheet" href="font-awesome/css/all.min.css">
	<script src="js/jquery.slim.min.js"></script>
</head>
<body>
<?php include("includes/header.php");?>
	<div class="container mt-5 pt-5">
		<h2 class="text-center mb-4">Jua Kali Mall Categories</h2>
<div class="row flex-wrap">
<?php
$get_categories="SELECT * from categories where cat_featured= 'yes'";
$run_categories=mysqli_query($con,$get_categories);
while ($row_categories=mysqli_fetch_array($run_categories)) {
	$cat_id=$row_categories['cat_id'];
	$cat_title=$row_categories['cat_title'];
	$cat_image=$row_categories['cat_image'];
	$cat_desc=substr($row_categories['cat_desc'],0,60).'...';//truncate .'...';
?>
<div class="col-lg-3 col-md-4 col-sm-6">
<div class="mobile-category">
<a href="category.php?cat_id=<?php echo $cat_id; ?>">
<div class="ml-2 mt-3 category-picture">
<img src="cat_images/<?php echo $cat_image; ?>">
</div>
<div class="category-text">
<p class="category-title">
<strong><?php echo $cat_title; ?></strong>
</p>
<div class="mb-4 category-desc">
<?php echo $cat_desc; ?>
</div>
</div>
</a></div>
</div><!--end of a single col-lg-3 col-md-4 col-sm-6-->
<?php }?>	
	</div>
</div>
<?php include("includes/footer.php");?>
</body>
</html>