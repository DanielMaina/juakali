<?php  
session_start();
include("includes/db.php");
if (!isset($_SESSION['seller_user_name'])) {
echo "
<script>
window.open('login.php','_self');
</script>
";
}

$login_seller_user_name=$_SESSION['seller_user_name'];
$select_login_seller="SELECT* from sellers where seller_user_name='$login_seller_user_name'";
$run_login_seller=mysqli_query($con,$select_login_seller);
$row_login_seller=mysqli_fetch_array($run_login_seller);
$login_seller_id=$row_login_seller['seller_id'];
$login_seller_referral=$row_login_seller['seller_referral'];

//code line to enble the referral get some benefit 
$get_general_settings="SELECT * from general_settings";
$run_general_settings=mysqli_query($con,$get_general_settings);
$row_general_settings=mysqli_fetch_array($run_general_settings);
$referral_money=$row_general_settings['referral_money'];

 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	<title>My Referrals</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="Juakali Mall">
	<link href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100" rel="stylesheet" >
	<link rel="stylesheet" href="styles/bootstrap.min.css">
	<link rel="stylesheet" href="styles/style.css">
	<!-- Custome css from the user -->
	<link rel="stylesheet" href="styles/custom.css">
	<link rel="stylesheet" href="styles/user_nav_style.css">
	<link rel="stylesheet" href="font-awesome/css/all.min.css">
	<script src="js/jquery.slim.min.js"></script>
</head>
<body>
<?php include("includes/user_header.php");?>
<div class="container-fluid">
<div class="row">
<div class="col-lg-10 col-md-10 mt-5 mb-5">
<div class="card rounded-0">
<div class="card-body">
<h1>My Referrals</h1>	
<p class="lead">
For each unique member you refer that signs up you will get
<span class="font-weight-bold text-success">Ksh <?php echo($referral_money); ?></span>
added to your account balance once it is approved by us	
</p>
<h4 class="border border-primary rounded p-3">
Your Unique Referral Link Is:
<mark><?php echo($site_url); ?>?referral=<?php echo($login_seller_referral); ?></mark>
</h4>
<p class="lead text-danger">
Note: if we decide a referral if fraud it will be declined and you will not receive anything. 	
</p>
<div class="row">
<div class="col-md-4 mb-3">
<div class="card text-white border-success">
<div class="card-header text-center bg-success">	
<div class="display-4">Ksh 
<?php 
$total=0;
$sel_referrals="SELECT * from referrals where seller_id='$login_seller_id' AND status='approved'";
$run_referrals=mysqli_query($con,$sel_referrals);
while ($row_referrals=mysqli_fetch_array($run_referrals)) {
$comission=	$row_referrals['comission'];
$total +=$comission;

}
echo($total);

 ?>
</div>
<div class="font-weight-bold">
Approved <small>Earned</small>
</div>	
</div><!--card-header text-center bg-success ends-->	
</div><!--card text-white border-success ends-->	
</div><!--col-md-4 mb-3 ends-->
<div class="col-md-4 mb-3">
<div class="card text-white border-secondary">
<div class="card-header text-center bg-secondary">
<div class="display-4">Ksh 
<?php 
$total=0;
$sel_referrals="SELECT * from referrals where seller_id='$login_seller_id' AND status='pending'";
$run_referrals=mysqli_query($con,$sel_referrals);
while ($row_referrals=mysqli_fetch_array($run_referrals)) {
$comission=	$row_referrals['comission'];
$total +=$comission;

}
echo($total);

 ?>
</div>
<div class="font-weight-bold">
Pending
</div>	
</div><!--card-header text-center bg-secondary ends-->	
</div><!--card text-white border-secondary ends-->	
</div><!--col-md-4 mb-3 ends-->	
<div class="col-md-4 mb-3">
<div class="card text-white border-danger">
<div class="card-header text-center bg-danger">
<div class="display-4">Ksh 
<?php 
$total=0;
$sel_referrals="SELECT * from referrals where seller_id='$login_seller_id' AND status='declined'";
$run_referrals=mysqli_query($con,$sel_referrals);
while ($row_referrals=mysqli_fetch_array($run_referrals)) {
$comission=	$row_referrals['comission'];
$total +=$comission;

}
echo($total);

 ?>
</div>
<div class="font-weight-bold">
Declined
</div>	
</div><!--card-header text-center bg-danger ends-->	
</div><!--card text-white border-danger ends-->	
</div><!--col-md-4 mb-3 ends-->		
</div><!--inner row ends-->
<div class="table-responsive border border-secondary rounded">
<table class="table table-hover">
<thead>
<tr class="card-header">
<th>Username</th>	
<th>Signup Date</th>	
<th>Your Commision</th>	
<th>Status</th>	
</tr>
</thead>
<tbody>
<?php 
$sel_referrals="SELECT * from referrals where seller_id='$login_seller_id' order by 1 DESC ";
$run_referrals=mysqli_query($con,$sel_referrals);
$count_referrals=mysqli_num_rows($run_referrals);

if ($count_referrals==0) {
echo "
<tr>
<td class='text-center' colspan='4'>
<h4 class='text-primary'>You Have Not Referred AnyOne Yet</h4>
</td>
</tr>
";
}else{
while ($row_referrals=mysqli_fetch_array($run_referrals)) {
$referred_id=$row_referrals['refarral_id'];			
$comission=$row_referrals['comission'];			
$date=$row_referrals['date'];			
$status=$row_referrals['status'];			

//copy from register_login_forget.php
$sel_seller="SELECT * from sellers where seller_id='$referred_id' ";	
$run_seller=mysqli_query($con,$sel_seller);
$row_seller=mysqli_fetch_array($run_seller);
$seller_user_name=$row_seller['seller_user_name'];

 ?>	
<tr>
<td><?php echo($seller_user_name); ?></td>		
<td><?php echo($date); ?></td>	
<td>Ksh <?php echo($comission); ?></td>
<td class="font-weight-bold
<?php 
if($status=="approved"){
echo "text-success";
}elseif($status=="pending"){
echo "text-secondary";
}elseif($status=="declined"){
echo "text-danger";
}

?>
"><?php echo($status); ?></td>	
</tr>
<?php } } ?>
</tbody>	
</table>	
</div><!--table-responsive border border-secondary rounded ends-->
</div><!--card-body ends-->	
</div><!--card rounded-0 ends-->	
</div><!--col-lg-10 col-md-10 mt-5 mb-5 ends-->	
</div><!--rows-->	
</div><!--container-fluid ends-->
<?php include("includes/footer.php");?>
</body>
</html>