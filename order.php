<?php 
session_start();
include("includes/db.php");
if (!isset($_SESSION['seller_user_name'])) {
echo "
<script>
window.open('login.php','_self');
</script>
";
}//end of if session

if (isset($_SESSION['seller_user_name'])) {

$login_seller_user_name=$_SESSION['seller_user_name'];
$select_login_seller="SELECT * from sellers where seller_user_name='$login_seller_user_name'";
$run_login_seller=mysqli_query($con,$select_login_seller);
$row_login_seller=mysqli_fetch_array($run_login_seller);
$login_seller_id=$row_login_seller['seller_id'];

//for the payment processing
$get_payment_setting="SELECT * from payment_settings";
$run_payment_setting=mysqli_query($con,$get_payment_setting);
$row_payament_setting=mysqli_fetch_array($run_payment_setting);
$processing_fee=$row_payament_setting['processing_fee'];



/// Single Product Checkout Order Code strt
if (isset($_SESSION['checkout_seller_id'])) {
$buyer_id=$_SESSION['checkout_seller_id'];
$proposal_id=$_SESSION['proposal_id'];
$order_price=$_SESSION['proposal_price'];
$proposal_qty=$_SESSION['proposal_qty'];
$payment_method=$_SESSION['method'];

$select_proposal="SELECT * from proposals where proposal_id='$proposal_id'";
$run_proposal=mysqli_query($con,$select_proposal);
$row_proposal=mysqli_fetch_array($run_proposal);
$proposal_title=$row_proposal['proposal_title'];
$proposal_seller_id=$row_proposal['proposal_seller_id'];
$delivery_id=$row_proposal['delivery_id'];

$select_delivery_time="SELECT * from delivery_times where delivery_id='$delivery_id'";
$run_delivery_time=mysqli_query($con,$select_delivery_time);
$row_delivery_time=mysqli_fetch_array($run_delivery_time);
$delivery_proposal_title=$row_delivery_time['delivery_proposal_title'];
$add_days=substr($delivery_proposal_title, 0, 1);
date_default_timezone_set("UTC");
$order_date=date("F d, Y");
$date_time=date("M d, Y h:i:s");
$order_time=date("M d, Y h:i:s", strtotime($date_time . " + $add_days days"));
$order_number=mt_rand();
if ($payment_method=="shopping_balance") {
$insert_order="INSERT INTO orders (order_number,order_duration,order_time,order_date,seller_id,buyer_id,proposal_id,order_price,order_qty,order_fee,order_active,order_status) values('$order_number','$delivery_proposal_title','$order_time','$order_date','$proposal_seller_id','$buyer_id','$proposal_id','$order_price','$proposal_qty','','yes','pending')";

}else{
	$insert_order="INSERT INTO orders (order_number,order_duration,order_time,order_date,seller_id,buyer_id,proposal_id,order_price,order_qty,order_fee,order_active,order_status) values('$order_number','$delivery_proposal_title','$order_time','$order_date','$proposal_seller_id','$buyer_id','$proposal_id','$order_price','$proposal_qty','$processing_fee','yes','pending')";
}//end of payment method
$run_order=mysqli_query($con,$insert_order);
$insert_order_id=mysqli_insert_id($con);

if ($run_order) {
$select_proposal_seller="SELECT * FROM sellers where seller_id='$proposal_seller_id'";
$run_proposal_seller=mysqli_query($con,$select_proposal_seller);
$row_proposal_seller=mysqli_fetch_array($run_proposal_seller);	
$proposal_seller_user_name=$row_proposal_seller['seller_user_name'];
$proposal_seller_email=$row_proposal_seller['seller_email'];

$select_general_setting="SELECT * from general_settings";
$run_genaral_settings=mysqli_query($con,$select_general_setting);
$row_general_settings=mysqli_fetch_array($run_genaral_settings);
$site_email_address=$row_general_settings['site_email_address'];

$subject="Jua Kali Mall: Congrats! You Have A New Order From $login_seller_user_name";

$email_message="
<html>
<head>
<style>

.container{
	background:#AFCED0;
	padding:80px;

}
.box{
	background:#fff;
	margin:0px 0px 30px;
	padding:8px 20px 20px;
	border:1px solid #e6e6e6;
	box-shandow:0px 1px 5px rgba(0,0,0,0.1);
}
.lead{
	font-size:16px;

}
.btn{
	background:green;
	color:white;
	text-decoration:none;
	padding:10px 16px;
	font-size:18px;
	border-radius:3px;
}
hr{
	margin-top:20px;
	margin-botton:20px;
	border:1px solid #eee;
}
.table{
max-width:100%;
background-color:#fff;
margin-bottom:;	
}
.table thead tr th{
border:4px solid red;
font-weight:bolder;
padding:10px;	
}

.table tbody tr td{
	border:4px solid red;
	padding:10px;
}   
</style>
</head>
<body>
<div class='container'>
<div class='box'>
<center>
<img src='$site_url/images/favicon.png' width='100'>
<h2>You have Received An ORDER From $login_seller_user_name</h2>
</center>
<hr>
<p class='lead'>Dear $proposal_seller_user_name, Order Details</p>
<table class='table'>
<thead>
<tr>
<th>Product</th>
<th>Quantity</th>
<th>Duration</th>
<th>Amount</th>
<th>Buyer</th>
</tr>
</thead>
<tbody>
<tr>
<td>$proposal_title</td>
<td>$proposal_qty</td>
<td>$delivery_proposal_title</td>
<td>$order_price</td>
<td>$login_seller_user_name</td>
</tr>
</tbody>
</table>
<center>
<a href='$site_url/order_details.php?order_id=$insert_order_id' class='btn'>View Your Order</a>
</center>
</div>
</div>	
</body>
</html>


";
$headers  ="From: JuaKaliMall\r\n";
$headers .="Reply-To: $site_email_address\r\n";
$headers .="content-type: text/html\r\n";
mail($proposal_seller_email, $subject, $email_message,$headers);
//update the db buyer account
$select_my_buyer="SELECT * from my_buyers where seller_id='proposal_seller_id' AND buyer_id='$login_seller_id'";

$run_my_buyer=mysqli_query($con,$select_my_buyer);
$count_my_buyer=mysqli_num_rows($run_my_buyer);
if ($count_my_buyer==1) {
$update_my_buyer="UPDATE my_buyers set completed_orders=completed_orders+1, amount_spent=amount_spent+$order_price,last_order_date='$order_date' where seller_id='$proposal_seller_id' AND  buyer_id='$login_seller_id'";
$run_my_buyer=mysqli_query($con,$update_my_buyer);
}else{
	$insert_my_buyer="INSERT INTO my_buyers(seller_id,buyer_id,completed_orders,amount_spent,last_order_date) values('$proposal_seller_id','$login_seller_id','1','$order_price','$order_date')";
$run_my_buyer=mysqli_query($con,$insert_my_buyer);

}
//getting to update Seller account!!!
$select_my_seller="SELECT * from my_sellers where buyer_id='login_seller_id' AND seller_id='$proposal_seller_id'";
$run_my_seller=mysqli_query($con,$select_my_seller);
$count_my_seller=mysqli_num_rows($run_my_seller);
if ($count_my_seller==1) {
	$update_my_seller="UPDATE my_sellers set completed_orders=completed_orders+1, amount_spent=amount_spent+$order_price,last_order_date='$order_date' where buyer_id='$login_seller_id' AND seller_id='$proposal_seller_id'";
	$run_my_seller=mysqli_query($con,$update_my_seller);
}else{
$insert_my_seller="INSERT INTO my_sellers(buyer_id,seller_id,completed_orders,amount_spent,last_order_date) values('$login_seller_id','$proposal_seller_id','1','$order_price','$order_date')";
$run_my_seller=mysqli_query($con,$insert_my_seller);
}
$total_amount=$order_price+$processing_fee;
if ($payment_method="shopping_balance") {
	$insert_purchase="INSERT INTO purchases(seller_id,order_id,amount,date,method) values('$login_seller_id','$insert_order_id','$order_price','$order_date','$payment_method')";
}else{
$insert_purchase="INSERT INTO purchases(seller_id,order_id,amount,date,method) values('$login_seller_id','$insert_order_id','$total_amount','$order_date','$payment_method')";
}
$run_purchase=mysqli_query($con,$insert_purchase);
 $insert_notification="INSERT INTO notifications(receiver_id,sender_id,order_id,reason,date,status) values('$proposal_seller_id','$login_seller_id','$insert_order_id','order','$order_date','unread')";

$run_notification=mysqli_query($con,$insert_notification);
unset($_SESSION['checkout_seller_id']);
unset($_SESSION['proposal_id']);
unset($_SESSION['proposal_qty']);
unset($_SESSION['proposal_price']);
unset($_SESSION['method']);

echo "
<script>alert('Your Order Has Been Successfully Submitted, Thank you!');
window.open('order_details.php?order_id=$insert_order_id','_self');
</script>
";

}

}
//end off single proposal from the chechout.php
#**********************************************************#
// Product order code from Cart.php strt
if (isset($_SESSION['cart_seller_id'])) {
	$buyer_id=$_SESSION['cart_seller_id'];
	$payment_method=$_SESSION['method'];

$select_cart="SELECT * from cart where seller_id='$buyer_id'";
$run_cart=mysqli_query($con,$select_cart);
while ($row_cart =mysqli_fetch_array($run_cart)) {
$proposal_id=$row_cart['proposal_id'];	
$proposal_price=$row_cart['proposal_price'];	
$proposal_qty=$row_cart['proposal_qty'];
$sub_total=$proposal_price*$proposal_qty;
$order_price=$sub_total;

$select_proposal="SELECT * from proposals where proposal_id='$proposal_id'";
$run_proposal=mysqli_query($con,$select_proposal);
$row_proposal=mysqli_fetch_array($run_proposal);
$proposal_title=$row_proposal['proposal_title'];
$proposal_seller_id=$row_proposal['proposal_seller_id'];
$delivery_id=$row_proposal['delivery_id'];

$select_delivery_time="SELECT * from delivery_times where delivery_id='$delivery_id'";
$run_delivery_time=mysqli_query($con,$select_delivery_time);
$row_delivery_time=mysqli_fetch_array($run_delivery_time);
$delivery_proposal_title=$row_delivery_time['delivery_proposal_title'];
$add_days=substr($delivery_proposal_title, 0, 1);
date_default_timezone_set("UTC");
$order_date=date("F d, Y");
$date_time=date("M d, Y h:i:s");
$order_time=date("M d, Y h:i:s", strtotime($date_time . " + $add_days days"));
$order_number=mt_rand();

$insert_order="INSERT INTO orders (order_number,order_duration,order_time,order_date,seller_id,buyer_id,proposal_id,order_price,order_qty,order_fee,order_active,order_status) values('$order_number','$delivery_proposal_title','$order_time','$order_date','$proposal_seller_id','$buyer_id','$proposal_id','$order_price','$proposal_qty','','yes','pending')";

$run_order=mysqli_query($con,$insert_order);
$insert_order_id=mysqli_insert_id($con);

if ($run_order) {
$select_proposal_seller="SELECT * FROM sellers where seller_id='$proposal_seller_id'";
$run_proposal_seller=mysqli_query($con,$select_proposal_seller);
$row_proposal_seller=mysqli_fetch_array($run_proposal_seller);	
$proposal_seller_user_name=$row_proposal_seller['seller_user_name'];
$proposal_seller_email=$row_proposal_seller['seller_email'];

$select_general_setting="SELECT * from general_settings";
$run_genaral_settings=mysqli_query($con,$select_general_setting);
$row_general_settings=mysqli_fetch_array($run_genaral_settings);
$site_email_address=$row_general_settings['site_email_address'];

$subject="JuaKaliMall: Congrats! You Have A New Order From $login_seller_user_name";

$email_message="
<html>
<head>
<style>

.container{
	background:#AFCED0;
	padding:80px;

}
.box{
	background:#fff;
	margin:0px 0px 30px;
	padding:8px 20px 20px;
	border:1px solid #e6e6e6;
	box-shandow:0px 1px 5px rgba(0,0,0,0.1);
}
.lead{
	font-size:16px;

}
.btn{
	background:green;
	color:white;
	text-decoration:none;
	padding:10px 16px;
	font-size:18px;
	border-radius:3px;
}
hr{
	margin-top:20px;
	margin-botton:20px;
	border:1px solid #eee;
}
.table{
max-width:100%;
background-color:#DCF9F7;
margin-bottom:;	
}
.table thead tr th{
border:2px solid #1C2322;
font-weight:bolder;
padding:10px;	
}

.table tbody tr td{
	border:2px solid #1C2322;
	padding:10px;
}   
</style>
</head>
<body>
<div class='container'>
<div class='box'>
<center>
<img src='$site_url/images/favicon.png' width='100'>
<h2>You have Received An ORDER From $login_seller_user_name</h2>
</center>
<hr>
<p class='lead'>Dear $proposal_seller_user_name, Order Details</p>
<table class='table'>
<thead>
<tr>
<th>Product</th>
<th>Quantity</th>
<th>Duration</th>
<th>Amount</th>
<th>Buyer</th>
</tr>
</thead>
<tbody>
<tr>
<td>$proposal_title</td>
<td>$proposal_qty</td>
<td>$delivery_proposal_title</td>
<td>$order_price</td>
<td>$login_seller_user_name</td>
</tr>
</tbody>
</table>
<center>
<a href='$site_url/order_details.php?order_id=$insert_order_id' class='btn'>View Your Order</a>
</center>
</div>
</div>	
</body>
</html>


";
$headers  ="From: JuaKaliMall\r\n";
$headers .="Reply-To: $site_email_address\r\n";
$headers .="content-type: text/html\r\n";
mail($proposal_seller_email, $subject, $email_message,$headers);
//update the db buyer account
$select_my_buyer="SELECT * from my_buyers where seller_id='proposal_seller_id' AND buyer_id='$login_seller_id'";

$run_my_buyer=mysqli_query($con,$select_my_buyer);
$count_my_buyer=mysqli_num_rows($run_my_buyer);
if ($count_my_buyer==1) {
$update_my_buyer="UPDATE my_buyers set completed_orders=completed_orders+1, amount_spent=amount_spent+$order_price,last_order_date='$order_date' where seller_id='$proposal_seller_id' AND  buyer_id='$login_seller_id'";
$run_my_buyer=mysqli_query($con,$update_my_buyer);
}else{
	$insert_my_buyer="INSERT INTO my_buyers(seller_id,buyer_id,completed_orders,amount_spent,last_order_date) values('$proposal_seller_id','$login_seller_id','1','$order_price','$order_date')";
$run_my_buyer=mysqli_query($con,$insert_my_buyer);

}
//getting to update Selert account!!!
$select_my_seller="SELECT * from my_sellers where buyer_id='login_seller_id' AND seller_id='$proposal_seller_id'";
$run_my_seller=mysqli_query($con,$select_my_seller);
$count_my_seller=mysqli_num_rows($run_my_seller);
if ($count_my_seller==1) {
	$update_my_seller="UPDATE my_sellers set completed_orders=completed_orders+1, amount_spent=amount_spent+$order_price,last_order_date='$order_date' where buyer_id='$login_seller_id' AND seller_id='$proposal_seller_id'";
	$run_my_seller=mysqli_query($con,$update_my_seller);
}else{
$insert_my_seller="INSERT INTO my_sellers(buyer_id,seller_id,completed_orders,amount_spent,last_order_date) values('$login_seller_id','$proposal_seller_id','1','$order_price','$order_date')";
$run_my_seller=mysqli_query($con,$insert_my_seller);
}


$insert_purchase="INSERT INTO purchases(seller_id,order_id,amount,date,method) values('$login_seller_id','$insert_order_id','$order_price','$order_date','$payment_method')";
$run_purchase=mysqli_query($con,$insert_purchase);
 $insert_notification="INSERT INTO notifications(receiver_id,sender_id,order_id,reason,date,status) values('$proposal_seller_id','$login_seller_id','$insert_order_id','order','$order_date','unread')";
$run_notification=mysqli_query($con,$insert_notification);
}
}
$delete_cart="DELETE from cart where seller_id='$buyer_id'";
$run_delete=mysqli_query($con,$delete_cart);
unset($_SESSION['cart_seller_id']);
unset($_SESSION['method']);
echo "<script>
alert('Your Order has been Submitted, Thank you!');
window.open('buying_orders.php','_self');
</script>";              

}
// Proposal order code from Cart.php ends


//Single Offer.php Code Result for the payment srt

if (isset($_SESSION['offer_id'])) {
	$buyer_id=$_SESSION['offer_buyer_id'];
	$offer_id=$_SESSION['offer_id'];
	$payment_method=$_SESSION['method'];

//copy from manage_requests.php
$select_offers="SELECT * from send_offers where offer_id='$offer_id'";
$run_offers=mysqli_query($con,$select_offers);
$row_offers=mysqli_fetch_array($run_offers);
$proposal_id=$row_offers['proposal_id'];	
$description=$row_offers['description'];	
$delivery_time=$row_offers['delivery_time'];	
$order_price=$row_offers['amount'];	
$proposal_qty="1";

//copy from the 36 line code for the checkout 
$select_proposal="SELECT * from proposals where proposal_id='$proposal_id'";
$run_proposal=mysqli_query($con,$select_proposal);
$row_proposal=mysqli_fetch_array($run_proposal);
$proposal_title=$row_proposal['proposal_title'];
$proposal_seller_id=$row_proposal['proposal_seller_id'];

$add_days=substr($delivery_time, 0, 1);
date_default_timezone_set("UTC");
$order_date=date("F d, Y");
$date_time=date("M d, Y h:i:s");
$order_time=date("M d, Y h:i:s", strtotime($date_time . " + $add_days days"));
$order_number=mt_rand();
if ($payment_method=="shopping_balance") {
$insert_order="INSERT INTO orders (order_number,order_duration,order_time,order_date,order_description,seller_id,buyer_id,proposal_id,order_price,order_qty,order_fee,order_active,order_status) values('$order_number','$delivery_time','$order_time','$order_date','$description','$proposal_seller_id','$buyer_id','$proposal_id','$order_price','$proposal_qty','','yes','pending')";

}else{//due to charge fee for the paypal and stripe
	$insert_order="INSERT INTO orders (order_number,order_duration,order_time,order_date,order_description,seller_id,buyer_id,proposal_id,order_price,order_qty,order_fee,order_active,order_status) values('$order_number','$delivery_time','$order_time','$order_date','$description','$proposal_seller_id','$buyer_id','$proposal_id','$order_price','$proposal_qty','$processing_fee','yes','pending')";
}//end of payment method
$run_order=mysqli_query($con,$insert_order);
$insert_order_id=mysqli_insert_id($con);

if ($run_order) {
//get to update some table
$update_offer_status="UPDATE send_offers set status='send'where offer_id='$offer_id'";
$run_update_offer_status=mysqli_query($con,$update_offer_status);
$select_proposal_seller="SELECT * FROM sellers where seller_id='$proposal_seller_id'";
$run_proposal_seller=mysqli_query($con,$select_proposal_seller);
$row_proposal_seller=mysqli_fetch_array($run_proposal_seller);	
$proposal_seller_user_name=$row_proposal_seller['seller_user_name'];
$proposal_seller_email=$row_proposal_seller['seller_email'];

$select_general_setting="SELECT * from general_settings";
$run_genaral_settings=mysqli_query($con,$select_general_setting);
$row_general_settings=mysqli_fetch_array($run_genaral_settings);
$site_email_address=$row_general_settings['site_email_address'];

$subject="JuaKaliMall: Congrats! You Have A New Order From $login_seller_user_name";

$email_message="
<!DOCTYPE html>
<html>
<head>
<style>

.container{
	background:#AFCED0;
	padding:80px;

}
.box{
	background:#fff;
	margin:0px 0px 30px;
	padding:8px 20px 20px;
	border:1px solid #e6e6e6;
	box-shandow:0px 1px 5px rgba(0,0,0,0.1);
}
.lead{
	font-size:16px;

}
.btn{
	background:green;
	color:white;
	text-decoration:none;
	padding:10px 16px;
	font-size:18px;
	border-radius:3px;
}
hr{
	margin-top:20px;
	margin-botton:20px;
	border:1px solid #eee;
}
.table{
max-width:100%;
background-color:#DCF9F7;
margin-bottom:;	
}
.table thead tr th{
border:2px solid #1C2322;
font-weight:bolder;
padding:10px;	
}

.table tbody tr td{
	border:2px solid #1C2322;
	padding:10px;
}   
</style>
</head>
<body>
<div class='container'>
<div class='box'>
<center>
<img src='$site_url/images/favicon.png' width='100'>
<h2>You have Received An ORDER From $login_seller_user_name</h2>
</center>
<hr>
<p class='lead'>Dear $proposal_seller_user_name, Order Details</p>
<table class='table'>
<thead>
<tr>
<th>Product</th>
<th>Quantity</th>
<th>Duration</th>
<th>Amount</th>
<th>Buyer</th>
</tr>
</thead>
<tbody>
<tr>
<td>$proposal_title</td>
<td>$proposal_qty</td>
<td>$delivery_time</td>
<td>$order_price</td>
<td>$login_seller_user_name</td>
</tr>
</tbody>
</table>
<center>
<a href='$site_url/order_details.php?order_id=$insert_order_id' class='btn'>View Your Order</a>
</center>
</div>
</div>	
</body>
</html>


";
$headers  ="From: JuaKaliMall\r\n";
$headers .="Reply-To: $site_email_address\r\n";
$headers .="content-type: text/html\r\n";
mail($proposal_seller_email, $subject, $email_message,$headers);
//update the db buyer account
$select_my_buyer="SELECT * from my_buyers where seller_id='proposal_seller_id' AND buyer_id='$login_seller_id'";

$run_my_buyer=mysqli_query($con,$select_my_buyer);
$count_my_buyer=mysqli_num_rows($run_my_buyer);
if ($count_my_buyer==1) {
$update_my_buyer="UPDATE my_buyers set completed_orders=completed_orders+1, amount_spent=amount_spent+$order_price,last_order_date='$order_date' where seller_id='$proposal_seller_id' AND  buyer_id='$login_seller_id'";
$run_my_buyer=mysqli_query($con,$update_my_buyer);
}else{
	$insert_my_buyer="INSERT INTO my_buyers(seller_id,buyer_id,completed_orders,amount_spent,last_order_date) values('$proposal_seller_id','$login_seller_id','1','$order_price','$order_date')";
$run_my_buyer=mysqli_query($con,$insert_my_buyer);

}
//getting to update Selert account!!!
$select_my_seller="SELECT * from my_sellers where buyer_id='login_seller_id' AND seller_id='$proposal_seller_id'";
$run_my_seller=mysqli_query($con,$select_my_seller);
$count_my_seller=mysqli_num_rows($run_my_seller);
if ($count_my_seller==1) {
	$update_my_seller="UPDATE my_buyers set completed_orders=completed_orders+1, amount_spent=amount_spent+$order_price,last_order_date='$order_date' where buyer_id='$login_seller_id' AND seller_id='$proposal_seller_id'";
	$run_my_seller=mysqli_query($con,$update_my_seller);
}else{
$insert_my_seller="INSERT INTO my_sellers(buyer_id,seller_id,completed_orders,amount_spent,last_order_date) values('$login_seller_id','$proposal_seller_id','1','$order_price','$order_date')";
$run_my_seller=mysqli_query($con,$insert_my_seller);
}
$total_amount=$order_price+$processing_fee;
if ($payment_method="shopping_balance") {
	$insert_purchase="INSERT INTO purchases(seller_id,order_id,amount,date,method) values('$login_seller_id','$insert_order_id','$order_price','$order_date','$payment_method')";
}else{
$insert_purchase="INSERT INTO purchases(seller_id,order_id,amount,date,method) values('$login_seller_id','$insert_order_id','$total_amount','$order_date','$payment_method')";
}
$run_purchase=mysqli_query($con,$insert_purchase);
 $insert_notification="INSERT INTO notifications(receiver_id,sender_id,order_id,reason,date,status) values('$proposal_seller_id','$login_seller_id','$insert_order_id','order','$order_date','unread')";

$run_notification=mysqli_query($con,$insert_notification);

//when all is done remove the session...
unset($_SESSION['offer_id']);
unset($_SESSION['offer_buyer_id']);
unset($_SESSION['method']);

echo "
<script>alert('Your Order Has Been Successfully Submitted, Thank you!');
window.open('order_details.php?order_id=$insert_order_id','_self');
</script>
";

}

}
//Single Offer Code Result for the payment ends

// Message offer code for payment convesation folder payment method

 if (isset($_SESSION['message_offer_id'])) {
 $message_offer_id=$_SESSION['message_offer_id'];	
 $buyer_id=$_SESSION['message_offer_buyer_id'];	
 $payment_method=$_SESSION['method'];

$select_offer="SELECT * from messages_offers where offer_id='$message_offer_id'";
$run_offer=mysqli_query($con,$select_offer);
$row_offer=mysqli_fetch_array($run_offer);
$proposal_id=$row_offer['proposal_id'];
$description=$row_offer['description'];
$delivery_time=$row_offer['delivery_time'];
$order_price=$row_offer['amount'];

$proposal_qty="1";

//copy from single offer result
$select_proposal="SELECT * from proposals where proposal_id='$proposal_id'";
$run_proposal=mysqli_query($con,$select_proposal);
$row_proposal=mysqli_fetch_array($run_proposal);
$proposal_title=$row_proposal['proposal_title'];
$proposal_seller_id=$row_proposal['proposal_seller_id'];

$add_days=substr($delivery_time, 0, 1);
date_default_timezone_set("UTC");
$order_date=date("F d, Y");
$date_time=date("M d, Y h:i:s");
$order_time=date("M d, Y h:i:s", strtotime($date_time . " + $add_days days"));
$order_number=mt_rand();
if ($payment_method=="shopping_balance") {
$insert_order="INSERT INTO orders (order_number,order_duration,order_time,order_date,order_description,seller_id,buyer_id,proposal_id,order_price,order_qty,order_fee,order_active,order_status) values('$order_number','$delivery_time','$order_time','$order_date','$description','$proposal_seller_id','$buyer_id','$proposal_id','$order_price','$proposal_qty','','yes','pending')";

}else{//due to charge fee for the paypal and stripe
	$insert_order="INSERT INTO orders (order_number,order_duration,order_time,order_date,order_description,seller_id,buyer_id,proposal_id,order_price,order_qty,order_fee,order_active,order_status) values('$order_number','$delivery_time','$order_time','$order_date','$description','$proposal_seller_id','$buyer_id','$proposal_id','$order_price','$proposal_qty','$processing_fee','yes','pending')";
}//end of payment method
$run_order=mysqli_query($con,$insert_order);
$insert_order_id=mysqli_insert_id($con);

if ($run_order) {

//get to update some table
//track the update on [18.10.2019 :2.34.A.M]
$update_message_offer_status= "UPDATE message_offers set order_id='$insert_order_id', status='accepted' where offer_id='$message_offer_id'";
$run_update_message_offer_status=mysqli_query($con,$update_message_offer_status);

$select_proposal_seller="SELECT * FROM sellers where seller_id='$proposal_seller_id'";
$run_proposal_seller=mysqli_query($con,$select_proposal_seller);
$row_proposal_seller=mysqli_fetch_array($run_proposal_seller);	
$proposal_seller_user_name=$row_proposal_seller['seller_user_name'];
$proposal_seller_email=$row_proposal_seller['seller_email'];

$select_general_setting="SELECT * from general_settings";
$run_genaral_settings=mysqli_query($con,$select_general_setting);
$row_general_settings=mysqli_fetch_array($run_genaral_settings);
$site_email_address=$row_general_settings['site_email_address'];

$subject="JuaKaliMall: Congrats! You Have A New Order From $login_seller_user_name";

$email_message="
<!DOCTYPE html>
<html>
<head>
<style>

.container{
	background:#AFCED0;
	padding:80px;

}
.box{
	background:#fff;
	margin:0px 0px 30px;
	padding:8px 20px 20px;
	border:1px solid #e6e6e6;
	box-shandow:0px 1px 5px rgba(0,0,0,0.1);
}
.lead{
	font-size:16px;

}
.btn{
	background:green;
	color:white;
	text-decoration:none;
	padding:10px 16px;
	font-size:18px;
	border-radius:3px;
}
hr{
	margin-top:20px;
	margin-botton:20px;
	border:1px solid #eee;
}
.table{
max-width:100%;
background-color:#DCF9F7;
margin-bottom:;	
}
.table thead tr th{
border:2px solid #1C2322;
font-weight:bolder;
padding:10px;	
}

.table tbody tr td {
	border:2px solid #1C2322;
	padding:10px;
} 
</style>
</head>
<body>
<div class='container'>
<div class='box'>
<center>
<img src='$site_url/images/favicon.png' width='100'>
<h2>You have Received An ORDER From $login_seller_user_name</h2>
</center>
<hr>
<p class='lead'>Dear $proposal_seller_user_name, Order Details</p>
<table class='table'>
<thead>
<tr>
<th>Product</th>
<th>Quantity</th>
<th>Duration</th>
<th>Amount</th>
<th>Buyer</th>
</tr>
</thead>
<tbody>
<tr>
<td>$proposal_title</td>
<td>$proposal_qty</td>
<td>$delivery_time</td>
<td>$order_price</td>
<td>$login_seller_user_name</td>
</tr>
</tbody>
</table>
<center>
<a href='$site_url/order_details.php?order_id=$insert_order_id' class='btn'>View Your Order</a>
</center>
</div>
</div>	
</body>
</html>


";
$headers  ="From: JuaKaliMall\r\n";
$headers .="Reply-To: $site_email_address\r\n";
$headers .="content-type: text/html\r\n";
mail($proposal_seller_email, $subject, $email_message,$headers);
//update the db buyer account
$select_my_buyer="SELECT * from my_buyers where seller_id='proposal_seller_id' AND buyer_id='$login_seller_id'";

$run_my_buyer=mysqli_query($con,$select_my_buyer);
$count_my_buyer=mysqli_num_rows($run_my_buyer);
if ($count_my_buyer==1) {
$update_my_buyer="UPDATE my_buyers set completed_orders=completed_orders+1, amount_spent=amount_spent+$order_price,last_order_date='$order_date' where seller_id='$proposal_seller_id' AND  buyer_id='$login_seller_id'";
$run_my_buyer=mysqli_query($con,$update_my_buyer);
}else{
	$insert_my_buyer="INSERT INTO my_buyers(seller_id,buyer_id,completed_orders,amount_spent,last_order_date) values('$proposal_seller_id','$login_seller_id','1','$order_price','$order_date')";
$run_my_buyer=mysqli_query($con,$insert_my_buyer);

}
//getting to update Selert account!!!
$select_my_seller="SELECT * from my_sellers where buyer_id='login_seller_id' AND seller_id='$proposal_seller_id'";
$run_my_seller=mysqli_query($con,$select_my_seller);
$count_my_seller=mysqli_num_rows($run_my_seller);
if ($count_my_seller==1) {
	$update_my_seller="UPDATE my_buyers set completed_orders=completed_orders+1, amount_spent=amount_spent+$order_price,last_order_date='$order_date' where buyer_id='$login_seller_id' AND seller_id='$proposal_seller_id'";
	$run_my_seller=mysqli_query($con,$update_my_seller);
}else{
$insert_my_seller="INSERT INTO my_sellers(buyer_id,seller_id,completed_orders,amount_spent,last_order_date) values('$login_seller_id','$proposal_seller_id','1','$order_price','$order_date')";
$run_my_seller=mysqli_query($con,$insert_my_seller);
}
$total_amount=$order_price + $processing_fee;
if ($payment_method="shopping_balance") {
	$insert_purchase="INSERT INTO purchases(seller_id,order_id,amount,date,method) values('$login_seller_id','$insert_order_id','$order_price','$order_date','$payment_method')";
}else{
$insert_purchase="INSERT INTO purchases(seller_id,order_id,amount,date,method) values('$login_seller_id','$insert_order_id','$total_amount','$order_date','$payment_method')";
}
$run_purchase=mysqli_query($con,$insert_purchase);
 $insert_notification="INSERT INTO notifications(receiver_id,sender_id,order_id,reason,date,status) values('$proposal_seller_id','$login_seller_id','$insert_order_id','order','$order_date','unread')";

$run_notification=mysqli_query($con,$insert_notification);
unset($_SESSION['message_offer_id']);
unset($_SESSION['message_offer_buyer_id']);
unset($_SESSION['method']);

echo "
<script>alert('Your Order Has Been Successfully Submitted, Thank you!');
window.open('order_details.php?order_id=$insert_order_id','_self');
</script>
";
}

}


//Message offer code from payment convesation folder ends

}
 

 ?>




