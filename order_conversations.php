<?php 
//create the session and redirect the unlogged
@session_start(); 
include("includes/db.php");
if (!isset($_SESSION['seller_user_name'])) {
echo "<script>window.open('login.php','_self');</script>";
}
//a copy(dashboard) get id and username/email... 
$login_seller_user_name=$_SESSION['seller_user_name'];
$select_login_seller="SELECT * from sellers where seller_user_name='$login_seller_user_name'";
$run_login_seller=mysqli_query($con,$select_login_seller);
$row_login_seller=mysqli_fetch_array($run_login_seller);
$login_seller_id=$row_login_seller['seller_id'];

@$order_id=$_GET['order_id'];

//copy(order_details) get order details
$get_orders="SELECT * from orders where order_id='$order_id'";
$run_orders=mysqli_query($con,$get_orders);
$row_orders=mysqli_fetch_array($run_orders);
$seller_id=$row_orders['seller_id'];
$buyer_id=$row_orders['buyer_id'];
$order_price=$row_orders['order_price'];
$order_status=$row_orders['order_status'];

//for the payment processing
$get_payment_setting="SELECT * from payment_settings";
$run_payment_setting=mysqli_query($con,$get_payment_setting);
$row_payament_setting=mysqli_fetch_array($run_payment_setting);
$commission_percentage=$row_payament_setting['commission_percentage'];
$days_before_withdraw=$row_payament_setting['days_before_withdraw'];

function getPercentNumber($amount, $percentage){
	$calculate_percentage=($percentage / 100)* $amount;
	return $amount-$calculate_percentage;
}
$seller_price = getPercentNumber($order_price, $commission_percentage); 
date_default_timezone_set("UTC");
$revenue_date= date("F d, Y");
$end_date= date("F d, Y h:i:s", strtotime(" + $days_before_withdraw days"));

$get_order_conversations="SELECT * from  order_conversations where order_id='$order_id'";
$run_order_conversations=mysqli_query($con,$get_order_conversations);
while($row_order_conversations=mysqli_fetch_array($run_order_conversations)){
$c_id=$row_order_conversations['c_id'];	
$sender_id=$row_order_conversations['sender_id'];	
$message=$row_order_conversations['message'];	
$file=$row_order_conversations['file'];	
$date=$row_order_conversations['date'];	
$status=$row_order_conversations['status'];	

//SELECT ORDER SELLER DETAILS
$select_seller="SELECT * from sellers where seller_id='$sender_id'";
$run_seller=mysqli_query($con,$select_seller);
$row_seller=mysqli_fetch_array($run_seller);
$seller_user_name=$row_seller['seller_user_name'];
$seller_image=$row_seller['seller_image'];
//defination of the receiver and sender.. 
if ($seller_id==$sender_id) {
	$receiver_name="Buyer";
}else{
	$receiver_name="Seller";
}
if($seller_id==$login_seller_id) {
$receiver_id=$buyer_id;	
}else{
$receiver_id=$seller_id;	
}
date_default_timezone_set("africa/nairobi");
$last_update_date=date("h:i: M d, Y");
 ?>

 <?php 
if ($status=="message") {  ?>

<div class="
<?php 
if($sender_id==$login_seller_id){
echo("message-div-hover");
}else{
echo("message-div");
} ?>


">
	<img src="user_images/<?php echo($seller_image); ?>" class="message-image">
	<h5>
		<a href="#" class="seller-buyer-name"><?php echo($seller_user_name); ?></a>
	</h5>
	<p class="message-desc"><?php echo($message); ?> 
	<?php if(!empty($file)){ ?>
<a href="order_files/<?php echo($file); ?>" class="d-block mt-2 ml-1" download><i class="fa fa-download"></i><?php echo($file); ?></a>

<?php }else{ ?>

<?php } ?>
</p>
<p class="text-right text-muted mb-0"><?php echo($date); ?></p>

</div><!--message-div ends-->
<?php }elseif($status=="delivered"){?>
<h3 class="text-center mt-3 mb-3">Order Delivered</h3>
 
<div class="
<?php 
if($sender_id==$login_seller_id){
echo("message-div-hover");
}else{
echo("message-div");
} ?>


">
	<img src="user_images/<?php echo($seller_image); ?>" class="message-image">
	<h5>
		<a href="#" class="seller-buyer-name"><?php echo($seller_user_name); ?></a>
	</h5>
	<p class="message-desc"><?php echo($message); ?> 
	<?php if(!empty($file)){ ?>
<a href="order_files/<?php echo($file); ?>" class="d-block mt-2 ml-1" download><i class="fa fa-download"></i><?php echo($file); ?></a>

<?php }else{ ?>

<?php } ?>
</p>
<p class="text-right text-muted mb-0"><?php echo($date); ?></p>
</div><!--message-div ends-->


<?php
// buyer option to request for the revision and accept order
if ($order_status=="delivered") {
	
  ?>

<?php   
if ($buyer_id==$login_seller_id) { ?>

<center class="pb-4 mt-4">
<form method="post">
<button name="complete" type="submit" class="btn btn-success">
Accept Order!
</button>
&nbsp; &nbsp; &nbsp;
<button type="button" data-toggle="modal" data-target="#revision-request-modal" class="btn btn-success">
Request A Revision</button>
</form>	
<?php 
if (isset($_POST['complete'])) {
$recent_delivery_date=date("F d, Y");
$update_recent_delivery="UPDATE sellers set seller_recent_delivery='$recent_delivery_date' where seller_id='$seller_id'";
$run_update_recent_delivery=mysqli_query($con,$update_recent_delivery);
$update_order="UPDATE orders set order_status='completed', order_active='no' where order_id='$order_id'";
$run_order=mysqli_query($con,$update_order);
$update_messages_status="UPDATE order_conversations set status='message' where order_id='$order_id' AND status='delivered'";
$run_message=mysqli_query($con,$update_messages_status);
$insert_notification ="INSERT INTO notifications (receiver_id,sender_id,order_id,reason,date,status)values('$seller_id','$buyer_id','$order_id','order_completed','$last_update_date','unread')";
$run_notifications=mysqli_query($con,$insert_notification);

$update_pending_clearance="UPDATE seller_accounts set pending_cleanrance = pending_cleanrance + $seller_price,month_earnings=month_earnings + $seller_price where seller_id='$seller_id'";
$run_pending_clearance=mysqli_query($con,$update_pending_clearance);

$insert_revenue="INSERT INTO revenues (seller_id,order_id,amount,date,end_date,status) values('$sender_id','$order_id','$seller_price','$revenue_date','$end_date','pending')";
$run_revenue=mysqli_query($con,$insert_revenue);
echo "<script>window.open('order_details.php?order_id=$order_id','_self');</script>";
}
 ?>
</center><!--mb-4 mt-4 -->
 <?php }?>


 <?php } ?> 

<?php }elseif($status=="revision"){ ?>

<h3 class="text-center mt-3 mb-3">Revision Requested By <?php echo($seller_user_name); ?></h3>
 
<div class="
<?php 
if($sender_id==$login_seller_id){
echo("message-div-hover");
}else{
echo("message-div");
} ?>


">
	<img src="user_images/<?php echo($seller_image); ?>" class="message-image">
	<h5>
		<a href="#" class="seller-buyer-name"><?php echo($seller_user_name); ?></a>
	</h5>
	<p class="message-desc"><?php echo($message); ?> 
	<?php if(!empty($file)){ ?>
<a href="order_files/<?php echo($file); ?>" class="d-block mt-2 ml-1" download><i class="fa fa-download"></i><?php echo($file); ?></a>

<?php }else{ ?>

<?php } ?>
</p>
<p class="text-right text-muted mb-0"><?php echo($date); ?></p>
</div><!--message-div ends-->

<?php }elseif($status=="cancellation_request"){ ?>
<h3 class="text-center mt-3 mb-3">Order Cancellation Request By <?php echo($seller_user_name); ?></h3>
 
<div class="
<?php 
if($sender_id==$login_seller_id){
echo("message-div-hover");
}else{
echo("message-div");
} ?>
">
	<img src="user_images/<?php echo($seller_image); ?>" class="message-image">
	<h5>
		<a href="#" class="seller-buyer-name"><?php echo($seller_user_name); ?></a>
	</h5>
	<p class="message-desc"><?php echo($message); ?> 
	<?php if(!empty($file)){ ?>
<a href="order_files/<?php echo($file); ?>" class="d-block mt-2 ml-1" download><i class="fa fa-download"></i><?php echo($file); ?></a>

<?php }else{ ?>

<?php } ?>
</p>
<?php if ($sender_id==$login_seller_id) { ?>

<?php }else{ ?>
<form class="mb-2" method="post">
<button name="accept_request" class="btn btn-success btn-sm">
Accept Request	
</button>

<button name="decline_request" class="btn btn-success btn-sm">
Decline Request	
</button>	
</form>
<?php 
if (isset($_POST['accept_request'])) { 
$update_messages_status="UPDATE order_conversations set status='accept_cancellation_request' where order_id='$order_id' AND status='cancellation_request'";
$run_message=mysqli_query($con,$update_messages_status);

$update_order="UPDATE orders set order_status='cancelled', order_active='no' where order_id='$order_id'";
$run_order=mysqli_query($con,$update_order);

$insert_notification ="INSERT INTO notifications (receiver_id,sender_id,order_id,reason,date,status)values('$receiver_id','$login_seller_id','$order_id','accept_cancellation_request','$last_update_date','unread')";
$run_notifications=mysqli_query($con,$insert_notification);
$update_my_buyer="UPDATE my_buyers SET completed_orders=completed_orders -1,amount_spent=amount_spent-$order_price where buyer_id='$buyer_id' AND seller_id='$seller_id'";
$run_update_my_buyer=mysqli_query($con,$update_my_buyer);

$update_my_seller="UPDATE my_sellers set completed_orders=completed_orders -1,amount_spent=amount_spent-$order_price
where seller_id='$seller_id' AND buyer_id='$buyer_id'";
$run_update_my_seller=mysqli_query($con,$update_my_seller);
$purchase_date=date("F d, Y");
$insert_purchase="INSERT INTO purchases (seller_id,order_id,amount,date,method)values('$buyer_id','$order_id','$order_price','$purchase_date','order_cancellation')";
$run_purchase=mysqli_query($con,$insert_purchase);
//update balance amount
$update_balance="UPDATE seller_accounts SET user_purchases=user_purchases - $order_price, current_balance=current_balance+$order_price where seller_id='$buyer_id'";
$run_update_balance=mysqli_query($con,$update_balance);
echo "<script>window.open('order_details.php?order_id=$order_id','_self');</script>";
}

if (isset($_POST['decline_request'])) {
$update_messages_status="UPDATE order_conversations set status='decline_cancellation_request' where order_id='$order_id' AND status='cancellation_request'";
$run_message=mysqli_query($con,$update_messages_status);
$update_order="UPDATE orders set order_status='progress', order_active='no' where order_id='$order_id'";
$run_order=mysqli_query($con,$update_order);

$insert_notification ="INSERT INTO notifications (receiver_id,sender_id,order_id,reason,date,status)values('$receiver_id','$login_seller_id','$order_id','decline_cancellation_request','$last_update_date','unread')";
$run_notifications=mysqli_query($con,$insert_notification);
echo "<script>window.open('order_details.php?order_id=$order_id','_self');</script>";



}

 ?>

<?php } ?>
<p class="text-right text-muted mb-0"><?php echo($date); ?></p>
</div><!--message-div ends-->
<?php }elseif($status=="decline_cancellation_request"){ ?>

<h3 class="text-center mt-3 mb-3">Order Cancellation Request By <?php echo($seller_user_name); ?></h3>
 
<div class="
<?php 
if($sender_id==$login_seller_id){
echo("message-div-hover");
}else{
echo("message-div");
} ?>


">
	<img src="user_images/<?php echo($seller_image); ?>" class="message-image">
	<h5>
		<a href="#" class="seller-buyer-name"><?php echo($seller_user_name); ?></a>
	</h5>
	<p class="message-desc"><?php echo($message); ?> 
	<?php if(!empty($file)){ ?>
<a href="order_files/<?php echo($file); ?>" class="d-block mt-2 ml-1" download><i class="fa fa-download"></i><?php echo($file); ?></a>

<?php }else{ ?>

<?php } ?>
</p>

<p class="text-right text-muted mb-0"><?php echo($date); ?></p>
</div><!--message-div ends-->
<div class="order-status-message">
<i class="fa fa-times fa-3x text-danger"></i>
<h5 class="text-danger">
Cancellation Request Declined By <?php echo $receiver_name ; ?>		
</h5>
</div><!--order-status-message-->
<?php }elseif($status=="accept_cancellation_request"){ ?>
<h3 class="text-center mt-3 mb-3">Order Cancellation Request By <?php echo($seller_user_name); ?></h3>
 
<div class="
<?php 
if($sender_id==$login_seller_id){
echo("message-div-hover");
}else{
echo("message-div");
} ?>


">
	<img src="user_images/<?php echo($seller_image); ?>" class="message-image">
	<h5>
		<a href="#" class="seller-buyer-name"><?php echo($seller_user_name); ?></a>
	</h5>
	<p class="message-desc"><?php echo($message); ?> 
	<?php if(!empty($file)){ ?>
<a href="order_files/<?php echo($file); ?>" class="d-block mt-2 ml-1" download><i class="fa fa-download"></i><?php echo($file); ?></a>

<?php }else{ ?>

<?php } ?>
</p>

<p class="text-right text-muted mb-0"><?php echo($date); ?></p>
</div><!--message-div ends-->
<?php if ($seller_id==$login_seller_id) { ?>
<div class="order-status-message">
<i class="fa fa-times fa-3x text-danger"></i>
<h5 class="text-danger">Order Cancelled By Mutual Agreement!</h5>
<p>Order was Cancelled By Mutual Agreement Between you and Your buyer. The order funds have been returened to Buyer</p>	
</div><!--order-status-message ends-->
<?php }else{ ?>	
<div class="order-status-message">
<i class="fa fa-times fa-3x text-danger"></i>
<h5 class="text-danger">Order Cancelled By Mutual Agreement!</h5>
<p>Order was Cancelled By Mutual Agreement Between you and Your seller. The order funds have been returened to You</p>	
</div><!--order-status-message ends-->
<?php } ?>

<?php }elseif($status=="cancelled_by_customer_support"){ ?>
<?php if ($seller_id==$login_seller_id) { ?>
<div class="order-status-message">
<i class="fa fa-times fa-3x text-danger"></i>
<h5 class="text-danger">Order Cancelled By CUSTOMER SUPPORT!</h5>
<p>Payment of this order was return to buyer shopping<br> balance. For further assistance visit <a href="contact.php">Customer support</a></p>	
</div><!--order-status-message ends-->
<?php }else{ ?>
<div class="order-status-message">
<i class="fa fa-times fa-3x text-danger"></i>
<h5 class="text-danger">Order Cancelled By CUSTOMER SUPPORT!</h5>
<p>Payment of this order was return to YOUR shopping<br> balance.</p>	
</div><!--order-status-message ends--> 
<?php } ?>

<?php } ?>

<?php } ?>