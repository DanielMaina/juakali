<?php 
session_start();
include("includes/db.php");
if (!isset($_SESSION['seller_user_name'])) {
echo "<script>window.open('login.php','_self');</script>";
}
//a copy(dashboard) get id and username/email... 
$login_seller_user_name=$_SESSION['seller_user_name'];
$select_login_seller="SELECT * from sellers where seller_user_name='$login_seller_user_name'";
$run_login_seller=mysqli_query($con,$select_login_seller);
$row_login_seller=mysqli_fetch_array($run_login_seller);
$login_seller_id=$row_login_seller['seller_id'];

//code to get the order details 
$order_id=$_GET['order_id'];
$get_orders="SELECT * from orders where (seller_id='$login_seller_id' OR buyer_id='$login_seller_id') AND order_id='$order_id'";
$run_orders=mysqli_query($con,$get_orders);
$count_orders=mysqli_num_rows($run_orders);
if ($count_orders==0) {
echo "<script>window.open('index.php?not_available','_self');</script>";	
}
$row_orders=mysqli_fetch_array($run_orders);

$order_number=$row_orders['order_number'];
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	<title>JuakaliMall/name/Order:# <?php echo($order_number); ?></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="JuakaliMall">
	<link href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100" rel="stylesheet" >
	<link rel="stylesheet" href="styles/bootstrap.min.css">
	<!--<link rel="stylesheet" href="font-awesome/css/all.min.css">-->
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
	<link rel="stylesheet" href="styles/fontawesome-stars.css">
	<!-- Custome css from the user -->
	<link rel="stylesheet" href="styles/custom.css">
	<link rel="stylesheet" href="styles/style.css">
	<link rel="stylesheet" href="styles/user_nav_style.css">
	<link rel="stylesheet" href="styles/owl.carousel.css">
	<link rel="stylesheet" href="styles/owl.theme.default.css">
	<!--<script src="js/jquery.slim.min.js"></script>-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="js/jquery.barrating.min.js"></script>
	<script src="js/jquery.sticky.js"></script>

</head>
<body>
<div class="preloader d-flex justify-content-center align-items-center">
    <img src="images/loadjuakali.gif" alt="the preloader"><br>
    <p>Loading...Please wait</p>
  </div>
<?php include("includes/user_header.php");?>
<?php 
$order_id=$_GET['order_id'];
$get_orders="SELECT * from orders where(seller_id='$login_seller_id' OR buyer_id='$login_seller_id') AND order_id='$order_id'";
$run_orders=mysqli_query($con,$get_orders);
$row_orders=mysqli_fetch_array($run_orders);
$order_id=$row_orders['order_id'];
$order_number=$row_orders['order_number'];
$proposal_id=$row_orders['proposal_id'];
$seller_id=$row_orders['seller_id'];
$buyer_id=$row_orders['buyer_id'];
$order_price=$row_orders['order_price'];
$order_qty=$row_orders['order_qty'];
$order_date=$row_orders['order_date'];
$order_duration=$row_orders['order_duration'];
$order_time=$row_orders['order_time'];
$order_fee=$row_orders['order_fee'];
$order_desc=$row_orders['order_description'];
$order_status=$row_orders['order_status'];
$total=$order_price + $order_fee;

//GET THE ORDER product DETAILS
$get_proposal="SELECT * from proposals where proposal_id='$proposal_id'";
$run_proposal=mysqli_query($con,$get_proposal);
$row_proposal = mysqli_fetch_array($run_proposal);
$proposal_title=$row_proposal['proposal_title'];
$proposal_img1=$row_proposal['proposal_img1'];
$proposal_url=$row_proposal['proposal_url'];
$buyer_instruction=$row_proposal['buyer_instruction'];
$proposal_title=$row_proposal['proposal_title'];

//for the payment processing
$get_payment_setting="SELECT * from payment_settings";
$run_payment_setting=mysqli_query($con,$get_payment_setting);
$row_payament_setting=mysqli_fetch_array($run_payment_setting);
$commission_percentage=$row_payament_setting['commission_percentage'];

function getPercentValue($amount,$percentage){
	$calculate_percentage=($percentage /100)* $amount;
	return $amount-$calculate_percentage;
}
$seller_price = getPercentValue($order_price, $commission_percentage); 

//SELECT ORDER SELLER DETAILS
$select_seller="SELECT * from sellers where seller_id='$seller_id'";
$run_seller=mysqli_query($con,$select_seller);
$row_seller=mysqli_fetch_array($run_seller);
$seller_user_name=$row_seller['seller_user_name'];
$order_seller_rating=$row_seller['seller_rating'];

//prevent individual fromating past 100
if ($order_seller_rating > "100") {
$update_seller_rating="UPDATE sellers set seller_rating ='100' where seller_id='$seller_id'";
$run_update_seller_rating=mysqli_query($con,$update_seller_rating);	
}
//SELLECT ORDER BUYER DETAILS
$select_buyer="SELECT * from sellers where seller_id='$buyer_id'";
$run_buyer=mysqli_query($con,$select_buyer); 
$row_buyer=mysqli_fetch_array($run_buyer);
$buyer_user_name=$row_buyer['seller_user_name'];

 ?>

<?php 
if($order_status=="pending" or $order_status=="progress" or $order_status=="delivered" or $order_status=="revision requested" or $order_status=="cancellation requested") {

 ?>
<div id="order-status-bar">
<div class="row">
<div class="col-md-8 offset-md-2">
<h6 class="float-left mt-2">
<span class="border border-primary rounded p-1">Order: #
 <?php echo($order_number); ?>:
  <?php if ($order_status=="progess") {echo "In";} ?>
   <?php echo ucwords($order_status); ?></span>
</h6>
<h2 class="float-right text-muted">
<?php if($order_status=="pending") {echo "In";} ?>
<?php echo ucwords($order_status); ?>
	
</h2>
</div>
</div>
</div><!--order-status bar ends--->
<?php }elseif($order_status == "cancelled"){ ?>
<div id="order-status-bar">
	<div class="row">
		<div class="col-md-8 offset-md-2">
			<h6 class="float-left mt-2">
				<i class="fa fa-lg fa-times-circle"></i>Order Cancelled
				<i class="fa fa-lg fa-check-circle"></i>Payment Returned to buyer
			</h6>
			<h2 class="float-right text-muted"> Order Cancelled</h2>
		</div>
	</div>
</div><!--order-status bar ends--->
<?php }elseif($order_status == "completed"){ ?>
<div id="order-status-bar" class="bg-success text-white">
	<div class="row">
		<div class="col-md-10 offset-md-1">
<?php if ($seller_id==$login_seller_id) {?>
<h6 class="float-left mt-2">
<i class="fa fa-lg fa-check-circle"></i>WORK DELIVERED
order <?php echo($order_number); ?>
</h6>
<h2 class="float-right">
<i class="fa fa-check-circle"></i>ORDER COMPLETED! YOU EARNED Ksh <?php echo($seller_price); ?>
</h2>
<?php } elseif($buyer_id==$login_seller_id){ ?>
<h6 class="float-left mt-2">
<i class="fa fa-lg fa-check-circle"></i>DELIVERY SUBMITTED
</h6>
<h2 class="float-right">
<i class="fa fa-check-circle"></i>ORDER COMPLETED!
</h2>
<?php }?>
		</div><!--col-md-10 offset-md-1-->
	</div>
</div><!--order-status bar ends--->
<?php } ?>
<div class="cointainer order-page mt-2">
<div class="row">
<div class="col-md-12">
<div class="row">
<div class="col-md-10 offset-md-1">
<ul class="nav nav-tabs mb-3 mt-3">
<li class="nav-item">
<a href="#order-activity" data-toggle="tab" class="nav-link active">Order Activity</a>
</li>
<?php 
if ($order_status=="pending" or $order_status=="progress" or $order_status=="delivered" or $order_status=="revision requested") { ?>
<li class="nav-item">
<a href="#resolution-center" data-toggle="tab" class="nav-link">Resolution Center</a>
</li>
<?php } ?>
</ul>
</div>
</div>
<div class="tab-content mt-2 mb-4">
<div id="order-activity" class="tab-pane fade show active">
<div class="row">
<div class="col-md-10 offset-md-1">
<div class="card">
<div class="card-body">
<div class="row">
<div class="col-md-2">
<img src="proposals/proposal_files/<?php echo($proposal_img1); ?>" class="img-fluid d-lg=block d-md-block d-none">
</div>
<div class="col-md-10">
<?php if ($seller_id==$login_seller_id) { ?>
<h1 class="text-success float-right d-lg-block d-md-block d-none">Ksh <?php echo($order_price); ?></h1>
<h4>Order: # <?php echo($order_number); ?> <small> <a href="proposals/<?php echo($proposal_url); ?>" target="_blank">View Product</a></small></h4> <p class="text-muted">
	<span class="font-weight-bold">Buyer :</span>
	<a href="user/<?php echo($buyer_user_name); ?>" target="_blank" class="seller-buyer-name mr-1"><?php echo($buyer_user_name); ?></a>
	| <span class="font-weight-bold ml-1">Status: </span><?php echo($order_status); ?>
	| <span class="font-weight-bold ml-1">Date :</span><?php echo($order_date); ?>
</p>
<?php } elseif($buyer_id==$login_seller_id){ ?>
<h1 class="text-success float-right d-lg-block d-md-block d-none">Ksh <?php echo($total); ?></h1>
<h4><?php echo($proposal_title); ?>n</h4><p class="text-muted">
	<span class="font-weight-bold">Seller :</span>
	<a href="user/<?php echo($seller_user_name); ?>" target="_blank" class="seller-buyer-name mr-1"><?php echo($seller_user_name); ?></a>
	| <span class="font-weight-bold ml-1">Order: </span>#<?php echo($order_number); ?>
	| <span class="font-weight-bold ml-1">Date :</span><?php echo($order_date); ?>
</p>
<?php }?>
</div>
</div>
<div class="row d-lg-flex d-md-flex d-none">
<div class="col-md-12">
<table class="table mt-3">
<thead>
<tr>
<th>Item</th>
<th>Quantity</th>
<th>Duration</th>
<th>Amount</th>
</tr>
</thead>
<tbody>
<tr>
<td class="font-weight-bold" width="600">
<?php echo($proposal_title); ?>
</td>
<td><?php echo($order_qty); ?></td>
<td><?php echo($order_duration); ?></td>
<td>
Ksh<?php if ($seller_id==$login_seller_id) { ?> 
<?php echo($order_price); ?>
<?php } elseif($buyer_id==$login_seller_id){ ?>
<?php echo($total); ?>
<?php }?>
</td>
</tr>
<tr>
<?php if ($buyer_id==$login_seller_id) {?>
<?php if (!empty($order_fee)) { ?>
<tr>
<td> Processing Fee</td>			
<td></td>			
<td></td>			
<td>ksh <?php echo($order_fee); ?></td>			
</tr>
<?php } ?>
	<?php } ?>
<td colspan="4">
<span class="float-right mr-5">
<strong>Total</strong>
Ksh<?php if ($seller_id==$login_seller_id) { ?> 
<?php echo($order_price); ?>
<?php } elseif($buyer_id==$login_seller_id){ ?>
<?php echo($total); ?>
<?php }?>
</span>
</td>
</tr>
</tbody>
</table><!--order summary table-->
<?php if (!empty($order_desc)) { ?>
<table class="table">
<thead>
<tr>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
 <td width="600">
<?php echo($order_desc); ?>
 </td>
</tr>
</tbody>
</table><!--description-->
<?php } ?>
</div>
</div><!--row d-lg-flex d-md-flex d-none-->
</div>
</div><!--card ends-->
<?php 
#code to stop timer after delivery done!
if ($order_status=="progress" or $order_status=="revision requested") { ?>
<?php  if ($seller_id==$login_seller_id) { ?>
<h2 class="text-center mt-2" id="countdown-heading">
You need to Deliver your order Before This Duration!
</h2>     
<?php }elseif ($buyer_id==$login_seller_id){ ?>
	<h2 class="text-center mt-2" id="countdown-heading">
	Seller should Deliver your order Before This Duration!
</h2>
<?php }?>
<div id="countdown-timer">
<div class="row">
<div class="col-3 countdown-box">
<p class="countdown-number" id="days"></p>
<p class="coundown-title">Day</p>
</div><!--ol-lg-3 col-md-6 col-sm-6 countdown-box days ends-->
<div class="col-3 countdown-box">
<p class="countdown-number" id="hours"></p>
<p class="coundown-title">Hrs.</p>
</div><!--ol-lg-3 col-md-6 col-sm-6 countdown-box hours ends-->
<div class="col-3 countdown-box">
<p class="countdown-number" id="minutes"></p>
<p class="coundown-title">Mint.</p>
</div><!--ol-lg-3 col-md-6 col-sm-6 countdown-box minutes ends-->
<div class="col-3 countdown-box">
<p class="countdown-number" id="seconds"></p>
<p class="coundown-title">Sec.</p>
</div><!--ol-lg-3 col-md-6 col-sm-6 countdown-box seconds ends-->
</div>
</div><!--countdown-timer ends-->
<?php } ?>
<?php if ($buyer_id==$login_seller_id) { ?>
<div class="card mb-3 mt-3">
<div class="card-header">
<h5>SELLER WILL ONLY GET STARTED AFTER YOU SUBMIT THIS INFORMATION!</h5>	
</div><!--card-header ends-->
<div class="card-body">
<h6>
<span class="text-info"><?php echo($seller_user_name); ?></span>
 Requests following information befor you get started!	
</h6>
<p><?php echo($buyer_instruction); ?></p>	
</div>
</div><!--card mb-3 mt-3-->
<?php } ?>

<div id="order-conversations" class="bg-white mt-3">
<?php include("order_conversations.php");?>
</div><!--order-conversations-->
<?php if ($seller_id==$login_seller_id) { ?>
<?php if ($order_status == "progress" or $order_status=="revision requested") { ?>	
<center>
<button class="btn btn-success btn-lg mt-4 mb-2" data-toggle="modal" data-target="#deliver-order-modal">Send Order Approve</button>
</center>
<?php } ?>
<?php if ($order_status == "delivered") { ?>	
<center>
<button class="btn btn-success btn-lg mt-4 mb-2" data-toggle="modal" data-target="#deliver-order-modal">Send Approve Again</button>
</center>
<?php } ?>
<?php } ?>
<div class="proposal-reviews mt-3">
<?php if ($order_status=="completed") { ?>
<?php 
$get_buyer_review="SELECT * from buyer_reviews where order_id='$order_id'";
$run_buyer_reviews=mysqli_query($con,$get_buyer_review);
$count_buyer_reviews=mysqli_num_rows($run_buyer_reviews);

$row_buyer_reviews=mysqli_fetch_array($run_buyer_reviews);
$buyer_rating=$row_buyer_reviews['buyer_rating'];
$buyer_review=$row_buyer_reviews['buyer_review'];
$review_buyer_id=$row_buyer_reviews['review_buyer_id'];
$review_date=$row_buyer_reviews['review_date'];

$select_review_buyer="SELECT * from sellers where seller_id='$review_buyer_id'";
$run_review_buyer=mysqli_query($con,$select_review_buyer);

$row_review_buyer=mysqli_fetch_array($run_review_buyer);
$buyer_user_name=$row_review_buyer['seller_user_name'];//note
$buyer_image=$row_review_buyer['seller_image'];//note

$get_seller_reviews="SELECT * from seller_reviews where order_id='$order_id'";
$run_seller_reviews=mysqli_query($con,$get_seller_reviews);
$count_seller_reviews=mysqli_num_rows($run_seller_reviews);

//code to populate data of the reviews
$row_seller_reviews=mysqli_fetch_array($run_seller_reviews);
$seller_rating=$row_seller_reviews['seller_rating'];
$seller_review=$row_seller_reviews['seller_review'];
$review_seller_id=$row_seller_reviews['review_seller_id'];

$select_review_seller="SELECT * FROM sellers where seller_id='$review_seller_id'";
$run_review_seller=mysqli_query($con,$select_review_seller);

$row_review_seller=mysqli_fetch_array($run_review_seller);
$seller_image=$row_review_seller['seller_image'];



$count_all_reviews="$count_buyer_reviews$count_seller_reviews";
if ($count_all_reviews=="00") {
	
}else{
 ?>	
<h2 class="text-center">Order Reviews</h2>
<div class="card rounded-0 mt-3">
<div class="card-body">
<ul class="reviews-list">
<?php if (!$count_buyer_reviews == 0) {
 ?>	
<li class="star-rating-row">
<span class="user-picture">
<img src="user_images/<?php echo($buyer_image); ?>" width="60" height="60">
</span>
<h4>
<a href="#" class="mr-1"><?php echo($buyer_user_name); ?></a><br>
<?php 
for($buyer_i=0; $buyer_i < $buyer_rating; $buyer_i++) {
echo "<img src='images/user_rate_full.png' >";
}
for($buyer_i=$buyer_rating; $buyer_i<5; $buyer_i++) {
echo "<img src='images/user_rate_blank.png' >";
}

 ?>
</h4>
<div class="msg-body">
<?php echo($buyer_review); ?>
</div>
<span class="rating-date"> <?php echo($review_date); ?></span>
</li>
<hr>
<?php } ?>
<?php 
if (!$count_seller_reviews==0) {
	

 ?>
<li class="rating-seller">
<span class="user-picture">
<img src="user_images/<?php echo($seller_image); ?>" width="40" height="40">
</span>
<h4>
<span class="mr-1">Seller's Feedback</span><br>
<?php 
for($seller_i=0; $seller_i<$seller_rating;$seller_i++){
echo "<img src='images/user_rate_full.png' >";
}
for ($seller_i=$seller_rating; $seller_i < 5; $seller_i++) { 
	echo "<img src='images/user_rate_blank.png' >";
}
 ?>
</h4>
<div class="msg-body"><?php echo($seller_review); ?>!</div>
</li>
<?php } ?>
</ul>
</div>
</div><!--card rounded-0 mt-3-->
<?php } ?>
<?php if ($seller_id == $login_seller_id) {  ?>
<?php if($count_seller_reviews==0){ ?>	
<div class="order-review-box mb-3 p-3">
<h3 class="text-center text-white">Please Review To Buyer</h3>
<div class="row">
<div class="col-md-8 offset-md-2">
<form action="" method="post" align="center">
<div class="form-group">
<label class="h6 text-white">Review Rating</label>
<select name="rating" class="rating-select">
<option value="1">1</option>
<option value="2">3</option>
<option value="3">4</option>
<option value="4">4</option>
<option value="5">5</option>
</select>
<script>
$(document).ready(function(){
$(".rating-select").barrating({
theme: 'fontawesome-stars'
});
});
</script>
<textarea name="review" class="form-control" placeholder="Write your experience with the buyer" rows="5"></textarea>
</div>
<input type="submit" value="Give Your Reviews" name="seller_review_submit" class="btn btn-success mb-3">
</form>
<?php 	
//php code to insert the reviews...
if (isset($_POST['seller_review_submit'])) {
$rating=$_POST['rating'];
$review=$_POST['review'];
$insert_review ="INSERT INTO seller_reviews (order_id,review_seller_id,seller_rating,seller_review)values('$order_id','$seller_id','$rating','$review')";
$run_review=mysqli_query($con,$insert_review);

//post some message to the notification(TO BUYER)
$last_update_date=date("h:m: M d Y");
$insert_notification ="INSERT INTO notifications (buyer_id,sender_id,order_id,seller_order_review,date,status)values('$receiver_id','$login_seller_id','$order_id','cancellation request','$last_update_date','unread')";
$run_notifications=mysqli_query($con,$insert_notification);
echo "<script>alert('Your Review Has Been Submited To Buyer');</script>";
echo "<script>window.open('order_details.php?order_id=$order_id','_self');</script>";
}

 ?>
</div>
</div>
</div><!--order-review-box-mb-3 p-3 ends-->
<?php }else{ ?>
<div class="order-review-box mb-3 p-3">
<h3 class="text-center text-white">Edit Review To Buyer</h3>
<div class="row">
<div class="col-md-8 offset-md-2">
<form action="" method="post" align="center">
<div class="form-group">
<label class="h6 text-white">Review Rating</label>
<select name="rating" class="rating-select-update">
<option value="1">1</option>
<option value="2">3</option>
<option value="3">4</option>
<option value="4">4</option>
<option value="5">5</option>
</select>
<script>
$(document).ready(function(){
$(".rating-select-update").barrating({
theme: 'fontawesome-stars',
initialRating: '<?php echo($seller_rating); ?>'
});
});
</script>
<textarea name="review" class="form-control" placeholder="Write your experience with the buyer" rows="5"><?php echo($seller_review); ?></textarea>
</div>
<input type="submit" value="Update Your Reviews" name="seller_review_update" class="btn btn-success mb-3">
</form>
<?php
if (isset($_POST['seller_review_update'])) {

	$rating=$_POST['rating'];
	$review=$_POST['review'];
$update_review="UPDATE seller_reviews set seller_review='$review', seller_rating='$rating' where order_id='$order_id' ";
$run_review=mysqli_query($con,$update_review);	
echo "<script>alert('Your Review Has Been Updated');</script>";
echo "<script>window.open('order_details.php?order_id=$order_id','_self');</script>";
}
 ?>
</div>
</div>
</div><!--ends order-review-box mb-3 p-3 second ends-->
<?php } ?>

<?php }elseif ($buyer_id==$login_seller_id) { ?>
<?php if ($count_buyer_reviews==0) {  ?>
<div class="order-review-box mb-3 p-3">
<h3 class="text-center text-white">Please Review To Expert/Seller</h3>
<div class="row">
<div class="col-md-8 offset-md-2">
<form action="" method="post" align="center">
<div class="form-group">
<label class="h6 text-white">Review Rating</label>
<select name="rating" class="rating-select">
<option value="1">1</option>
<option value="2">3</option>
<option value="3">4</option>
<option value="4">4</option>
<option value="5">5</option>
</select>
<script>
$(document).ready(function(){
$(".rating-select").barrating({
theme: 'fontawesome-stars'
});
});
</script>
<textarea name="review" class="form-control" placeholder="Write your experience with the Expert/Seller" rows="5"></textarea>
</div>
<input type="submit" value="Give Your Reviews" name="buyer_review_submit" class="btn btn-success mb-3">
</form>
<?php 
if(isset($_POST['buyer_review_submit'])) {
$rating = $_POST['rating'];	
$review = $_POST['review'];	

$date=date("M d Y");

$insert_review="INSERT INTO buyer_reviews(proposal_id,order_id,review_buyer_id,buyer_rating,buyer_review,review_seller_id,review_date)values('$proposal_id','$order_id','$buyer_id','$rating','$review','$seller_id','$date')";
$run_review=mysqli_query($con,$insert_review);

//post some message to the notification(TO SELLER)
$last_update_date=date("h:m: M d Y");
$insert_notification ="INSERT INTO notifications (receiver_id,sender_id,order_id,reason,date,status)values('$seller_id','$login_seller_id','$order_id','buyer_order_review','$last_update_date','unread')";
$run_notifications=mysqli_query($con,$insert_notification);

$ratings=array();

$sel_proposal_reviews="SELECT * from  buyer_reviews where proposal_id='$proposal_id'";
$run_proposal_reviews=mysqli_query($con,$sel_proposal_reviews);
while ($row_proposal_reviews=mysqli_fetch_array($run_proposal_reviews)) {
$proposal_buyer_rating=$row_proposal_reviews['buyer_rating'];
array_push($ratings,$proposal_buyer_rating);

}
array_push($ratings,$rating);
$total=array_sum($ratings);
$avg = $total/count($ratings);
$update_proposal_rating=substr($avg,0,1);
if($rating == "5") {
if($order_seller_rating=="100"){
	
}else{
$update_seller_rating="UPDATE sellers set seller_rating=seller_rating+7 where seller_id='$seller_id'";
$run_seller_rating=mysqli_query($con,$update_seller_rating);
}
}elseif($rating=="4"){
if($order_seller_rating=="100"){
	
}else{
$update_seller_rating="UPDATE sellers set seller_rating=seller_rating+2 where seller_id='$seller_id'";
$run_seller_rating=mysqli_query($con,$update_seller_rating);
}
}elseif($rating=="3"){
$update_seller_rating="UPDATE sellers set seller_rating=seller_rating-3 where seller_id='$seller_id'";
$run_seller_rating=mysqli_query($con,$update_seller_rating);
}elseif($rating=="2"){
$update_seller_rating="UPDATE sellers set seller_rating=seller_rating-5 where seller_id='$seller_id'";
$run_seller_rating=mysqli_query($con,$update_seller_rating);
}elseif($rating=="1"){
$update_seller_rating="UPDATE sellers set seller_rating=seller_rating-7 where seller_id='$seller_id'";
$run_seller_rating=mysqli_query($con,$update_seller_rating);
}
$update_proposal_rating="UPDATE proposals set proposal_rating='$update_proposal_rating' where proposal_id='$proposal_id'";
$run_update=mysqli_query($con,$update_proposal_rating);
if ($run_update) {
echo "<script>alert('Your Review Has Been Submited To Seller');</script>";
echo "<script>window.open('order_details.php?order_id=$order_id','_self');</script>";
}
}
 ?>
</div>
</div>
</div><!--order-review-box-mb-3 p-3 ends-->
<?php }else{ ?>
<div class="order-review-box mb-3 p-3">
<h3 class="text-center text-white">Edit Review To Seller</h3>
<div class="row">
<div class="col-md-8 offset-md-2">
<form action="" method="post" align="center">
<div class="form-group">
<label class="h6 text-white">Review Rating</label>
<select name="rating" class="rating-select-update">
<option value="1">1</option>
<option value="2">3</option>
<option value="3">4</option>
<option value="4">4</option>
<option value="5">5</option>
</select>
<script>
$(document).ready(function(){
$(".rating-select-update").barrating({
theme: 'fontawesome-stars',
initialRating: '<?php echo($buyer_rating); ?>'
});
});
</script>
<textarea name="review" class="form-control" placeholder="Write your experience with the Expert/Seller" rows="5"><?php echo($buyer_review); ?></textarea>
</div>
<input type="submit" value="Update Your Reviews" name="buyer_review_update" class="btn btn-success mb-3">
</form>
<?php 
if (isset($_POST['buyer_review_update'])) {
$rating=$_POST['rating'];	
$review=$_POST['review'];

$update_review="UPDATE buyer_reviews set buyer_review='$review',buyer_rating='$rating' where order_id='$order_id'";
$run_review=mysqli_query($con,$update_review);	


$ratings=array();

$sel_proposal_reviews="SELECT * from  buyer_reviews where proposal_id='$proposal_id'";
$run_proposal_reviews=mysqli_query($con,$sel_proposal_reviews);
while ($row_proposal_reviews=mysqli_fetch_array($run_proposal_reviews)) {
$proposal_buyer_rating=$row_proposal_reviews['buyer_rating'];
array_push($ratings,$proposal_buyer_rating);

}
array_push($ratings,$rating);
$total=array_sum($ratings);
$avg = $total/count($ratings);
$update_proposal_rating=substr($avg,0,1);
if($rating == "5") {
if($order_seller_rating=="100"){
	
}else{
$update_seller_rating="UPDATE sellers set seller_rating=seller_rating+7 where seller_id='$seller_id'";
$run_seller_rating=mysqli_query($con,$update_seller_rating);
}
}elseif($rating=="4"){
if($order_seller_rating=="100"){
	
}else{
$update_seller_rating="UPDATE sellers set seller_rating=seller_rating+2 where seller_id='$seller_id'";
$run_seller_rating=mysqli_query($con,$update_seller_rating);
}
}elseif($rating=="3"){
$update_seller_rating="UPDATE sellers set seller_rating=seller_rating-3 where seller_id='$seller_id'";
$run_seller_rating=mysqli_query($con,$update_seller_rating);
}elseif($rating=="2"){
$update_seller_rating="UPDATE sellers set seller_rating=seller_rating-5 where seller_id='$seller_id'";
$run_seller_rating=mysqli_query($con,$update_seller_rating);
}elseif($rating=="1"){
$update_seller_rating="UPDATE sellers set seller_rating=seller_rating-7 where seller_id='$seller_id'";
$run_seller_rating=mysqli_query($con,$update_seller_rating);
}
$update_proposal_rating="UPDATE proposals set proposal_rating='$update_proposal_rating' where proposal_id='$proposal_id'";
$run_update=mysqli_query($con,$update_proposal_rating);
if ($run_update) {
echo "<script>alert('Your Review Has Been UPDATE!');</script>";
echo "<script>window.open('order_details.php?order_id=$order_id','_self');</script>";
}

}
 ?>
</div>
</div>
</div><!--ends order-review-box mb-3 p-3 second ends-->
<?php } ?>
<?php }?>
<?php } ?>
</div><!--proposal-reviews mt-3-->
<?php if ($order_status=="pending" or $order_status=="progress" or $order_status=="pending" or $order_status=="delivered" or $order_status=="revision requested") { ?>
<div class="insert-message-box">
<?php if ($buyer_id==$login_seller_id AND $order_status=="pending") { ?>
<div class="flloat-left">
<span class="font-weight-bold text-danger">
	RESPONSE TO START ORDER!
</span>	
</div>	

<?php } ?>	
<div class="float-right">
<?php if ($seller_id==$login_seller_id){

//SELLECT ORDER BUYER DETAILS
$select_buyer="SELECT * from sellers where seller_id='$buyer_id'";
$run_buyer=mysqli_query($con,$select_buyer); 
$row_buyer=mysqli_fetch_array($run_buyer);
$buyer_user_name=$row_buyer['seller_user_name'];
$buyer_status=$row_buyer['seller_status'];

}elseif ($buyer_id==$login_seller_id) {
//SELECT ORDER SELLER DETAILS
$select_seller="SELECT * from sellers where seller_id='$seller_id'";
$run_seller=mysqli_query($con,$select_seller);
$row_seller=mysqli_fetch_array($run_seller);
$seller_user_name=$row_seller['seller_user_name'];	
$seller_status=$row_seller['seller_status'];	
} ?>	
<p class="text-muted mt-1">
<?php if ($seller_id==$login_seller_id) { ?>	
<?php echo($buyer_user_name); ?> <span class="text-success"><?php echo($buyer_status); ?></span> | Local Time 
<?php }elseif ($buyer_id==$login_seller_id) { ?>
<?php echo($seller_user_name); ?> <span class="text-success"><?php echo($seller_status); ?></span> | Local Time
<?php } ?>
<i class="fas fa-sun"></i>
<?php
date_default_timezone_set("africa/nairobi");
echo date("h:i A");
?>
</p>
</div>
<form   id="insert-message-form" class="clearfix">
<textarea  id="message" class=" form-control mb-3" rows="5" placeholder="Type Your Message Here..!"></textarea>
<label  class="float-left h6 mt-2 mr-2">Attach File (Optional)</label>
<label class="float-left">
<input type="file" id="file" class="form-control-file float-left">
</label>
<input type="submit" value="Send" class="btn btn-success float-right">
</form>
</div>
<!--below div will be used to display messges after upload-->
<div id="upload_file_div"></div>

<div id="message_data_div"></div>
<?php } ?>
</div><!--col-md-10 offset-md-1 ends-->
</div>
</div><!--order-activity tab-pane faade show active ends-->
<div id="resolution-center" class="tab-pane fade">
<div class="row">
<div class="col-md-10 offset-md-1">
<div class="card">
<div class="card-body">
<div class="row">
<div class="col-md-8 offset-md-2">
<h3 class="text-center mb-3">Order Cancellation</h3>
<form method="post">
<div class="form-group">
	<label class="font-weight-bold">Cancellation Request Message</label>
	<textarea name="cancellation_message" placeholder="Enter Your Cancellation Request"  rows="10" class="form-control"></textarea>
	</div>
	<div class="form-group">
		<label class="font-weight-bold">Cancellation Request Reason</label>
	<select name="cancellation_reason" class="form-control">
	<option class="hidden">Select Cancellation Reason</option>
<?php if ($seller_id==$login_seller_id) { ?>
		<option> Buyer is not Responding</option>
		<option> Buyer does not accept work</option>
		<option> Buyer tells me to cancel this order.</option>
		
<?php  }elseif ($buyer_id==$login_seller_id) { ?>
<option> Expert/Seller is not Responding</option>
<option> Expert/Seller does not accept work</option>
<option> Expert/Seller tells me to cancel this order.</option>
<?php }?>
</select>
</div><!--form-group ends-->
<input type="submit" value="Send Cancellation Request" class="btn btn-success float-right" name="submit_cancellation_request" >
</form>	

<?php 
if (isset($_POST['submit_cancellation_request'])) {
$cancellation_message=mysqli_real_escape_string($con,$_POST['cancellation_message']);
$cancellation_reason=mysqli_real_escape_string($con,$_POST['cancellation_reason']);
date_default_timezone_set("africa/nairobi");
$last_update_date=date("h:i: M d, Y");
$insert_cancellation_message="INSERT INTO order_conversations(order_id,sender_id,message,file,date,reason,status)values('$order_id','$login_seller_id','$cancellation_message','','$last_update_date','$cancellation_reason','cancellation_request')";
$run_cancellation_message=mysqli_query($con,$insert_cancellation_message);
//code to identify who is the buyer and the seller
if ($seller_id==$login_seller_id) {
$receiver_id==$buyer_id;
}else{
$receiver_id==$seller_id;

}
if ($run_cancellation_message) {
$insert_notification ="INSERT INTO notifications (receiver_id,sender_id,order_id,reason,date,status)values('$receiver_id','$login_seller_id','$order_id','cancellation request','$last_update_date','unread')";
$run_notifications=mysqli_query($con,$insert_notification);
$update_order="UPDATE orders set order_status='cancellation requested' where order_id='$order_id'";
$run_update=mysqli_query($con,$update_order);
echo "<script>window.open('order_details.php?order_id=$order_id','_self');</script>";
}
}

 ?>

</div>
</div>
</div>
</div>
</div><!--col-md-10 offset-md-1-->
</div>
</div><!--ends #resolution center   .tab-pane fade-->
</div>
</div>
</div>
</div><!--cointainer order-page mt-2 ends-->
<?php if ($seller_id==$login_seller_id) { ?>
<div id="deliver-order-modal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Deliver Your Order Now</h5>
				<button class="close" data-dismiss="modal"><span>&times;</span></button>
			</div>
			<div class="modal-body">
				<form method="post" enctype="multipart/form-data">
					<div class="form-group">
						<label class="font-weight-bold">Message</label>
						<textarea name="deliver_message" class="form-control mb-2" placeholder="Type your Message here..."></textarea>
					</div>
					<div class="form-group clearfix"></div>
					<input type="file" name="deliver_file" class="mt-1">
					<input type="submit" name="submit_delivered" value="Send Order Approval" class="btn btn-success float-right">
				</form>
<?php 
//get to deliver the necessary materials...
if (isset($_POST["submit_delivered"])) {
$d_message=mysqli_real_escape_string($con,$_POST["deliver_message"]);
$d_file=$_FILES["deliver_file"]["name"];
$d_file_tmp=$_FILES["deliver_file"]["tmp_name"];
move_uploaded_file($d_file_tmp, "order_files/$d_file");
$last_update_date=date("h:m M d Y");
$update_messages=" UPDATE order_conversations set status='message' where order_id='$order_id' AND status='delivered'";
$run_update_messages=mysqli_query($con,$update_messages);

$insert_delivered_message="INSERT INTO  order_conversations (order_id,sender_id,message,file,date,reason,status) values('$order_id','$seller_id','$d_message','$d_file','$last_update_date','','delivered')";
$run_delivered_message=mysqli_query($con,$insert_delivered_message);
if ($run_delivered_message) {
$insert_notification ="INSERT INTO notifications (receiver_id,sender_id,order_id,reason,date,status)values('$buyer_id','$seller_id','$order_id','order_delivered','$last_update_date','unread')";
$run_notifications=mysqli_query($con,$insert_notification);
$update_order="UPDATE orders set order_status='delivered' where order_id='$order_id'";
$run_update=mysqli_query($con,$update_order);
echo "<script>window.open('order_details.php?order_id=$order_id','_self');</script>";
	}
}
 ?>				
			</div>
		</div>
	</div>
</div><!--modal fade ends-->
<?php }elseif ($buyer_id==$login_seller_id) { ?>
<div id="revision-request-modal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Request for change NOW!</h5>
				<button class="close" data-dismiss="modal"><span>&times;</span></button>
			</div>
			<div class="modal-body">
				<form action="" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<label class="font-weight-bold">Message</label>
						<textarea name="revision_message" class="form-control mb-2" placeholder="Type your Message here..."></textarea>
					</div>
					<div class="form-group clearfix"></div>
					<input type="file" name="revision_file"  class="mt-1">
					<input type="submit" name="submit_revision" value="Send Change Request" class="btn btn-success float-right">
				</form>
<?php 
if (isset($_POST["submit_revision"])) {
$revision_message =mysqli_real_escape_string($con,$_POST["revision_message"]);
$revision_file=$_FILES["revision_file"]["name"];
$revision_file_tmp=$_FILES["revision_file"]["tmp_name"];
move_uploaded_file($revision_file_tmp,"order_files/$revision_file");

date_default_timezone_set("africa/nairobi");
 $last_update_date=date("h:i: M d, Y");

$update_messages_status="UPDATE order_conversations set status='message' where order_id='$order_id' AND status='delivered'";
$run_message=mysqli_query($con,$update_messages_status);
$insert_revision_message="INSERT INTO order_conversations (order_id,sender_id,message,file,date,reason,status) values('$order_id','$buyer_id','$revision_message','$revision_file','$last_update_date','','revision')";
$run_revision_message=mysqli_query($con,$insert_revision_message);
if ($run_revision_message) {
//notify the seller fro the buyer 	
$insert_notification = " INSERT INTO notifications (receiver_id,sender_id,order_id,reason,date,status)values('$seller_id','$buyer_id','$order_id','order_revision','$last_update_date','unread')";
$run_notifications=mysqli_query($con,$insert_notification);
$update_order="UPDATE orders SET order_status='revision requested' WHERE order_id='$order_id'";
$run_update=mysqli_query($con,$update_order);
echo "<script>window.open('order_details.php?order_id=$order_id','_self');</script>";
	
}

}

 ?>				
			</div>
		</div>
	</div>
</div><!-- revision-request-modal modal fade ends-->
<?php }?>
<script>
$(document).ready(function(){
		// make the sticky order status bar
$("#order-status-bar").sticky({
	topSpacing:0,
	zIndex:1000
});
	////  Countdown Timer Code Starts  ////
// Set the date we're counting down to
var countDownDate = new Date("<?php echo($order_time); ?>").getTime() + (409*1600*22);
// Update the count down every 1 second
var x = setInterval(function(){
// Get todays date and time
var now = new Date().getTime();
// Find the distance between now an the count down date
var distance = countDownDate - now;
// Time calculations for days, hours, minutes and seconds
var days = Math.floor(distance / (1000 * 60 * 60 * 24));
var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
var seconds = Math.floor((distance % (1000 * 60)) / 1000);
document.getElementById("days").innerHTML = days;
document.getElementById("hours").innerHTML = hours;
document.getElementById("minutes").innerHTML = minutes;
document.getElementById("seconds").innerHTML = seconds;
// If the count down is over, write some text 
if (distance < 0){

clearInterval(x);

<?php if(isset($_GET["selling_order"])){ ?>

document.getElementById("countdown-heading").innerHTML = "You Failed To Deliver Your Order On Time";

<?php }elseif (isset($_GET['buying_order'])) { ?>

document.getElementById("countdown-heading").innerHTML = "Seller/Expert Failed To Deliver Your Order On Time";

<?php } ?>

$("#countdown-timer .countdown-number").addClass("countdown-number-late");

document.getElementById("days").innerHTML = "Your";

document.getElementById("hours").innerHTML = "Order";

document.getElementById("minutes").innerHTML = "Is";

document.getElementById("seconds").innerHTML = "Late";

}
}, 1000);

////  Countdown Timer Code Ends  ////
	
//jquery code to get the upload files
$(document).on('change','#file', function(){
var form_data = new FormData();
form_data.append("file", document.getElementById('file').files[0]);
$.ajax({
	url: "upload_file.php",
	method: "POST",
	data: form_data,
	contentType: false,
	cache: false,
	processData: false,

}).done(function(data){
$("#upload_file_div").empty();
$("#upload_file_div").append(data);
});
});//ends document.on
//get the message uploaded
$('#insert-message-form').submit(function(e){
e.preventDefault();
order_id= "<?php echo $order_id; ?>";
order_message=$('#message').val();
file = $('#file').val();
if (file=="") {
order_file=file;
}else{
order_file=document.getElementById('file').files[0].name;
}
$.ajax({
method: "POST",
url: "insert_order_message.php",
data: {message: order_message, file:order_file, order_id:order_id},
success:function(data){
$('#message_data_div').html(data);	
$('#message').val("");	
$('#file').val("");	
}
});
});//ends submit

//code to get conversation info from db to display..
setInterval(function(){

order_id="<?php echo $order_id; ?>";

$.ajax({
	method: "GET",
	url: "order_conversations.php",
	data: {order_id: order_id }

}).done(function(data){
$("#order-conversations").empty();
$("#order-conversations").append(data);
});	
}, 1000);

});//ends document.ready..		
</script>
<?php include("includes/footer.php");?>
</body>
</html>