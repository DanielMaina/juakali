<?php 
session_start();
include("../includes/db.php");
if (!isset($_SESSION['seller_user_name'])) {
echo "<script>window.open('../login.php','_self');</script>";
}
//get the buyer/user deals to create the session
$login_seller_user_name=$_SESSION['seller_user_name'];
$select_login_seller="select * from sellers where seller_user_name='$login_seller_user_name'";
$run_login_seller=mysqli_query($con,$select_login_seller);
$row_login_seller=mysqli_fetch_array($run_login_seller);
$login_seller_id=$row_login_seller['seller_id'];
$login_seller_level=$row_login_seller['seller_level'];
$login_seller_language=$row_login_seller['seller_language'];


 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	<title>CreateANewProduct</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="Products juakali Mall">
	<link href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100" rel="stylesheet" >
	<link rel="stylesheet" href="../styles/bootstrap.min.css">
	<link rel="stylesheet" href="../styles/style.css">
	<link rel="stylesheet" href="../styles/user_nav_style.css">
	<!-- Custome css from the user -->
	<link rel="stylesheet" href="../styles/custom.css">
	<link rel="stylesheet" href="../font-awesome/css/all.min.css">
	<link rel="stylesheet" href="../styles/bootstrap-tokenfield.min.css">
	<!-- The jquery version support -->
   <script src="../js/jquery.slim.min.js"></script>
<!-- for the textarea customized UI tinymce -->
	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
  <script>tinymce.init({ selector:'.proposal-desc'});</script>
  <!--  for the tags bootstrap tolkenfield-->
  <script src="../js/bootstrap-tokenfield.min.js"></script>
</head>
<body>
<?php include("../includes/user_header.php");?>
<div class="container">
	<div class="row">
		<div class="col-md-12 mb-5 mt-5">
			<h1>Add A New PRODUCT</h1>

		</div><!--col-md-12 mb-5 mt-5 ends-->
		<div class="col-md-12">
			<div class="card rounded-0 mb-5">
				<div class="card-body">
<form method="post" enctype="multipart/form-data" >
						<div class="form-group row">
							<div class="col-md-3 control-label h6">Products Title</div>
							<div class="col-md-8">
								<input type="text" name="proposal_title" maxlength="70" class="form-control" required>
							</div>
						</div><!--form-group row 1 ends-->
						<div class="form-group row">
							<div class="col-md-3 control-label h6">Products Category</div>
<div class="col-md-8">
<select name="proposal_category" id="category" class="form-control mb-3" required>
<option value="" class="hidden">Select A Category</option>
<?php 
$get_cats="SELECT * from categories";
$run_cats=mysqli_query($con,$get_cats);
while ($row_cats=mysqli_fetch_array($run_cats)) {
$cat_id=$row_cats['cat_id'];	
$cat_title=$row_cats['cat_title'];	

 ?>
<option value="<?php echo $cat_id; ?>"><?php echo $cat_title; ?></option>
<?php } ?>
</select>
<select name="proposal_sub_category" id="sub-category" class="form-control" required>
<option value="" class="hidden">Select A Sub Category</option>

</select>
</div>
</div><!--form-group row 2 ends-->
<div class="form-group row">
<div class="col-md-3 control-label h6">Products Price</div>
<div class="col-md-8">
<input type="number" class="form-control" name="proposal_price" required>	

<!-- <select name="proposal_price" class="form-control" required>
<option value="5">Ksh 500</option>
<option value="10">Ksh 1000</option>
<option value="15">Ksh 1500</option>
<option value="20">Ksh 2000</option>
</select> -->
</div>
</div><!--form-group row 3 ends-->
						<div class="form-group row">
							<div class="col-md-3 control-label h6">Products Description <br> <small>Briefly Describe Your Products.</small> </div>
							<div class="col-md-8">
								<textarea name="proposal_description" id="" rows="7" placeholder="Enter Your Proposals Description" class="form-control proposal-desc"></textarea>
							</div>
						</div><!--form-group row ends-->
						<div class="form-group row">
							<div class="col-md-3 control-label h6">Instruction to buyer: <br> <small>Give Buyer a head start.</small><br>
								<small>If you need to obtain information, files or other material from the buyer prior to made customer desired product, please add your instructions here. For example:Add size,color, type, and any other if need.</small></div>
							<div class="col-md-8">
								<textarea name="buyer_instruction" class="form-control"  rows="7"></textarea>
							</div>
						</div><!--form-group row ends-->
						<div class="form-group row">
							<div class="col-md-3 control-label h6">Products Tags</div>
							<div class="col-md-8">
								<br> <small>(Unique Descriptive words USE coma to Seperate)</small><br>
								<input type="text" name="proposal_tags" placeholder="Tags ie shoe, men, women" id="tags"  class="form-control" required>
							</div>
						</div><!--form-group row ends-->
						<div class="form-group row">
							<div class="col-md-3 control-label h6">Products Delivery Time</div>
							<div class="col-md-8">
<select name="delivery_id" class="form-control" required>
<?php 
$get_delivery_times="SELECT * from delivery_times";
$run_delivery_times=mysqli_query($con,$get_delivery_times);
while ($row_delivery_times=mysqli_fetch_array($run_delivery_times)) {
$delivery_id=$row_delivery_times['delivery_id'];	
$delivery_proposal_title=$row_delivery_times['delivery_proposal_title'];	

echo "<option value='$delivery_id'>$delivery_proposal_title</option>";	
}
 ?>	

</select>
							</div>
						</div><!--form-group row ends-->
						<div class="form-group row">
							<div class="col-md-3 control-label h6"> Add Products Image</div>
							<div class="col-md-8">
								<input type="file" name="proposal_img1"  class="form-control" required>
							</div>
						</div><!--form-group row ends-->
						<div class="form-group row">
							<div class="col-md-3 control-label h6">Add Products More Images <br> <small>(Optional)</small></div>
							<div class="col-md-8">
								<a href="#" data-toggle="collapse" data-target="#more-images" class="btn btn-success btn-block">Add More Images</a>
								<div id="more-images" class="collapse">
									<input type="file" name="proposal_img2" class="form-control mt-3 mb-2" >
									<input type="file" name="proposal_img3" class="form-control mb-2" >
									<input type="file" name="proposal_img4" class="form-control  mb-2" >
								</div><!--ends of themore-images-->
							</div>
						</div><!--form-group row ends-->
						<div class="form-group row">
							<div class="col-md-3 control-label h6">Add Products Video <br> <small>(Optional)</small> </div>
							<div class="col-md-8">
								<input type="file" name="proposal_video" class="form-control" >
							</div>
						</div><!--form-group row ends-->
						<div class="form-group row">
							<div class="col-md-3 control-label h6"></div>
							<div class="col-md-8">
								<button type="submit" name="submit" class="btn btn-success form-control">Insert New Products</button>
							</div>
						</div><!--form-group row ends-->
</form>
				</div><!--card-body ends-->
			</div><!--card.rounded-0.mb-5 ends-->
		</div><!--col-md-12-->
	</div><!--row ends-->
</div><!--container ends-->
<script>
//to propagete the sub-category after making a selections
$(document).ready(function(){
$("#sub-category").hide();
$("#category").change(function(){
$("#sub-category").show();
//now get the sub-category relevant to the category...
var category_id = $(this).val();
$.ajax({
url:"fetch_subcategory.php",
method: "POST",
data:{category_id:category_id},
success:function(data){
$("#sub-category").html(data);	
}
});
});
//for the tags field display
$("#tags").tokenfield();
	});
</script>
<?php
#code to fetch data from form to  db  
if (isset($_POST['submit'])) {
$proposal_title	=mysqli_real_escape_string($con,$_POST['proposal_title']);
//make proposal/product seo url
class SanitizeUrl {

    public static function slug($string, $space="-") {
        $string = utf8_encode($string);
        if (function_exists('iconv')) {
            $string = iconv('UTF-8', 'ASCII//TRANSLIT', $string);
        }

        $string = preg_replace("/[^a-zA-Z0-9 \-]/", "", $string);
        $string = trim(preg_replace("/\\s+/", " ", $string));
        $string = strtolower($string);
        $string = str_replace(" ", $space, $string);

        return $string;
    }
}	
$sanitize_url = SanitizeUrl::slug($proposal_title);

$proposal_url=mysqli_real_escape_string($con,$sanitize_url);
$proposal_category=mysqli_real_escape_string($con,$_POST['proposal_category']);
$proposal_sub_category=mysqli_real_escape_string($con,$_POST['proposal_sub_category']);
$proposal_price=mysqli_real_escape_string($con,$_POST['proposal_price']);
$proposal_description=mysqli_real_escape_string($con,$_POST['proposal_description']);
$buyer_instruction=mysqli_real_escape_string($con,$_POST['buyer_instruction']);	
$proposal_tags=mysqli_real_escape_string($con,$_POST['proposal_tags']);
$delivery_id=mysqli_real_escape_string($con,$_POST['delivery_id']);

$proposal_img1=$_FILES['proposal_img1']['name'];	
$proposal_img2=$_FILES['proposal_img2']['name'];	
$proposal_img3=$_FILES['proposal_img3']['name'];	
$proposal_img4=$_FILES['proposal_img4']['name'];	
$proposal_video=$_FILES['proposal_video']['name'];

$tmp_proposal_img1=$_FILES['proposal_img1']['tmp_name'];	
$tmp_proposal_img2=$_FILES['proposal_img2']['tmp_name'];	
$tmp_proposal_img3=$_FILES['proposal_img3']['tmp_name'];	
$tmp_proposal_img4=$_FILES['proposal_img4']['tmp_name'];	
$tmp_proposal_video=$_FILES['proposal_video']['tmp_name'];

$allowed_img=array('jpeg','jpg','gif','png','tif');
$allowed_video=array('mp4','mov','avi','flv','wmv');

$proposal_img1_extension=pathinfo($proposal_img1,PATHINFO_EXTENSION);
$proposal_img2_extension=pathinfo($proposal_img2,PATHINFO_EXTENSION);
$proposal_img3_extension=pathinfo($proposal_img3,PATHINFO_EXTENSION);
$proposal_img4_extension=pathinfo($proposal_img4,PATHINFO_EXTENSION);
$proposal_video_extension=pathinfo($proposal_video,PATHINFO_EXTENSION);

if (!in_array($proposal_img1_extension,$allowed_img)) {
	echo "<script>alert('Your Products Image one file Extension is NOT SUPPORTED');</script>";
	exit();
}
if (!empty($proposal_img2)) {
if (!in_array($proposal_img2_extension,$allowed_img)) {
	echo "<script>alert('Your Products Image TWO file Extension is NOT SUPPORTED');</script>";
	exit();
}	
}

if (!empty($proposal_img3)) {
if (!in_array($proposal_img3_extension,$allowed_img)) {
	echo "<script>alert('Your Products Image THREE file Extension is NOT SUPPORTED');</script>";
	exit();
}	
}

if (!empty($proposal_img4)) {
if (!in_array($proposal_img4_extension,$allowed_img)) {
	echo "<script>alert('Your Products Image FOUR file Extension is NOT SUPPORTED');</script>";
	exit();
}	
}
if (!empty($proposal_video)) {
if (!in_array($proposal_video_extension,$allowed_video)) {
	echo "<script>alert('Your Products VIDEO file Extension is NOT SUPPORTED');</script>";
	exit();
}	
}

move_uploaded_file($tmp_proposal_img1,"proposal_files/$proposal_img1");
move_uploaded_file($tmp_proposal_img2,"proposal_files/$proposal_img2");
move_uploaded_file($tmp_proposal_img3,"proposal_files/$proposal_img3");
move_uploaded_file($tmp_proposal_img4,"proposal_files/$proposal_img4");
move_uploaded_file($tmp_proposal_video,"proposal_files/$proposal_video");



$insert_proposal = "INSERT INTO proposals (proposal_title,proposal_url,proposal_cat_id,proposal_child_id,proposal_price,proposal_img1,proposal_img2,proposal_img3,proposal_img4,proposal_video,proposal_desc,buyer_instruction,proposal_tags,proposal_featured,proposal_seller_id,delivery_id,level_id,language_id,proposal_views,proposal_status) values ('$proposal_title','$proposal_url','$proposal_category','$proposal_sub_category','$proposal_price','$proposal_img1','$proposal_img2','$proposal_img3','$proposal_img4','$proposal_video','$proposal_description','$buyer_instruction','$proposal_tags','no','$login_seller_id','$delivery_id','$login_seller_level','$login_seller_language','','pending')";
$run_proposal=mysqli_query($con,$insert_proposal);
if ($run_proposal) {
echo "<script>alert('Your Products Has Been ADDED, Thank YOU! ');</script>";	
echo "<script>window.open('view_proposals.php','_self');</script>";	
}

}



 ?>

<?php include("../includes/footer.php");?>
</body>
</html>