<?php 
session_start();
include("../includes/db.php");
if (!isset($_SESSION['seller_user_name'])) {
echo "<script>window.open('../login.php','_self');</script>";
}
	
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	
	<title>EditYourProduct</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="Juakali Mall">
	<link href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100" rel="stylesheet" >
	<link rel="stylesheet" href="../styles/bootstrap.min.css">
	<link rel="stylesheet" href="../styles/style.css">
	<link rel="stylesheet" href="../styles/user_nav_style.css">
	<!-- Custome css from the user -->
	<link rel="stylesheet" href="../styles/custom.css">
	<link rel="stylesheet" href="../font-awesome/css/all.min.css">
	<link rel="stylesheet" href="../styles/bootstrap-tokenfield.min.css">
	<!-- The jquery version support -->
   <script src="../js/jquery.slim.min.js"></script>
<!-- for the textarea customized UI tinymce -->
	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
  <script>tinymce.init({ selector:'.proposal-desc'});</script>
  <!--  for the tags bootstrap tolkenfield-->
  <script src="../js/bootstrap-tokenfield.min.js"></script>
</head>
<body>
<?php include("../includes/user_header.php");?>
<?php 
//gett the stored specific id from view_proposal.php at end point
$proposal_id=$_GET['proposal_id'];
//get the main content
$select_proposal="SELECT * from proposals where proposal_id='$proposal_id'";
$run_proposals=mysqli_query($con,$select_proposal);
$row_proposal=mysqli_fetch_array($run_proposals);
$d_proposal_title=$row_proposal['proposal_title'];
$d_proposal_cat_id=$row_proposal['proposal_cat_id'];
$d_proposal_child_id=$row_proposal['proposal_child_id'];
$d_proposal_price=$row_proposal['proposal_price'];
$d_proposal_desc=$row_proposal['proposal_desc'];
$d_buyer_instruction=$row_proposal['buyer_instruction'];
$d_proposal_tags=$row_proposal['proposal_tags'];
$d_proposal_video=$row_proposal['proposal_video'];
$d_proposal_img1=$row_proposal['proposal_img1'];
$d_proposal_img2=$row_proposal['proposal_img2'];
$d_proposal_img3=$row_proposal['proposal_img3'];
$d_proposal_img4=$row_proposal['proposal_img4'];
$d_delivery_id=$row_proposal['delivery_id'];
//fetch time assigned to the proposal submision
$get_delivery_times="SELECT * from delivery_times where delivery_id='$d_delivery_id'";
$run_delivery_times=mysqli_query($con,$get_delivery_times);
$row_delivery_times=mysqli_fetch_array($run_delivery_times);
$delivery_proposal_title=$row_delivery_times['delivery_proposal_title'];
//get title form the cartegory table
$get_cats="SELECT * from categories where cat_id='$d_proposal_cat_id'";
$run_cats=mysqli_query($con,$get_cats);
$row_cats=mysqli_fetch_array($run_cats);
$cat_title=$row_cats['cat_title'];
//get the the sub-category title
$get_c_cats="SELECT * from categories_childs where child_id='$d_proposal_child_id'";
$run_c_cats=mysqli_query($con,$get_c_cats);
$row_c_cats=mysqli_fetch_array($run_c_cats);
$child_title=$row_c_cats['child_title']; 
 ?>
<div class="container">
	<div class="row">
		<div class="col-md-12 mb-5 mt-5">
			<h1>Edit The Product</h1>

		</div><!--col-md-12 mb-5 mt-5 ends-->
		<div class="col-md-12">
			<div class="card rounded-0 mb-5">
				<div class="card-body">
					<form method="post" enctype="multipart/form-data" >
						<div class="form-group row">
							<div class="col-md-3 control-label h6">Product Title</div>
							<div class="col-md-8">
								<input type="text" name="proposal_title" maxlength="70" class="form-control" value="<?php echo($d_proposal_title); ?>" required>
							</div>
						</div><!--form-group row 1 ends-->
						<div class="form-group row">
							<div class="col-md-3 control-label h6">Product Category</div>
							<div class="col-md-8">
<select name="proposal_category" id="category" class="form-control mb-3" required>
<option value="<?php echo($d_proposal_cat_id); ?>" selected><?php echo($cat_title); ?></option>
<?php 
$get_cats="SELECT * from categories where not cat_id='$d_proposal_cat_id'";
$run_cats=mysqli_query($con,$get_cats);
while ($row_cats=mysqli_fetch_array($run_cats)) {
$cat_id=$row_cats['cat_id'];	
$cat_title=$row_cats['cat_title'];	

 ?>
<option value="<?php echo $cat_id; ?>"><?php echo $cat_title; ?></option>
<?php } ?>								
</select>
<select name="proposal_sub_category" id="sub_category" class="form-control" required>
<option value="<?php echo($d_proposal_child_id); ?>" selected><?php echo($child_title); ?></option>
</select>
</div>
						</div><!--form-group row 2 ends-->
						<div class="form-group row">
							<div class="col-md-3 control-label h6">Product Price</div>
							<div class="col-md-8">
<select name="proposal_price" class="form-control" required>
<option value="<?php echo($d_proposal_price); ?>">Ksh <?php echo($d_proposal_price); ?></option>

<option value="5">Ksh 500</option>
<option value="10" selected>Ksh 1000</option>
<option value="15">Ksh 1500</option>
<option value="20">Ksh 2000</option>
</select>
							</div>
						</div><!--form-group row 3 ends-->
						<div class="form-group row">
							<div class="col-md-3 control-label h6">Product Description <br> <small>Briefly Describe Your Product.</small> </div>
							<div class="col-md-8">
								<textarea name="proposal_description" id="" rows="7" placeholder="Enter Your Proposals Description" class="form-control proposal-desc">
									<?php echo($d_proposal_desc); ?>
								</textarea>
							</div>
						</div><!--form-group row ends-->
						<div class="form-group row">
							<div class="col-md-3 control-label h6">Instruction to buyer: <br> <small>Give Buyer a head start.</small><br>
								<small>If you need to obtain information, files or other material from the buyer prior to starting your work, please add your instructions here. For example: Please send me your company name or Please send me the photo you need me to edit.</small></div>
							<div class="col-md-8">
								<textarea name="buyer_instruction" class="form-control"  rows="7">
									<?php echo($d_buyer_instruction); ?>
								</textarea>
							</div>
						</div><!--form-group row ends-->
						<div class="form-group row">
							<div class="col-md-3 control-label h6">Product Tags</div>
							<div class="col-md-8">
								<input type="text" name="proposal_tags" placeholder="Tags" id="tags" value="<?php echo($d_proposal_tags); ?>" required>
							</div>
						</div><!--form-group row ends-->
						<div class="form-group row">
							<div class="col-md-3 control-label h6">Product Delivery Time</div>
							<div class="col-md-8">
								<select name="delivery_id" class="form-control" required>
<option value="<?php echo($d_delivery_id); ?>"><?php echo($delivery_proposal_title); ?></option>
<?php 
$get_delivery_times="SELECT * from delivery_times where not delivery_id='$d_delivery_id'";
$run_delivery_times=mysqli_query($con,$get_delivery_times);
while ($row_delivery_times=mysqli_fetch_array($run_delivery_times)) {
$delivery_id=$row_delivery_times['delivery_id'];	
$delivery_proposal_title=$row_delivery_times['delivery_proposal_title'];	

echo "<option value='$delivery_id'>$delivery_proposal_title</option>";	
}
 ?>
									
</select>
							</div>
						</div><!--form-group row ends-->
						<div class="form-group row">
							<div class="col-md-3 control-label h6"> Add Product Image</div>
							<div class="col-md-8">
<input type="file" name="proposal_img1"  class="form-control mb-3" >
<img src="proposal_files/<?php 
if(empty($d_proposal_img1)){
echo "no-image.jpg";
	}else{
echo($d_proposal_img1);
	}
 ?>

" width="60" height="70">
							</div>
						</div><!--form-group row ends-->
						<div class="form-group row">
							<div class="col-md-3 control-label h6">Add Product More Images <br> <small>(Optional)</small></div>
							<div class="col-md-8">
								<a href="#" data-toggle="collapse" data-target="#more-images" class="btn btn-success btn-block">Add More Images</a>
								<div id="more-images" class="collapse">
<input type="file" name="proposal_img2" class="form-control mt-3 mb-2" >
<img src="proposal_files/<?php 
if(empty($d_proposal_img2)){
echo "no-image.jpg";
	}else{
echo($d_proposal_img2);
	}
 ?>
 " width="60" height="70">
<input type="file" name="proposal_img3" class="form-control mb-2 mt-3" >
<img src="proposal_files/<?php 
if(empty($d_proposal_img3)){
echo "no-image.jpg";
	}else{
echo($d_proposal_img3);
	}
 ?>" width="60" height="70">
<input type="file" name="proposal_img4" class="form-control  mb-2 mt-3" >
<img src="proposal_files/<?php 
if(empty($d_proposal_img4)){
echo "no-image.jpg";
	}else{
echo($d_proposal_img4);
	}
 ?>" width="60" height="70">
</div><!--ends of themore-images-->
</div>
</div><!--form-group row ends-->
<div class="form-group row">
<div class="col-md-3 control-label h6">Add Product Video <br> <small>(Optional)</small> </div>
<div class="col-md-8">
<input type="file" name="proposal_video" class="form-control " >
<?php 
if (empty($d_proposal_video)) {
	

 ?>
<img src="proposal_files/no-video.jpg" width="60" height="70" class="mt-3">
<?php }else{ ?>
<br>

<video  width="150" height="150" controls>
<source src="proposal_files/<?php echo($d_proposal_video); ?>">
</video>
<?php } ?>
</div>
						</div><!--form-group row ends-->
						<div class="form-group row">
							<div class="col-md-3 control-label h6"></div>
							<div class="col-md-8">
								<button type="submit" name="update" class="btn btn-success form-control">Update Product</button>
							</div>
						</div><!--form-group row ends-->
					</form>
				</div><!--card-body ends-->
			</div><!--card.rounded-0.mb-5 ends-->
		</div><!--col-md-12-->
	</div><!--row ends-->
</div><!--container ends-->
<script>
//to propagete the sub-category after making a selections
$(document).ready(function(){
//now get the sub-category relevant to the category...
$("#category").change(function(){
var category_id = $(this).val();
$.ajax({
url:"fetch_subcategory.php",
method: "POST",
data:{category_id:category_id},
success:function(data){
$("#sub-category").html(data);	
}
});
});
//for the tags field display
$("#tags").tokenfield();
	});
</script>
<?php 
//code to update database....
#code to fetch data from form to  db  
if (isset($_POST['update'])) {
$proposal_title	=mysqli_real_escape_string($con,$_POST['proposal_title']);
$proposal_url=mysqli_real_escape_string($con,$sanitize_url);
$proposal_category=mysqli_real_escape_string($con,$_POST['proposal_category']);
$proposal_sub_category=mysqli_real_escape_string($con,$_POST['proposal_sub_category']);
$proposal_price=mysqli_real_escape_string($con,$_POST['proposal_price']);
$proposal_description=mysqli_real_escape_string($con,$_POST['proposal_description']);
$buyer_instruction=mysqli_real_escape_string($con,$_POST['buyer_instruction']);	
$proposal_tags=mysqli_real_escape_string($con,$_POST['proposal_tags']);
$delivery_id=mysqli_real_escape_string($con,$_POST['delivery_id']);

$proposal_img1=$_FILES['proposal_img1']['name'];	
$proposal_img2=$_FILES['proposal_img2']['name'];	
$proposal_img3=$_FILES['proposal_img3']['name'];	
$proposal_img4=$_FILES['proposal_img4']['name'];	
$proposal_video=$_FILES['proposal_video']['name'];

$tmp_proposal_img1=$_FILES['proposal_img1']['tmp_name'];	
$tmp_proposal_img2=$_FILES['proposal_img2']['tmp_name'];	
$tmp_proposal_img3=$_FILES['proposal_img3']['tmp_name'];	
$tmp_proposal_img4=$_FILES['proposal_img4']['tmp_name'];	
$tmp_proposal_video=$_FILES['proposal_video']['tmp_name'];

$allowed_img=array('jpeg','jpg','gif','png','tif');
$allowed_video=array('mp4','mov','avi','flv','wmv');

$proposal_img1_extension=pathinfo($proposal_img1,PATHINFO_EXTENSION);
$proposal_img2_extension=pathinfo($proposal_img2,PATHINFO_EXTENSION);
$proposal_img3_extension=pathinfo($proposal_img3,PATHINFO_EXTENSION);
$proposal_img4_extension=pathinfo($proposal_img4,PATHINFO_EXTENSION);
$proposal_video_extension=pathinfo($proposal_video,PATHINFO_EXTENSION);
if(!empty($proposal_img1)){
if (!in_array($proposal_img1_extension,$allowed_img)) {
	echo "<script>alert('Your Product Image one file Extension is NOT SUPPORTED');</script>";
	exit();
}
}else{
$proposal_img1=$d_proposal_img1;	
}
if (!empty($proposal_img2)) {
if (!in_array($proposal_img2_extension,$allowed_img)) {
	echo "<script>alert('Your Product Image TWO file Extension is NOT SUPPORTED');</script>";
	exit();
}	
}else{
$proposal_img2=$d_proposal_img2;	
}

if (!empty($proposal_img3)) {
if (!in_array($proposal_img3_extension,$allowed_img)) {
	echo "<script>alert('Your Product Image THREE file Extension is NOT SUPPORTED');</script>";
	exit();
}	
}else{
$proposal_img3=$d_proposal_img3;	
}

if (!empty($proposal_img4)) {
if (!in_array($proposal_img4_extension,$allowed_img)) {
	echo "<script>alert('Your Product Image FOUR file Extension is NOT SUPPORTED');</script>";
	exit();
}	
}else{
$proposal_img4=$d_proposal_img4;	
}
if (!empty($proposal_video)) {
if (!in_array($proposal_video_extension,$allowed_video)) {
	echo "<script>alert('Your Product VIDEO file Extension is NOT SUPPORTED');</script>";
	exit();
}	
}else{
$proposal_video=$d_proposal_video;	
}

move_uploaded_file($tmp_proposal_img1,"proposal_files/$proposal_img1");
move_uploaded_file($tmp_proposal_img2,"proposal_files/$proposal_img2");
move_uploaded_file($tmp_proposal_img3,"proposal_files/$proposal_img3");
move_uploaded_file($tmp_proposal_img4,"proposal_files/$proposal_img4");
move_uploaded_file($tmp_proposal_video,"proposal_files/$proposal_video");




$update_proposal="UPDATE proposals set proposal_title='$proposal_title',proposal_cat_id='$proposal_category',proposal_child_id='$proposal_sub_category',proposal_price='$proposal_price',proposal_img1='$proposal_img1',proposal_img2='$proposal_img2',proposal_img3='$proposal_img3',proposal_img4='$proposal_img4',proposal_video='$proposal_video',proposal_desc='$proposal_description',buyer_instruction='$buyer_instruction',delivery_id='$delivery_id' where proposal_id='$proposal_id'";

$run_proposal=mysqli_query($con,$update_proposal);
if ($run_proposal) {
echo "<script>alert('Your Product Has Been UPDATED, Thank YOU! ');</script>";	
echo "<script>window.open('view_proposals.php','_self');</script>";	
}

}

 ?>
<?php include("../includes/footer.php");?>
</body>
</html>
