<?php
session_start();
include("../includes/db.php");
$proposal_url=$_GET['proposal_url'];
$get_proposal="SELECT * from proposals where proposal_url='$proposal_url'";
$run_proposal=mysqli_query($con,$get_proposal);
$count_proposal=mysqli_num_rows($run_proposal);
if ($count_proposal == 0) {
	echo "<script>window.open('../index.php?not_available','_self');</script>";
}

$row_proposal=mysqli_fetch_array($run_proposal);
$proposal_id=$row_proposal['proposal_id'];

//select proposal Details from proposal id 

$get_proposal="SELECT * from proposals where proposal_id='$proposal_id'";
$run_proposal=mysqli_query($con,$get_proposal);
$row_proposal=mysqli_fetch_array($run_proposal);
$proposal_title=$row_proposal['proposal_title'];
$proposal_cat_id=$row_proposal['proposal_cat_id'];
$proposal_child_id=$row_proposal['proposal_child_id'];
$proposal_price=$row_proposal['proposal_price'];
$proposal_img1=$row_proposal['proposal_img1'];
$proposal_img2=$row_proposal['proposal_img2'];
$proposal_img3=$row_proposal['proposal_img3'];
$proposal_img4=$row_proposal['proposal_img4'];
$proposal_video=$row_proposal['proposal_video'];
$proposal_desc=$row_proposal['proposal_desc'];
$proposal_short_desc=substr($row_proposal['proposal_desc'],0,100);
$proposal_tags=$row_proposal['proposal_tags'];
$proposal_seller_id=$row_proposal['proposal_seller_id'];
$delivery_id=$row_proposal['delivery_id'];
$proposal_rating=$row_proposal['proposal_rating'];

//Select proposal category...

$get_cat="SELECT * from categories where cat_id='$proposal_cat_id'";
$run_cat=mysqli_query($con,$get_cat);
$row_cat=mysqli_fetch_array($run_cat);
$proposal_cat_title=$row_cat['cat_title'];

//Select chold categories

$get_child_cat="SELECT * from categories_childs where child_id='$proposal_child_id'";
$run_child_cat=mysqli_query($con,$get_child_cat);
$row_child_cat=mysqli_fetch_array($run_child_cat);
$proposal_child_title=$row_child_cat['child_title'];

//Select Product Delivery Time

$get_delivery_time="SELECT * from delivery_times where delivery_id='$delivery_id'";
$run_delivery_time=mysqli_query($con,$get_delivery_time);
$row_delivery_time=mysqli_fetch_array($run_delivery_time);
$delivery_proposal_title=$row_delivery_time['delivery_proposal_title'];

//Select proposal Active Orders

$select_orders="SELECT * from orders where proposal_id='$proposal_id' AND order_active='yes'";
$run_select_order=mysqli_query($con,$select_orders);
$proposal_order_queue=mysqli_num_rows($run_select_order);

 //Select Proposal Review Then Count them

$proposal_reviews= array();
$select_buyer_reviews="SELECT * from buyer_reviews where proposal_id='$proposal_id'";
$run_buyer_reviews=mysqli_query($con,$select_buyer_reviews);
$count_reviews=mysqli_num_rows($run_buyer_reviews);
while ($row_buyer_reviews=mysqli_fetch_array($run_buyer_reviews)) {
	$proposal_buyer_rating =$row_buyer_reviews['buyer_rating'];
	array_push($proposal_reviews,$proposal_buyer_rating);
	
}
$total=array_sum($proposal_reviews);
@$average_rating=$total/count($proposal_reviews);

//select Proposal Seller details

$select_proposal_seller="SELECT * from sellers where seller_id='$proposal_seller_id'";
$run_proposal_seller= mysqli_query($con,$select_proposal_seller);
$row_proposal_seller=mysqli_fetch_array($run_proposal_seller);
 $proposal_seller_user_name=$row_proposal_seller['seller_user_name'];
 $proposal_seller_image=$row_proposal_seller['seller_image'];
 $proposal_seller_country=$row_proposal_seller['seller_country'];
 $proposal_seller_about=$row_proposal_seller['seller_about'];
 $proposal_seller_level=$row_proposal_seller['seller_level'];
 $proposal_seller_recent_delivery=$row_proposal_seller['seller_recent_delivery'];
 $proposal_seller_rating=$row_proposal_seller['seller_rating'];
 $proposal_seller_vacation=$row_proposal_seller['seller_vacation'];
 $proposal_seller_status=$row_proposal_seller['seller_status'];

//Select proposal seller level

 $select_seller_level="SELECT * from seller_levels where level_id='$proposal_seller_level'";
 $run_seller_level=mysqli_query($con,$select_seller_level);
 $row_seller_level=mysqli_fetch_array($run_seller_level);
 $level_title=$row_seller_level['level_title'];

 //update proposal Views determine if any one has viewed.
if (!isset($_SESSION['seller_user_name'])) {
$update_proposal_views="UPDATE proposals set proposal_views=proposal_views+1 where proposal_id='$proposal_id'";
$run_update_views=mysqli_query($con,$update_proposal_views);

}
if (isset($_SESSION['seller_user_name'])) {
$login_seller_user_name=$_SESSION['seller_user_name'];
$select_login_seller="SELECT * from sellers where seller_user_name='$login_seller_user_name'";
$run_login_seller=mysqli_query($con,$select_login_seller);
$row_login_seller=mysqli_fetch_array($run_login_seller);
$login_seller_id=$row_login_seller['seller_id'];

if ($proposal_seller_id !=$login_seller_id) {
$update_proposal_views="UPDATE proposals set proposal_views=proposal_views+1 where proposal_id='$proposal_id'";
$run_update_views=mysqli_query($con,$update_proposal_views);

}
$select_recent_proposal="SELECT * from recent_proposals where seller_id='$login_seller_id' AND proposal_id='$proposal_id'";
$run_recent_proposal=mysqli_query($con,$select_recent_proposal);
$count_recent_proposal=mysqli_num_rows($run_recent_proposal);
if ($count_recent_proposal==1) {

$delete_recent_proposal="DELETE from recent_proposals where seller_id='$login_seller_id' AND proposal_id='$proposal_id'";
$run_delete=mysqli_query($con,$delete_recent_proposal);
	if ($proposal_seller_id !=$login_seller_id) {
$insert_recent_proposal="INSERT INTO  recent_proposals (seller_id, proposal_id)values('$login_seller_id','$proposal_id')";
$run_recent_proposal=mysqli_query($con,$insert_recent_proposal);
}
}else{
if ($proposal_seller_id !=$login_seller_id) {
$insert_recent_proposal="INSERT INTO  recent_proposals (seller_id, proposal_id)values('$login_seller_id','$proposal_id')";
$run_recent_proposal=mysqli_query($con,$insert_recent_proposal);
}
}
}

 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	
	<title><?php echo $proposal_title;?></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="<?php echo $proposal_short_desc;?>">
	<meta name="keywords" content="<?php echo $proposal_tags;?>">
	<meta name="author" content="<?php echo $proposal_seller_user_name;?>">
	<link href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100" rel="stylesheet" >
	<link rel="stylesheet" href="../styles/bootstrap.min.css">
	<link rel="stylesheet" href="../styles/style.css">
	<link rel="stylesheet" href="../styles/category_nav_style.css">
		<!-- Custome css from the user -->
	<link rel="stylesheet" href="../styles/custom.css">
	<link rel="stylesheet" href="../font-awesome/css/all.min.css">
	<link rel="stylesheet" href="../styles/owl.carousel.css">
	<link rel="stylesheet" href="../styles/owl.theme.default.css">

	<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5d788a6d33dc9b15"></script>


</script>
<script src="../js/jquery.slim.min.js"></script>
</head>
<body>
<div class="preloader d-flex justify-content-center align-items-center">
    <img src="../images/loadjuakali.gif" alt="the preloader"><br>
    <p>Loading...Please wait</p>
  </div>	
<?php include("../includes/header.php");?>
<div class="container mt-5">
<div class="row">
<div class="col-lg-8 col-md-7 mb-3">
<div class="card rounded-0 mb-3">
<div class="card-body details">
<div class="proposal-info">
<h3><?php echo $proposal_title; ?></h3>
<?php
for($proposal_i=0 ; $proposal_i < $proposal_rating; $proposal_i++){
	echo "<img src='../images/user_rate_full.png' alt='rating' class='rating'>";
}
for($proposal_i=$proposal_rating; $proposal_i < 5 ; $proposal_i++){
	echo "<img src='../images/user_rate_blank.png' alt='rating' class='rating'>";

}
?> 		
<span class="text-muted span">(<?php echo $count_reviews; ?>) &nbsp; &nbsp;<?php echo $proposal_order_queue; ?> Order In Queue</span>
	<hr>
<a href="../category.php?cat_id=<?php echo($proposal_cat_id)?>"><?php echo($proposal_cat_title); ?></a> / <a href="../category.php?cat_child_id=<?php echo($proposal_child_id)?>"><?php echo($proposal_child_title); ?></a>
	</div>
<div id="myCarousel" class="carousel slide">
<ol class="carousel-indicators">
<?php if(!empty($proposal_video)){ ?>	
<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
<?php } ?>
<li data-target="#myCarousel" data-slide-to="1"
<?php
if (empty($proposal_video)) { echo "class='active'";}
	?>
></li>
<?php if(!empty($proposal_img2)){ ?>
<li data-target="#myCarousel" data-slide-to="2"></li>
<?php }?>
<?php if(!empty($proposal_img3)){ ?>
<li data-target="#myCarousel" data-slide-to="3"></li>
<?php } ?>
<?php if(!empty($proposal_img4)){ ?>
<li data-target="#myCarousel" data-slide-to="4"></li>
<?php }?>
</ol>
<div class="carousel-inner">
<?php if(!empty($proposal_video)){ ?>		
<div class="carousel-item active">
<script src="https://content.jwplatform.com/libraries/IKBOrtS2.js"></script>
<div class="d-block w-100" id="player" style="height: auto;"></div>
<script>
var player=jwplayer('player');
player.setup({
file: "proposal_files/<?php echo $proposal_video; ?>",
image: "proposal_files/<?php echo $proposal_img1; ?>"
});
</script>
</div><!--carousel-item active ends-->
<?php } ?>
<div class="carousel-item
<?php
if (empty($proposal_video)) { echo "active"; }
?>">
<img src="proposal_files/<?php echo $proposal_img1; ?>" class="d-block w-100" height="100%">
</div>
<?php if(!empty($proposal_img2)){ ?>
<div class="carousel-item">
<img src="proposal_files/<?php echo $proposal_img2; ?>" class="d-block w-100"  height="100%">
</div>
<?php } ?>
<?php if(!empty($proposal_img3)){ ?>
<div class="carousel-item">
<img src="proposal_files/<?php echo $proposal_img3; ?>" class="d-block w-100"  height="100%">
</div>
<?php } ?>
<?php if(!empty($proposal_img4)){ ?>
<div class="carousel-item">
<img src="proposal_files/<?php echo $proposal_img4;?>" class="d-block w-100"  height="100%">
</div>
<?php } ?>
</div>
<a href="#myCarousel" class="carousel-control-prev slide-nav slide-right" data-slide="prev">
<span class="carousel-control-prev-icon carousel-icon"></span>
</a>
<a href="#myCarousel" class="carousel-control-next slide-nav slide-left" data-slide="next">
<span class="carousel-control-next-icon carousel-icon"></span>
</a>
</div>
</div>
</div>
<div class="card rounded-0 mb-3">
<div class="card-header">
<h4>About This Product</h4>
</div>
<div class="card-body">
<p><?php echo($proposal_desc) ?></p>
</div>
</div>
<div class="card proposal-reviews rounded-0 mb-3">
<div class="card-header">
<h4>
<span class="">(<?php echo($count_reviews) ;?>)Rate</span>
<?php
for($proposal_i=0 ; $proposal_i < $proposal_rating; $proposal_i++){
	echo "<img src='../images/user_rate_full_big.png' alt='rating' class='rating'>";
}
for($proposal_i=$proposal_rating; $proposal_i < 5 ; $proposal_i++){
	echo "<img src='../images/user_rate_blank_big.png' alt='rating' class='rating'>";

}
?> 	
<span class="text-muted">
<?php
if ($proposal_rating == "0") {
	echo "0.0";
}else{
	printf("%.1f", $average_rating);
}
?>
</span>
<div class="dropdown float-right">
<button id="dropdown-button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown">Most Recent</button>
<ul class="dropdown-menu">
<li class="dropdown-item active all">Most Recent</li>
<li class="dropdown-item good">Positive Review</li>
<li class="dropdown-item bad">Negative Review</li>
</ul>
</div>
</h4>
</div>
<div class="card-body">
<article id="all" class="proposal-reviews">
<ul class="reviews-list">
<?php 
$select_buyer_reviews="SELECT * from  buyer_reviews where proposal_id='$proposal_id'";
$run_buyer_reviews=mysqli_query($con,$select_buyer_reviews);
$count_reviews=mysqli_num_rows($run_buyer_reviews);
if ($count_reviews == 0) {
echo "
<li class='alert alert-light bg-dark'>
 <h3 align='center'>This Product Has No Review, Become First To Write  Reviews!</h3>
</li>
";
}
while ($row_buyer_reviews=mysqli_fetch_array($run_buyer_reviews)) {
	$order_id=$row_buyer_reviews['order_id'];
	$review_buyer_id=$row_buyer_reviews['review_buyer_id'];
	$buyer_rating=$row_buyer_reviews['buyer_rating'];
	$buyer_review=$row_buyer_reviews['buyer_review'];
	$review_date=$row_buyer_reviews['review_date'];

$select_seller="select * from sellers where seller_id='$review_buyer_id'";
	$run_seller=mysqli_query($con, $select_seller);
	$row_seller=mysqli_fetch_array($run_seller);
	$buyer_user_name=$row_seller['seller_user_name'];
	$buyer_image=$row_seller['seller_image'];

$select_seller_review="select * from seller_reviews where order_id='$order_id'";
$run_seller_review=mysqli_query($con,$select_seller_review);
$count_seller_review=mysqli_num_rows($run_seller_review);
$row_seller_review=mysqli_fetch_array($run_seller_review);
$seller_rating=$row_seller_review['seller_rating'];
$seller_review=$row_seller_review['seller_review'];

?>
<li class="star-rating-row">
<span class="user-picture">
<?php if (!empty($buyer_image)) { ?>	
<img src="../user_images/<?php echo $buyer_image;?>" width="60" height="60">
<?php }else{ ?>
<img src="../user_images/empty-image.png" width="60" height="60">

<?php } ?>
</span>
<h4>
<a href="#" class="mr-1"><?php echo($buyer_user_name); ?></a><br>
<?php
for($buyer_i=0 ; $buyer_i < $buyer_rating; $buyer_i++){
	echo "<img src='../images/user_rate_full.png' alt='rating' class='rating'>";
}
for($buyer_i=$buyer_rating; $buyer_i < 5 ; $buyer_i++){
	echo "<img src='../images/user_rate_blank.png' alt='rating' class='rating'>";

}
?> 
</h4>
<div class="msg-body">
<?php echo($buyer_review); ?>
</div>
<span class="rating-date"><?php echo($review_date); ?></span>
</li><!--star-rating-row ends-->
<?php
if (!$count_seller_review ==0) { ?>
<li class="rating-seller">
<h4>
<span class="mr-1">Seller's Feedback</span><br>
<?php
for($seller_i=0 ; $seller_i < $seller_rating; $seller_i++){
	echo "<img src='../images/user_rate_full.png' alt='rating' class='rating'>";
}
for($seller_i=$seller_rating; $seller_i < 5 ; $seller_i++){
	echo "<img src='../images/user_rate_blank.png' alt='rating' class='rating'>";

}
?>
</h4>
<span class="user-picture">
<?php 
 if (!empty($proposal_seller_image)) { ?>
<img src="../user_images/<?php echo $proposal_seller_image; ?>" width="40" height="40">
<?php }else{?>
<img src="../user_images/empty-image.png" width="40" height="40">
<?php } ?>
</span>
<div class="msg-body">
<?php echo($seller_review);  ?>
</div>
</li>
<?php }?>
<hr>
<?php } ?>
</ul>
</article>

<article id="good" class="proposal-reviews">
<ul class="reviews-list">
<?php 
$select_buyer_reviews="SELECT * from  buyer_reviews where proposal_id='$proposal_id' AND (buyer_rating='5' or buyer_rating='4')";
$run_buyer_reviews=mysqli_query($con,$select_buyer_reviews);
$count_reviews=mysqli_num_rows($run_buyer_reviews);
if ($count_reviews == 0) {
echo "
<li class='alert alert-info'>
 <h3 align='center'>There is Currently No Positive Review For this Product!</h3>
</li>
";
}
while ($row_buyer_reviews=mysqli_fetch_array($run_buyer_reviews)) {
	$order_id=$row_buyer_reviews['order_id'];
	$review_buyer_id=$row_buyer_reviews['review_buyer_id'];
	$buyer_rating=$row_buyer_reviews['buyer_rating'];
	$buyer_review=$row_buyer_reviews['buyer_review'];
	$review_date=$row_buyer_reviews['review_date'];

$select_seller="SELECT * from sellers where seller_id='$review_buyer_id'";
	$run_seller=mysqli_query($con, $select_seller);
	$row_seller=mysqli_fetch_array($run_seller);
	$buyer_user_name=$row_seller['seller_user_name'];
	$buyer_image=$row_seller['seller_image'];

$select_seller_review="SELECT * from seller_reviews where order_id='$order_id'";
$run_seller_review=mysqli_query($con,$select_seller_review);
$count_seller_review=mysqli_num_rows($run_seller_review);
$row_seller_review=mysqli_fetch_array($run_seller_review);
$seller_rating=$row_seller_review['seller_rating'];
$seller_review=$row_seller_review['seller_review'];

?>
<li class="star-rating-row">
<span class="user-picture">
<?php if (!empty($buyer_image)) { ?>	
<img src="../user_images/<?php echo $buyer_image;?>" width="60" height="60">
<?php }else{ ?>
<img src="../user_images/empty-image.png" width="60" height="60">

<?php } ?>
</span>
<h4>
<a href="#" class="mr-1"><?php echo($buyer_user_name); ?></a><br>
<?php
for($buyer_i=0 ; $buyer_i < $buyer_rating; $buyer_i++){
	echo "<img src='../images/user_rate_full.png' alt='rating' class='rating'>";
}
for($buyer_i=$buyer_rating; $buyer_i < 5 ; $buyer_i++){
	echo "<img src='../images/user_rate_blank.png' alt='rating' class='rating'>";

}
?> 
</h4>
<div class="msg-body">
<?php echo($buyer_review); ?>
</div>
<span class="rating-date"><?php echo($review_date); ?></span>
</li><!--star-rating-row ends-->
<?php
if (!$count_seller_review ==0) { ?>
<li class="rating-seller">
<h4>
<span class="mr-1">Seller's Feedback</span><br>
<?php
for($seller_i=0 ; $seller_i < $seller_rating; $seller_i++){
	echo "<img src='../images/user_rate_full.png' alt='rating' class='rating'>";
}
for($seller_i=$seller_rating; $seller_i < 5 ; $seller_i++){
	echo "<img src='../images/user_rate_blank.png' alt='rating' class='rating'>";

}
?>
</h4>
<span class="user-picture">
<?php 
 if (!empty($proposal_seller_image)) { ?>
<img src="../user_images/<?php echo $proposal_seller_image; ?>" width="40" height="40">
<?php }else{?>
<img src="../user_images/empty-image.png" width="40" height="40">
<?php } ?>
</span>
<div class="msg-body">
<?php echo($seller_review);  ?>
</div>
</li>
<?php }?>
<hr>
<?php } ?>
</ul>
</article>
<article id="bad" class="proposal-reviews">
<ul class="reviews-list">
<?php 
$select_buyer_reviews="SELECT * from  buyer_reviews where proposal_id='$proposal_id' AND (buyer_rating='1' or buyer_rating='2' or buyer_rating='3')";
$run_buyer_reviews=mysqli_query($con,$select_buyer_reviews);
$count_reviews=mysqli_num_rows($run_buyer_reviews);
if ($count_reviews == 0) {
echo "
<li class='alert alert-danger bg-secondary'>
 <h3 align='center'>There is Currently No Negative Review For this Product!</h3>
</li>
";
}
while ($row_buyer_reviews=mysqli_fetch_array($run_buyer_reviews)) {
	$order_id=$row_buyer_reviews['order_id'];
	$review_buyer_id=$row_buyer_reviews['review_buyer_id'];
	$buyer_rating=$row_buyer_reviews['buyer_rating'];
	$buyer_review=$row_buyer_reviews['buyer_review'];
	$review_date=$row_buyer_reviews['review_date'];

$select_seller="SELECT * from sellers where seller_id='$review_buyer_id'";
	$run_seller=mysqli_query($con, $select_seller);
	$row_seller=mysqli_fetch_array($run_seller);
	$buyer_user_name=$row_seller['seller_user_name'];
	$buyer_image=$row_seller['seller_image'];

$select_seller_review="SELECT * from seller_reviews where order_id='$order_id'";
$run_seller_review=mysqli_query($con,$select_seller_review);
$count_seller_review=mysqli_num_rows($run_seller_review);
$row_seller_review=mysqli_fetch_array($run_seller_review);
$seller_rating=$row_seller_review['seller_rating'];
$seller_review=$row_seller_review['seller_review'];

?>
<li class="star-rating-row">
<span class="user-picture">
<?php if (!empty($buyer_image)) { ?>	
<img src="../user_images/<?php echo $buyer_image;?>" width="60" height="60">
<?php }else{ ?>
<img src="../user_images/empty-image.png" width="60" height="60">

<?php } ?>
</span>
<h4>
<a href="#" class="mr-1"><?php echo($buyer_user_name); ?></a><br>
<?php
for($buyer_i=0 ; $buyer_i < $buyer_rating; $buyer_i++){
	echo "<img src='../images/user_rate_full.png' alt='rating' class='rating'>";
}
for($buyer_i=$buyer_rating; $buyer_i < 5 ; $buyer_i++){
	echo "<img src='../images/user_rate_blank.png' alt='rating' class='rating'>";

}
?> 
</h4>
<div class="msg-body">
<?php echo($buyer_review); ?>
</div>
<span class="rating-date"><?php echo($review_date); ?></span>
</li><!--star-rating-row ends-->
<?php
if (!$count_seller_review ==0) { ?>
<li class="rating-seller">
<h4>
<span class="mr-1">Seller's Feedback</span><br>
<?php
for($seller_i=0 ; $seller_i < $seller_rating; $seller_i++){
	echo "<img src='../images/user_rate_full.png' alt='rating' class='rating'>";
}
for($seller_i=$seller_rating; $seller_i < 5 ; $seller_i++){
	echo "<img src='../images/user_rate_blank.png' alt='rating' class='rating'>";

}
?>
</h4>
<span class="user-picture">
<?php 
 if (!empty($proposal_seller_image)) { ?>
<img src="../user_images/<?php echo $proposal_seller_image; ?>" width="40" height="40">
<?php }else{?>
<img src="../user_images/empty-image.png" width="40" height="40">
<?php } ?>
</span>
<div class="msg-body">
<?php echo($seller_review);  ?>
</div>
</li>
<?php }?>
<hr>
<?php } ?>	
</ul>
</article>
</div>
</div>
<!-- end of the rating-bar content -->
<div class="proposal-tags-container mt-2">
<?php
$tags=explode(",", $proposal_tags);
foreach ($tags as $tag) {

?>


<div class="proposal-tag mb-3"><span><?php  echo $tag; ?></span></div>
<?php } ?>
</div>
</div><!--col-lg-8 col-md-7 mb-3 ends-->
<div class="col-lg-4 col-md-5 proposal-sidebar">
<div class="card mb-3 rounded-0">
<div class="card-body">
<?php
//show the ower a direct message to about proposal status
if ($proposal_seller_vacation=="on") {  ?>
<?php if ($proposal_seller_user_name == @$_SESSION['seller_user_name']) {  ?>
	<h4 class="alert alert-danger">Your Vacation Mode Has Been Turn On! <br>
	No One Is Able to Purchase Your Product!	
	</h4>

<?php }else{ ?>	
<h4 class="alert-danger p-3"><strong>Alert!</strong> Seller Vacation Mode Has Been Turn On Sorry Anyone cannot Purchase This Product!</h4>
	<?php } ?>
<?php }elseif ($proposal_seller_vacation=="off") {
?>	
<h3><strong class="text-success">Ksh <?php echo $proposal_price;?></strong> Order Details</h3>
<h4><i class="fa fa-clock"></i>&nbsp; <?php echo $delivery_proposal_title;?> Delivery</h4>
<?php
if (!isset($_SESSION['seller_user_name'])) { ?>
<button class="btn btn-success btn-lg button-lg1" data-toggle="modal" data-target="#login-modal">Order(<?php echo $proposal_price;?>)</button>
<button class="btn btn-success btn-lg button-lg2" data-toggle="modal" data-target="#login-modal"><i class="fa fa-shopping-cart"></i></button><!--The #login-modal is from the header.php-->
<hr>
<div class="form-group row">
<label class="col-md-6 control-label">Product Quantity</label>
<div class="col-md-6">
<input type="number" name="proposal_qty" class="form-control" min="1" max="10" required>	
<!-- <select name="proposal_qty" id="" class="form-control">
<option>1</option>
<option>2</option>
<option>3</option>
<option>4</option>
</select> -->
</div>
</div>

<?php }else{ ?>

<?php if ($proposal_seller_user_name==@$_SESSION['seller_user_name']) { ?>
	<a href="edit_proposal.php?proposal_id=<?php echo $proposal_id; ?>" class="btn btn-lg btn-block btn-success">Edit Product</a>

<?php }else{ ?>
<form action="../checkout.php" method="post">
<input type="hidden" name="proposal_id" value="<?php echo $proposal_id; ?>">
<button class="btn btn-success btn-lg button-lg1" type="submit" name="add_order">Order(<?php echo $proposal_price;?>)</button>
<button class="btn btn-success btn-lg button-lg2" type="submit" name="add_cart"><i class="fa fa-shopping-cart"></i></button><hr>
<div class="form-group row">
<label class="col-md-6 control-label">Product Quantity</label>
<div class="col-md-6">
<input type="number" name="proposal_qty" class="form-control" min="1" max="10" required>	
<!-- <select name="proposal_qty" id="" class="form-control">
<option>1</option>
<option>2</option>
<option>3</option>
<option>4</option>
<option>5</option>
</select> -->
</div>
</div>
</form>
<?php } ?>
<?php } ?>
<?php } ?>
</div><!--end of the card-body -->
</div>
<center class="mb-3">
<!-- Go to www.addthis.com/dashboard to customize your tools social media platform-->
<div class="addthis_inline_follow_toolbox"></div>
           
</center>
<div class="card seller-bio mb-3 rounded-0">
<div class="card-body">
<center class="mb-4">
<?php if(!empty($proposal_seller_image)) { ?>	
<img src="../user_images/<?php echo($proposal_seller_image);?>" width="130" class="rounded-cicle">
<?php }else{ ?>
<img src="../user_images/empty-image.png" width="130" class="rounded-cicle">
<?php } ?>	

<?php if ($proposal_seller_level == 2) { ?>
<img src="../images/level_badge_1.png" width="55" class="seller_level_badge">

<?php } ?>
<?php if ($proposal_seller_level == 3) { ?>
<img src="../images/level_badge_2.png" width="55" class="seller_level_badge">

<?php } ?>
<?php if ($proposal_seller_level == 4) { ?>
<img src="../images/level_badge_3.png" width="55" class="seller_level_badge">

<?php } ?>
</center>
<a href="../user/<?php echo($proposal_seller_user_name);?>" class="text-center">
<h3><?php echo($proposal_seller_user_name);?></h3>
</a>
<div class="text-muted tex-center"><?php echo($level_title); ?></div>
<hr>

<div class="row">
<div class="col-md-6">
<p class="text-muted">From</p>
<p><?php echo($proposal_seller_country); ?></p>
</div>
<div class="col-md-6">
<p class="text-muted">Positive Rating</p>
<p><?php echo($proposal_seller_rating); ?>%</p>
</div>
<div class="col-md-6">
<p class="text-muted">Performace</p>
<p>

<?php 
$select_languages_relation="SELECT * from language_relation where seller_id='$proposal_seller_id'";
$run_languages_relation=mysqli_query($con,$select_languages_relation);
while($row_languages_relation=mysqli_fetch_array($run_languages_relation)) {
$language_id=$row_languages_relation['language_id'];
$get_language="SELECT * from seller_languages where language_id='$language_id'";
$run_language=mysqli_query($con,$get_language);
$row_language=mysqli_fetch_array($run_language);
$language_title=$row_language['language_title'];
?>

<?php echo($language_title); ?>,
	
<?php } ?>
	
</p>
</div>
<div class="col-md-6">
<p class="text-muted">Recent Delivery</p>
<p><?php echo $proposal_seller_recent_delivery; ?></p>
</div>
</div>
<hr>
<p>
<?php echo($proposal_seller_about); ?>
</p>
<a href="../user/<?php echo $proposal_seller_user_name; ?>">Read More</a>
</div>
<div class="card-footer">
<?php 
if ($proposal_seller_status == "online") { ?>	
<p class="float-left online">
<i class="fa fa-check"></i> Online
</p>
<?php } ?>
<a class="float-right btn btn-primary" href="../conversations/message.php?seller_id=<?php echo $proposal_seller_id; ?>">
<i class="fa fa-comment"></i> Contact Us
</a>
</div>
</div>
</div><!--col-lg-4 col-md-5 proposal-sidebar ends-->
</div>
</div>
<div class="container-fluid bg-light">
<div class="row">
<div class="col-md-10 offset-md-1">
<h2 class="p-2 mt-3">
RECOMMENDED FOR YOU IN <a href="../<?php echo($proposal_seller_user_name); ?>"><?php echo($proposal_seller_user_name); ?></a>
</h2>
<div class="row flex-wrap mb-3">

<?php  

$get_proposals="SELECT * from proposals where proposal_seller_id='$proposal_seller_id' AND proposal_status='active'";
$run_proposals=mysqli_query($con,$get_proposals);
while ($row_proposals=mysqli_fetch_array($run_proposals)) {
  $proposal_id=$row_proposals['proposal_id'];
  $proposal_title=$row_proposals['proposal_title'];
  $proposal_price=$row_proposals['proposal_price'];
  $proposal_img1=$row_proposals['proposal_img1'];
  $proposal_video=$row_proposals['proposal_video'];
  $proposal_seller_id=$row_proposals['proposal_seller_id'];
  $proposal_rating=$row_proposals['proposal_rating'];
  $proposal_url=$row_proposals['proposal_url'];
  $proposal_featured=$row_proposals['proposal_featured'];
 if (empty($proposal_video)) {
$video_class="";
 }else{
$video_class="video-img";
 }
 $select_seller="select * from sellers where seller_id='$proposal_seller_id'";
 $run_seller=mysqli_query($con,$select_seller);
 $row_seller=mysqli_fetch_array($run_seller);
 $seller_user_name=$row_seller['seller_user_name'];

 $seller_buyer_reviews="select * from buyer_reviews where proposal_id='$proposal_id'";
 $run_buyer_reviews=mysqli_query($con,$seller_buyer_reviews);
 $count_reviews=mysqli_num_rows($run_buyer_reviews);


?>	

<div class="col-lg-3 col-md-6 col-sm-6">
<div class="proposal-div">
<div class="proposal_nav">
<span class="float-left mt-2">
<strong class="ml-2 mr-1">By </strong> <?php echo($seller_user_name) ?>
</span>
<span class="float-right mt-2">
<?php
for($proposal_i=0 ; $proposal_i < $proposal_rating; $proposal_i++){
	echo "<img src='../images/user_rate_full.png' alt='rating' class='rating'>";
}
for($proposal_i=$proposal_rating ; $proposal_i < 5 ; $proposal_i++){
	echo "<img src='../images/user_rate_blank.png' alt='rating' class='rating'>";

}
?>
<span class="ml-1 mr-2"> (<?php echo $count_reviews; ?>) </span>

</span>
<div class="clearfix mb-2"></div>
</div>
<a href="<?php echo $proposal_url; ?>">
<hr class="m-0 p-0">
<img src="proposal_files/<?php echo $proposal_img1; ?>" class="resp-img">
</a>
<div class="text">
<p>
<a href="<?php echo $proposal_url; ?>" class="<?php echo $video_class; ?> text-dark font-weight-bold"><?php echo $proposal_title; ?></a>
</p>
<hr>
<p class="button clearfix">
<span class="float-right">STARTING AT <strong class="price">Ksh <?php echo $proposal_price; ?></strong></span>
</p>
</div><!--text ends-->
<?php 
if ($proposal_featured== "yes") { ?>
<div class="ribbon">
<div class="theribbon">Featured</div>
<div class="ribbon-background"></div>
</div>
<?php } ?>
</div>
</div><!--col-lg-3 col-md-6 col-sm-6 ends-->
<?php }?>
			</div><!--row flex-wrap mb-3 ends-->
		</div>
</div><!--end of the row recommeneded for you-->
<?php
if (isset($_SESSION['seller_user_name'])) { ?>
<div class="row">
<div class="col-md-10 offset-md-1">
<h2 class="p-2 mt-3">
Your Recent Viewed Product
</h2>
<div class="row flex-wrap mb-3">
<?php 

//code to get the recent viewed proposal

$select_recent="SELECT * from recent_proposals where seller_id='$login_seller_id'order by 1 DESC LIMIT 0,4";
$run_recent=mysqli_query($con,$select_recent);
while ($row_recent=mysqli_fetch_array($run_recent)) {
$proposal_id=$row_recent['proposal_id'];
 
$get_proposals="SELECT * from proposals where proposal_id='$proposal_id' AND proposal_status='active'";
$run_proposals=mysqli_query($con, $get_proposals);
$row_proposals=mysqli_fetch_array($run_proposals);
//a copy from ubave code
$proposal_id=$row_proposals['proposal_id'];
  $proposal_title=$row_proposals['proposal_title'];
  $proposal_price=$row_proposals['proposal_price'];
  $proposal_img1=$row_proposals['proposal_img1'];
  $proposal_video=$row_proposals['proposal_video'];
  $proposal_seller_id=$row_proposals['proposal_seller_id'];
  $proposal_rating=$row_proposals['proposal_rating'];
  $proposal_url=$row_proposals['proposal_url'];
  $proposal_featured=$row_proposals['proposal_featured'];
 if (empty($proposal_video)) {
$video_class="";
 }else{
$video_class="video-img";
 }
 $select_seller="SELECT * from sellers where seller_id='$proposal_seller_id'";
 $run_seller=mysqli_query($con,$select_seller);
 $row_seller=mysqli_fetch_array($run_seller);
 $seller_user_name=$row_seller['seller_user_name'];

 $seller_buyer_reviews="SELECT * from buyer_reviews where proposal_id='$proposal_id'";
 $run_buyer_reviews=mysqli_query($con,$seller_buyer_reviews);
 $count_reviews=mysqli_num_rows($run_buyer_reviews);

 ?>	
<div class="col-lg-3 col-md-6 col-sm-6">
<div class="proposal-div">
<div class="proposal_nav">
<span class="float-left mt-2">
<strong class="ml-2 mr-1">By</strong> <?php echo $seller_user_name; ?>
</span>
<span class="float-right mt-2">
<?php
for($proposal_i=0 ; $proposal_i < $proposal_rating; $proposal_i++){
	echo "<img src='../images/user_rate_full.png' alt='rating' class='rating'>";
}
for($proposal_i=$proposal_rating ; $proposal_i < 5 ; $proposal_i++){
	echo "<img src='../images/user_rate_blank.png' alt='rating' class='rating'>";

}
?>							
<span class="ml-1 mr-2"> (<?php echo $count_reviews; ?>) </span>

</span>
<div class="clearfix mb-2"></div>
</div>
<a href="proposal.php">
<hr class="m-0 p-0">
<img src="proposal_files/<?php echo $proposal_img1; ?>" class="resp-img">
</a>
<div class="text">
<p>
<a href="<?php echo $proposal_url; ?>" class="<?php echo $video_class; ?> text-dark font-weight-bold"><?php echo $proposal_title; ?></a>
</p>
<hr>
<p class="button clearfix">
<span class="float-right">STARTING AT <strong class="price">Ksh <?php echo $proposal_price; ?></strong></span>
</p>
</div>
<?php 
if ($proposal_featured) {
	
 ?>
<div class="ribbon">
<div class="theribbon">Featured</div>
<div class="ribbon-background"></div>
</div>
<?php } ?>
</div>
</div><!--col-lg-3 col-md-6 col-sm-6 ends-->
<?php } ?>
</div>
</div>
</div><!--end of the row recommeneded for you-->
<?php } ?>
</div>
<?php include("../includes/footer.php");?>
<script>
	$(document).ready(function(){
		$('#good').hide();
		$('#bad').hide();

		$('.all').click(function(){
			$("#dropdown-button").html("Most Recent");
			$(".all").attr('class','dropdown-item all active');
			$(".bad").attr('class','dropdown-item bad');
			$(".good").attr('class','dropdown-item good');
			$("#all").show();
			$("#good").hide();
			$("#bad").hide();
		});
		$('.good').click(function(){
			$("#dropdown-button").html("Positive Reviews");
			$(".all").attr('class','dropdown-item all');
			$(".bad").attr('class','dropdown-item bad');
			$(".good").attr('class','dropdown-item good active');
			$("#all").hide();
			$("#good").show();
			$("#bad").hide();
		});
		$('.bad').click(function(){
			$("#dropdown-button").html("Negative Reviews");
			$(".all").attr('class','dropdown-item all ');
			$(".bad").attr('class','dropdown-item bad active');
			$(".good").attr('class','dropdown-item good');
			$("#all").hide();
			$("#good").hide();
			$("#bad").show();
		});
	});
</script>
</body>
</html>