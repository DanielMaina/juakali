<?php 
session_start();
include("../includes/db.php");
if (!isset($_SESSION['seller_user_name'])) {
echo "<script>window.open('../login.php','_self');</script>";
}
//get the buyer/user deals to create the session
$login_seller_user_name=$_SESSION['seller_user_name'];
$select_login_seller="select * from sellers where seller_user_name='$login_seller_user_name'";
$run_login_seller=mysqli_query($con,$select_login_seller);
$row_login_seller=mysqli_fetch_array($run_login_seller);
$login_seller_id=$row_login_seller['seller_id'];
$login_seller_vacation=$row_login_seller['seller_vacation'];

 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	<title>View Product</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Juakali products">
	<meta name="keywords" content="Juakali products">
	<meta name="author" content="Juakali products">
	<link href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100" rel="stylesheet" >
	<link rel="stylesheet" href="../styles/bootstrap.min.css">
	<link rel="stylesheet" href="../styles/style.css">
	<link rel="stylesheet" href="../styles/user_nav_style.css">
		<!-- Custome css from the user -->
	<link rel="stylesheet" href="../styles/custom.css">
	<link rel="stylesheet" href="../font-awesome/css/all.min.css">
	<script src="https://checkout.stripe.com/v2/checkout.js"></script>
	<link rel="stylesheet" href="../styles/owl.carousel.css">
	<link rel="stylesheet" href="../styles/owl.theme.default.css">
	<script src="../js/jquery.slim.min.js"></script>
</head>
<body>
<?php include("../includes/user_header.php");?>
<div class="container-fluid view-proposals">
<div class="row">
<div class="col-md-12 mt-5 mb-3">
<h1 class="float-left">View Products</h1>
<label  class="float-right lead">Vacation Mode
<?php if ($login_seller_vacation=="off") { ?>	
<button id="turn_on_seller_vacation" class="btn btn-lg btn-toggle" data-toggle="button">
<div class="toggle-handle"></div>
</button>
<?php }else{ ?>
<button id="turn_off_seller_vacation" class="btn btn-lg btn-toggle active" data-toggle="button">
<div class="toggle-handle"></div>
</button>
<?php } ?>
</label>
<script>
$(document).ready(function(){
$(document).on('click','#turn_on_seller_vacation',function(){
seller_id = "<?php echo($login_seller_id); ?>";
$.ajax({
method: "POST",
url: "seller_vacation.php",
data: {seller_id: seller_id, turn_on: 'on'},	
}).done(function(){
$("#turn_on_seller_vacation").attr('id','turn_off_seller_vacation');
alert('You Seller Vacation Mode Has Been Turn On');	
});
});//end of the first toggle btn (TURN ON VARCATION) 

$(document).on('click','#turn_off_seller_vacation',function(){
seller_id="<?php echo($login_seller_id); ?>";
$.ajax({
method: "POST",
url: "seller_vacation.php",
data: {seller_id: seller_id, turn_off: 'off'},	
}).done(function(){
$("#turn_off_seller_vacation").attr('id','turn_off_seller_vacation');
alert('You Seller Vacation Mode Has Been Turn Off');	
});
});

});	
</script>
</div><!--col-md-12 mt-5 mb-3-->
<div class="col-md-12">
			<a href="create_proposal.php" class="btn btn-success float-right">Add New Product</a>
			<div class="clearfix"></div>
<ul class="nav nav-tabs mt-3">
<?php 
$select_proposals="SELECT * from proposals where proposal_seller_id='$login_seller_id' AND proposal_status='active'";
$run_proposals=mysqli_query($con,$select_proposals);
$count_proposals=mysqli_num_rows($run_proposals);

 ?>	
<li class="nav-item">
<a href="#active-proposals" data-toggle="tab" class="nav-link active">
Active 
<span class="badge badge-success"><?php echo $count_proposals; ?></span></a>
</li>
<?php 
$select_proposals="SELECT * from proposals where proposal_seller_id='$login_seller_id' AND proposal_status='pause'";
$run_proposals=mysqli_query($con,$select_proposals);
$count_proposals=mysqli_num_rows($run_proposals);

 ?>	
<li class="nav-item">
<a href="#pause-proposals" data-toggle="tab" class="nav-link">Paused <span class="badge badge-success"><?php echo $count_proposals; ?></span></a>
</li>
<?php 
$select_proposals="SELECT * from proposals where proposal_seller_id='$login_seller_id' AND proposal_status='pending'";
$run_proposals=mysqli_query($con,$select_proposals);
$count_proposals=mysqli_num_rows($run_proposals);

 ?>
<li class="nav-item">
<a href="#pending-proposals" data-toggle="tab" class="nav-link">Pending Approval<span class="badge badge-success"><?php echo($count_proposals); ?></span></a>
</li>
<?php 
$select_proposals="SELECT * from proposals where proposal_seller_id='$login_seller_id' AND proposal_status='modification'";
$run_proposals=mysqli_query($con,$select_proposals);
$count_proposals=mysqli_num_rows($run_proposals);

 ?>
<li class="nav-item">
<a href="#modification-proposals" data-toggle="tab" class="nav-link">Requests Modification <span class="badge badge-success"><?php echo($count_proposals); ?></span></a>
</li>
</ul>
<div class="tab-content">
<div id="active-proposals" class="tab-pane fade show active"><!--tab-pane fade show active-->
<div class="table-responsive box-table mt-4">
<table class="table table-hover">
<thead>
<tr>
<th>Product Title</th>
<th>Product Price</th>
<th>View</th>
<th>Orders</th>
<th>Actions</th>

</tr>
</thead>
<tbody>
<?php 
$select_proposals="SELECT * from proposals where proposal_seller_id='$login_seller_id' AND proposal_status='active'";
$run_proposals=mysqli_query($con,$select_proposals);
while ($row_proposals=mysqli_fetch_array($run_proposals)) {
$proposal_id =$row_proposals['proposal_id'];
$proposal_title=$row_proposals['proposal_title'];
$proposal_views =$row_proposals['proposal_views'];
$proposal_price =$row_proposals['proposal_price'];
$proposal_img1 =$row_proposals['proposal_id'];
$proposal_url =$row_proposals['proposal_url'];
$proposal_featured =$row_proposals['proposal_featured'];

$select_orders="SELECT * from orders where proposal_id='$proposal_id'";
$run_orders=mysqli_query($con,$select_orders);
$count_orders=mysqli_num_rows($run_orders);

 ?>	
<tr>
<td class="proposal-title"><?php echo $proposal_title; ?></td>
<td class="text-success">Ksh <?php echo $proposal_price; ?></td>
<td><?php echo $proposal_views; ?></td>
<td><?php echo $count_orders; ?></td>
<td>
<div class="dropdown">
<button class="btn btn-success dropdown-toggle" data-toggle="dropdown"></button>
<div class="dropdown-menu">
<a href="<?php echo $proposal_url; ?>" class="dropdown-item">Preview!</a>
<?php if($proposal_featured=="no"){ ?>											
<a href="#" class="dropdown-item" id="featured-button-<?php echo $proposal_id; ?>">Featured Listing!</a>
<?php }else{ ?>
<a href="#" class="dropdown-item bg-success active">Already Featured!</a>
<?php } ?>	
<a href="pause_proposal.php?proposal_id=<?php echo $proposal_id; ?>" class="dropdown-item">Pause!</a>										
<a href="edit_proposal.php?proposal_id=<?php echo $proposal_id; ?>" class="dropdown-item">Edit!</a>													
<a href="delete_proposal.php?proposal_id=<?php echo $proposal_id; ?>" class="dropdown-item">Delete!</a>											
</div><!--dropdown menu ends-->
</div><!--dropdown ends-->
<script>
	// javascript for the purpose of adding to featured
	$("#featured-button-<?php echo $proposal_id; ?>").click(function(){
		proposal_id="<?php echo $proposal_id; ?>";
		$.ajax({
			method:"POST",
			url: "pay_featured_listing.php",
			data:{proposal_id: proposal_id}
		})
		.done(function(data){
			$(".featured-proposal-modal").html(data);
		});
	}); 
</script>
									</td>
</tr>
<?php } ?>	
</tbody>						
</table>
					</div><!--div.table-responsive.box-table.mt-4-->
				</div><!--active-proposals tab-page fade show active  ends-->
<div id="pause-proposals" class="tab-pane fade show "><!--tab-pane fade show -->
					<div class="table-responsive box-table mt-4">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Products Title</th>
									<th>Products Price</th>
									<th>View</th>
									<th>Orders</th>
									<th>Actions</th>

								</tr>
							</thead>
<tbody>
<?php 
$select_proposals="SELECT * from proposals where proposal_seller_id='$login_seller_id' AND proposal_status='pause'";
$run_proposals=mysqli_query($con,$select_proposals);
while ($row_proposals=mysqli_fetch_array($run_proposals)) {
$proposal_id =$row_proposals['proposal_id'];
$proposal_title=$row_proposals['proposal_title'];
$proposal_views =$row_proposals['proposal_views'];
$proposal_price =$row_proposals['proposal_price'];
$proposal_img1 =$row_proposals['proposal_id'];
$proposal_url =$row_proposals['proposal_url'];
$proposal_featured =$row_proposals['proposal_featured'];

$select_orders="SELECT * from orders where proposal_id='$proposal_id'";
$run_orders=mysqli_query($con,$select_orders);
$count_orders=mysqli_num_rows($run_orders);

 ?>	
<tr>
<td class="proposal-title"><?php echo($proposal_title); ?></td>
<td class="text-success">Ksh <?php echo($proposal_price); ?></td>
<td><?php echo($proposal_views); ?></td>
<td><?php echo($count_orders); ?></td>
<td>
<div class="dropdown">
<button class="btn btn-success dropdown-toggle" data-toggle="dropdown"></button>
<div class="dropdown-menu">
<a href="<?php echo($proposal_url); ?>" class="dropdown-item">Preview!</a>		
<a href="activate_proposal.php?proposal_id=<?php echo($proposal_id); ?>" class="dropdown-item">Activate!</a>											
<a href="edit_proposal.php?proposal_id=<?php echo($proposal_id); ?>" class="dropdown-item">Edit!</a>													
<a href="delete_proposal.php?proposal_id=<?php echo($proposal_id); ?>" class="dropdown-item">Delete!</a>											
</div><!--dropdown menu ends-->
</div><!--dropdown ends-->
</td>
</tr>
<?php } ?>
</tbody>
</table>
					</div><!--div.table-responsive.box-table.mt-4-->
				</div><!--pause-proposals tab-page fade show ends-->
<div id="pending-proposals" class="tab-pane fade show "><!--tab-pane fade show -->
					<div class="table-responsive box-table mt-4">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Products Title</th>
									<th>Products Price</th>
									<th>View</th>
									<th>Orders</th>
									<th>Actions</th>

								</tr>
							</thead>
<tbody>
<?php 
$select_proposals="SELECT * from proposals where proposal_seller_id='$login_seller_id' AND proposal_status='pending'";
$run_proposals=mysqli_query($con,$select_proposals);
while ($row_proposals=mysqli_fetch_array($run_proposals)) {
$proposal_id =$row_proposals['proposal_id'];
$proposal_title=$row_proposals['proposal_title'];
$proposal_views =$row_proposals['proposal_views'];
$proposal_price =$row_proposals['proposal_price'];
$proposal_img1 =$row_proposals['proposal_id'];
$proposal_url =$row_proposals['proposal_url'];
$proposal_featured =$row_proposals['proposal_featured'];

$select_orders="SELECT * from orders where proposal_id='$proposal_id'";
$run_orders=mysqli_query($con,$select_orders);
$count_orders=mysqli_num_rows($run_orders);

 ?>		
<tr>
<td class="proposal-title"><?php echo($proposal_title); ?></td>
<td class="text-success">Ksh <?php echo($proposal_price); ?></td>
<td><?php echo($proposal_views); ?></td>
<td><?php echo($count_orders); ?></td>
<td>
<div class="dropdown">
<button class="btn btn-success dropdown-toggle" data-toggle="dropdown"></button>
<div class="dropdown-menu">
<a href="<?php echo($proposal_url); ?>" class="dropdown-item">Preview!</a>											
<a href="edit_proposal.php?proposal_id=<?php echo($proposal_id); ?>" class="dropdown-item">Edit!</a>													
<a href="delete_proposal.php?proposal_id=<?php echo($proposal_id); ?>" class="dropdown-item">Delete!</a>											
</div><!--dropdown menu ends-->
</div><!--dropdown ends-->
</td>
</tr>
<?php } ?>
</tbody>
</table>
</div><!--div.table-responsive.box-table.mt-4-->
</div><!--pending-proposals tab-page fade show ends-->
				
<div id="modification-proposals" class="tab-pane fade show "><!--tab-pane fade show -->
					<div class="table-responsive box-table mt-4">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Modification Products</th>
									<th>Modification Message</th>
									<th>Actions</th>

								</tr>
							</thead>
<tbody>
<?php 
$select_proposals="SELECT * from proposals where proposal_seller_id='$login_seller_id' AND proposal_status='modification'";
$run_proposals=mysqli_query($con,$select_proposals);
while ($row_proposals=mysqli_fetch_array($run_proposals)) {
$proposal_id =$row_proposals['proposal_id'];
$proposal_title=$row_proposals['proposal_title'];
$proposal_url =$row_proposals['proposal_url'];
$select_modification="SELECT * from proposal_modifications where proposal_id='$proposal_id'";
$run_modification=mysqli_query($con,$select_modification);
$row_modification=mysqli_fetch_array($run_modification);
$modification_message=$row_modification['modification_message'];
 ?>		
<tr>
<td class="proposal-title"><?php echo($proposal_title); ?></td>
<td><?php echo($modification_message); ?></td>
<td>
<div class="dropdown">
<button class="btn btn-success dropdown-toggle" data-toggle="dropdown"></button>
<div class="dropdown-menu">
<a href="submit_approval.php?proposal_id=<?php echo($proposal_id); ?>" class="dropdown-item">Submit for Approval</a>
<a href="<?php echo($proposal_url); ?>" class="dropdown-item">Preview!</a>											
<a href="edit_proposal.php?proposal_id=<?php echo($proposal_id); ?>" class="dropdown-item">Edit!</a>													
<a href="delete_proposal.php?proposal_id=<?php echo($proposal_id); ?>" class="dropdown-item">Delete!</a>											
</div><!--dropdown menu ends-->
</div><!--dropdown ends-->
</td>
</tr>
<?php } ?>
</tbody>
</table>
</div><!--div.table-responsive.box-table.mt-4-->
</div><!--modification-proposals tab-page fade show ends-->
</div><!--tab-content-->
</div>
</div>
</div><!--container-fluid view-proposals-->
<div class="featured-proposal-modal">
	
</div><!--enable the payment method where for feature occures-->
<?php include("../includes/footer.php");?>
</body>
</html>