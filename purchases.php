<?php 
session_start();
include("includes/db.php");
if (!isset($_SESSION['seller_user_name'])) {
echo "<script>window.open('login.php','_self');</script>";
}
//get the buyer/user deals to create the session
$login_seller_user_name=$_SESSION['seller_user_name'];
$select_login_seller="SELECT * from sellers where seller_user_name='$login_seller_user_name'";
$run_login_seller=mysqli_query($con,$select_login_seller);
$row_login_seller=mysqli_fetch_array($run_login_seller);
$login_seller_id=$row_login_seller['seller_id'];
$login_seller_paypal_email=$row_login_seller['seller_paypal_email'];
 ?>

<!DOCTYPE html>
<html >
<head>
	<title>Purchases</title>
	<meta charset="UTF-8">
	<meta name="keywords" content="jua kali products">
	<meta name="author" content="Juakali Mall">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100" rel="stylesheet" >
	<link rel="stylesheet" href="styles/bootstrap.min.css">
	<link rel="stylesheet" href="styles/style.css">
	<link rel="stylesheet" href="styles/category_nav_style.css">
	<!-- Custome css from the user -->
	<link rel="stylesheet" href="styles/custom.css">
	<link rel="stylesheet" href="font-awesome/css/all.min.css">
	
	<script src="js/jquery.slim.min.js"></script>
</head>
<body>
<?php include("includes/user_header.php");?>
<div class="container">
<div class="row">
<div class="col-md-12 mt-5">
<h1 class="mb-4">Purchases</h1>
<div class="table responsive box-table">
<table class="table table-hover table-condensed table-striped">
<thead>
<tr>
<th>Date</th>
<th>For</th>
<th>Amount</th>
</tr>
</thead>
<tbody>
<?php 

$get_purchases="SELECT * from purchases where seller_id='$login_seller_id' order by 1 DESC";
$run_purchases=mysqli_query($con,$get_purchases);
while ($row_purchses=mysqli_fetch_array($run_purchases)) {
$order_id=$row_purchses['order_id'];
$amount=$row_purchses['amount'];
$date=$row_purchses['date'];
$method=$row_purchses['method'];


 ?>	
<tr>
<td><?php echo $date; ?></td>	
<td>
<?php if($method=="shopping_balance"){ ?>
Product Purchased From Shopping Balance!
<a href="order-details.php?order_id=<?php echo($order_id); ?>">(View Order)</a>
<?php }elseif($method=="stripe") { ?>
Deposited from Credit Card/Stripe!
<a href="order-details.php?order_id=<?php echo($order_id); ?>">(View Order)</a>
<?php }elseif ($method=="paypal") { ?>
 Payment from purchase / paypal	
<a href="order-details.php?order_id=<?php echo($order_id); ?>">(View Order)</a>	
<?php }elseif ($method=="order_cancellation") { ?>
Cancelled Order Payment Has been Refunded to Your Shopping Balance!	
<a href="order-details.php?order_id=<?php echo($order_id); ?>">(View Order)</a>	
<?php } ?>
</td>	
<td class="text-danger">
<?php 
if ($method=="order_cancellation") {
echo "+Kshs $amount.00";	
}else{
echo "-Kshs $amount.00";		
}
 ?>


</td>	
</tr>
<?php } ?>
</tbody>
</table><!--table table-hover ends-->
</div><!--table responsive box-table ends-->
</div><!--col-md-12 mt-5 ends-->
</div><!--row ends-->	
</div><!--container ends-->
<?php include("includes/footer.php");?>
</body>
</html>