<?php 
session_start();
include("../includes/db.php");
if (!isset($_SESSION['seller_user_name'])) {
echo "<script>window.open('../login.php','_self');</script>";
}
//get the buyer/user details to create the session
$login_seller_user_name=$_SESSION['seller_user_name'];
$select_login_seller="SELECT * from sellers where seller_user_name='$login_seller_user_name'";
$run_login_seller=mysqli_query($con,$select_login_seller);
$row_login_seller=mysqli_fetch_array($run_login_seller);
$login_seller_id=$row_login_seller['seller_id'];

 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	<title>Manage Requests</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="JuaKali Mall">
	<link href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100" rel="stylesheet" >
	<link rel="stylesheet" href="../styles/bootstrap.min.css">
	<link rel="stylesheet" href="../styles/style.css">
	<link rel="stylesheet" href="../styles/user_nav_style.css">
		<!-- Custome css from the user -->
	<link rel="stylesheet" href="../styles/custom.css">
	<link rel="stylesheet" href="../font-awesome/css/all.min.css">
	<script src="../js/jquery.slim.min.js"></script>
</head>
<body>
<?php include("../includes/user_header.php");?>
<div class="container-fluid mt-5">
<div class="row">
<div class="col-md-12 mb-4 mt-3">
	<h1 class="float-left">Manage Request</h1>
	<a href="post_request.php" class="btn btn-success float-right">Post New Request</a>
</div><!--col-ms-12 mb-4 ends-->
<div class="col-md-12">
<ul class="nav nav-tabs mt-3">
<?php 
$get_requests="SELECT * from buyer_requests where seller_id='$login_seller_id' AND request_status='active'"; 
$run_requests=mysqli_query($con,$get_requests);
$count_requests=mysqli_num_rows($run_requests);
 ?>	
<li class="nav-item">
<a href="#active" data-toggle="tab" class="nav-link active">Active <span class="badge badge-success"><?php echo($count_requests); ?></span></a>
</li>
<?php 
$get_requests="SELECT * from buyer_requests where seller_id='$login_seller_id' AND request_status='pause'"; 
$run_requests=mysqli_query($con,$get_requests);
$count_requests=mysqli_num_rows($run_requests);
 ?>
<li class="nav-item">
<a href="#pause" data-toggle="tab" class="nav-link">Paused <span class="badge badge-success"><?php echo($count_requests); ?></span></a>
</li>
<?php 
$get_requests="SELECT * from buyer_requests where seller_id='$login_seller_id' AND request_status='pending'"; 
$run_requests=mysqli_query($con,$get_requests);
$count_requests=mysqli_num_rows($run_requests);
 ?>
<li class="nav-item">
<a href="#pending" data-toggle="tab" class="nav-link">Pending <span class="badge badge-success"><?php echo($count_requests); ?></span></a>
</li>
<?php 
$get_requests="SELECT * from buyer_requests where seller_id='$login_seller_id' AND request_status='unapproved'"; 
$run_requests=mysqli_query($con,$get_requests);
$count_requests=mysqli_num_rows($run_requests);
 ?>
<li class="nav-item">
<a href="#unapproved" data-toggle="tab" class="nav-link">Unapproved <span class="badge badge-success"><?php echo($count_requests); ?></span></a>
</li>
</ul><!--nav nav-tabs mt-3 ends-->
<div class="tab-content mt-4">
<div id="active" class="tab-pane fade show active">
<div class="table-responsive box-table">
 <table class="table table-hover">
 <thead>
 <tr>
<th>Title</th> 	
<th>Description</th> 	
<th>Date</th> 	
<th>Offers</th> 	
<th>Budget</th> 	
<th>Actions</th> 	
 </tr>	
 </thead>	
 <tbody>
 <?php 
 $get_requests="SELECT * from buyer_requests where seller_id='$login_seller_id' AND request_status='active' order by 1 DESC";
$run_requests=mysqli_query($con,$get_requests);
while($row_requests=mysqli_fetch_array($run_requests)){
	$request_id=$row_requests['request_id'];
	$request_title=$row_requests['request_title'];
	$request_description=$row_requests['request_description'];
	$request_date=$row_requests['request_date'];
	$request_budget=$row_requests['request_budget'];
	
// copy from buyer_request.php
$select_offers="SELECT * from send_offers where request_id='$request_id' AND status='active'";
$run_offers=mysqli_query($con,$select_offers);
$count_offers=mysqli_num_rows($run_offers);	   	
  ?>	
 <tr>
 <td><?php echo($request_title); ?></td>
 <td> <?php echo($request_description); ?></td>
 <td><?php echo($request_date); ?></td>
 <td><?php echo($count_offers); ?></td>
 <td class="text-success"> Ksh <?php echo($request_budget); ?></td>
 <td>
 <div class="dropdown">
 <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown"></button>
<div class="dropdown-menu">
<a href="view_offers.php?request_id=<?php echo($request_id); ?>" target="_blank" class="dropdown-item">View Offers</a>
<a href="pause_request.php?request_id=<?php echo($request_id); ?>"  class="dropdown-item">Pause</a>
<a href="delete_request.php?request_id=<?php echo($request_id); ?>" class="dropdown-item">Delete</a>
</div><!--dropdown-menu ends-->
 	</div><!--dropdown ends-->
 </td>
 </tr>
<?php } ?>
 </tbody>
 </table><!--table table-hover ends-->			
</div>
</div><!--tab-pane fade show active ends-->

<div id="pause" class="tab-pane fade">
<div class="table-responsive box-table">
 <table class="table table-hover">
 <thead>
 <tr>
<th>Title</th> 	
<th>Description</th> 	
<th>Date</th> 	
<th>Offers</th> 	
<th>Budget</th> 	
<th>Actions</th> 	
 </tr>	
 </thead>	
 <tbody>
  <?php 
 $get_requests="SELECT * from buyer_requests where seller_id='$login_seller_id' AND request_status='pause' order by 1 DESC";
$run_requests=mysqli_query($con,$get_requests);
while($row_requests=mysqli_fetch_array($run_requests)){
	$request_id=$row_requests['request_id'];
	$request_title=$row_requests['request_title'];
	$request_description=$row_requests['request_description'];
	$request_date=$row_requests['request_date'];
	$request_budget=$row_requests['request_budget'];
	
// copy from buyer_request.php
$select_offers="SELECT * from send_offers where request_id='$request_id' AND status='active'";
$run_offers=mysqli_query($con,$select_offers);
$count_offers=mysqli_num_rows($run_offers);	   	
  ?>	
 <tr>
 <td><?php echo($request_title); ?></td>
 <td><?php echo($request_description); ?></td>
 <td><?php echo($request_date); ?></td>
 <td><?php echo($count_offers); ?></td>
 <td class="text-success"> Ksh <?php echo($request_budget); ?></td>
 <td>
 <div class="dropdown">
 <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown"></button>
<div class="dropdown-menu">
<a href="active_request.php?request_id=<?php echo($request_id); ?>"  class="dropdown-item">Activate</a>
<a href="delete_request.php?request_id=<?php echo($request_id); ?>" class="dropdown-item">Delete</a>
</div><!--dropdown-menu ends-->
 	</div><!--dropdown ends-->
 </td>
 </tr>
<?php } ?>
 </tbody>
 </table><!--table table-hover ends-->			
</div>
</div><!--tab-pane fade pause ends-->

<div id="pending" class="tab-pane fade ">
<div class="table-responsive box-table">
 <table class="table table-hover">
 <thead>
 <tr>
<th>Title</th> 	
<th>Description</th> 	
<th>Date</th> 	
<th>Offers</th> 	
<th>Budget</th> 	
<th>Actions</th> 	
 </tr>	
 </thead>	
 <tbody>
 <?php 
 $get_requests="SELECT * from buyer_requests where seller_id='$login_seller_id' AND request_status='pending' order by 1 DESC";
$run_requests=mysqli_query($con,$get_requests);
while($row_requests=mysqli_fetch_array($run_requests)){
	$request_id=$row_requests['request_id'];
	$request_title=$row_requests['request_title'];
	$request_description=$row_requests['request_description'];
	$request_date=$row_requests['request_date'];
	$request_budget=$row_requests['request_budget'];
	
   	
  ?>		
 <tr>
 <td><?php echo($request_title); ?></td>
 <td><?php echo($request_description); ?></td>
 <td><?php echo($request_date); ?></td>
 <td>0</td>
 <td class="text-success"> Ksh <?php echo($request_budget); ?></td>
 <td>
<a href="delete_request.php?request_id=<?php echo($request_id); ?>" class="btn btn-danger">Delete</a>
 </td>
 </tr>
<?php } ?>
 </tbody>
 </table><!--table table-hover ends-->			
</div>
</div><!--tab-pane fade pending ends-->
<div id="unapproved" class="tab-pane fade">
<div class="table-responsive box-table">
 <table class="table table-hover">
 <thead>
 <tr>
<th>Title</th> 	
<th>Description</th> 	
<th>Date</th> 	
<th>Offers</th> 	
<th>Budget</th> 	
<th>Actions</th> 	
 </tr>	
 </thead>	
 <tbody>
   <?php 
 $get_requests="SELECT * from buyer_requests where seller_id='$login_seller_id' AND request_status='unapproved' order by 1 DESC";
$run_requests=mysqli_query($con,$get_requests);
while($row_requests=mysqli_fetch_array($run_requests)){
	$request_id=$row_requests['request_id'];
	$request_title=$row_requests['request_title'];
	$request_description=$row_requests['request_description'];
	$request_date=$row_requests['request_date'];
	$request_budget=$row_requests['request_budget'];
	
// copy from buyer_request.php
$select_offers="SELECT * from send_offers where request_id='$request_id' AND status='active'";
$run_offers=mysqli_query($con,$select_offers);
$count_offers=mysqli_num_rows($run_offers);	   	
  ?>	
 <tr>
 <td><?php echo($request_title); ?></td>
 <td><?php echo($request_description); ?></td>
 <td><?php echo($request_date); ?></td>
 <td>0</td>
 <td class="text-success"> Ksh <?php echo($request_budget); ?></td>
 <td>
<a href="delete_request.php?request_id
=<?php echo($request_id); ?>" class="btn btn-danger">Delete</a>
 </td>
 </tr>
<?php } ?>
 </tbody>
 </table><!--table table-hover ends-->			
</div>
</div><!--tab-pane fade unapproved ends-->
</div><!--tab-content mt-4 ends-->
</div>		
</div><!--row ends-->
</div><!--container-fluid ends-->
<?php include("../includes/footer.php");?>
</body>
</html>