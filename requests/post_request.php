<?php 
session_start();
include("../includes/db.php");
if (!isset($_SESSION['seller_user_name'])) {
echo "
<script>
window.open('../login.php','_self');
</script>
";
}

$login_seller_user_name=$_SESSION['seller_user_name'];
$select_login_seller="SELECT* from sellers where seller_user_name='$login_seller_user_name'";
$run_login_seller=mysqli_query($con,$select_login_seller);
$row_login_seller=mysqli_fetch_array($run_login_seller);
$login_seller_id=$row_login_seller['seller_id'];

 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	<title>Post A New Request</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="ReachComputers">
	<link href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100" rel="stylesheet" >
	<link rel="stylesheet" href="../styles/bootstrap.min.css">
	<link rel="stylesheet" href="../styles/style.css">
	<link rel="stylesheet" href="../styles/user_nav_style.css">
		<!-- Custome css from the user -->
	<link rel="stylesheet" href="../styles/custom.css">
	<link rel="stylesheet" href="../font-awesome/css/all.min.css">
	<script src="../js/jquery.slim.min.js"></script>
</head>
<body>
<?php include("../includes/user_header.php");?>
<div class="container-fluid mt-5 mb-5">
<div class="row">
<div class="col-lg-9 col-md-11">
<h1 class="mb-4">Post A New Request To The Expert/Seller Community</h1>
<div class="card rounded-0 ">
<div class="card-body">
<form method="post" enctype="multipart/form-data">
<div class="row">
<div class="col-md-2 d-md-block d-none">
<i class="fas fa-edit fa-4x"></i>	
</div><!--col-md-2 b-md-block d-none ends-->
<div class="col-md-10 col-sm-12">
<div class="row">
<div class="col-lg-8">
<div class="form-group">
<input type="text" name="request_title" placeholder="Request Title"  class="form-control input-lg" required>
</div><!--form-group ends-->	
<div class="form-group">
<textarea name="request_description" id="textarea" cols="30" rows="5" maxlength="380" placeholder="Request Descriptions" class="form-control" required></textarea>
</div><!--form-group ends-->
<div class="form-group">
<input type="file" name="request_file" id="file">
<div class="font-weight-bold float-right">
<span class="count"> 0 </span>/380 Max	
</div>
</div><!--form-group ends-->
</div><!--col-lg-8 ends-->		
</div><!-- inner row end -->
</div><!--col-md-10 col-sm-12 ends-->
</div><!-- parent row ends -->	
<hr class="card-hr">
<h5>Chose A Category</h5>
<div class="row mb-2">
<div class="col-md-2 d-md-block d-none">
<i class="fa fa-folder-open fa-4x"></i>
</div><!--col-md-2 d-md-block d-none ends-->
<div class="col-md-10 col-sm-12">
<div class="row">
<div class="col-md-4 mb-2">
<select name="cat_id" id="category" class="form-control" required>
<option value="" class="hidden">Select A Category</option>
<?php 
$get_cats="SELECT * from categories";
$run_cats=mysqli_query($con,$get_cats);
while ($row_cats=mysqli_fetch_array($run_cats)) {
$cat_id=$row_cats['cat_id'];	
$cat_title=$row_cats['cat_title'];	

 ?>
<option value="<?php echo $cat_id; ?>"><?php echo $cat_title; ?></option>
<?php } ?>
</select>
</div><!--col-md-4 mb-2 ends-->	
<div class="col-md-4 mb-2">
<select name="child_id" id="sub-category" class="form-control" required>
<option value="" class="hidden">Select A Sub Category</option>
		
</select>
</div><!--col-md-4 mb-2 ends-->	
</div>
</div><!--col-mb-10 col-sm-12 ends-->	
	
</div><!--row mb-2 ends-->
<hr class="card-hr">
<h5>One You place Your Order, How long ccould it take</h5>
<div class="row mb-4">
<div class="col-md-1 d-md-block d-none">
<i class="far fa-clock fa-4x"></i>
</div><!--col-md-1 d-md-block d-none ends-->
<div class="col-md-11 col-sm-12 mt-3">
<?php 
$get_delivery_times="SELECT * from delivery_times";
$run_delivery_times=mysqli_query($con,$get_delivery_times);
while ($row_delivery_times=mysqli_fetch_array($run_delivery_times)) {	
$delivery_proposal_title=$row_delivery_times['delivery_proposal_title'];

 ?>	
<label class="custom-control custom-radio custom-control-inline">
<input type="radio" value="<?php echo($delivery_proposal_title); ?>" name="delivery_time" class="custom-control-input" request>
<span class="custom-control-label"><?php echo($delivery_proposal_title); ?></span>		
</label>
<?php } ?>	
</div><!--col-md-11 col-sm-12 mt-3-->
</div><!--row mb-4 ends-->
<hr class="card-hr">
<h5>What is Your Budget For this services</h5>
<div class="col-md-4 mb-2">
<div class="input-group">
<span class="input-group-prepend font-weight-bold"> <span class="input-group-text">Ksh </span></span>
<input type="number" name="request_budget" min="100" placeholder="100 Minimum" class="form-control input-lg">		
</div><!--input-group ends-->
</div><!--col-md-4 mb-2 ends-->
<input type="submit" value="Post Request" name="submit" class="btn btn-outline-success btn-lg float-right">
</form>	
</div><!--card-body ends-->	
</div><!--card-rounded-0 ends-->	
</div><!--col-lg-9.col-md-11 ends-->		
</div><!--row ends-->
</div><!--container-fluid mt-5 mb-5-->
<script>
	$(document).ready(function(){
		//jquery to get number values from the textarea
		$("#textarea").keydown(function(){
			var textarea =$("#textarea").val();
			$(".count").text(textarea.length);
		});
		//hode and show sub-category only on clicks.
		$("#sub-category").hide();
		$("#category").change(function(){
		$("#sub-category").show();
//now get the sub-category relevant to the category...
var category_id = $(this).val();
$.ajax({
url:"fetch_subcategory.php",
method: "POST",
data:{category_id:category_id},
success:function(data){
$("#sub-category").html(data);	
}
});	
		});
	});
</script>
<?php 
//code to upload to database as a request
if (isset($_POST['submit'])) {
$request_title=mysqli_real_escape_string($con,$_POST['request_title']);	
$request_description=mysqli_real_escape_string($con,$_POST['request_description']);	
$cat_id=mysqli_real_escape_string($con,$_POST['cat_id']);	
$child_id=mysqli_real_escape_string($con,$_POST['child_id']);	
$request_budget=mysqli_real_escape_string($con,$_POST['request_budget']);	
$delivery_time=mysqli_real_escape_string($con,$_POST['delivery_time']);	
$request_file=$_FILES['request_file']['name'];
$request_file_tmp=$_FILES['request_file']['tmp_name'];
$request_date= date("F d, Y");
move_uploaded_file($request_file_tmp, "request_files/$request_file");
$insert_request="INSERT INTO buyer_requests (seller_id,cat_id,child_id,request_title,request_description,request_file,delivery_time,request_budget,request_date,request_status) values ('$login_seller_id','$cat_id','$child_id','$request_title','$request_description','$request_file','$delivery_time','$request_budget','$request_date','pending') ";

$run_request=mysqli_query($con,$insert_request);
if ($run_request) {
	echo "<script>alert('Request Added Successfully!');</script>";
	echo "<script>window.open('manage_requests.php','_self');</script>";
}

}
 ?> 
<?php include("../includes/footer.php");?>
</body>
</html>