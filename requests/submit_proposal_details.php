<?php 
session_start();
include("../includes/db.php");
if (!isset($_SESSION['seller_user_name'])) {
echo "
<script>
window.open('../login.php','_self');
</script>
";
}
$login_seller_user_name=$_SESSION['seller_user_name'];
$select_login_seller="SELECT * from sellers where seller_user_name='$login_seller_user_name'";
$run_login_seller=mysqli_query($con,$select_login_seller);
$row_login_seller=mysqli_fetch_array($run_login_seller);
$login_seller_id=$row_login_seller['seller_id'];
$proposal_id=$_POST['proposal_id'];
$request_id=$_POST['request_id'];

$get_requests="SELECT * from buyer_requests where request_id='$request_id'";
$run_requests=mysqli_query($con,$get_requests);
$row_requests=mysqli_fetch_array($run_requests);
$request_title=$row_requests['request_title'];
$request_description=$row_requests['request_description'];
$request_seller_id=$row_requests['seller_id'];

$select_request_seller="SELECT * from sellers where seller_id='$request_seller_id'";
$run_request_seller=mysqli_query($con,$select_request_seller);
$row_request_seller=mysqli_fetch_array($run_request_seller);
$request_seller_image=$row_request_seller['seller_image'];

$get_proposals="SELECT * from proposals where proposal_status='active' AND proposal_seller_id='$login_seller_id' AND proposal_id='$proposal_id'";
$run_proposals=mysqli_query($con,$get_proposals);
$row_proposals=mysqli_fetch_array($run_proposals);
$proposal_title=$row_proposals['proposal_title'];
 ?>

<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title h5">Specify your Product Details</h5><button class="close" data-dismiss="modal">&times;</button>
</div>
<div class="modal-body p-0">
<div class="request-summary">
<?php if (!empty($request_seller_image)) { ?>
<img src="<?php echo($site_url);?>/user_images/<?php echo($request_seller_image); ?>" width="50" height="50" class="rounded-circle">
<?php }else{ ?>
<img src="<?php echo($site_url);?>/user_images/empty-image.png" width="50" height="50" class="rounded-circle">
<?php } ?>
<div id="request-description">
<h6 class="text-primary mb-1"><?php echo($request_title); ?></h6>
<p><?php echo($request_description); ?></p>
</div>
</div>
<form id="proposal-details-form">
	<div class="selected-proposal p-3">
		<h5><?php echo($proposal_title); ?></h5>
		<hr>
		<input type="hidden" name="proposal_id" value="<?php echo($proposal_id); ?>">
		<input type="hidden" name="request_id" value="<?php echo($request_id); ?>">
		<div class="form-group">
			<label class="font-weight-bold">Description :</label>
			<textarea name="description" class="form-control" required></textarea>
		</div>
		<hr>
		<div class="form-group">
			<label class="font-weight-bold">Delivery Time :</label>
			<select name="delivery_time" class="form-control float-right" >
<?php 
$get_delivery_times="SELECT * from delivery_times"; 
$run_delivery_times=mysqli_query($con,$get_delivery_times);
while ($row_delivery_time=mysqli_fetch_array($run_delivery_times)) {
	$delivery_proposal_title=$row_delivery_time['delivery_proposal_title'];
	echo "<option value='$delivery_proposal_title'>$delivery_proposal_title</option>";

}
 ?>
			</select>
		</div>
		<hr>
		<div class="form-group">
			<label class="font-weight-bold">Total Offer Amount :</label>
			<div class="input-group float-right">
				<span class="input-group-prepend">
					<span class="input-group-text">Ksh</span>
				</span>
				<input type="number" name="amount" min="100" placeholder="100 Minimum" class="form-control">
			</div>
		</div>
	
	</div>
	<div class="modal-footer">
		<button class="btn btn-secondary" type="button" data-dismiss="modal" data-toggle="modal" data-target="#send-offer-modal">
			Back
		</button>
		<button class="btn btn-success" type="submit">Submit Offer</button>
	</div>
</form>
	</div>
	
</div>
<div id="insert_offer"> </div>
	<script>
$(document).ready(function(){
$("#proposal-details-form").submit(function(event){
event.preventDefault();
//request to send data
$.ajax({
method: "POST",
url: "<?php echo($site_url);?>/requests/insert_offer.php",
data: $('#proposal-details-form').serialize()
})
.done(function(data){
$("#submit-proposal-details").modal('hide');
$("#insert_offer").html(data);
});
});
});
	</script>





