<?php 
session_start();
include("includes/db.php");
if (!isset($_SESSION['seller_user_name'])) {
echo "<script>window.open('login.php','_self');</script>";
}
//get the buyer/user deals to create the session
$login_seller_user_name=$_SESSION['seller_user_name'];
$select_login_seller="SELECT * from sellers where seller_user_name='$login_seller_user_name'";
$run_login_seller=mysqli_query($con,$select_login_seller);
$row_login_seller=mysqli_fetch_array($run_login_seller);
$login_seller_id=$row_login_seller['seller_id'];
$login_seller_paypal_email=$row_login_seller['seller_paypal_email'];

//for the payment processing
$get_payment_setting="SELECT * from payment_settings";
$run_payment_setting=mysqli_query($con,$get_payment_setting);
$row_payament_setting=mysqli_fetch_array($run_payment_setting);
$withdrawal_limit=$row_payament_setting['withdrawal_limit'];

 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	
<title>Revenues</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="author" content="Juakali Mall">
<link href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100" rel="stylesheet" >
<link rel="stylesheet" href="styles/bootstrap.min.css">
<link rel="stylesheet" href="styles/style.css">
<!-- Custome css from the user -->
	<link rel="stylesheet" href="styles/custom.css">
<link rel="stylesheet" href="styles/user_nav_style.css">
<link rel="stylesheet" href="font-awesome/css/all.min.css">
	
<script src="js/jquery.slim.min.js"></script>
</head>
<body>
<div class="preloader d-flex justify-content-center align-items-center">
    <img src="images/loadjuakali.gif" alt="the preloader"><br>
    <p>Loading...Please wait</p>
  </div>	
<?php include("includes/user_header.php");?>
<div class="container">
<div class="row">
<div class="col-md-12 mb-3 mt-5">
<h1 class="float-left">Revenues</h1>
<?php 

//to update the seller account amount 
$get_seller_accounts="SELECT * from seller_accounts where seller_id='$login_seller_id'";
$run_seller_accounts=mysqli_query($con,$get_seller_accounts);
$row_seller_accounts=mysqli_fetch_array($run_seller_accounts);
$current_balance=$row_seller_accounts['current_balance'];
$used_purchases=$row_seller_accounts['used_purchases'];
$pending_cleanrance=$row_seller_accounts['pending_cleanrance'];
$withdrawn=$row_seller_accounts['withdrawn'];


 ?>
	<?php 
if ($current_balance >= $withdrawal_limit) {
 ?>
<p class="float-right">Available for Withdrwal: <span class="font-weight-bold">Ksh <?php echo($current_balance); ?></span></p>
<?php }else{ ?>
<p class="float-right text-danger">Unpaid Debt: <span class="font-weight-bold">Ksh <?php echo($current_balance); ?></span></p>
<?php } ?>	
</div><!--col-md-12 mb-3 mt-5 ends-->	
<div class="col-md-12">
<div class="card mb-3 rounded-0">
<div class="card-body">
<div class="row">
<div class="col-md-3 text-center border-box">
	<p>Withdrwals</p>
	<h2>Ksh <?php echo($withdrawn); ?></h2>
</div><!--col-md-3 text-center border-box-->
<div class="col-md-3 text-center border-box">
	<p>Used To Order Products</p>
	<h2>Ksh <?php echo($used_purchases); ?></h2>
</div><!--col-md-3 text-center border-box-->
<div class="col-md-3 text-center border-box">
	<p>Pedding Clearance</p>
	<h2>Ksh <?php echo($pending_cleanrance); ?></h2>
</div><!--col-md-3 text-center border-box-->
<div class="col-md-3 text-center border-box">
	<p>Available Income</p>
	<?php 
if ($current_balance >= $withdrawal_limit) {
 ?>
	<h2>Ksh <?php echo($current_balance); ?></h2>
<?php }else{ ?>
<h2>Kshs 0</h2>	
<?php } ?>
</div><!--col-md-3 text-center border-box-->
</div>	
</div><!--card-body ends-->	
</div><!--card mb-3 rounded-0 ends-->	
<label  class="lead float-left mt-1">Withdraw:</label>
<?php 
if ($current_balance >= $withdrawal_limit) {
	
 ?>
<button class="btn btn-outline-success ml-2" data-toggle="modal" data-target="#paypal_withdraw_modal">
<i class="fab fa-paypal"></i>&nbsp; Cash withdrawal	
</button>
<?php }else{ ?>

<button class="btn btn-outline-success ml-2" onclick="return alert('You Must Have A Minimum Available Kshs <?php echo($withdrawal_limit); ?> To Withdrwal!');">
<i class="fab fa-paypal"></i>&nbsp;Cash Withdrawal	
</button>

<?php } ?>	
<div class="table-responsive box-table mt-4">
<table class="table table-hover">
<thead>
<tr>
<th>Purchases Date </th>	
<th>For Order</th>	
<th>Amount Earned</th>	
</tr>
</thead>
<tbody>
<?php 
$get_revenues="SELECT * from revenues where seller_id='$login_seller_id' order by 1 DESC";
$run_revenues=mysqli_query($con,$get_revenues);
while ($row_revenues=mysqli_fetch_array($run_revenues)) {
$revenue_id=$row_revenues['revenue_id'];	
$order_id=$row_revenues['order_id'];	
$amount=$row_revenues['amount'];	
$date=$row_revenues['date'];	
$status=$row_revenues['status'];	
	

 ?>	
<tr>
<td><?php echo($date); ?></td>

<td>
<?php if($status=="pending"){ ?>
Order Revenue Pending Clearance (<a href="order_details.php?order_id=<?php echo($order_id); ?>" target="_blank">View Order</a>)
<?php }else{ ?>	

Order Revenue (<a href="order_details.php?order_id=<?php echo($order_id); ?>" target="_blank">View Order</a>)
<?php } ?>
</td>
<td class="text-success">+ Ksh <?php echo($amount); ?></td>
</tr>
<?php } ?>
</tbody>		
</table>
</div><!--table-responsive box-table mt-4 ends-->
</div><!--col-md-12 ends-->
</div><!--row ends-->	
</div><!--container ends-->
<div id="paypal_withdraw_modal" class="modal fade">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title">Withdrw Revenues from Your Account</h5>	
<button class="close" data-dismiss="modal">
	<span>&times;</span>
</button>
</div><!--modal-header ends-->
<div class="modal-body">
<center>
<?php if (empty($login_seller_paypal_email)) {

 ?>	
<p class="lead">
For withdraw Revenues To Your add payment token or contact customer support
<a href="<?php echo($site_url); ?>/settings.php?account_settings">Setting Account Action Tab!</a>
</p>
<?php }else{ ?>
Your Revenues Will be withdraw:
<?php 
function generateRandomString($length = 10) {
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	return $randomString;
}
 ?>
<br> <strong>Receipt NO. <?php echo(strtoupper(generateRandomString())); //$login_seller_paypal_email); ?></strong>
<form action="paypal_adaptive.php" method="post">
<div class="form-group row">
<label for="" class="col-md-3 col-form-label font-weight-bold">Enter Amount</label>
<div class="col-md-8">
<div class="input-group">
	<span class="input-group-pepend font-weight-bold">
<span class="input-group-text">Ksh</span>
	</span>
<input type="number" name="amount" class="form-control input-lg" min="<?php echo($withdrawal_limit); ?>" max="<?php echo($current_balance); ?>" placeholder="<?php echo($withdrawal_limit); ?> minimum" required>
<input type="hidden" name="receipt_no" value="<?php echo(strtoupper(generateRandomString()));?>">
</div>			
</div>
</div><!--form-group row ends-->
<div class="form-group-row">
	<div class="col-md-8 offset-md-3">
	<input type="submit" name="withdraw" value="Withdraw" class="btn btn-success form-control">	
	</div>
</div><!--form-group row ends-->
</form><!--form ends-->
<?php } ?>		
</center>
</div><!--modal-body ends-->
<div class="modal-footer">
	<button class="btn btn-default" data-dismiss="modal">Close</button>
</div><!--modal-footer ends-->		
</div><!--modal-content ends-->	
</div><!--modal-dialog ends-->
</div><!--paypal_withdrwa_modal ends-->
<?php include("includes/footer.php");?>
</body>
</html>