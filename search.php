<?php
session_start();
include("includes/db.php");
include("functions/functions.php");

?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Search Result for <?php echo @$_SESSION['search_query']; ?></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="Jua kali mall">
	<link href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100" rel="stylesheet" >
	<link rel="stylesheet" href="styles/bootstrap.min.css">
	<link rel="stylesheet" href="styles/style.css">
	<link rel="stylesheet" href="styles/category_nav_style.css">
	<!-- Custome css from the user -->
	<link rel="stylesheet" href="styles/custom.css">
	<link rel="stylesheet" href="font-awesome/css/all.min.css">
	<script src="js/jquery.slim.min.js"></script>
</head>
<body class="bg-white">
<div class="preloader d-flex justify-content-center align-items-center">
    <img src="images/loadjuakali.gif" alt="the preloader"><br>
    <p>Loading...Please wait</p>
  </div>	
<?php include("includes/header.php");?>
	
<div class="container-fluid mt-5">
	<div class="row">
		<div class="col-md-12">
			<center>
<h1>Search Reaults</h1>
<p class="lead">"<?php echo @$_SESSION['search_query']; ?>"</p>
</center>
<hr>
</div>
</div>
<div class="row mt-3">
<div class="col-lg-3 col-md-4 col-sm-12 display-small-none">
<?php include("includes/search_sidebar.php")?>
</div><!--col-lg-3 col-md-4 col-sm-12-->
<div class="col-lg-9 col-md-8 col-sm-12">
<div class="row flex-wrap" id="search_proposals">
<?php get_search_proposals()?>				
</div>
<div id="wait" class="loader"></div>
<br><br>
<div class="row justify-content-center">
<nav>
<ul class="pagination" id="search_pagination">
<?php get_search_pagination();?>
</ul>
</nav>
</div>
</div><!--col-lg-9 col-md-8 col-sm-12 end-->
</div>
</div>
<?php include("includes/footer.php");?>
<script>
function get_search_proposals(){
var sPath = '';
var aInputs = Array();
var aInputs = $('li').find('.get_online_sellers');
var aKeys   = Array();
var aValues = Array();
iKey = 0;
$.each(aInputs,function(key,oInput){
if(oInput.checked){
aKeys[iKey] =  oInput.value
};
iKey++;
});
if(aKeys.length>0){
var sPath = '';
for(var i = 0; i < aKeys.length; i++){
sPath = sPath + 'online_sellers[]=' + aKeys[i]+'&';
}
}
var aInputs = $('li').find('.get_cat_id');
var aKeys   = Array();
var aValues = Array();
iKey = 0;
$.each(aInputs,function(key,oInput){
if(oInput.checked){
aKeys[iKey] = oInput.value
};
iKey++;
});
if(aKeys.length>0){
for(var i = 0; i < aKeys.length; i++){
sPath = sPath + 'cat_id[]=' + aKeys[i]+'&';
}
}
var aInputs = $('li').find('.get_delivery_time');
var aKeys   = Array();
var aValues = Array();
iKey = 0;
$.each(aInputs,function(key,oInput){
if(oInput.checked){
aKeys[iKey] =  oInput.value
};
iKey++;
});
if(aKeys.length>0){
for(var i = 0; i < aKeys.length; i++){
sPath = sPath + 'delivery_time[]=' + aKeys[i]+'&';
}
}
var aInputs = Array();
var aInputs = $('li').find('.get_seller_level');
var aKeys   = Array();
var aValues = Array();
iKey = 0;
$.each(aInputs,function(key,oInput){
if(oInput.checked){
aKeys[iKey] =  oInput.value
};
iKey++;
});
if(aKeys.length>0){
for(var i = 0; i < aKeys.length; i++){
sPath = sPath + 'seller_level[]=' + aKeys[i]+'&';
}
}
var aInputs = Array();
var aInputs = $('li').find('.get_seller_language');
var aKeys   = Array();
var aValues = Array();
iKey = 0;
$.each(aInputs,function(key,oInput){
if(oInput.checked){
aKeys[iKey] =  oInput.value
};
iKey++;
});
if(aKeys.length>0){
for(var i = 0; i < aKeys.length; i++){
sPath = sPath + 'seller_language[]=' + aKeys[i]+'&';
}
}
$('#wait').addClass("loader");
$.ajax({  
url:"search_load.php",  
method:"POST",  
data: sPath+'zAction=get_search_proposals',  
success:function(data){
$('#search_proposals').html('');  
$('#search_proposals').html(data); 
$('#wait').removeClass("loader");
}  
});							  
$.ajax({  
url:"search_load.php",  
method:"POST",  
data: sPath+'zAction=get_search_pagination',  
success:function(data){  
$('#search_pagination').html('');  
$('#search_pagination').html(data); 
}  
});
}
$('.get_online_sellers').click(function(){ 
get_search_proposals(); 
});
$('.get_cat_id').click(function(){ 
get_search_proposals(); 
});
$('.get_delivery_time').click(function(){ 
get_search_proposals(); 
});
$('.get_seller_level').click(function(){ 
get_search_proposals(); 
}); 
$('.get_seller_language').click(function(){ 
get_search_proposals(); 
});
</script>

<script>
$(document).ready(function(){
//strt get the cat_id for the show and hide function	
	$(".get_cat_id").click(function(){
		if($('.get_cat_id:checked').length > 0){
			$(".clear_cat_id").show();
		}else{
			$(".clear_cat_id").hide();
		}

	}) //end hide/show category
	$(".get_delivery_time").click(function(){
		if($('.get_delivery_time:checked').length > 0){
			$(".clear_delivery_time").show();
		}else{
			$(".clear_delivery_time").hide();
		}
	})//end hide/show delivery time...
	$(".get_seller_level").click(function(){
		if($(".get_seller_level:checked").length > 0){
			$(".clear_seller_level").show();
		}else{
			$(".clear_seller_level").hide();
		}
	})//end hide /show seller levels
	$(".get_seller_language").click(function(){
		if ($(".get_seller_language").length > 0) {
			$(".clear_seller_language").show();
		}else{
			$(".clear_seller_language").hide();
		}
	});//end of hide/show seller language
$(".clear_cat_id").click(function(){
 $(".clear_cat_id").hide();
});//hide clear filter categories...
$(".clear_delivery_time").click(function(){
 $(".clear_delivery_time").hide();
});//hide clear filter time...
$(".clear_seller_level").click(function(){
 $(".clear_seller_level").hide();
});//hide clear filter level...
$(".clear_seller_language").click(function(){
 $(".clear_seller_language").hide();
});//hide clear filter language...

});
function clearCat(){
	$('.get_cat_id').prop('checked',false);
	get_search_proposals(); 
}
function clearDelivery(){
	$('.get_delivery_time').prop('checked',false);
	get_search_proposals(); 
}
function clearLevel(){
	$('.get_seller_level').prop('checked',false);
	get_search_proposals(); 
}
function clearLanguage(){
	$('.get_seller_language').prop('checked', false);
	get_search_proposals(); 
}

</script>
</body>
</html>