<?php 
session_start();
include("includes/db.php");
if (!isset($_SESSION['seller_user_name'])) {
	echo "<script>window.open('login.php','_self');</script>";
}

//a copy get id and username/email... 
$login_seller_user_name=$_SESSION['seller_user_name'];
$select_login_seller="SELECT * from sellers where seller_user_name='$login_seller_user_name'";
$run_login_seller=mysqli_query($con,$select_login_seller);
$row_login_seller=mysqli_fetch_array($run_login_seller);
$login_seller_id=$row_login_seller['seller_id'];
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	<title>Product sales orders</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="JuaKali Mall">
	<link href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100" rel="stylesheet" >
	<link rel="stylesheet" href="styles/bootstrap.min.css">
	<link rel="stylesheet" href="styles/style.css">
	<!-- Custome css from the user -->
	<link rel="stylesheet" href="styles/custom.css">
	<link rel="stylesheet" href="styles/user_nav_style.css">
	<link rel="stylesheet" href="font-awesome/css/all.min.css">
	<script src="js/jquery.slim.min.js"></script>
</head>
<body>
<?php include("includes/user_header.php");?>
<div class="container-fluid mt-5">
	<div class="row">
		<div class="col-md-12">
			<h1>Manage Product Sale Orders</h1>

		</div> <!--col-md-12 ends-->
	</div><!--row ends-->
<div class="row">
<div class="col-md-12 mt-2 mb-3">
<ul class="nav nav-tabs">
<li class="nav-item">
<?php 
//copy ftom database to get seller info
$sel_orders="SELECT * from orders where seller_id='$login_seller_id' AND order_active ='yes'";
$run_orders=mysqli_query($con,$sel_orders);
$count_orders=mysqli_num_rows($run_orders);
 ?>	
<a href="#active" data-toggle="tab" class="nav-link active">Active <span class="badge badge-success"><?php echo($count_orders); ?></span></a>
</li>
<li class="nav-item">
<?php 
//copy ftom database to get seller info
$sel_orders="SELECT * from orders where seller_id='$login_seller_id' AND order_status ='delivered'";
$run_orders=mysqli_query($con,$sel_orders);
$count_orders=mysqli_num_rows($run_orders);
 ?>
<a href="#delivered" data-toggle="tab" class="nav-link">Delivered <span class="badge badge-success"><?php echo($count_orders); ?></span></a>
</li>
<li class="nav-item">
<?php 
//copy ftom database to get seller info
$sel_orders="SELECT * from orders where seller_id='$login_seller_id' AND order_status ='completed'";
$run_orders=mysqli_query($con,$sel_orders);
$count_orders=mysqli_num_rows($run_orders);
 ?>	
<a href="#completed" data-toggle="tab" class="nav-link ">Completed <span class="badge badge-success"><?php echo($count_orders); ?></span></a>
</li>
<li class="nav-item">
<?php 
//copy ftom database to get seller info
$sel_orders="SELECT * from orders where seller_id='$login_seller_id' AND order_status ='cancelled'";
$run_orders=mysqli_query($con,$sel_orders);
$count_orders=mysqli_num_rows($run_orders);
 ?>
<a href="#cancelled" data-toggle="tab" class="nav-link">Cancelled <span class="badge badge-success"><?php echo($count_orders); ?></span></a>
</li>
<li class="nav-item">
<?php 
//copy ftom database to get seller info
$sel_orders="SELECT * from orders where seller_id='$login_seller_id'";
$run_orders=mysqli_query($con,$sel_orders);
$count_orders=mysqli_num_rows($run_orders);
 ?>	
<a href="#all" data-toggle="tab" class="nav-link">ALL <span class="badge badge-success"><?php echo($count_orders); ?></span></a>
			</li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane fade show active" id="active">
				<?php include("manage_orders/order_active_selling.php");?>
			</div><!--end active-->
			<div class="tab-pane fade" id="delivered">
				<?php include("manage_orders/order_delivered_selling.php");?>
			</div><!--end delivered-->
			<div class="tab-pane fade" id="completed">
				<?php include("manage_orders/order_completed_selling.php");?>
			</div><!--end completed-->
			<div class="tab-pane fade" id="cancelled">
				<?php include("manage_orders/order_cancelled_selling.php");?>
			</div><!--end cancelled-->
			<div class="tab-pane fade" id="all">
				<?php include("manage_orders/order_all_selling.php");?>
			</div><!--end all-->
		</div><!--tab-content-->
	</div>
</div><!--end of the row tabs-->
</div><!--end of the selling container-->
<?php include("includes/footer.php");?>
</body>
</html>