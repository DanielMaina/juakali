<?php 
session_start();
include("includes/db.php");
if (!isset($_SESSION['seller_user_name'])) {
echo "<script>window.open('login.php','_self');</script>";
}
//get the buyer/user details to create the session
$login_seller_user_name=$_SESSION['seller_user_name'];
$select_login_seller="select * from sellers where seller_user_name='$login_seller_user_name'";
$run_login_seller=mysqli_query($con,$select_login_seller);
$row_login_seller=mysqli_fetch_array($run_login_seller);
$login_seller_id=$row_login_seller['seller_id'];
$login_seller_name=$row_login_seller['seller_name'];
$login_seller_email=$row_login_seller['seller_email'];
$login_seller_paypal_email=$row_login_seller['seller_paypal_email'];
$login_seller_image=$row_login_seller['seller_image'];
$login_seller_headline=$row_login_seller['seller_headline'];
$login_seller_country=$row_login_seller['seller_country'];
$login_seller_language=$row_login_seller['seller_language'];
$login_seller_about=$row_login_seller['seller_about'];

//a copy from cart_payment_option.php
//to update the seller account amount 
$get_seller_accounts="select * from seller_accounts where seller_id='$login_seller_id'";
$run_seller_accounts=mysqli_query($con,$get_seller_accounts);
$row_seller_accounts=mysqli_fetch_array($run_seller_accounts);
$current_balance=$row_seller_accounts['current_balance'];


 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	<title>My Settings</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="ReachComputers">
	<link href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100" rel="stylesheet" >
	<link rel="stylesheet" href="styles/bootstrap.min.css">
	<link rel="stylesheet" href="styles/style.css">
	<!-- Custome css from the user -->
	<link rel="stylesheet" href="styles/custom.css">
	<link rel="stylesheet" href="styles/user_nav_style.css">
	<link rel="stylesheet" href="font-awesome/css/all.min.css">
	<script src="js/jquery.slim.min.js"></script>

</head>
<body>
<?php include("includes/user_header.php");?>
<div class="container mt-5 mb-5 pt-5">
<div class="row">
<div class="col-md-3">
<ul class="nav flex-column nav-pills nav-stacked mb-4">
<li class="nav-item">
<a href="#profile_setting" class="nav-link 
<?php 
if(!isset($_GET['profile_setting']) and !isset($_GET['account_setting'])){
echo "active";
}
if(isset($_GET['profile_setting'])){
echo "active";
}
 ?>
" data-toggle="pill">Profile Setting</a>	
</li>
<li class="nav-item">
<a href="#account_setting" class="nav-link
<?php 
if(isset($_GET['account_setting'])){
echo "active";
}
 ?>

" data-toggle="pill">Account Setting</a>	
</li>	
</ul>	
</div><!--col-md-3 ends-->
<div class="col-md-9">
<div class="card rounded-0">
<div class="card-body">
<div class="tab-content">
<div id="profile_setting" class="tab-pane fade 
<?php 
if(!isset($_GET['profile_setting']) and !isset($_GET['account_setting'])){
echo "show active";
}
if(isset($_GET['profile_setting'])){
echo "show active";
}
 ?>
">
<?php include("profile_setting.php");?>		
</div><!--tab-pane fade show active ends-->
<div id="account_setting" class="tab-pane fade
<?php 
if(isset($_GET['account_setting'])){
echo "show active";
}
 ?>
">
<?php include("account_setting.php");?>	
</div><!---tab-pane fade ends--->
</div><!--tab-content ends-->	
</div><!--card-body ends-->		
</div><!--card rounded-0 ends-->	
</div><!--col-md-9-->	
</div><!--row ends-->	
</div><!--container mt-5 mb-5 ends-->
<?php include("includes/footer.php");?>
</body>
</html>