<?php
session_start();
include("includes/db.php");

?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	<title>Terms ||Conditions</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="juakali product">
	<meta name="keywords" content="agriculture kitchen,domestic,cloth">
	<meta name="author" content="JuaKali Mall">
	<link href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100" rel="stylesheet" >
	<link rel="stylesheet" href="styles/bootstrap.min.css">
	<link rel="stylesheet" href="styles/style.css">
	<link rel="stylesheet" href="styles/category_nav_style.css">
	<!-- Custome css from the user -->
	<link rel="stylesheet" href="styles/custom.css">
	<link rel="stylesheet" href="font-awesome/css/all.min.css">
	<script src="js/jquery.slim.min.js"></script>
</head>
<body>
<?php include("includes/header.php");?>
<div class="container-fluid mt-5 mb-5">
	<div class="row mb-3">
		<div class="col-md-12 text-center">
<h1>Terms And Conditions</h1>
<div class="row">
<div class="col-md-3 mb-3">
	<div class="card">
<div class="card-body">
<ul class="nav nav-pills flex-column mt-2">
<?php
$get_terms="select * from terms LIMIT 0,1";
$run_terms=mysqli_query($con,$get_terms);
while ($row_terms=mysqli_fetch_array($run_terms)) {
	$term_title=$row_terms['term_title'];
	$term_link=$row_terms['term_link'];
?>
<li class="nav-item">
<a href="#<?php echo($term_link)?>" class="nav-link active" data-toggle="pill"><?php echo($term_title)?></a>
	</li>
<?php }?>

<?php
$get_terms="select * from terms";
$run_terms=mysqli_query($con,$get_terms);
$count_terms=mysqli_num_rows($run_terms);
$get_terms="select * from terms LIMIT 1,$count_terms";
$run_terms=mysqli_query($con,$get_terms);
while ($row_terms=mysqli_fetch_array($run_terms)) {
	$term_title=$row_terms['term_title'];
	$term_link=$row_terms['term_link'];
?>
<li class="nav-item">
<a href="#<?php echo($term_link)?>" class="nav-link" data-toggle="pill"><?php echo($term_title)?></a>
	</li>
<?php }?>
	</ul>
	</div>
	</div>
	</div>
<div class="col-md-9">
<div class="card">
<div class="card-body">
<div class="tab-content">
<?php
$get_terms="select * from terms LIMIT 0,1";
$run_terms=mysqli_query($con,$get_terms);
while ($row_terms=mysqli_fetch_array($run_terms)) {
	$term_title=$row_terms['term_title'];
	$term_link=$row_terms['term_link'];
	$term_description=$row_terms['term_description'];

?>
<div id="<?php echo($term_link)?>" class="tab-pane fade show active">
<h1><?php echo($term_title)?></h1>
<p><?php echo($term_description)?></p>
</div><!--terms-conditions ends-->
<?php }?>

<?php
$get_terms="select * from terms LIMIT 1,$count_terms";
$run_terms=mysqli_query($con,$get_terms);
while ($row_terms=mysqli_fetch_array($run_terms)) {
	$term_title=$row_terms['term_title'];
	$term_link=$row_terms['term_link'];
	$term_description=$row_terms['term_description'];

?>
<div id="<?php echo($term_link)?>" class="tab-pane fade ">
<h1><?php echo($term_title)?></h1>
<p><?php echo($term_description)?></p>
</div><!--single tab ends-->
<?php }?>
</div>
</div><!--card-body ends-->
</div>
</div><!--col-md-9 ends--->
</div>
</div>
	</div>
</div>
<?php include("includes/footer.php");?>
</body>
</html>