<?php
session_start();
include("../includes/db.php");
//if conditionToPreventOthersFromEditingYourPofile
if (isset($_SESSION['seller_user_name'])) {
$login_seller_user_name=$_SESSION['seller_user_name'];
//codeDeleteLanguageIn language_relation
 if (isset($_GET['delete_language'])) {
 $delete_language_id=$_GET['delete_language'];

 $delete_language="DELETE from language_relation where relation_id='$delete_language_id'";
 $run_delete_language=mysqli_query($con,$delete_language);
if ($run_delete_language){
echo "<script>alert('One performance has been Deleted');</script>";
echo "<script>window.open('$login_seller_user_name','_self');</script>";
}
}
//code delete skills in skills_relation
if (isset($_GET['delete_skill'])) {
$delete_skill_id=$_GET['delete_skill'];

$delete_skill="DELETE from skills_relation where relation_id='$delete_skill_id'";	
$run_delete_skill=mysqli_query($con,$delete_skill);
if ($run_delete_skill){
echo "<script>alert('One skill has been Deleted');</script>";
echo "<script>window.open('$login_seller_user_name','_self');</script>";
}
}
}
$get_seller_user_name=$_GET['seller_user_name'];
$select_seller="SELECT * from sellers where seller_user_name='$get_seller_user_name' AND NOT seller_status='deactivated' AND NOT seller_status='block-ban'";
$run_seller=mysqli_query($con,$select_seller);
$count_seller=mysqli_num_rows($run_seller);
if ($count_seller == 0) {
	echo "
<script> window.open('../index.php?not_available','_self');</script>
	";
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	<title>JuaKaliMall/<?php echo($get_seller_user_name); ?></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="JuaKali Mall">
	<link href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100" rel="stylesheet" >
	<link rel="stylesheet" href="../styles/bootstrap.min.css">
	<link rel="stylesheet" href="../styles/style.css">
	<link rel="stylesheet" href="../styles/category_nav_style.css">
	<!-- Custome css from the user -->
	<link rel="stylesheet" href="../styles/custom.css">
	<link rel="stylesheet" href="../font-awesome/css/all.min.css">
	<script src="../js/jquery.slim.min.js"></script>
</head>
<body>
<?php include("../includes/header.php");?>
	
<?php include("../includes/user_profile_header.php");?>
<div class="container-fluid">
<div class="row">
<div class="col-md-4 mt-4">
<?php include("../includes/user_sidebar.php")?>
</div>
<div class="col-md-8">
<div class="row">
<div class="col-md-12">
<div class="card mt-4 mb-4 rounded-0">
<div class="card-body">
<h2><?php echo($seller_user_name) ?>'s Products</h2>
</div>
</div>
</div>
</div>

<div class="row">
<?php 

$get_proposals="SELECT * from proposals where proposal_seller_id='$seller_id' AND proposal_status='active'";
$run_proposals=mysqli_query($con,$get_proposals);
$count_proposal=mysqli_num_rows($run_proposals);
if ($count_proposal == 0) { ?>

<div class="col-ms-12">
<h3 class="bg-secondary text-white text-center mb-5 p-2">
<strong> <?php echo($get_seller_user_name); ?></strong> Does not have any products to display!		
</h3>
</div>
<?php
}
while ($row_proposals=mysqli_fetch_array($run_proposals)) {
  $proposal_id=$row_proposals['proposal_id'];
  $proposal_title=$row_proposals['proposal_title'];
  $proposal_price=$row_proposals['proposal_price'];
  $proposal_img1=$row_proposals['proposal_img1'];
  $proposal_video=$row_proposals['proposal_video'];
  $proposal_seller_id=$row_proposals['proposal_seller_id'];
  $proposal_rating=$row_proposals['proposal_rating'];
  $proposal_url=$row_proposals['proposal_url'];
  $proposal_featured=$row_proposals['proposal_featured'];
 if (empty($proposal_video)) {
$video_class="";
 }else{
$video_class="video-img";
 }
 $select_seller="SELECT * from sellers where seller_id='$proposal_seller_id'";
 $run_seller=mysqli_query($con,$select_seller);
 $row_seller=mysqli_fetch_array($run_seller);
 $seller_user_name=$row_seller['seller_user_name'];

 $seller_buyer_reviews="SELECT * from buyer_reviews where proposal_id='$proposal_id'";
 $run_buyer_reviews=mysqli_query($con,$seller_buyer_reviews);
 $count_reviews=mysqli_num_rows($run_buyer_reviews);

?>
<div class="col-lg-4 col-md-6 col-sm-6">
<div class="proposal-div">
<div class="proposal_nav">
<span class="float-left mt-2">
<strong class="ml-2 mr-1">By</strong><?php echo($seller_user_name); ?>
</span>
<span class="float-right mt-2">
<?php
for($proposal_i=0 ; $proposal_i < $proposal_rating; $proposal_i++){
	echo "<img src='../images/user_rate_full.png' alt='rating' class='rating'>";
}
for($proposal_i=$proposal_rating ; $proposal_i < 5 ; $proposal_i++){
	echo "<img src='../images/user_rate_blank.png' alt='rating' class='rating'>";

}
?>
<span class="ml-1 mr-2">(<?php echo($count_reviews); ?>)</span>
</span>
<div class="clearfix mb-2"></div>
</div>
<a href="../proposals/<?php echo($proposal_url); ?>">
<hr class="m-0 p-0">
<img src="../proposals/proposal_files/<?php echo($proposal_img1); ?>" class="resp-img">
</a>
<div class="text">
<h4>
<a href="../proposals/<?php echo($proposal_url); ?>" class="<?php echo($video_class); ?>"><?php echo($proposal_title); ?></a>
</h4>
<hr>
<p class="buttons clearfix">
<span class="float-right">STARTING AT <strong class="price">Ksh <?php echo($proposal_price); ?></strong></span>
</p>
</div>
<?php if ($proposal_featured =="yes") { ?>
<div class="ribbon">
<div class="theribbon">Featured</div>
<div class="ribbon-background"></div>
</div>
<?php } ?>
</div>
</div><!--col-lg-4 col-md-6 col-sm-6-->
<?php } ?>			

</div><!--the ends row-->
<?php 
$select_buyer_reviews="SELECT * from buyer_reviews where review_seller_id='$seller_id'";
$run_buyer_reviews=mysqli_query($con,$select_buyer_reviews);
$count_reviews=mysqli_num_rows($run_buyer_reviews);
if (!$count_reviews == 0) {
	
?>
<div class="row">
<div class="col-md-12"><div class="card user-reviews mt-4 mb-4 rounded-0">
<div class="card-header">
<h4><?php echo($seller_user_name); ?>'s Reviews
<?php
for($seller_i=0 ; $seller_i < $average_rating; $seller_i++){
	echo "<img src='../images/user_rate_full_big.png' alt='rating' class='rating'>";
}
for($seller_i=$average_rating ; $seller_i < 5 ; $seller_i++){
	echo "<img src='../images/user_rate_blank_big.png' alt='rating' class='rating'>";

}
?>
<span class="text-muted"><?php printf("%.1f",$average);?> (<?php echo($count_reviews); ?>)</span>
<div class="dropdown float-right">
<button id="dropdown-button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown">
Most Recent
</button>
<ul class="dropdown-menu">
<li class="dropdown-item active all">Most Recent</li>
<li class="dropdown-item good">Positive Reviews</li>
<li class="dropdown-item bad">Negative Reviews</li>
</ul>
</div>
</h4>
</div>
<div class="card-body">
<article id="all" class="proposal-reviews">
<ul class="reviews-list">
<?php
$select_buyer_reviews="SELECT * from  buyer_reviews where review_seller_id='$seller_id'";
$run_buyer_reviews=mysqli_query($con,$select_buyer_reviews);
while ($row_buyer_reviews=mysqli_fetch_array($run_buyer_reviews)) {
	$review_buyer_id=$row_buyer_reviews['review_buyer_id'];
	$buyer_rating=$row_buyer_reviews['buyer_rating'];
	$buyer_review=$row_buyer_reviews['buyer_review'];
	$review_date=$row_buyer_reviews['review_date'];

$select_seller="SELECT * from sellers where seller_id='$review_buyer_id'";
	$run_seller=mysqli_query($con, $select_seller);
	$row_seller=mysqli_fetch_array($run_seller);
	$buyer_user_name=$row_seller['seller_user_name'];
	$buyer_image=$row_seller['seller_image'];
?>	
<li class="star-rating-row">
<span class="user-picture">
<?php if(!empty($buyer_image)){  ?>
<img src="../user_images/<?php echo($buyer_image); ?>" width="60" height="60">
<?php }else{ ?>
<img src="../user_images/empty-image.png" width="60" height="60">
<?php  } ?>	
</span>
<h4>
<a href="#" class="mr-1"><?php echo($buyer_user_name); ?></a>
<?php
for($buyer_i=0 ; $buyer_i < $buyer_rating; $buyer_i++){
	echo "<img src='../images/user_rate_full.png' alt='rating' class='rating'>";
}
for($buyer_i=$buyer_rating ; $buyer_i < 5 ; $buyer_i++){
	echo "<img src='../images/user_rate_blank.png' alt='rating' class='rating'>";

}
?>
</h4>
<div class="msg-body">
<?php echo($buyer_review); ?>
</div>
<span class="rating-date"><?php echo($review_date); ?></span>
</li>
<hr>
<?php } ?>
</ul>
</article>

<article id="good" class="proposal-reviews">
<ul class="reviews-list">
<?php
$select_buyer_reviews="SELECT * from  buyer_reviews where review_seller_id='$seller_id' AND (buyer_rating='5' or buyer_rating='4')";

$run_buyer_reviews=mysqli_query($con,$select_buyer_reviews);
$count_reviews=mysqli_num_rows($run_buyer_reviews);
if ($count_reviews==0) {
	echo "
<li>
<h3 align='center'>There Is Currently No Positive Reviews Of This Seller!</h3>
</li>

	";
}

while ($row_buyer_reviews=mysqli_fetch_array($run_buyer_reviews)) {
	$review_buyer_id=$row_buyer_reviews['review_buyer_id'];
	$buyer_rating=$row_buyer_reviews['buyer_rating'];
	$buyer_review=$row_buyer_reviews['buyer_review'];
	$review_date=$row_buyer_reviews['review_date'];

$select_seller="SELECT * from sellers where seller_id='$review_buyer_id'";
	$run_seller=mysqli_query($con, $select_seller);
	$row_seller=mysqli_fetch_array($run_seller);
	$buyer_user_name=$row_seller['seller_user_name'];
	$buyer_image=$row_seller['seller_image'];
?>	
<li class="star-rating-row">
<span class="user-picture">
<?php if(!empty($buyer_image)){  ?>
<img src="../user_images/<?php echo($buyer_image); ?>" width="60" height="60">
<?php }else{ ?>
<img src="../user_images/empty-image.png" width="60" height="60">
<?php  } ?>	
</span>
<h4>
<a href="#" class="mr-1"><?php echo($buyer_user_name); ?></a>
<?php
for($buyer_i=0 ; $buyer_i < $buyer_rating; $buyer_i++){
	echo "<img src='../images/user_rate_full.png' alt='rating' class='rating'>";
}
for($buyer_i=$buyer_rating ; $buyer_i < 5 ; $buyer_i++){
	echo "<img src='../images/user_rate_blank.png' alt='rating' class='rating'>";

}
?>
</h4>
<div class="msg-body">
<?php echo($buyer_review); ?>
</div>
<span class="rating-date"><?php echo($review_date); ?></span>
</li>
<hr>
<?php } ?>								
</ul>
</article>
<article id="bad" class="proposal-reviews">
<ul class="reviews-list">
<?php
$select_buyer_reviews="SELECT * from  buyer_reviews where review_seller_id='$seller_id' AND (buyer_rating='1' or buyer_rating='2' or buyer_rating='3')";

$run_buyer_reviews=mysqli_query($con,$select_buyer_reviews);
$count_reviews=mysqli_num_rows($run_buyer_reviews);
if ($count_reviews==0) {
	echo "
<li>
<h3 align='center'>There Is Currently No Negative Reviews Of This Seller!</h3>
</li>

	";
}

while ($row_buyer_reviews=mysqli_fetch_array($run_buyer_reviews)) {
	$review_buyer_id=$row_buyer_reviews['review_buyer_id'];
	$buyer_rating=$row_buyer_reviews['buyer_rating'];
	$buyer_review=$row_buyer_reviews['buyer_review'];
	$review_date=$row_buyer_reviews['review_date'];

$select_seller="SELECT * from sellers where seller_id='$review_buyer_id'";
	$run_seller=mysqli_query($con, $select_seller);
	$row_seller=mysqli_fetch_array($run_seller);
	$buyer_user_name=$row_seller['seller_user_name'];
	$buyer_image=$row_seller['seller_image'];
?>	
<li class="star-rating-row">
<span class="user-picture">
<?php if(!empty($buyer_image)){  ?>
<img src="../user_images/<?php echo($buyer_image); ?>" width="60" height="60">
<?php }else{ ?>
<img src="../user_images/empty-image.png" width="60" height="60">
<?php  } ?>	
</span>
<h4>
<a href="#" class="mr-1"><?php echo($buyer_user_name); ?></a>
<?php
for($buyer_i=0 ; $buyer_i < $buyer_rating; $buyer_i++){
	echo "<img src='../images/user_rate_full.png' alt='rating' class='rating'>";
}
for($buyer_i=$buyer_rating ; $buyer_i < 5 ; $buyer_i++){
	echo "<img src='../images/user_rate_blank.png' alt='rating' class='rating'>";

}
?>
</h4>
<div class="msg-body">
<?php echo($buyer_review); ?>
</div>
<span class="rating-date"><?php echo($review_date); ?></span>
</li>
<hr>
<?php } ?>								
</ul>
</article>
</div>
</div>
</div>
</div><!--ends of the rows-->
<?php } ?>
		</div>
	</div>
</div>
<?php include("../includes/footer.php");?>
<script>
	$(document).ready(function(){
		$('#good').hide();
		$('#bad').hide();

		$('.all').click(function(){
			$("#dropdown-button").html("Most Recent");
			$(".all").attr('class','dropdown-item all active');
			$(".bad").attr('class','dropdown-item bad');
			$(".good").attr('class','dropdown-item good');
			$("#all").show();
			$("#good").hide();
			$("#bad").hide();
		});
		$('.good').click(function(){
			$("#dropdown-button").html("Positive Reviews");
			$(".all").attr('class','dropdown-item all');
			$(".bad").attr('class','dropdown-item bad');
			$(".good").attr('class','dropdown-item good active');
			$("#all").hide();
			$("#good").show();
			$("#bad").hide();
		});
		$('.bad').click(function(){
			$("#dropdown-button").html("Negative Reviews");
			$(".all").attr('class','dropdown-item all ');
			$(".bad").attr('class','dropdown-item bad active');
			$(".good").attr('class','dropdown-item good');
			$("#all").hide();
			$("#good").hide();
			$("#bad").show();
		});
	});
</script>
</body>
</html>