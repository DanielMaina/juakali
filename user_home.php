<?php 
$login_seller_user_name=$_SESSION['seller_user_name'];
$select_login_seller="SELECT * from sellers where seller_user_name='$login_seller_user_name'";
$run_login_seller=mysqli_query($con,$select_login_seller);
$row_login_seller=mysqli_fetch_array($run_login_seller);

$login_seller_id=$row_login_seller['seller_id'];
$login_seller_name=$row_login_seller['seller_name'];
$login_seller_offers=$row_login_seller['seller_offers'];
?>
<div class="margin-small-device-logged-home"></div>
<div id="myCarousel" class="carousel slide">
	<ol class="carousel-indicators">
		<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
<?php
$select_slides="SELECT * from slider";
$run_slides=mysqli_query($con,$select_slides);
$count_slides=mysqli_num_rows($run_slides);
$i=0;
$get_slides="SELECT * from slider LIMIT 1,$count_slides";
$run_slides=mysqli_query($con,$get_slides);
while ($row_slides=mysqli_fetch_array($run_slides)) {
$i++;
?>	
<li data-target="#myCarousel" data-slide-to="<?php echo($i);?>" ></li> 
<?php } ?>		
	</ol>
	<div class="carousel-inner">
<?php
$get_slides="SELECT * from slider LIMIT 0,1";
$run_slides=mysqli_query($con,$get_slides);
while ($row_slides=mysqli_fetch_array($run_slides)) {
$slide_image=$row_slides['slide_image'];
$slide_name=$row_slides['slide_name'];
$slide_desc=$row_slides['slide_desc'];
$slide_url=$row_slides['slide_url'];
?>
<div class="carousel-item active">
<a href="<?php echo($slide_url); ?>">
<img src="slides_images/<?php echo($slide_image); ?>" class="img-fluid small-resp" style="width: 100%;">
<div class="carousel-caption" style="background-color: rgba(0,0,0,0.7);">
					<h3 class="d-lg-block d-md-block d-none">
						<?php echo($slide_name); ?></h3>
						<p class="d-lg-block d-md-block d-none">
							<?php echo($slide_desc) ?>
						</p>
					
				</div>
			</a>
		</div><!--single carousel-->
<?php } ?>

<?php
$get_slides="SELECT * from slider LIMIT 1,$count_slides";
$run_slides=mysqli_query($con,$get_slides);
while ($row_slides=mysqli_fetch_array($run_slides)) {
$slide_image=$row_slides['slide_image'];
$slide_name=$row_slides['slide_name'];
$slide_desc=$row_slides['slide_desc'];
$slide_url=$row_slides['slide_url'];
?>
<div class="carousel-item ">
<a href="<?php echo($slide_url); ?>">
<img src="slides_images/<?php echo($slide_image); ?>" class="img-fluid small-resp" style="width: 100%;">
<div class="carousel-caption" style="background-color: rgba(0,0,0,0.7);">
<h3 class="d-lg-block d-md-block d-none">
						<?php echo($slide_name); ?></h3>
						<p class="d-lg-block d-md-block d-none">
							<?php echo($slide_desc) ?>
						</p>
					
				</div>
			</a>
		</div><!--single carousel-->
<?php } ?>				
</div><!--end of the inner carousel-->
<a href="#myCarousel" class="carousel-control-prev" data-slide="prev">
<span class="carousel-control-prev-icon"></span>
</a>
<a href="#myCarousel" class="carousel-control-next" data-slide="next">
<span class="carousel-control-next-icon"></span>
</a>
</div><!-- carousel slide ends-->
<div class="container-fluid">
<div class="row">
<div class="col-md-3">
<?php include("includes/user_home_sidebar.php");?>
</div><!--for the sidebar name, recent,buy again -->
<div class="col-md-9">
<div class="row">
<div class="col-md-12">
<h2>Featured Products</h2>
</div>
</div>
<div class="row">
<div class="col-md- col-md-12 flex-wrap">
<div class="owl-carousel user-home-featured-carousel owl-theme">
<?php
$get_proposals="SELECT * from proposals where proposal_featured='yes' AND proposal_status='active'";
$run_proposals=mysqli_query($con,$get_proposals);
while ($row_proposals=mysqli_fetch_array($run_proposals)) {
  $proposal_id=$row_proposals['proposal_id'];
  $proposal_title=$row_proposals['proposal_title'];
  $proposal_price=$row_proposals['proposal_price'];
  $proposal_img1=$row_proposals['proposal_img1'];
  $proposal_video=$row_proposals['proposal_video'];
  $proposal_seller_id=$row_proposals['proposal_seller_id'];
  $proposal_rating=$row_proposals['proposal_rating'];
  $proposal_url=$row_proposals['proposal_url'];
 if (empty($proposal_video)) {
$video_class="";
 }else{
$video_class="video-img";
 }
 $select_seller="SELECT * from sellers where seller_id='$proposal_seller_id'";
 $run_seller=mysqli_query($con,$select_seller);
 $row_seller=mysqli_fetch_array($run_seller);
 $seller_user_name=$row_seller['seller_user_name'];

 $seller_buyer_reviews="SELECT * from buyer_reviews where proposal_id='$proposal_id'";
 $run_buyer_reviews=mysqli_query($con,$seller_buyer_reviews);
 $count_reviews=mysqli_num_rows($run_buyer_reviews);
 $select_favorites="SELECT * from favorites where proposal_id='$proposal_id' AND seller_id='$login_seller_id'";
 $run_favorites=mysqli_query($con,$select_favorites);
 $count_favorites=mysqli_num_rows($run_favorites);
 if ($count_favorites == 0) {
 	$show_favorite_id="favorite_$proposal_id";
 	$show_favorite_class="favorite";
 }else{
    $show_favorite_id="unfavorite_$proposal_id";
 	$show_favorite_class="favorited";
 }

 ?>		
<div class="proposal-div">
<div class="proposal_nav">
<span class="float-left mt-2">
<strong class="ml-2 mr-1"><?php echo($seller_user_name); ?></strong>
</span>
<span class="float-right mt-2">
<?php
for($proposal_i=0 ; $proposal_i < $proposal_rating; $proposal_i++){
	echo "<img src='images/user_rate_full.png' alt='rating' class='rating'>";
}
for($proposal_i=$proposal_rating ; $proposal_i < 5 ; $proposal_i++){
	echo "<img src='images/user_rate_blank.png' alt='rating' class='rating'>";

}
?>
<span class="ml-1 mr-2">(<?php echo($count_reviews); ?>)</span>

</span>
<div class="clearfix mb-2"></div>
</div>
<a href="proposals/<?php echo $proposal_url; ?>">
<hr class="p-0 m-0">
<img src="proposals/proposal_files/<?php echo($proposal_img1); ?>" class="resp-img">
</a>
<div class="text">
<h4><a href="proposals/<?php echo $proposal_url ?>" class="<?php echo($video_class); ?>"><?php echo($proposal_title); ?></a>
<a title="Add To Cart" data-toggle="tooltip" class="text-danger float-right" href="proposals/<?php echo($proposal_url); ?>"><i class="fa fa-fw fa-lg fa-shopping-cart"></i></a>
</h4>
<hr>
<p class="buttons clearfix">
<a href="#" id="<?php echo($show_favorite_id); ?>" class="<?php echo($show_favorite_class); ?> mt-2 float-left" data-toggle="tooltip" title="Add To Favorites"><i class="fa fa-heart fa-lg"></i></a>
<span class="float-right">STARTING AT <strong class="price">Ksh <?php echo($proposal_price); ?> </strong></span>
</p>
</div>
<div class="ribbon">
<div class="theribbon">Featured</div>
<div class="ribbon-background"></div>
</div>
<script>
$(document).on("click","#favorite_<?php echo $proposal_id ?>", function(event){
 event.preventDefault();
var seller_id="<?php echo($login_seller_id); ?>";
var proposal_id="<?php echo($proposal_id); ?>";
$.ajax({
type:"POST",
url: "includes/add_delete_favorite.php",
data: {seller_id: seller_id, proposal_id: proposal_id, favorite: "add_favorite"},
success:function(){
	$("#favorite_<?php echo $proposal_id; ?>").attr({
		id: "unfavorite_<?php echo $proposal_id?>", class: " favorited mt-2 float-left"
	});
}
});
});

//copy for the same favorite fanctionality
$(document).on("click","#unfavorite_<?php echo $proposal_id ?>", function(event){
 event.preventDefault();
var seller_id="<?php echo($login_seller_id); ?>";
var proposal_id="<?php echo($proposal_id); ?>";
$.ajax({
type:"POST",
url: "includes/add_delete_favorite.php",
data: {seller_id: seller_id, proposal_id: proposal_id, favorite: "delete_favorite"},
success:function(){
	$("#unfavorite_<?php echo $proposal_id; ?>").attr({
		id: "favorite_<?php echo $proposal_id?>", class: "favorite mt-2 float-left"
	});
}
});
});
</script>
</div><!--proposal-div ends single proposal- oneHere-->
<?php } ?>
</div>
</div>
</div>
<?php 
$select_proposals="SELECT * from proposals WHERE proposal_seller_id='$login_seller_id'";
$run_hidder=mysqli_query($con,$select_proposals);
$count_hidder=mysqli_num_rows($run_hidder);

if ($count_hidder > 0) {

 ?>
<div class="row">
<div class="col-md-12">
<h2>Recent Buyer Request</h2>
</div>
</div>
<div class="row buyer-requests">
<div class="col-md-12">

<div class="table-responsive box-table">
<table class="table table-hover">
<thead>
<tr>
<th>Request</th>
<th>Offers</th>
<th>Duration</th>
<th>Budget</th>
</tr>
</thead>
<tbody>
<?php
$request_child_id= array();
$select_proposals="SELECT DISTINCT proposal_child_id from proposals WHERE proposal_seller_id='$login_seller_id'";
$run_proposals=mysqli_query($con,$select_proposals);
while ($row_proposals=mysqli_fetch_array($run_proposals)) {
	$proposal_child_id=$row_proposals['proposal_child_id'];
	array_push($request_child_id, $proposal_child_id);
}
$where_child_id=array();
foreach ($request_child_id as $child_id) {
	$where_child_id[]="child_id =" . $child_id;

}
if (count($where_child_id )>0) {
	$query_where="and  (" . implode(" or ", $where_child_id) . ")";
}
if (!empty($query_where)) {
$select_requests="SELECT * from buyer_requests where request_status='active'" . $query_where . " AND NOT seller_id='$login_seller_id' order by 1 DESC LIMIT 0,4";
$run_requests=mysqli_query($con,$select_requests);

while ($row_requests=mysqli_fetch_array($run_requests)) {
	$request_id=$row_requests['request_id'];
	$seller_id=$row_requests['seller_id'];
	$request_title=$row_requests['request_title'];
	$request_description=$row_requests['request_description'];
	$delivery_time=$row_requests['delivery_time'];
	$request_budget=$row_requests['request_budget'];
	$request_file=$row_requests['request_file'];
$select_request_seller="SELECT * from sellers where seller_id='$seller_id'";
$run_requests_seller=mysqli_query($con,$select_request_seller);
$row_seller_seller=mysqli_fetch_array($run_requests_seller);
$request_seller_user_name=$row_seller_seller['seller_user_name'];	
$request_seller_image=$row_seller_seller['seller_image'];

$select_send_offers="SELECT * from send_offers where request_id='$request_id'";
$run_send_offers=mysqli_query($con,$select_send_offers);
$count_send_offers=mysqli_num_rows($run_send_offers);

$select_offers="SELECT * from send_offers where request_id='$request_id' AND sender_id='$login_seller_id'";
$run_offers=mysqli_query($con,$select_offers);
$count_offers=mysqli_num_rows($run_offers);
 if ($count_offers==0) {
?>	
<tr id="request_tr_<?php echo($request_id); ?>">
<td>
<?php 
if (!empty($request_seller_image)) {   ?>	
<img src="user_images/<?php echo($request_seller_image); ?>" class="request-img rounded-circle">
<?php }else{ ?>
<img src="user_images/empty-image.png" class="request-img rounded-circle">
<?php } ?>
<div class="request-description"><h6><?php echo $request_seller_user_name; ?></h6> <h6 class="text-primary"><?php echo $request_title; ?></h6>
<p class="lead"><?php echo $request_description; ?></p></div>
<?php 
 if (!empty($request_file)) { ?>
 <a href="request_files/<?php echo($request_file) ?>" download>
 	<i class="fa fa-arrow-circle-down"></i><?php echo($request_file); ?>
 </a>
<?php  }?>
</td>
<td><?php echo($count_send_offers); ?></td>
<td><?php echo($delivery_time); ?></td>
<td class="text-success">Ksh
 <?php if (!empty($request_budget)) {?>
<?php echo($request_budget); ?>
<?php }else{ ?>
---
<?php } ?>	
<br>
<?php if ($login_seller_offers==0) { ?>
<button class="btn btn-success btn-sm mt-4 send_button_<?php echo($request_id); ?>" data-toggle="modal" data-target="#quota-finish">Send Offer</button>
<?php }else{ ?>
<button class="btn btn-success btn-sm mt-4 send_button_<?php echo($request_id); ?>">Send Offer</button>
<?php } ?>
</td>
<script>
$(".send_button_<?php echo($request_id); ?>").css("visibility","hidden");
$(document).on("mouseenter","#request_tr_<?php echo($request_id); ?>", function(){
$(".send_button_<?php echo($request_id); ?>").css("visibility","visible");
});
$(document).on("mouseleave","#request_tr_<?php echo($request_id); ?>", function(){
$(".send_button_<?php echo($request_id); ?>").css("visibility","hidden");
});
<?php if ($login_seller_offers == "0") {?>

<?php }else{ ?>	


$(".send_button_<?php echo($request_id); ?>").click(function(){
request_id="<?php echo($request_id); ?>";
$.ajax({
method: "POST",
url: "requests/send_offer_modal.php",
data: {request_id: request_id}
})
.done(function(data){
$(".append-modal").html(data);
});
});
<?php } ?>	
</script>
</tr><!--single request-->
<?php } } } ?>		
</tbody>	
</table>
<hr>
<center>
<a href="requests/buyer_requests.php" class="btn btn-success btn-lg mb-3">Load More</a>
</center>
</div>
</div>
</div>
<?php } ?>
<!-- end of the buyer request section -->
</div><!--main section col-md-9-->
</div><!--end of the section featured main and sidebar -->
<div class="row">
<div class="col-md-6">
		<div class="card border-primary mb-3">
			<div class="card-header bg-primary">
				<h5 class="h5 text-white">Related Product</h5>
			</div>
			<div class="card-body vertical-proposals">
<?php 
$per_page=4;
if (isset($_GET['random_proposals_page'])) {
$page =$_GET['random_proposals_page'];

}else{
$page=1;
}

//Page strt 0 *(per/page)
$start_from =($page-1)*$per_page;
//select the data from table with LIMIT
$select_proposals="SELECT * from proposals where proposal_status='active' order by rand() LIMIT $start_from,$per_page";
$run_proposals=mysqli_query($con,$select_proposals);


while ($row_proposals=mysqli_fetch_array($run_proposals)) {
  $proposal_id=$row_proposals['proposal_id'];
  $proposal_title=$row_proposals['proposal_title'];
  $proposal_price=$row_proposals['proposal_price'];
  $proposal_img1=$row_proposals['proposal_img1'];
  $proposal_video=$row_proposals['proposal_video'];
  $proposal_seller_id=$row_proposals['proposal_seller_id'];
  $proposal_rating=$row_proposals['proposal_rating'];
  $proposal_url=$row_proposals['proposal_url'];
$proposal_desc=substr(strip_tags($row_proposals['proposal_desc']),0,45)."...";
 if (empty($proposal_video)) {
$video_class="";
 }else{
$video_class="video-img";
 }
 $select_seller="SELECT * from sellers where seller_id='$proposal_seller_id'";
 $run_seller=mysqli_query($con,$select_seller);
 $row_seller=mysqli_fetch_array($run_seller);
 $seller_user_name=$row_seller['seller_user_name'];

 $seller_buyer_reviews="SELECT * from buyer_reviews where proposal_id='$proposal_id'";
 $run_buyer_reviews=mysqli_query($con,$seller_buyer_reviews);
 $count_reviews=mysqli_num_rows($run_buyer_reviews);

 ?>				
<div class="row mb-3">
<div class="col-md-3">
<a href="proposals/<?php echo $proposal_url; ?>" class="<?php echo $video_class; ?>">
<img src="proposals/proposal_files/<?php echo($proposal_img1); ?>" class="vertical-proposals-img">
</a>
</div>
<div class="col-md-9">
<div class="text">
<p>
<a href="proposals/<?php echo($proposal_url); ?>"><?php echo($proposal_title); ?></a>
<span class="text-success float-right">Ksh <?php echo($proposal_price); ?></span>
</p>
<p><?php echo($proposal_desc); ?> &nbsp;
<a href="proposals/<?php echo($proposal_url); ?>">Read More</a></p>
<hr>
<span class="float-left">
<strong class="ml-2 mr-1">By</strong> <?php echo($seller_user_name); ?>
</span>
<span class="float-right">
<?php
for($proposal_i=0 ; $proposal_i < $proposal_rating; $proposal_i++){
	echo "<img src='images/user_rate_full.png' alt='rating' class='rating'>";
}
for($proposal_i=$proposal_rating ; $proposal_i < 5 ; $proposal_i++){
	echo "<img src='images/user_rate_blank.png' alt='rating' class='rating'>";

}
?>
<span class="ml-1 mr-2">(<?php echo($count_reviews) ?>)</span>
</span>
</div>
</div>
</div><!--inner row for the random proposals-->
<hr style="border: 1px solid black">
<?php } ?>
<ul class="pagination justify-content-center">
<?php 
$select_proposals="SELECT * from proposals where proposal_status='active' order by rand() LIMIT 0,16";
$run_proposals=mysqli_query($con,$select_proposals);
$count_proposals=mysqli_num_rows($run_proposals);
$total_pages=ceil($count_proposals /$per_page);

if (isset($_GET['top_proposals_page'])) {
	$top_proposals_page=$_GET['top_proposals_page'];
}else{
	$top_proposals_page=1;
}

 ?>	
<li class="page-item">
<a href="index.php?random_proposals_page=1&top_proposals_page=<?php echo($top_proposals_page);?>" class="page-link">First page</a>
</li>
<?php for ($i=1; $i <=$total_pages; $i++) {

if ($i==$page) {
	$active= "active";
}else{
$active="";
}
 ?>

	
<li class="page-item <?php echo($active); ?>">
<a href="index.php?random_proposals_page=<?php echo($i); ?>&top_proposals_page=<?php echo($top_proposals_page); ?>" class="page-link"><?php echo($i); ?></a>
</li>
<?php } ?>
<li class="page-item">
<a href="index.php?random_proposals_page=<?php echo($total_pages);?>&top_proposals_page=<?php echo($top_proposals_page);?>" class="page-link">Last Page</a>
</li>

</ul>
</div><!--end of the random card body-->
</div><!--end of the random proposals card-->
	</div><!--col-md-6 ends-->
		<div class="col-md-6">
		<div class="card border-primary mb-3">
			<div class="card-header bg-primary">
				<h5 class="h5 text-white">Top Rated Products</h5>
			</div>
<div class="card-body vertical-proposals">
<?php 
$per_page=4;
if (isset($_GET['top_proposals_page'])) {
$page =$_GET['top_proposals_page'];

}else{
$page=1;
}

//Page strt 0 *(per%page)
$start_from =($page-1)*$per_page;
//select the data from tabke with LIMIT
$select_proposals="SELECT * from proposals where proposal_status='active' AND proposal_rating='5' LIMIT $start_from,$per_page";
$run_proposals=mysqli_query($con,$select_proposals);


while ($row_proposals=mysqli_fetch_array($run_proposals)) {
  $proposal_id=$row_proposals['proposal_id'];
  $proposal_title=$row_proposals['proposal_title'];
  $proposal_price=$row_proposals['proposal_price'];
  $proposal_img1=$row_proposals['proposal_img1'];
  $proposal_video=$row_proposals['proposal_video'];
  $proposal_seller_id=$row_proposals['proposal_seller_id'];
  $proposal_rating=$row_proposals['proposal_rating'];
  $proposal_url=$row_proposals['proposal_url'];
$proposal_desc=substr(strip_tags($row_proposals['proposal_desc']),0,45).'... ';
 if (empty($proposal_video)) {
$video_class="";
 }else{
$video_class="video-img";
 }
 $select_seller="SELECT * from sellers where seller_id='$proposal_seller_id'";
 $run_seller=mysqli_query($con,$select_seller);
 $row_seller=mysqli_fetch_array($run_seller);
 $seller_user_name=$row_seller['seller_user_name'];

 $seller_buyer_reviews="SELECT * from buyer_reviews where proposal_id='$proposal_id'";
 $run_buyer_reviews=mysqli_query($con,$seller_buyer_reviews);
 $count_reviews=mysqli_num_rows($run_buyer_reviews);

 ?>				
<div class="row mb-3">
<div class="col-md-3">
<a href="proposals/<?php echo $proposal_url; ?>" class="<?php echo $video_class; ?>">
<img src="proposals/proposal_files/<?php echo($proposal_img1); ?>" class="vertical-proposals-img">
</a>
</div>
<div class="col-md-9">
<div class="text">
<h6>
<a href="proposals/<?php echo($proposal_url); ?>"><?php echo($proposal_title); ?></a>
<span class="text-success float-right">Ksh <?php echo($proposal_price); ?></span>
</h6>
<p><?php echo($proposal_desc); ?> &nbsp;
<a href="proposals/<?php echo($proposal_url); ?>">Read More</a></p>
<hr>
<span class="float-left">
<strong class="ml-2 mr-1">By</strong> <?php echo($seller_user_name); ?>
</span>
<span class="float-right">
<?php
for($proposal_i=0 ; $proposal_i < $proposal_rating; $proposal_i++){
	echo "<img src='images/user_rate_full.png' alt='rating' class='rating'>";
}
for($proposal_i=$proposal_rating ; $proposal_i < 5 ; $proposal_i++){
	echo "<img src='images/user_rate_blank.png' alt='rating' class='rating'>";

}
?>
<span class="ml-1 mr-2">(<?php echo($count_reviews) ?>)</span>
</span>
</div>
</div>
</div><!--inner row for the random proposals-->
<hr style="border: 1px solid black">
<?php } ?>				

<ul class="pagination justify-content-center">
<?php 
$select_proposals="SELECT * from proposals where proposal_status='active' AND proposal_rating='5' LIMIT 0,16";
$run_proposals=mysqli_query($con,$select_proposals);
$count_proposals=mysqli_num_rows($run_proposals);
$total_pages=ceil($count_proposals /$per_page);

if (isset($_GET['random_proposals_page'])) {
	$random_proposals_page=$_GET['random_proposals_page'];
}else{
	$random_proposals_page=1;
}

 ?>
 <li class="page-item">
<a href="index.php?random_proposals_page=<?php echo($random_proposals_page); ?>&top_proposals_page=1" class="page-link">First page</a>

</li>
<?php for ($i=1; $i <=$total_pages; $i++) {

if ($i==$page) {
	$active= "active";
}else{
$active="";
}
 ?>
<li class="page-item <?php echo($active); ?>">
<a href="index.php?random_proposals_page=<?php echo $random_proposals_page; ?>&top_proposals_page=<?php echo $i; ?>" class="page-link"><?php echo $i; ?></a>
</li>
<?php } ?>
<li class="page-item">
<a href="index.php?random_proposals_page=<?php echo $random_proposals_page; ?>&top_proposals_page=<?php echo $total_pages; ?>" class="page-link">Last Page</a>
</li>
				</ul>
			</div><!--end of the random card body-->
		</div><!--end of the random proposals card-->
	</div><!--col-md-6 ends-->
</div><!--end of row recent and top rated proposals-->
</div><!--container-fluid ends-->
<div class="append-modal"></div>
<div class="modal fade" id="quota-finish">
	 <div class="modal-dialog">
	 	<div class="modal-content">
	 		<div class="modal-header">
	 			<h5 class="modal-title">Request Quota Finished</h5>
	 			<button class="close" data-dismiss="modal">&times;</button>
	 		</div>
	 		<div class="modal-body">
	 			<center>
	 				<h3>You have Already Sent 10 Offers Tody, Quota Finished</h3>
	 			</center>
	 		</div>
	 		<div class="modal-footer">
	 			<button class="btn btn-secondary" data-dismiss="modal">Close</button>
	 		</div>
	 	</div>
	 </div>
</div><!--modal fade #quota-finish ends-->



